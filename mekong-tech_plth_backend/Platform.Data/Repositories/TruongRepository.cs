﻿

using Platform.Data.Infrastructure;
using Platform.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Platform.Data.Repositories
{

    public interface ITruongRepository : IRepository<Truong>
    {

    }

    class TruongRepository : RepositoryBase<Truong>, ITruongRepository
    {
        public TruongRepository(IDbFactory dbFactory) : base(dbFactory)
        {
        }

    }
}