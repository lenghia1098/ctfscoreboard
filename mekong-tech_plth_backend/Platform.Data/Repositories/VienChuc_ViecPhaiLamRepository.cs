﻿using Platform.Data.Infrastructure;
using Platform.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Platform.Data.Repositories
{

    public interface IVienChuc_ViecPhaiLamRepository : IRepository<VienChuc_ViecPhaiLam>
    {
        VienChuc_ViecPhaiLam getIDViecPhaiLam(int mscv);
        IEnumerable<VienChuc_ViecPhaiLam> getIdAll(string msvc);
    }

    class VienChuc_ViecPhaiLamRepository : RepositoryBase<VienChuc_ViecPhaiLam>, IVienChuc_ViecPhaiLamRepository
    {
        public VienChuc_ViecPhaiLamRepository(IDbFactory dbFactory) : base(dbFactory)
        {
        }

        public IEnumerable<VienChuc_ViecPhaiLam> getIdAll(string msvc)
        {
            var query = from A in DbContext.VienChuc_ViecPhaiLam
                        where A.MaSoVC.Equals(msvc) && A.DaHoanThanh == true
                        select A;
            return query;
        }

        public VienChuc_ViecPhaiLam getIDViecPhaiLam(int mscv)
        {
            var query = from A in DbContext.VienChuc_ViecPhaiLam
                        where A.MaSoCongViec.Equals(mscv)
                        select A;
            return query.First();
        }
    }
}
