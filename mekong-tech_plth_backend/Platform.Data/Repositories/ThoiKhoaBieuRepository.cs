﻿
using Platform.Data.Infrastructure;
using Platform.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Platform.Data.Repositories
{

    public interface IThoiKhoaBieuRepository : IRepository<ThoiKhoaBieu>
    {
        IQueryable<ChucNangTKB_GiaoVien> getTKB(string MaSoVC, string hocKy, string tuan);
        IQueryable<DanhSachMonHocTheoMaLopNghanhKhoa> xemTKBmonhoc(string tenMH,string hocKy, string namhoc);
        IQueryable<DanhSachMonHocTheoMaLopNghanhKhoa> xemThongTinNhom(string maNhom, string maMH, string malop);

    }

    class ThoiKhoaBieuRepository : RepositoryBase<ThoiKhoaBieu>, IThoiKhoaBieuRepository
    {
        public ThoiKhoaBieuRepository(IDbFactory dbFactory) : base(dbFactory)
        {
        }

        public IQueryable<ChucNangTKB_GiaoVien> getTKB(string MaSoVC, string hocKy, string tuan)
        {
            var query = from A in DbContext.GiangDay
                        join B in DbContext.Nhom
                        on A.MaNhom equals B.MaNhom
                        join C in DbContext.ThoiKhoaBieu
                        on B.MaNhom equals C.MaNhom
                        join D in DbContext.MonHocs
                        on B.MaMH equals D.MaMH
                        join E in DbContext.PhongHoc
                        on C.MaPH equals E.MaPH
                        where A.MaSoVC.Equals(MaSoVC) && B.HocKy.Equals(hocKy) && C.Tuan.Equals(tuan)
                        select new ChucNangTKB_GiaoVien()
                        {
                            MaSoVC = A.MaSoVC,
                            MaNhom = C.MaNhom,
                            Tiet = C.Tiet,
                            Thu = C.Thu,
                            TenPH = E.TenPH,
                            SoTiet = C.SoTiet,
                            Tuan = C.Tuan,
                            TenMH = D.TenMH
                        };




            return query;
        }



        public IQueryable<DanhSachMonHocTheoMaLopNghanhKhoa> xemTKBmonhoc(string tenMH, string hocKy, string namhoc)
        {

            var query = from P in DbContext.Lops

                        join D in DbContext.Lop_ChuongTrinhDaoTao
                        on P.MaLop equals D.MaLop
                        join E in DbContext.ChuongTrinhDaoTaos
                        on D.MaCT equals E.MaCT
                        join F in DbContext.ChuongTrinhDaoTao_MonHoc
                        on E.MaCT equals F.MaCT
                        join p in DbContext.MonHocs
                        on F.MaMH equals p.MaMH
                        join A in DbContext.Nhom
                        on p.MaMH equals A.MaMH
                        join B in DbContext.ThoiKhoaBieu
                        on A.MaNhom equals B.MaNhom
                        join C in DbContext.PhongHoc
                        on B.MaPH equals C.MaPH
                        
                        
                        where A.HocKy.Equals(hocKy) && A.NamHoc.Equals(namhoc) && p.TenMH.Equals(tenMH)
                        select new DanhSachMonHocTheoMaLopNghanhKhoa()
                        {
                            MaMH = p.MaMH,
                            TenMH = p.TenMH,
                            SoTinChi = p.SoTinChi,


                            TenPH = C.TenPH,
                            MaNhom = A.MaNhom,

                            SoGioThucHanh = p.SoGioThucHanh,
                            SoTiet = B.SoTiet,


                            Thu = B.Thu,
                            MaLop = P.MaLop,
                            NgayBatDau = A.NgayBatDau,
                            NgayKetThuc = A.NgayKetThuc,
                            Tiet = B.Tiet,
                           

                        };
            return query;
        }
        public IQueryable<DanhSachMonHocTheoMaLopNghanhKhoa> xemThongTinNhom(string maNhom, string maMH, string malop)
        {
            var query = from P in DbContext.Lops

                        join D in DbContext.Lop_ChuongTrinhDaoTao
                        on P.MaLop equals D.MaLop
                        join E in DbContext.ChuongTrinhDaoTaos
                        on D.MaCT equals E.MaCT
                        join F in DbContext.ChuongTrinhDaoTao_MonHoc
                        on E.MaCT equals F.MaCT
                        join p in DbContext.MonHocs
                        on F.MaMH equals p.MaMH
                        join A in DbContext.Nhom
                        on p.MaMH equals A.MaMH
                        join B in DbContext.ThoiKhoaBieu
                        on A.MaNhom equals B.MaNhom
                        join C in DbContext.PhongHoc
                        on B.MaPH equals C.MaPH
                        join G in DbContext.DangKy
                        on A.MaNhom equals G.MaNhom
                        join H in DbContext.SinhViens
                        on G.MaSoSV equals H.MaSoSV
                        join K in DbContext.GiangDay
                        on A.MaNhom equals K.MaNhom
                        join L in DbContext.VienChucs
                        on K.MaSoVC equals L.MaSoVC
                        
                        where A.MaNhom.Equals(maNhom) && A.MaMH.Equals(maMH) && P.MaLop.Equals(malop)
                        select new DanhSachMonHocTheoMaLopNghanhKhoa()
                        {
                            MaMH = p.MaMH,
                            TenMH = p.TenMH,
                            SoTinChi = p.SoTinChi,


                            TenPH = C.TenPH,
                            MaNhom = A.MaNhom,

                            SoGioThucHanh = p.SoGioThucHanh,
                            SoTiet = B.SoTiet,


                            Thu = B.Thu,
                            MaLop = P.MaLop,
                            NgayBatDau = A.NgayBatDau,
                            NgayKetThuc = A.NgayKetThuc,
                            MaSoSV = H.MaSoSV,
                            HoLot = H.HoLot,
                            Ten = H.Ten,
                            HoTen = L.HoTen,
                            Tiet = B.Tiet

                        };
            return query;
        }

       
    }
}