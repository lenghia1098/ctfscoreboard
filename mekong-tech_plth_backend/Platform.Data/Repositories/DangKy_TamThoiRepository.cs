﻿
using Platform.Data.Infrastructure;
using Platform.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Platform.Data.Repositories
{

    public interface IDangKy_TamThoiRepository : IRepository<DangKy_TamThoi>
    {
        IQueryable<DanhSachMonHocTheoMaLopNghanhKhoa> getDangKy_TamThoi(string mssv);
        IEnumerable<chucnangtinhhocphi> getDangKyTanThoi_TinhHocPhi();
       
        void xoaDangKy_TamThoi();

        IQueryable<DanhSachMonHocTheoMaLopNghanhKhoa> getDKMHTH(string mssv, string hocKy, string namHoc);
    }

    class DangKy_TamThoiRepository : RepositoryBase<DangKy_TamThoi>, IDangKy_TamThoiRepository
    {
        public DangKy_TamThoiRepository(IDbFactory dbFactory) : base(dbFactory)
        {
        }

        public IEnumerable<chucnangtinhhocphi> getDangKyTanThoi_TinhHocPhi()
        {
            var query = from p in DbContext.DangKy_TamThoi
                        join A in DbContext.Nhom_TamThoi
                        on p.MaNhom equals A.MaNhom
                        join B in DbContext.MonHocs
                        on A.MaMH equals B.MaMH
                        join C in DbContext.ThoiKhoaBieu_TamThoi
                        on A.MaNhom equals C.MaNhom
                      
                        select new chucnangtinhhocphi()
                        {
                          
                            TenMH = B.TenMH,
                            MaNhom = A.MaNhom,
                            SoTinChi = B.SoTinChi,
                            SoTinChiHocPhi = B.SoTinChiHocPhi,
                          
                            MaSoSV=p.MaSoSV,
                           
                            SoTiet = C.SoTiet,
                           HocKy=A.HocKy,
                           NamHoc=A.NamHoc,
                            Dong = A.Dong,
                           

                       
                          
                        };
            return query;

        }

       

        public IQueryable<DanhSachMonHocTheoMaLopNghanhKhoa> getDangKy_TamThoi(string mssv)
        {
            var query = from p in DbContext.DangKy_TamThoi
                        join A in DbContext.Nhom_TamThoi
                        on p.MaNhom equals A.MaNhom
                        join B in DbContext.MonHocs
                        on A.MaMH equals B.MaMH
                        join C in DbContext.ThoiKhoaBieu_TamThoi
                        on A.MaNhom equals C.MaNhom
                        where p.MaSoSV.Equals(mssv)
                        select new DanhSachMonHocTheoMaLopNghanhKhoa {
                            MaMH = B.MaMH,
                            TenMH = B.TenMH,
                            MaNhom = A.MaNhom,
                            SoTinChi = B.SoTinChi,
                            Thu=C.Thu,

                            TongSoTienHocPhiHocKy = A.TongSoTienHocPhiHocKy,
                            SoGioThucHanh = B.SoGioThucHanh,
                            SoTiet = C.SoTiet,
                            //NgayBatDau = H.NgayBatDau,
                            //NgayKetThuc = H.NgayKetThuc,
                            Dong = A.Dong,
                            Tiet=C.Tiet,
                            
                            //MaLop = B.MaLop,
                            //TenPH = J.TenPH,
                            ID = p.ID
                        };
            return query;
        }

        public IQueryable<DanhSachMonHocTheoMaLopNghanhKhoa> getDKMHTH(string mssv, string hocKy, string namHoc)
        {
            var query = from p in DbContext.DangKy_TamThoi
                        join A in DbContext.Nhom_TamThoi
                        on p.MaNhom equals A.MaNhom
                        join B in DbContext.MonHocs
                        on A.MaMH equals B.MaMH
                        join C in DbContext.HocPhi
                        on p.MaSoSV equals C.MaSoSV
                        join D in DbContext.ThoiKhoaBieu_TamThoi
                        on A.MaNhom equals D.MaNhom

                        where p.MaSoSV.Equals(mssv) && A.HocKy.Equals(hocKy) && A.NamHoc.Equals(namHoc)
                        select new DanhSachMonHocTheoMaLopNghanhKhoa
                        {
                            MaMH = B.MaMH,
                            TenMH = B.TenMH,
                            MaNhom = A.MaNhom,
                            SoTinChi = B.SoTinChi,
                            TongSoTienHocPhiHocKy = C.TongSoHocPhi,
                            SoGioThucHanh = B.SoGioThucHanh,
                            Dong = A.Dong,
                            Thu = D.Thu

                        };
            return query;
        }

        public void xoaDangKy_TamThoi()
        {
            var Query = from A in DbContext.DangKy_TamThoi
                        select A;
            foreach (var Item in Query)
            {
                DbContext.DangKy_TamThoi.Remove(Item);
            }
        }
    }
}
