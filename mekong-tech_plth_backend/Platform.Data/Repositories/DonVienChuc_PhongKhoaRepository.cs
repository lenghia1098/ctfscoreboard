﻿
using Platform.Data.Infrastructure;
using Platform.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Platform.Data.Repositories
{

    public interface IDonVienChuc_PhongKhoaRepository : IRepository<DonVienChuc_PhongKhoa>
    {

    }

    class DonVienChuc_PhongKhoaRepository : RepositoryBase<DonVienChuc_PhongKhoa>, IDonVienChuc_PhongKhoaRepository
    {
        public DonVienChuc_PhongKhoaRepository(IDbFactory dbFactory) : base(dbFactory)
        {
        }

      
    }
}