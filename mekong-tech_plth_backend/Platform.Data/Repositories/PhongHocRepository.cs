﻿
using Platform.Data.Infrastructure;
using Platform.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Platform.Data.Repositories
{

    public interface IPhongHocRepository : IRepository<PhongHoc>
    {

    }

    class PhongHocRepository : RepositoryBase<PhongHoc>, IPhongHocRepository
    {
        public PhongHocRepository(IDbFactory dbFactory) : base(dbFactory)
        {
        }

    }
}