﻿
using Platform.Data.Infrastructure;
using Platform.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Platform.Data.Repositories
{

    public interface IDonSinhVien_PhongKhoaRepository : IRepository<DonSinhVien_PhongKhoa>
    {

    }

    class DonSinhVien_PhongKhoaRepository : RepositoryBase<DonSinhVien_PhongKhoa>, IDonSinhVien_PhongKhoaRepository
    {
        public DonSinhVien_PhongKhoaRepository(IDbFactory dbFactory) : base(dbFactory)
        {
        }

      
    }
}