﻿
using Platform.Data.Infrastructure;
using Platform.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Platform.Data.Repositories
{

    public interface IKetQuaRepository : IRepository<KetQua>
    {
        IQueryable<LayTatCaBangDiem> getAllBangDiem(string mssv);
        IQueryable<LayBangDiemTheoHocKy> getBangDiemHocKy(string mssv, string hocKy, string namHoc);
        IQueryable<DanhSachMonHocTheoMaLopNghanhKhoa> getBangDiemMonHoc(string maNhom, string maMH, string malop, string hocKy, string namHoc);
        IQueryable<getHocKy> getHocKy(string mssv);
        IQueryable<KetQua> getbyMaNhom(string manhom);
        IQueryable<DanhSachMonHocTheoMaLopNghanhKhoa> getSuaDiem(string mssv,string manhom);


    }

    class KetQuaRepository : RepositoryBase<KetQua>, IKetQuaRepository
    {
        public KetQuaRepository(IDbFactory dbFactory) : base(dbFactory)
        {
        }

        public IQueryable<LayTatCaBangDiem> getAllBangDiem(string mssv)
        {
            var query = from A in DbContext.MonHocs
                        join B in DbContext.Nhom
                        on A.MaMH equals B.MaMH
                        join C in DbContext.KetQuas
                        on B.MaNhom equals C.MaNhom
                        where C.MaSoSV.Equals(mssv)
                        select new LayTatCaBangDiem
                        {
                            MaMH = A.MaMH,
                            TenMH = A.TenMH,
                            SoTinChi = A.SoTinChi,
                            DiemTongKet = C.DiemTongKet,
                            DiemChu = C.DiemChu,
                            HocKy=B.HocKy,
                            NamHoc=B.NamHoc,
                             DiemChuyenCan = C.DiemChuyenCan,
                            DiemKiemTra = C.DiemKiemTra,
                            DiemSeminar = C.DiemSeminar,
                            DiemThi = C.DiemThi,
                            DiemThucHanh = C.DiemThucHanh,
                            TyLeChuyenCan = A.TyLeChuyenCan,
                            TyLeKiemTra = A.TyLeKiemTra,
                            TyLeSeminar = A.TyLeSeminar,
                            TyLeThi = A.TyLeThi,
                            TyLeThucHanh = A.TyLeThucHanh,
                            Dau = C.Dau,
                            
                        };
            return query;
        }

        public IQueryable<LayBangDiemTheoHocKy> getBangDiemHocKy(string mssv, string hocKy, string namHoc)
        {
            var query = from A in DbContext.MonHocs
                        join B in DbContext.Nhom
                        on A.MaMH equals B.MaMH
                        join C in DbContext.KetQuas
                        on B.MaNhom equals C.MaNhom
                        where C.MaSoSV.Equals(mssv) && B.HocKy.Equals(hocKy) && B.NamHoc.Equals(namHoc)
                        select new LayBangDiemTheoHocKy
                        {
                            MaMH = A.MaMH,
                            TenMH = A.TenMH,
                            SoTinChi = A.SoTinChi,
                            Diem = C.DiemTongKet,
                            DiemChu = C.DiemChu,
                            HocKy=B.HocKy,
                            NamHoc=B.NamHoc,
                            DiemChuyenCan=C.DiemChuyenCan,
                            DiemKiemTra=C.DiemKiemTra,
                            DiemSeminar=C.DiemSeminar,
                            DiemThi=C.DiemThi,
                            DiemThucHanh=C.DiemThucHanh,
                            TyLeChuyenCan=A.TyLeChuyenCan,
                            TyLeKiemTra=A.TyLeKiemTra,
                            TyLeSeminar=A.TyLeSeminar,
                            TyLeThi=A.TyLeThi,
                            TyLeThucHanh=A.TyLeThucHanh,
                            Dau=C.Dau,
                            DiemTongKet=C.DiemTongKet
                           

                            
                            
                        };
            return query;
        }

        public IQueryable<getHocKy> getHocKy(string mssv)
        {
            var query = from A in DbContext.MonHocs
                        join B in DbContext.Nhom
                        on A.MaMH equals B.MaMH
                        join C in DbContext.KetQuas
                        on B.MaNhom equals C.MaNhom
                        where C.MaSoSV.Equals(mssv)
                        group B by new {

                            B.HocKy,
                            B.NamHoc
                        } into gcs
            select new getHocKy
                        {
                           
                            HocKy = gcs.Key.HocKy,
                            NamHoc =gcs.Key.NamHoc
                           
                        };

            return query;


        }
        public IQueryable<DanhSachMonHocTheoMaLopNghanhKhoa> getBangDiemMonHoc(string maNhom, string maMH, string malop,string hocKy, string namHoc)
        {

            var query = from P in DbContext.Lops

                        join D in DbContext.Lop_ChuongTrinhDaoTao
                        on P.MaLop equals D.MaLop
                        join E in DbContext.ChuongTrinhDaoTaos
                        on D.MaCT equals E.MaCT
                        join F in DbContext.ChuongTrinhDaoTao_MonHoc
                        on E.MaCT equals F.MaCT
                        join p in DbContext.MonHocs
                        on F.MaMH equals p.MaMH
                        join A in DbContext.Nhom
                        on p.MaMH equals A.MaMH
                        join B in DbContext.ThoiKhoaBieu
                        on A.MaNhom equals B.MaNhom
                        join C in DbContext.PhongHoc
                        on B.MaPH equals C.MaPH
                        join G in DbContext.DangKy
                        on A.MaNhom equals G.MaNhom
                        join H in DbContext.SinhViens
                        on G.MaSoSV equals H.MaSoSV
                        join K in DbContext.GiangDay
                        on A.MaNhom equals K.MaNhom
                        join L in DbContext.VienChucs
                        on K.MaSoVC equals L.MaSoVC
                        join M in DbContext.KetQuas
                        on H.MaSoSV equals M.MaSoSV

                        where A.MaNhom.Equals(maNhom) && A.MaMH.Equals(maMH) && P.MaLop.Equals(malop) && A.HocKy.Equals(hocKy)&& A.NamHoc.Equals(namHoc)&&M.MaNhom.Equals(maNhom)
                        select new DanhSachMonHocTheoMaLopNghanhKhoa()
                        {
                            MaMH = p.MaMH,
                            TenMH = p.TenMH,
                            SoTinChi = p.SoTinChi,
                            

                            TenPH = C.TenPH,
                            MaNhom = A.MaNhom,

                            SoGioThucHanh = p.SoGioThucHanh,
                            SoTiet = B.SoTiet,


                            Thu = B.Thu,
                            MaLop = P.MaLop,
                            NgayBatDau = A.NgayBatDau,
                            NgayKetThuc = A.NgayKetThuc,
                            MaSoSV = H.MaSoSV,
                            HoLot = H.HoLot,
                            Ten = H.Ten,
                            HoTen = L.HoTen,
                            Tiet = B.Tiet,
                            DiemChuyenCan = M.DiemChuyenCan,
                            DiemKiemTra = M.DiemKiemTra,
                            DiemSeminar = M.DiemSeminar,
                            DiemThi = M.DiemThi,
                            DiemThucHanh = M.DiemThucHanh,
                            Dau = M.Dau,
                            DiemTongKet = M.DiemTongKet,
                            NgayNhapGiuaKi=M.NgayNhapGiuaKi,
                            NgayNhapCuoiKi=M.NgayNhapCuoiKi,
                            GiangVienNhapGiuaKi=M.GiangVienNhapGiuaKi,
                            GiangVienNhapCuoiKi=M.GiangVienNhapCuoiKi,
                            NgayDieuChinhGiuaKi=M.NgayDieuChinhGiuaKi,
                            NgayDieuChinhCuoiKi=M.NgayDieuChinhCuoiKi,
                            GiangVienDieuChinhCuoiKi=M.GiangVienDieuChinhCuoiKi,
                            GiangVienDieuChinhGiuaKi=M.GiangVienDieuChinhGiuaKi

                        };
            return query;
        }

        public IQueryable<KetQua> getbyMaNhom(string manhom)
        {
            var query = from p in DbContext.KetQuas
                        where p.MaNhom.Equals(manhom)
                        select p;
            return query;
        }

        public IQueryable<DanhSachMonHocTheoMaLopNghanhKhoa> getSuaDiem(string mssv, string manhom)
        {
            var query = from p in DbContext.KetQuas
                        join A in DbContext.Nhom
                        on p.MaNhom equals A.MaNhom
                        join B in DbContext.ThoiKhoaBieu
                        on A.MaNhom equals B.MaNhom
                        join C in DbContext.PhongHoc
                        on B.MaPH equals C.MaPH
                        join D in DbContext.MonHocs
                        on A.MaMH equals D.MaMH
                        join E in DbContext.GiangDay
                        on A.MaNhom equals E.MaNhom
                        join F in DbContext.VienChucs
                        on E.MaSoVC equals F.MaSoVC
                        join G in DbContext.SinhViens
                        on p.MaSoSV equals G.MaSoSV
                        where p.MaSoSV.Equals(mssv) && p.MaNhom.Equals(manhom)
                        select new DanhSachMonHocTheoMaLopNghanhKhoa()
                        {
                            MaSoSV = p.MaSoSV,
                            MaNhom=p.MaNhom,
                            HoLot = G.HoLot,
                            Ten = G.Ten,
                            TenMH = D.TenMH,
                            MaMH = D.MaMH,
                            HoTen = F.HoTen,
                            NgayBatDau = A.NgayBatDau,
                            NgayKetThuc = A.NgayKetThuc,
                            TenPH = C.TenPH,
                            DiemChuyenCan = p.DiemChuyenCan,
                            DiemKiemTra = p.DiemKiemTra,
                            DiemSeminar = p.DiemSeminar,
                            DiemThi = p.DiemThi,
                            DiemThucHanh = p.DiemThucHanh,
                            Dau = p.Dau,
                            DiemTongKet = p.DiemTongKet,
                            NgayNhapGiuaKi = p.NgayNhapGiuaKi,
                            NgayNhapCuoiKi = p.NgayNhapCuoiKi,
                            GiangVienNhapGiuaKi = p.GiangVienNhapGiuaKi,
                            GiangVienNhapCuoiKi = p.GiangVienNhapCuoiKi,
                            NgayDieuChinhGiuaKi = p.NgayDieuChinhGiuaKi,
                            NgayDieuChinhCuoiKi = p.NgayDieuChinhCuoiKi,
                            GiangVienDieuChinhCuoiKi = p.GiangVienDieuChinhCuoiKi,
                            GiangVienDieuChinhGiuaKi = p.GiangVienDieuChinhGiuaKi

                        };
            return query;
        }
    }
}