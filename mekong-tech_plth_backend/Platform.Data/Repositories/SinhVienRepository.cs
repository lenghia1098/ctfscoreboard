﻿
using Platform.Data.Infrastructure;
using Platform.Model;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Platform.Data.Repositories
{

    public interface ISinhVienRepository : IRepository<SinhVien>
    {
       SinhVien GetByStringId(string Id);
        IQueryable<TTCN> GetTTCN(string Id);
       IQueryable<SinhVien>  ThongTinSinhVien(string Id);
        IEnumerable<chucnangxemthongtinsinhvien_dauvao> getthongtinsinhvien_dauvao(string mssv);
        
    }
  

    public class SinhVienRepository : RepositoryBase<SinhVien>, ISinhVienRepository
    {
        public SinhVienRepository(IDbFactory dbFactory) : base(dbFactory)
        {

        }

        public SinhVien GetByStringId(string Id)
        {
            var query = from p in DbContext.SinhViens
                        where p.MaSoSV.Equals(Id)
                        select p;
            return query.First();
        }

        public IEnumerable<chucnangxemthongtinsinhvien_dauvao> getthongtinsinhvien_dauvao(string mssv)
    {
            var query = from A in DbContext.SinhViens
                        join B in DbContext.sinhVienDauVao_MSSVs
                        on A.MaSoSV equals B.MaSoSV
                        join C in DbContext.SinhVienDauVao
                        on B.SoBaoDanh equals C.SoBaoDanh
                        where A.MaSoSV.Equals(mssv)
                        select new chucnangxemthongtinsinhvien_dauvao
                        {
                            HoLot = A.HoLot,
                            Ten = A.Ten,
                            NgaySinh = A.NgaySinh,
                            HoKhau = A.HoKhau,
                            DiaChi = A.DiaChi,
                            Email = A.Email,
                            DienThoai = A.DienThoai,
                            HeDaoTao = A.HeDaoTao,
                            Hinh = A.Hinh,
                            SoTaiKhoan = A.SoTaiKhoan,
                            TenChuTaiKhoan = A.TenChuTaiKhoan,
                            ChiNhanh = A.ChiNhanh,
                            SoBaoDanh = B.SoBaoDanh,
                            Phai = C.Phai,
                            NoiSinh = C.NoiSinh,
                            dm1 = C.dm1,
                            dm2 = C.dm2,
                            dm3 = C.dm3,
                            dtc0 = C.dtc0,
                            dtc = C.dtc,
                            NamTrungTuyen = C.NamTrungTuyen,
                            Huyen = C.Huyen,
                            DaDongTienNhapHoc = C.DaDongTienNhapHoc,
                            DanToc = C.DanToc,
                            Tongiao = C.Tongiao,
                            SoDienThoai1 = C.SoDienThoai1,
                            SoDienThoai2 = C.SoDienThoai2,
                            email = C.email,
                            DiaChiLienHe = C.DiaChiLienHe,
                            Anh34 = C.Anh34,
                            TenCha = C.TenCha,
                            NgheNghiepCha = C.NgheNghiepCha,
                            TenMe = C.TenMe,
                            NgheNghiepMe = C.NgheNghiepMe,
                            NamTotNghiepTHPT = C.NamTotNghiepTHPT,
                            NgayNhapHoc = C.NgayNhapHoc,
                            NamTotNghiep = C.NamTotNghiep,
                            NoiLamViec = C.NoiLamViec,
                            DoiTuong = C.DoiTuong,
                            KhuVuc = C.KhuVuc,
                            Ghichu = C.Ghichu,
                            DaTotNghiep = C.DaTotNghiep,
                            TayNamBo = C.TayNamBo,

                        };

            return query;



        }

        public IQueryable<TTCN> GetTTCN(string Id)
        {
            var query = from P in DbContext.SinhViens
                        join A in DbContext.SinhVien_Lop
                        on P.MaSoSV equals A.MaSoSV
                        join B in DbContext.Lops
                        on A.MaLop equals B.MaLop
                        join C in DbContext.Nganhs
                        on B.MaNganh equals C.MaNganh
                        join D in DbContext.PhongKhoas
                        on C.MaPK equals D.MaPK
                        join E in DbContext.Lop_ChuongTrinhDaoTao
                        on B.MaLop equals E.MaLop
                        join F in DbContext.ChuongTrinhDaoTaos
                        on E.MaCT equals F.MaCT
                        where P.MaSoSV.Equals(Id)
                        select new TTCN()
                        {
                            
                            MaSoSV = P.MaSoSV,
                            HoLot = P.HoLot,
                            Ten = P.Ten,
                            Email = P.Email,
                            DienThoai = P.DienThoai,
                            TenNganh = C.TenNganh,
                            TenPhongKhoa=D.TenPhongKhoa,
                            NgaySinh=P.NgaySinh,
                            HoKhau=P.HoKhau,
											
											
                            NienKhoa=B.NienKhoa,
                            DiaChi=P.DiaChi,
                            TenChuong=F.TenChuong,
                            TenLop=B.TenLop,
                            HeDaoTao=P.HeDaoTao,
                            SoTaiKhoan=P.SoTaiKhoan,
                            TenChuTaiKhoan=P.TenChuTaiKhoan,
                            ChiNhanh=P.ChiNhanh
                            

                        }
                                            ;

          

            return query;
        }

        public IQueryable<SinhVien> ThongTinSinhVien(string Id)
        {
            var query = from A in DbContext.SinhViens
                        where A.MaSoSV.Equals(Id)
                        select A;
            return query;
        }
    }
}

    