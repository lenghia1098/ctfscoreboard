﻿
using Platform.Data.Infrastructure;
using Platform.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Platform.Data.Repositories
{

    public interface INhomRepository : IRepository<Nhom>
    {
        IQueryable<ChucNangTKB> getNhom(string mssv, string hocKy, string tuan);
        IQueryable<getHocKy> getHocKy(string mssv);
        IQueryable<getHocKy> getHocKyTH(string mssv);
        IQueryable<ChucNangTKB> getNgayBatDauVaKetThuc(string mssv);
    }

    class NhomRepository : RepositoryBase<Nhom>, INhomRepository
    {
        public NhomRepository(IDbFactory dbFactory) : base(dbFactory)
        {
        }

       

        public IQueryable<ChucNangTKB> getNhom(string mssv, string hocKy,string tuan)
        {
            var query = from A in DbContext.DangKy
                        join B in DbContext.Nhom
                        on A.MaNhom equals B.MaNhom
                        join C in DbContext.ThoiKhoaBieu
                        on B.MaNhom equals C.MaNhom
                        join D in DbContext.MonHocs
                        on B.MaMH equals D.MaMH
                        join E in DbContext.PhongHoc
                        on C.MaPH equals E.MaPH
                        where A.MaSoSV.Equals(mssv) && B.HocKy.Equals(hocKy) && C.Tuan.Equals(tuan)
                        select new ChucNangTKB()
                        {
                            MaSoSV = A.MaSoSV,
                            MaNhom = C.MaNhom,
                            Tiet = C.Tiet,
                            Thu = C.Thu,
                            TenPH = E.TenPH,
                            SoTiet = C.SoTiet,
                            Tuan = C.Tuan,
                            TenMH = D.TenMH,
                            HocKy = B.HocKy,
                            NamHoc = B.NamHoc,
                            NgayBatDau=B.NgayBatDau,
                            NgayKetThuc=B.NgayKetThuc
                            
                        };




            return query;

        }
         public IQueryable<getHocKy> getHocKy(string mssv)
                {

                    var query = from A in DbContext.Nhom
                                join B in DbContext.DangKy
                                on A.MaNhom equals B.MaNhom


                                where B.MaSoSV.Equals(mssv)
                                select new getHocKy()
                                {
                                    HocKy = A.HocKy,
                                    NamHoc = A.NamHoc
                                };
                        
          
                    return query;       

                }

        public IQueryable<getHocKy> getHocKyTH(string mssv)
        {
            var query = from A in DbContext.Nhom_TamThoi
                        join B in DbContext.DangKy_TamThoi
                        on A.MaNhom equals B.MaNhom


                        where B.MaSoSV.Equals(mssv)
                        select new getHocKy()
                        {
                            HocKy = A.HocKy,
                            NamHoc = A.NamHoc
                        };


            return query;
        }

        public IQueryable<ChucNangTKB> getNgayBatDauVaKetThuc(string mssv)
        {
            var query = from A in DbContext.Nhom
                        join B in DbContext.DangKy
                        on A.MaNhom equals B.MaNhom
                        join C in DbContext.ThoiKhoaBieu
                        on A.MaNhom equals C.MaNhom


                        where B.MaSoSV.Equals(mssv)
                        select new ChucNangTKB()
                        {
                           NgayBatDau=A.NgayBatDau,
                           NgayKetThuc=A.NgayKetThuc
                        };

            return query;
        }
    }



}
