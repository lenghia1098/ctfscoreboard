﻿using Platform.Data.Infrastructure;
using Platform.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Platform.Data.Repositories
{

    public interface INhacNho_SinhVienRepository : IRepository<NhacNho_SinhVien>
    {
        IQueryable<string> GetMaNhacNhoSV(string mssv);
        IQueryable<NhacNhoSinhVienChuaHoanThanh> GetNhacNhoChuaHoanThanh(string mssv);

    }

    class NhacNho_SinhVienRepository : RepositoryBase<NhacNho_SinhVien>, INhacNho_SinhVienRepository
    {
        public NhacNho_SinhVienRepository(IDbFactory dbFactory) : base(dbFactory)
        {

        }

        public IQueryable<string> GetMaNhacNhoSV(string mssv)
        {
            var query = from A in DbContext.NhacNho_SinhVien
                        where A.MaSoSV == mssv && A.HoanThanh == false
                        select A.MaSoNN.ToString();

            return query;
        }

        public IQueryable<NhacNhoSinhVienChuaHoanThanh> GetNhacNhoChuaHoanThanh(string mssv)
        {
            var query = from A in DbContext.NhacNhos
                        join B in DbContext.NhacNho_SinhVien
                        on A.MaSoNN equals B.MaSoNN
                        where B.MaSoSV.Equals(mssv) && B.HoanThanh == false
                        select new NhacNhoSinhVienChuaHoanThanh()
                        {
							MaSoNN=A.MaSoNN,
                            NoiDungNhac = A.NoiDungNhac
                        };
            return query;
        }
    }

}
