﻿
using Platform.Data.Infrastructure;
using Platform.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Platform.Data.Repositories
{

    public interface IViecPhaiLamSinhVienRepository : IRepository<ViecPhaiLamSinhVien>
    {

       IEnumerable<chucnangvieclamsinhvien> getviecphailam(string mssv);
        int getviecdalam(string mssv);
        IQueryable<chitietviecdalam> getchitietviecdalam(int MaSoCongViec);
        string DemViecPhaiLamSinhVien(string mssv);
    }

    class ViecPhaiLamSinhVienRepository : RepositoryBase<ViecPhaiLamSinhVien>, IViecPhaiLamSinhVienRepository
    {
        public ViecPhaiLamSinhVienRepository(IDbFactory dbFactory) : base(dbFactory)
        {
        }

        public string DemViecPhaiLamSinhVien(string mssv)
        {
            var query = from A in DbContext.SinhVien_ViecPhaiLam
                        join B in DbContext.ViecPhaiLamSinhViens
                        on A.MaSoCongViec equals B.MaSoCongViec
                        where A.MaSoSV.Equals(mssv) && A.DaHoanThanh == false
                        select B;
            int dem = query.Count();
            return dem.ToString();
        }

        public IQueryable<chitietviecdalam> getchitietviecdalam(int MaSoCongViec)
        {
            var query = from A in DbContext.SinhVien_ViecPhaiLam
                        join B in DbContext.SinhViens
                        on A.MaSoSV equals B.MaSoSV
                        join C in DbContext.ViecPhaiLamSinhViens
                        on A.MaSoCongViec equals C.MaSoCongViec
                        where C.MaSoCongViec.Equals(MaSoCongViec) 
                        select new chitietviecdalam
                        {
                            MaSoSV = B.MaSoSV,
                            MoTa = C.MoTa,
                            HoLot = B.HoLot,
                            Ten = B.Ten,
                            NgayHoanThanh = A.NgayHoanThanh,
                            MaSoCongViec = C.MaSoCongViec,
                            TrangThai=A.TrangThai
                        };

            return query;
        }

        public int getviecdalam(string mssv)
        {
            var query = from A in DbContext.SinhVien_ViecPhaiLam
                        join B in DbContext.SinhViens
                        on A.MaSoSV equals B.MaSoSV
                        join C in DbContext.ViecPhaiLamSinhViens
                        on A.MaSoCongViec equals C.MaSoCongViec
                        where A.MaSoSV.Equals(mssv) && A.DaHoanThanh == true
                        select A;
            int dem = query.Count();
            return dem;
        }

        public IEnumerable<chucnangvieclamsinhvien> getviecphailam(string mssv)
        {
            var query = from A in DbContext.SinhVien_ViecPhaiLam
                        join B in DbContext.SinhViens
                        on A.MaSoSV equals B.MaSoSV
                        join C in DbContext.ViecPhaiLamSinhViens
                        on A.MaSoCongViec equals C.MaSoCongViec
                        where A.MaSoSV.Equals(mssv) && A.DaHoanThanh == false
                        select new chucnangvieclamsinhvien
                        {
                            MaSoSV = B.MaSoSV,
                            MoTa = C.MoTa,
                            HoLot = B.HoLot,
                            Ten = B.Ten,
                            NgayHoanThanh = A.NgayHoanThanh,
                            MaSoCongViec = C.MaSoCongViec

                        };

            return query;

        }

    }
}