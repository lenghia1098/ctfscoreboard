﻿

using Platform.Data.Infrastructure;
using Platform.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Platform.Data.Repositories
{

    public interface IThoiGianCongTacRepository : IRepository<ThoiGianCongTac>
    {
        IQueryable<ThoiGianCongTac> getThoiGianCongTac(string msvc);
        ThoiGianCongTac getid(int ID);
    }

    class ThoiGianCongTacRepository : RepositoryBase<ThoiGianCongTac>, IThoiGianCongTacRepository
    {
        public ThoiGianCongTacRepository(IDbFactory dbFactory) : base(dbFactory)
        {
        }

        public ThoiGianCongTac getid(int ID)
        {
            var query = from A in DbContext.ThoiGianCongTac
                        where A.ID.Equals(ID)
                        select A;

            return query.First();
        }

        public IQueryable<ThoiGianCongTac> getThoiGianCongTac(string msvc)
        {
            var query = from A in DbContext.ThoiGianCongTac
                        join B in DbContext.VienChucs
                        on A.MaSoVC equals B.MaSoVC
                        where A.MaSoVC.Equals(msvc)
                        select A;

            return query;
        }
    }
}