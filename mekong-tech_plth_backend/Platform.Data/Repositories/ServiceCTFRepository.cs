﻿
using Platform.Data.Infrastructure;
using Platform.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Platform.Data.Repositories
{

    public interface IServiceCTFRepository : IRepository<ServiceCTF>
    {
        void deleteall();
    }

    class ServiceCTFRepository : RepositoryBase<ServiceCTF>, IServiceCTFRepository
    {
        public ServiceCTFRepository(IDbFactory dbFactory) : base(dbFactory)
        {
        }

        public void deleteall()
        {
            var query = from a in DbContext.serviceCTFs
                        select a;
            foreach(var item in query)
            {
                DbContext.serviceCTFs.Remove(item);
            }

            DbContext.SaveChanges();
        }
    }



}
