﻿

using Platform.Data.Infrastructure;
using Platform.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Platform.Data.Repositories
{

    public interface IChuongTrinhDaoTao_MonHocRepository : IRepository<ChuongTrinhDaoTao_MonHoc>
    {
        IEnumerable<ChuongTrinhDaoTao_MonHoc> GetAllByMACT(string Id);
        void deleteById(string Id);
        IEnumerable<ChuongTrinhDaoTao_MonHoc> GetAllByMAMH(string Id);
        IEnumerable<ChuongTrinhDaoTao_MonHoc> GetAllByKey(string maCT, string maMH);
    }

    class ChuongTrinhDaoTao_MonHocRepository : RepositoryBase<ChuongTrinhDaoTao_MonHoc>, IChuongTrinhDaoTao_MonHocRepository
    {
        public ChuongTrinhDaoTao_MonHocRepository(IDbFactory dbFactory) : base(dbFactory)
        {
        }

        public void deleteById(string Id)
        {
            var query = from p in DbContext.ChuongTrinhDaoTao_MonHoc
                        where p.MaCT.Equals(Id)
                        select p;
            foreach (var MACT in query)
            {
                DbContext.ChuongTrinhDaoTao_MonHoc.Remove(MACT);
            }
            //totalRow = query.Count();

            //query = query.Skip((pageIndex - 1) * pageSize).Take(pageSize);


        }

        public IEnumerable<ChuongTrinhDaoTao_MonHoc> GetAllByKey(string maCT, string maMH)
        {
            var query = from p in DbContext.ChuongTrinhDaoTao_MonHoc
                        where p.MaCT.Equals(maCT) && p.MaMH.Equals(maMH)
                        select p;

            //totalRow = query.Count();

            //query = query.Skip((pageIndex - 1) * pageSize).Take(pageSize);

            return query;
        }

        public IEnumerable<ChuongTrinhDaoTao_MonHoc> GetAllByMACT(string Id)
        {
            var query = from p in DbContext.ChuongTrinhDaoTao_MonHoc
                        where p.MaCT.Equals(Id)
                        select p;

            //totalRow = query.Count();

            //query = query.Skip((pageIndex - 1) * pageSize).Take(pageSize);

            return query;
        }

        public IEnumerable<ChuongTrinhDaoTao_MonHoc> GetAllByMAMH(string Id)
        {
            var query = from p in DbContext.ChuongTrinhDaoTao_MonHoc
                        where p.MaMH.Equals(Id)
                        select p;

            //totalRow = query.Count();

            //query = query.Skip((pageIndex - 1) * pageSize).Take(pageSize);

            return query;
        }
    }
}
