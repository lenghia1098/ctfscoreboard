﻿
using Platform.Data.Infrastructure;
using Platform.Model;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Platform.Data.Repositories
{

    public interface IHocKyHienTaiRepository : IRepository<HocKyHienTai>
    {
        HocKyHienTai getID();

    }


    public class HocKyHienTaiRepository : RepositoryBase<HocKyHienTai>, IHocKyHienTaiRepository
    {
        public HocKyHienTaiRepository(IDbFactory dbFactory) : base(dbFactory)
        {

        }

        public HocKyHienTai getID()
        {
            var query = from A in DbContext.HocKyHienTai
                        select A;

            return query.First();
        }
    }
}

