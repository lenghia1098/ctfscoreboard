﻿
using Platform.Data.Infrastructure;
using Platform.Model;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Platform.Data.Repositories
{

    public interface IThoiHanDangKyMonHocRepository : IRepository<ThoiHanDangKyMonHoc>
    {
        

    }


    public class ThoiHanDangKyMonHocRepository : RepositoryBase<ThoiHanDangKyMonHoc>, IThoiHanDangKyMonHocRepository
    {
        public ThoiHanDangKyMonHocRepository(IDbFactory dbFactory) : base(dbFactory)
        {

        }

     
    }
}

