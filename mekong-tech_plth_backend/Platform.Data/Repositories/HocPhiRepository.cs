﻿using Platform.Data.Infrastructure;
using Platform.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Platform.Data.Repositories
{
    public interface IHocPhiRepository : IRepository<HocPhi>
    {
        IEnumerable<HocPhi> GetAllById(string mssv,string hocKy,string namHoc);
        IEnumerable<getHocKyHocPhi> GetHocKy(string mssv);

    }
    public class HocPhiRepository : RepositoryBase<HocPhi>, IHocPhiRepository
    {
        public HocPhiRepository(IDbFactory dbFactory) : base(dbFactory)
        {

        }

        public IEnumerable<HocPhi> GetAllById(string mssv,string hocKy,string namHoc)
        {
            var query = from p in DbContext.HocPhi
                        where p.MaSoSV.Equals(mssv) && p.HocKy.Equals(hocKy)&& p.NamHoc.Equals(namHoc)
                        select p;

            //totalRow = query.Count();

            //query = query.Skip((pageIndex - 1) * pageSize).Take(pageSize);

            return query;
        }

        public IEnumerable<getHocKyHocPhi> GetHocKy(string mssv)
        {
            var query = from p in DbContext.HocPhi
                        where p.MaSoSV.Equals(mssv)
                        select new getHocKyHocPhi() { 
                            HocKy=p.HocKy,
                            NamHoc=p.NamHoc
                        };
            return query;

        }
    }
}
