﻿
using Platform.Data.Infrastructure;
using Platform.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Platform.Data.Repositories
{

    public interface INhom_TamThoiRepository : IRepository<Nhom_TamThoi>
    {
      
    }

    class Nhom_TamThoiRepository : RepositoryBase<Nhom_TamThoi>, INhom_TamThoiRepository
    {
        public Nhom_TamThoiRepository(IDbFactory dbFactory) : base(dbFactory)
        {
        }

       

       
    }



}
