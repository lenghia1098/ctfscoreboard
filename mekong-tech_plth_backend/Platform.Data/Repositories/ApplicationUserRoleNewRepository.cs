﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Platform.Data.Infrastructure;
using Platform.Model.Models;

namespace Platform.Data.Repositories
{
    public interface IApplicationUserRoleNewRepository : IRepository<ApplicationUserRoleNew>
    {

    }
    public class ApplicationUserRoleNewRepository : RepositoryBase<ApplicationUserRoleNew>, IApplicationUserRoleNewRepository
    {
        public ApplicationUserRoleNewRepository(IDbFactory dbFactory) : base(dbFactory)
        {

        }
    }
}
