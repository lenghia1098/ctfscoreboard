﻿
using Platform.Data.Infrastructure;
using Platform.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Platform.Data.Repositories
{

    public interface IQuaTrinhDaoTaoRepository : IRepository<QuaTrinhDaoTao>
    {
        IEnumerable<QuaTrinhDaoTao> getQuaTrinhDaoTao(string msvc);
       QuaTrinhDaoTao getid(int ID);
    }

    class QuaTrinhDaoTaoRepository : RepositoryBase<QuaTrinhDaoTao>, IQuaTrinhDaoTaoRepository
    {
        public QuaTrinhDaoTaoRepository(IDbFactory dbFactory) : base(dbFactory)
        {
        }

        public QuaTrinhDaoTao getid(int ID)
        {
            var query = from A in DbContext.QuaTrinhDaoTao
                        where A.ID.Equals(ID)
                        select A;

            return query.First();
        }

        public IEnumerable<QuaTrinhDaoTao> getQuaTrinhDaoTao(string msvc)
        {
            var query = from A in DbContext.QuaTrinhDaoTao
                        join B in DbContext.VienChucs
                        on A.MaSoVC equals B.MaSoVC
                        where A.MaSoVC.Equals(msvc)
                        select A;

            return query;
        }
    }
}