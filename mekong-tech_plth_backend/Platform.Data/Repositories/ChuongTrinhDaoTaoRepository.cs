﻿
using Platform.Data.Infrastructure;
using Platform.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Platform.Data.Repositories
{

    public interface IChuongTrinhDaoTaoRepository : IRepository<ChuongTrinhDaoTao>
    {
        IQueryable<chucnangchuongtrinhdaotao> GetChucnangchuongtrinhdaotaos(string mssv);
        IQueryable<chucnangchuongtrinhdaotao> getMonDaHoc(string mssv);
    }

    class ChuongTrinhDaoTaoRepository : RepositoryBase<ChuongTrinhDaoTao>, IChuongTrinhDaoTaoRepository
    {
        public ChuongTrinhDaoTaoRepository(IDbFactory dbFactory) : base(dbFactory)
        {
        }

        public IQueryable<chucnangchuongtrinhdaotao> GetChucnangchuongtrinhdaotaos(string mssv)
        {
            
            var query = from p in DbContext.SinhVien_Lop
                        join E in DbContext.Lops
                        on p.MaLop equals E.MaLop
                        join A in DbContext.Lop_ChuongTrinhDaoTao
                        on E.MaLop equals A.MaLop
                        join B in DbContext.ChuongTrinhDaoTaos
                        on A.MaCT equals B.MaCT
                        join C in DbContext.ChuongTrinhDaoTao_MonHoc
                        on B.MaCT equals C.MaCT
                        join D in DbContext.MonHocs
                        on C.MaMH equals D.MaMH
                       
                        
                        //join F in DbContext.KetQuas
                        //on E.MaNhom equals F.MaNhom 
                        where p.MaSoSV.Equals(mssv)
                        select new chucnangchuongtrinhdaotao
                        {

                            MaCT = B.MaCT,
                            TenChuong = B.TenChuong,
                            MaMH = D.MaMH,
                            TenMH = D.TenMH,
                            SoTinChi = D.SoTinChi,
                            MaLop = A.MaLop,
                            SoGioLyThuyet=D.SoGioLyThuyet,
                            SoGioThucHanh=D.SoGioThucHanh,
                           // Dau=F.Dau,
                           HocKy=C.HocKy,
                           //NamHoc=F.NamHoc

                            


                        };
            return query;
                       
        }

        public IQueryable<chucnangchuongtrinhdaotao> getMonDaHoc(string mssv)
        {
            var query = from p in DbContext.KetQuas
                        join A in DbContext.Nhom
                        on p.MaNhom equals A.MaNhom
                        join B in DbContext.MonHocs
                        on A.MaMH equals B.MaMH
                        where p.MaSoSV.Equals(mssv)
                        select new chucnangchuongtrinhdaotao()
                        {
                            MaMH = B.MaMH,
                            Dau = p.Dau

                        };
            return query;


        }
    }
}
