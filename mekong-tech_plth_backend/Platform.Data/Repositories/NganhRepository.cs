﻿
using Platform.Data.Infrastructure;
using Platform.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Platform.Data.Repositories
{

    public interface INganhRepository : IRepository<Nganh>
    {

    }

    class NganhRepository : RepositoryBase<Nganh>, INganhRepository
    {
        public NganhRepository(IDbFactory dbFactory) : base(dbFactory)
        {
        }

    }
}