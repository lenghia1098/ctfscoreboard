﻿
using Platform.Data.Infrastructure;
using Platform.Model;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Platform.Data.Repositories
{

    public interface ISinhVien_LopRepository : IRepository<SinhVien_Lop>
    {
    
        
    }
  

    public class SinhVien_LopRepository : RepositoryBase<SinhVien_Lop>, ISinhVien_LopRepository
    {
        public SinhVien_LopRepository(IDbFactory dbFactory) : base(dbFactory)
        {

        }

      
    }
}

    