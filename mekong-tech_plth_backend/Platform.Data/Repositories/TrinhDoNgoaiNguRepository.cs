﻿

using Platform.Data.Infrastructure;
using Platform.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Platform.Data.Repositories
{

    public interface ITrinhDoNgoaiNguRepository : IRepository<TrinhDoNgoaiNgu>
    {
        IEnumerable<TrinhDoNgoaiNgu> GetTrinhDoNgoaiNgu(string msvc);
        TrinhDoNgoaiNgu GetId(int ID);
    }

    class TrinhDoNgoaiNguRepository : RepositoryBase<TrinhDoNgoaiNgu>, ITrinhDoNgoaiNguRepository
    {
        public TrinhDoNgoaiNguRepository(IDbFactory dbFactory) : base(dbFactory)
        {
        }

        public TrinhDoNgoaiNgu GetId(int ID)
        {
            var query = from A in DbContext.TrinhDoNgoaiNgu
                       
                       
                        where A.ID.Equals(ID)
                        select A;

            return query.First();
        }

        public IEnumerable<TrinhDoNgoaiNgu> GetTrinhDoNgoaiNgu(string msvc)
        {
            var query = from A in DbContext.TrinhDoNgoaiNgu
                        join B in DbContext.VienChucs
                        on A.MaSoVC equals B.MaSoVC
                        where A.MaSoVC.Equals(msvc)
                        select A;

            return query;
        }
    }
}