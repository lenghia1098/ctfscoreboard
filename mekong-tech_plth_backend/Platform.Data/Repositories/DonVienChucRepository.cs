﻿
using Platform.Data.Infrastructure;
using Platform.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Platform.Data.Repositories
{

    public interface IDonVienChucRepository : IRepository<DonVienChuc>
    {
        IEnumerable<chucnangdondanopvienchuc> GetChucnangdondanopvienchucs(string msvc);
     
		IQueryable<ChiTietDonDaNopVienChuc> GetChiTietDonDaNopVienChuc(int MaDon);
																  
        string demdondanopvienchuc(string msvc);
    }

    class DonVienChucRepository : RepositoryBase<DonVienChuc>, IDonVienChucRepository
    {
        public DonVienChucRepository(IDbFactory dbFactory) : base(dbFactory)
        {
        }

        public string demdondanopvienchuc(string msvc)
        {
            var query = from A in DbContext.DonVienChucs
                        join B in DbContext.Don_VienChuc
                        on A.MaDon equals B.MaDon
                        where B.MaSoVC.Equals(msvc) && B.DaXuLy == false
                        select A;
            int dem = query.Count();
            return dem.ToString();
        }
		public IQueryable<ChiTietDonDaNopVienChuc> GetChiTietDonDaNopVienChuc(int MaDon)
        {
            var query = from A in DbContext.Don_VienChuc
                        join B in DbContext.VienChucs
                        on A.MaSoVC equals B.MaSoVC
                        join C in DbContext.DonVienChucs
                        on A.MaDon equals C.MaDon
                        where A.MaDon.Equals(MaDon)
                        select new ChiTietDonDaNopVienChuc
                        {
                            ChuDe = C.ChuDe,
                            NoiDung = C.NoiDung,
                            ChuThich = C.ChuThich,
                            NgayNop = A.NgayNop,
                            DaXuLy = A.DaXuLy,
                            HoTen = B.HoTen,
                            MaSoVC = B.MaSoVC,

                        };


            return query;
        }
        public IEnumerable<chucnangdondanopvienchuc> GetChucnangdondanopvienchucs(string msvc)
        {
           var query = from A in DbContext.DonVienChucs
                       join B in DbContext.Don_VienChuc
                       on A.MaDon equals B.MaDon
                       join C in DbContext.VienChucs 
                       on B.MaSoVC equals C.MaSoVC
                       where B.MaSoVC.Equals(msvc) && B.DaXoa==null ||B.DaXoa==false
                       select new chucnangdondanopvienchuc
                       {
                            ChuDe = A.ChuDe,
                            NoiDung = A.NoiDung,
                            ChuThich = A.ChuThich,
                            MaDon=A.MaDon,
                            MaSoVC=B.MaSoVC,
                            NgayNop=B.NgayNop,
                            DaXuLy=B.DaXuLy,
                            DaXoa=B.DaXoa,
                            NguoiXoa=B.NguoiXoa,
                            ThoiGianXoa=B.ThoiGianXoa,
                            HoTen=C.HoTen
                       };
            return query;
        }

        
    }
}