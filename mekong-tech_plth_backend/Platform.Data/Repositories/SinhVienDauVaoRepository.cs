﻿
using Platform.Data.Infrastructure;
using Platform.Model;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity.Infrastructure;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Platform.Data.Repositories
{

    public interface ISinhVienDauVaoRepository : IRepository<SinhVienDauVao>
    {
        List<string> GetListDatabase();
        List<string> GetAllTable();
        IQueryable<SinhVienDauVao_MSSV> getallsv();

  
    }

    class SinhVienDauVaoRepository : RepositoryBase<SinhVienDauVao>, ISinhVienDauVaoRepository
    {
        public SinhVienDauVaoRepository(IDbFactory dbFactory) : base(dbFactory)
        {
        }

        public IQueryable<SinhVienDauVao_MSSV> getallsv()
        {
            var query = from p in DbContext.sinhVienDauVao_MSSVs
                        select p;
            return query;
        }

        public List<string> GetAllTable()
        {
            List<string> list = new List<string>();

            // Open connection to the database
            string conString = "server=mekong-tech.com;uid=mek90334_nghia;pwd=816nNyf?; database= mek90334_qldt";
            //string conString = "server=.\\SQLEXPRESS;uid=sa;pwd=123; database=test";

            using (SqlConnection con = new SqlConnection(conString))
            {
                con.Open();

                // Set up a command with the given query and associate
                // this with the current connection.
                using (SqlCommand cmd = new SqlCommand("USE mek90334_qldt  SELECT name FROM sys.Tables ORDER BY name ", con))
                {
                    using (IDataReader dr = cmd.ExecuteReader())
                    {
                        while (dr.Read())
                        {
                            list.Add(dr[0].ToString());

                        }
                    }
                }
            }
            foreach (var item in list)
            {
                Console.WriteLine(item);
            }
            Console.WriteLine(1);
            return list;
        }

        public List<string> GetListDatabase()
        {
            List<string> list = new List<string>();

            // Open connection to the database
            string conString = "server=.\\SQLEXPRESS;uid=sa;pwd=123; database=PlatformTH";

            using (SqlConnection con = new SqlConnection(conString))
            {
                con.Open();

                // Set up a command with the given query and associate
                // this with the current connection.
                using (SqlCommand cmd = new SqlCommand("SELECT name from sys.databases WHERE name NOT IN ('master', 'tempdb', 'model', 'msdb') ORDER BY name", con))
                {
                    using (IDataReader dr = cmd.ExecuteReader())
                    {
                        while (dr.Read())
                        {
                            list.Add(dr[0].ToString());

                        }
                    }
                }
            }
           foreach(var item in list)
            { Console.WriteLine(item);
            }
            Console.WriteLine(1);
            return list;

        }


    }
}

