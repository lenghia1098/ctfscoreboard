﻿
using Platform.Data.Infrastructure;
using Platform.Model;
using System;
using System.Collections.Generic;
using System.Data.Entity.Core.Metadata.Edm;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Platform.Data.Repositories
{

    public interface IKhungGioRepository : IRepository<KhungGio>
    {
      void DeleteAll();
        IQueryable<KhungGio> AllKhungGio();

    }

    class KhungGioRepository : RepositoryBase<KhungGio>, IKhungGioRepository
    {
        public KhungGioRepository(IDbFactory dbFactory) : base(dbFactory)
        {

        }

        public IQueryable<KhungGio> AllKhungGio()
        {
            var Query = from A in DbContext.KhungGios
                        select A;
            return Query;
           
        }

        public void DeleteAll()
        {
            var Query=from A in DbContext.KhungGios
                      select A ;
            foreach (var Item in Query) {
                DbContext.KhungGios.Remove(Item);
            }
            
        }
    }
}
