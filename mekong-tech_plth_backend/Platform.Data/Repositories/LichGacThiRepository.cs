﻿
using Platform.Data.Infrastructure;
using Platform.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Platform.Data.Repositories
{

    public interface ILichGacThiRepository : IRepository<LichGacThi>
    {

    }

    class LichGacThiRepository : RepositoryBase<LichGacThi>, ILichGacThiRepository
    {
        public LichGacThiRepository(IDbFactory dbFactory) : base(dbFactory)
        {
        }

      
    }
}