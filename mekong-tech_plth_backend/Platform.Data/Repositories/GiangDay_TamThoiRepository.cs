﻿
using Platform.Data.Infrastructure;
using Platform.Model;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Platform.Data.Repositories
{

    public interface IGiangDay_TamThoiRepository : IRepository<GiangDay_TamThoi>
    {

      

    }


    public class GiangDay_TamThoiRepository : RepositoryBase<GiangDay_TamThoi>, IGiangDay_TamThoiRepository
    {
        public GiangDay_TamThoiRepository(IDbFactory dbFactory) : base(dbFactory)
        {

        }

       
       
    }
}

