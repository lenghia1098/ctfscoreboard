﻿
using Platform.Data.Infrastructure;
using Platform.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Platform.Data.Repositories
{

    public interface ILopRepository : IRepository<Lop>
    {

    }

    class LopRepository : RepositoryBase<Lop>, ILopRepository
    {
        public LopRepository(IDbFactory dbFactory) : base(dbFactory)
        {
        }

    }
}