﻿

using Platform.Data.Infrastructure;
using Platform.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Platform.Data.Repositories
{

    public interface IThiRepository : IRepository<Thi>
    {

    }

    class ThiRepository : RepositoryBase<Thi>, IThiRepository
    {
        public ThiRepository(IDbFactory dbFactory) : base(dbFactory)
        {
        }

    }
}