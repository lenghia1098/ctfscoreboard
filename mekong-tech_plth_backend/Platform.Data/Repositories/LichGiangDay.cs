﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Platform.Model
{
   public class LichGiangDay
    {
        public string MaMH { get; set; }
        public string TenMH { get; set; }
        public Nullable<byte> SoTinChi { get; set; }
        public Nullable<short> SoGioLyThuyet { get; set; }
        public Nullable<short> SoGioThucHanh { get; set; }
        public string MaLop { get; set; }
        public string MaNhom { get; set; }
        public short Tiet { get; set; }
        public string Thu { get; set; }
        public string TenPH { get; set; }
        public Nullable<int> SoTiet { get; set; }
        public string HocKy { get; set; }
        public string NamHoc { get; set; }
        public Nullable<System.DateTime> NgayBatDau { get; set; }
        public Nullable<System.DateTime> NgayKetThuc { get; set; }
    }
}
