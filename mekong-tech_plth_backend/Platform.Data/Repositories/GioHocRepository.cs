﻿
using Platform.Data.Infrastructure;
using Platform.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Platform.Data.Repositories
{

    public interface IGioHocRepository : IRepository<GioHoc>
    {

    }

    class GioHocRepository : RepositoryBase<GioHoc>, IGioHocRepository
    {
        public GioHocRepository(IDbFactory dbFactory) : base(dbFactory)
        {
        }

    }
}