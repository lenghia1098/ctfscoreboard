﻿


using Platform.Data.Infrastructure;
using Platform.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Platform.Data.Repositories
{

    public interface IDon_Don_VienChucRepository : IRepository<Don_VienChuc>
    {
        Don_VienChuc getIdDonDaNop(int maDon);
    }

    class Don_Don_VienChucRepository : RepositoryBase<Don_VienChuc>, IDon_Don_VienChucRepository
    {
        public Don_Don_VienChucRepository(IDbFactory dbFactory) : base(dbFactory)
        {
        }

        public Don_VienChuc getIdDonDaNop(int maDon)
        {
            var query = from A in DbContext.Don_VienChuc
                        where A.MaDon.Equals(maDon)
                        select A;
            return query.First();

        }
    }
}
