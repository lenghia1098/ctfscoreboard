﻿
using Platform.Data.Infrastructure;
using Platform.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Platform.Data.Repositories
{

    public interface INhacNhoRepository : IRepository<NhacNho>
    {
        IQueryable<NhacNho> GetNoiDungNhacNho(string maNN);
        IQueryable<chitietnhacnho> Getchitietnhacnho(int MaSoNN);
    }

    class NhacNhoRepository : RepositoryBase<NhacNho>, INhacNhoRepository
    {
        public NhacNhoRepository(IDbFactory dbFactory) : base(dbFactory)
        {
        }

        public IQueryable<chitietnhacnho> Getchitietnhacnho(int MaSoNN)
        {
            var query = from A in DbContext.SinhViens
                        join B in DbContext.NhacNho_SinhVien
                        on A.MaSoSV equals B.MaSoSV
                        join C in DbContext.NhacNhos
                        on B.MaSoNN equals C.MaSoNN
                        where C.MaSoNN.Equals(MaSoNN)
                        select new chitietnhacnho { 
                           NoiDungNhac=C.NoiDungNhac,
                           HoLot=A.HoLot,
                           Ten=A.Ten,
                           HoanThanh=B.HoanThanh

                        };
            return query;
        }

        public IQueryable<NhacNho> GetNoiDungNhacNho(string maNN)
        {
            var query = from A in DbContext.NhacNhos
                        where A.MaSoNN.Equals(maNN)
                        select A;
            return query;
        }
    }
}