﻿


using Platform.Data.Infrastructure;
using Platform.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Platform.Data.Repositories
{

    public interface IDonSinhVienRepository : IRepository<DonSinhVien>
    {
		IEnumerable<chucnangdondanop> GetChucnangdondanops(string mssv);
        IQueryable<chitietdondanop> ChiTietDonDaNop(int MaDon);
       
       
        string demdonsinhvien(string mssv);
    }

    class DonSinhVienRepository : RepositoryBase<DonSinhVien>, IDonSinhVienRepository
    {
        public DonSinhVienRepository(IDbFactory dbFactory) : base(dbFactory)
        {
        }

        public IQueryable<chitietdondanop> ChiTietDonDaNop( int MaDon)
        {
            var query = from A in DbContext.Don_SinhVien
                        join B in DbContext.SinhViens
                        on A.MaSoSV equals B.MaSoSV
                        join C in DbContext.DonSinhViens
                        on A.MaDon equals C.MaDon
                        where A.MaDon.Equals(MaDon)
                        select new chitietdondanop
                        {
                            ChuDe = C.ChuDe,
                            NoiDung = C.NoiDung,
                            ChuThich = C.ChuThich,
                            MaSoSV=B.MaSoSV,
                            HoLot=B.HoLot,
                            Ten=B.Ten,
                            NgayNop=A.NgayNop,
                            DaXuLy=A.DaXuLy
                            


                        };


            return query;
        }

        public string demdonsinhvien(string mssv)
        {
            var query = from A in DbContext.DonSinhViens
                        join B in DbContext.Don_SinhVien
                        on A.MaDon equals B.MaDon
                        where B.MaSoSV.Equals(mssv) && B.DaXuLy == false 
                        select new chucnangdondanop
                        {
                            ChuDe = A.ChuDe,
                            NoiDung = A.NoiDung,
                            ChuThich = A.ChuThich,
                           
                            
                        };
            int dem = query.Count();
            return dem.ToString();
        }

        

        public IEnumerable<chucnangdondanop> GetChucnangdondanops(string mssv)
        {
            var query = from A in DbContext.DonSinhViens
                        join B in DbContext.Don_SinhVien
                        on A.MaDon equals B.MaDon
                        join C in DbContext.SinhViens
                        on B.MaSoSV equals C.MaSoSV
                        where B.MaSoSV.Equals(mssv) && B.DaXoa == null||B.DaXoa==false
                        select new chucnangdondanop
                        {
                            MaDon=A.MaDon,
                            ChuDe = A.ChuDe,
                            NoiDung = A.NoiDung,
                            ChuThich = A.ChuThich,
                            DaXuLy=B.DaXuLy,
                            HoLot=C.HoLot,
                            Ten=C.Ten,
                            NgayNop=B.NgayNop,
                            MaSoSV=B.MaSoSV,
                            DaXoa=B.DaXoa,
                            NguoiXoa=B.NguoiXoa,
                            ThoiGianXoa=B.ThoiGianXoa

                        };
            return query;
           
        }
    }
}