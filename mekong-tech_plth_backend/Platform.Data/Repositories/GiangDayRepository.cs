﻿
using Platform.Data.Infrastructure;
using Platform.Model;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Platform.Data.Repositories
{

    public interface IGiangDayRepository : IRepository<GiangDay>
    {

        IQueryable<DanhSachLopTheoMaVC> getDanhSachLop(string msvc, string hocKy);
        IQueryable getDanhSachHocKy(string msvc);
        IQueryable<DanhSachGiangDayTheoMaNhom> getDanhSachGiangDay(string maNhom);
        IQueryable<LichGiangDay> GetLichGiangDay(string msvc,string hocky,string namhoc);

    }


    public class GiangDayRepository : RepositoryBase<GiangDay>, IGiangDayRepository
    {
        public GiangDayRepository(IDbFactory dbFactory) : base(dbFactory)
        {

        }

        public IQueryable<DanhSachLopTheoMaVC> getDanhSachLop(string msvc, string hocKy)
        {
            var query = from A in DbContext.GiangDay
                        join B in DbContext.Nhom
                        on A.MaNhom equals B.MaNhom
                        join C in DbContext.MonHocs
                        on B.MaMH equals C.MaMH

                        join F in DbContext.ChuongTrinhDaoTao_MonHoc
                        on C.MaMH equals F.MaMH
                        join G in DbContext.ChuongTrinhDaoTaos
                        on F.MaCT equals G.MaCT
                        join H in DbContext.Lop_ChuongTrinhDaoTao
                        on G.MaCT equals H.MaCT
                        join I in DbContext.Lops
                        on H.MaLop equals I.MaLop

                        where A.MaSoVC.Equals(msvc) && B.HocKy.Equals(hocKy)
                        select new DanhSachLopTheoMaVC()
                        {
                            MaMH = C.MaMH,
                            TenMH = C.TenMH,
                            MaNhom = B.MaNhom,
                            TenLop = I.TenLop,
                            HocKy = B.HocKy

                        };



            return query;
        }

        public IQueryable getDanhSachHocKy(string msvc)
        {
            var query = from A in DbContext.GiangDay
                        join B in DbContext.Nhom
                        on A.MaNhom equals B.MaNhom
                        where A.MaSoVC.Equals(msvc)
                        select B.HocKy;
            return query;
        }

        public IQueryable<DanhSachGiangDayTheoMaNhom> getDanhSachGiangDay(string maNhom)
        {
            var query = from A in DbContext.DangKy
                        join B in DbContext.SinhViens
                        on A.MaSoSV equals B.MaSoSV
                        join C in DbContext.SinhVien_Lop
                        on B.MaSoSV equals C.MaSoSV
                        join D in DbContext.Lops
                        on C.MaLop equals D.MaLop
                        join E in DbContext.Nganhs
                        on D.MaNganh equals E.MaNganh
                        join F in DbContext.PhongKhoas
                        on E.MaPK equals F.MaPK
                        where A.MaNhom.Equals(maNhom)
                        select new DanhSachGiangDayTheoMaNhom()
                        {
                            MaSoSV = B.MaSoSV,
                            HoLot = B.HoLot,
                            Ten = B.Ten,
                            TenNganh = E.TenNganh,
                            TenPhongKhoa = F.TenPhongKhoa,
                            Email = B.Email
                        };
            return query;

        }

        public IQueryable<LichGiangDay> GetLichGiangDay(string msvc, string hocky, string namhoc)
        {
            var query = from A in DbContext.GiangDay
                        join B in DbContext.Nhom
                        on A.MaNhom equals B.MaNhom
                        join C in DbContext.MonHocs
                        on B.MaMH equals C.MaMH
                        join D in DbContext.ThoiKhoaBieu
                        on B.MaNhom equals D.MaNhom
                        join E in DbContext.PhongHoc
                        on D.MaPH equals E.MaPH
                        join G in DbContext.ChuongTrinhDaoTao_MonHoc
                        on C.MaMH equals G.MaMH
                        join H in DbContext.Lop_ChuongTrinhDaoTao
                        on G.MaCT equals H.MaCT
                        join J in DbContext.Lops
                        on H.MaLop equals J.MaLop
                        where A.MaSoVC.Equals(msvc) &&B.HocKy.Equals(hocky) && B.NamHoc.Equals(namhoc)
                        select new LichGiangDay()
                        {

                            TenMH = C.TenMH,
                            MaNhom = B.MaNhom,
                            MaMH = C.MaMH,
                            SoTinChi = C.SoTinChi,
                            MaLop = J.MaLop,
                            Tiet = D.Tiet,
                            Thu = D.Thu,
                            TenPH = E.TenPH,
                            SoGioThucHanh = C.SoGioThucHanh,
                            SoGioLyThuyet = C.SoGioLyThuyet,
                            SoTiet = D.SoTiet,
                            HocKy = B.HocKy,
                            NamHoc = B.NamHoc,
                            NgayBatDau = B.NgayBatDau,
                            NgayKetThuc=B.NgayKetThuc




                        };
            return query;



        }

       
    }
}

