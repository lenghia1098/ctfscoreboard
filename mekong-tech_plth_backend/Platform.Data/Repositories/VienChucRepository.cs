﻿
using Platform.Data.Infrastructure;
using Platform.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Platform.Data.Repositories
{

    public interface IVienChucRepository : IRepository<VienChuc>
    {
        IQueryable<TTCNVienChuc> GetTTCN(string Id);
       VienChuc GetLyLichVienChuc(string msvc);
    }

    class VienChucRepository : RepositoryBase<VienChuc>, IVienChucRepository
    {
        public VienChucRepository(IDbFactory dbFactory) : base(dbFactory)
        {
        }

        public VienChuc GetLyLichVienChuc(string msvc)
        {
            var query = from A in DbContext.VienChucs


                        where A.MaSoVC.Equals(msvc)
                        select A;
                        
            return query.First();
        }

        public IQueryable<TTCNVienChuc> GetTTCN(string Id)
        {
            var query = from A in DbContext.VienChucs
                        join B in DbContext.PhongKhoas
                        on A.MaPK equals B.MaPK
                        where A.MaSoVC.Equals(Id)
                        select new TTCNVienChuc { 
                            MaSoVC=A.MaSoVC,
                            HoTen=A.HoTen,
                            Email=A.Email,
                            TenPhongKhoa=B.TenPhongKhoa

                        };
            return query;

        }
    }
}