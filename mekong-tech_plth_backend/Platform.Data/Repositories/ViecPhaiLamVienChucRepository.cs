﻿

using Platform.Data.Infrastructure;
using Platform.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Platform.Data.Repositories
{

    public interface IViecPhaiLamVienChucRepository : IRepository<ViecPhaiLamVienChuc>
    {
        IQueryable<chucnangviecphailamvienchuc> GetViecPhaiLamVienChucs(string msvc);
        string DemViecPhaiLamVienChuc(string MaSoVienChuc);
        IQueryable<ViecDaLamVienChuc> ViecDaLamVienChuc(string msvc);
        IQueryable<ChiTietViecDaLamVienChuc> getChiTietViecDaLamVienChuc(int MaSoCongViec);
    }

    class ViecPhaiLamVienChucRepository : RepositoryBase<ViecPhaiLamVienChuc>, IViecPhaiLamVienChucRepository
    {
        public ViecPhaiLamVienChucRepository(IDbFactory dbFactory) : base(dbFactory)
        {
        }

        public string DemViecPhaiLamVienChuc(string MaSoVienChuc)
        {
            var query = from A in DbContext.VienChuc_ViecPhaiLam
                        join B in DbContext.ViecPhaiLamVienChucs
                        on A.MaSoCongViec equals B.MaSoCongViec
                        where A.MaSoVC.Equals(MaSoVienChuc) && A.DaHoanThanh == false
                        select B;
            int dem = query.Count();
            return dem.ToString();
        }

         public IQueryable<ChiTietViecDaLamVienChuc> getChiTietViecDaLamVienChuc(int MaSoCongViec)
        {
            var query = from A in DbContext.VienChuc_ViecPhaiLam
                        join B in DbContext.VienChucs
                        on A.MaSoVC equals B.MaSoVC
                        join C in DbContext.ViecPhaiLamVienChucs
                        on A.MaSoCongViec equals C.MaSoCongViec
                        where A.MaSoCongViec.Equals(MaSoCongViec)
                        select new ChiTietViecDaLamVienChuc
                        {
                            MaSoSV = B.MaSoVC,
                            MoTa = C.MoTa,
                            HoTen = B.HoTen,
                            NgayHoanThanh = A.NgayHoanThanh,
                            MaSoCongViec = C.MaSoCongViec,
                            TrangThai = A.TrangThai
                        };

            return query;
        }

        public IQueryable<chucnangviecphailamvienchuc> GetViecPhaiLamVienChucs(string msvc)
        {
            var query = from A in DbContext.VienChuc_ViecPhaiLam
                        join B in DbContext.VienChucs
                        on A.MaSoVC equals B.MaSoVC
                        join C in DbContext.ViecPhaiLamVienChucs
                        on A.MaSoCongViec equals C.MaSoCongViec
                        where A.MaSoVC.Equals(msvc) && A.DaHoanThanh == false
                        select new chucnangviecphailamvienchuc { 

                            MaSoCongViec=C.MaSoCongViec,
                            TenCongViec=C.TenCongViec,
                            MoTa=C.MoTa,
                            HoTen=B.HoTen,
                            NgayHoanThanh=A.NgayHoanThanh,
                            ThoiHanHoanThanh=A.ThoiHanHoanThanh
                            
                        };
            return query;
        }

        public IQueryable<ViecDaLamVienChuc> ViecDaLamVienChuc(string msvc)
        {
            var query = from A in DbContext.VienChuc_ViecPhaiLam
                        join B in DbContext.VienChucs
                        on A.MaSoVC equals B.MaSoVC
                        join C in DbContext.ViecPhaiLamVienChucs
                        on A.MaSoCongViec equals C.MaSoCongViec
                        where A.MaSoVC.Equals(msvc) && A.DaHoanThanh == true && A.DaXoa==false||A.DaXoa==null
                        select new ViecDaLamVienChuc
                        {
                            MaSoVC = B.MaSoVC,
                            MoTa = C.MoTa,
                            HoTen = B.HoTen,
                            NgayHoanThanh = A.NgayHoanThanh,
                            MaSoCongViec = C.MaSoCongViec,
                            TrangThai = A.TrangThai,
                            ThoiHanHoanThanh=A.ThoiHanHoanThanh,
                            HanChot=A.HanChot,
                            DaHoanThanh=A.DaHoanThanh,
                            DaXoa=A.DaXoa,
                            NguoiXoa=A.NguoiXoa
                        };

            return query;
        }
    }
}