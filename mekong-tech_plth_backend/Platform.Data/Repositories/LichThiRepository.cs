﻿
using Platform.Data.Infrastructure;
using Platform.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Platform.Data.Repositories
{

    public interface ILichThiRepository : IRepository<LichThi>
    {
        IQueryable<ChucNangLichThi> GetLichThi(string mssv, string hocKy, string namhoc);
        IQueryable<ChucNangLichThi> GetLichThiGV(string msvc);
        IQueryable<droplist_allmonhoc> getAllMonHoc(string NamHoc, string hocKy);
        IQueryable<getLichThiMonHoc> getLichThiMH(string tenMH, string hocKy, string namhoc);
        IQueryable<getLichThiMonHoc> getTTMH(string maMH, string maNhom, string malop);
        IQueryable<HocKyHienTai> getHocKyHienTai();
       
    }

    class LichThiRepository : RepositoryBase<LichThi>, ILichThiRepository
    {
        public LichThiRepository(IDbFactory dbFactory) : base(dbFactory)
        {
        }

        public IQueryable<ChucNangLichThi> GetLichThi(string mssv, string hocKy, string namhoc) 
        {
            var query = from A in DbContext.LichThi
                        join B in DbContext.Nhom
                        on A.MaNhom equals B.MaNhom
                        join C in DbContext.MonHocs
                        on B.MaMH equals C.MaMH
                        join D in DbContext.Thi
                        on A.LichThiID equals D.LichThiID
                        join E in DbContext.PhongHoc
                        on A.MaPH equals E.MaPH
                        join F in DbContext.SinhViens
                        on D.MaSoSV equals F.MaSoSV
                        join G in DbContext.SinhVien_Lop
                        on F.MaSoSV equals G.MaSoSV
                        join H in DbContext.Lops
                        on G.MaLop equals H.MaLop
                        join M in DbContext.Nganhs
                        on H.MaNganh equals M.MaNganh


                        where F.MaSoSV.Equals(mssv) && B.HocKy.Equals(hocKy) && B.NamHoc.Equals(namhoc)
                       

                        select new ChucNangLichThi
                        {
                            MaMH = C.MaMH,
                            TenMH = C.TenMH,
                            NgayThi = A.NgayThi,
                            GioThi = A.GioThi,
                            TenPH = E.TenPH,
                            TenLop = H.TenLop,
                            MaNhom = B.MaNhom,
                             HocKy = B.HocKy,
                             NamHoc = B.NamHoc,
                             TenNganh=M.TenNganh,
                             MaLop=H.MaLop
                           
                           
                            

                        };
                            



            return query;
        }

        public IQueryable<ChucNangLichThi> GetLichThiGV(string msvc)
        {
            var query = from A in DbContext.GiangDay
                        join B in DbContext.Nhom
                        on A.MaNhom equals B.MaNhom
                        join C in DbContext.MonHocs
                        on B.MaMH equals C.MaMH
                        join D in DbContext.LichThi
                        on B.MaNhom equals D.MaNhom
                        join E in DbContext.PhongHoc
                        on D.MaPH equals E.MaPH
                        join F in DbContext.ChuongTrinhDaoTao_MonHoc
                        on C.MaMH equals F.MaMH
                        join G in DbContext.ChuongTrinhDaoTaos
                        on F.MaCT equals G.MaCT
                        join H in DbContext.Lop_ChuongTrinhDaoTao
                        on G.MaCT equals H.MaCT
                        join I in DbContext.Lops
                        on H.MaLop equals I.MaLop
                        join V in DbContext.LichGacThi
                        on B.MaNhom equals V.MaNhom
                       
                        join Z in DbContext.VienChucs
                        on A.MaSoVC equals Z.MaSoVC
                        join S in DbContext.PhongKhoas
                        on Z.MaPK equals S.MaPK
                        join U in DbContext.Nganhs
                        on S.MaPK equals U.MaPK


                        where A.MaSoVC.Equals(msvc)
                        select new ChucNangLichThi()
                        {
                            MaMH = C.MaMH,
                            TenMH = C.TenMH,
                            NgayThi = D.NgayThi,
                            GioThi = D.GioThi,
                            TenPH = E.TenPH,
                            TenLop = I.TenLop,
                            TenChuong = G.TenChuong,
                            NoiLayDeThi = V.NoiLayDeThi,
                            HocKy = B.HocKy,
                            NamHoc=B.NamHoc,
                            MaSoVC=A.MaSoVC,
                            TenNganh=U.TenNganh
                            


                            


                        };



            return query;
        }

        public IQueryable<droplist_allmonhoc> getAllMonHoc(string NamHoc, string hocKy)
        {
            var query = from A in DbContext.MonHocs
                        join B in DbContext.Nhom
                        on A.MaMH equals B.MaMH
                        join C in DbContext.LichThi
                        on B.MaNhom equals C.MaNhom
                        //join D in DbContext.HocKyHienTai
                       // on B.NamHoc equals D.NamHoc
                        where B.NamHoc.Equals(NamHoc) && B.HocKy.Equals(hocKy)
                        select new droplist_allmonhoc
                        {
                            MaNhom = B.MaNhom,
                            MaMH = A.MaMH,
                            TenMH = A.TenMH,
                            HocKy = B.HocKy,
                            NamHoc = B.NamHoc


                        };
            return query;


        }

        public IQueryable<getLichThiMonHoc> getLichThiMH(string tenMH, string hocKy, string namhoc)
        {
            var query = from A in DbContext.MonHocs
                        join B in DbContext.ChuongTrinhDaoTao_MonHoc
                        on A.MaMH equals B.MaMH
                        join C in DbContext.ChuongTrinhDaoTaos
                        on B.MaCT equals C.MaCT
                        join D in DbContext.Lop_ChuongTrinhDaoTao
                        on C.MaCT equals D.MaCT
                        join E in DbContext.Lops
                        on D.MaLop equals E.MaLop
                        join F in DbContext.Nhom
                        on A.MaMH equals F.MaMH
                        join G in DbContext.LichThi
                        on F.MaNhom equals G.MaNhom
                        join H in DbContext.PhongHoc
                        on G.MaPH equals H.MaPH
                        join I in DbContext.Nganhs
                        on E.MaNganh equals I.MaNganh
                        where F.HocKy.Equals(hocKy) && F.NamHoc.Equals(namhoc) && A.TenMH.Equals(tenMH)
                        select new getLichThiMonHoc
                        {
                            MaMH = A.MaMH,
                            TenMH = A.TenMH,
                            NgayThi = G.NgayThi,
                            GioThi = G.GioThi,
                            TenPH = H.TenPH,
                            TenLop = E.TenLop,
                            TenChuong = C.TenChuong,
                            MaNhom = F.MaNhom,
                            TenNganh = I.TenNganh,
                            MaLop=E.MaLop



                        };



            return query;
        }

        public IQueryable<getLichThiMonHoc> getTTMH(string maMH, string maNhom,string malop)
        {
            var query = from A in DbContext.MonHocs
                        join B in DbContext.ChuongTrinhDaoTao_MonHoc
                        on A.MaMH equals B.MaMH
                        join C in DbContext.ChuongTrinhDaoTaos
                        on B.MaCT equals C.MaCT
                        join D in DbContext.Lop_ChuongTrinhDaoTao
                        on C.MaCT equals D.MaCT
                        join E in DbContext.Lops
                        on D.MaLop equals E.MaLop
                        join F in DbContext.Nhom
                        on A.MaMH equals F.MaMH
                        join G in DbContext.LichThi
                        on F.MaNhom equals G.MaNhom
                        join H in DbContext.PhongHoc
                        on G.MaPH equals H.MaPH
                        join K in DbContext.Nganhs
                        on E.MaNganh equals K.MaNganh
                        join W in DbContext.DangKy
                        on F.MaNhom equals W.MaNhom
                        join Q in DbContext.SinhViens
                        on W.MaSoSV equals Q.MaSoSV
                        join R in DbContext.ThoiKhoaBieu
                        on F.MaNhom equals R.MaNhom
                        join P in DbContext.GiangDay
                        on F.MaNhom equals P.MaNhom
                        join S in DbContext.VienChucs
                        on P.MaSoVC equals S.MaSoVC







                        where A.MaMH.Equals(maMH) && F.MaNhom.Equals(maNhom) && E.MaLop.Equals(malop)
                        select new getLichThiMonHoc
                        {
                            MaMH = A.MaMH,
                            TenMH = A.TenMH,
                            SoTinChi = A.SoTinChi,
                            MaLop = E.MaLop,
                            TenPH = H.TenPH,
                            SoGioThucHanh = A.SoGioThucHanh,
                            NgayBatDau = F.NgayBatDau,
                            NgayKetThuc = F.NgayKetThuc,
                            NgayThi = G.NgayThi,
                            GioThi = G.GioThi,
                            TenNganh = K.TenNganh,
                            HoLot = Q.HoLot,
                            Ten = Q.Ten,
                            HoTen = S.HoTen,
                            SoTiet = R.SoTiet,

                            MaSoSV = Q.MaSoSV







                        };



            return query;
        }

        public IQueryable<HocKyHienTai> getHocKyHienTai()
        {
            var query = from A in DbContext.HocKyHienTai
                        select A;
            return query;
                     
        }
    }
}