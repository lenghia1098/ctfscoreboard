﻿

using Platform.Data.Infrastructure;
using Platform.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Platform.Data.Repositories
{

    public interface IThoiKhoaBieu_TamThoiRepository : IRepository<ThoiKhoaBieu_TamThoi>
    {

    }

    class ThoiKhoaBieu_TamThoiRepository : RepositoryBase<ThoiKhoaBieu_TamThoi>, IThoiKhoaBieu_TamThoiRepository
    {
        public ThoiKhoaBieu_TamThoiRepository(IDbFactory dbFactory) : base(dbFactory)
        {
        }

    }
}