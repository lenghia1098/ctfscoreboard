﻿
using Platform.Data.Infrastructure;
using Platform.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Platform.Data.Repositories
{

    public interface INgayLeRepository : IRepository<NgayLe>
    {
      
    }

    class NgayLeRepository : RepositoryBase<NgayLe>, INgayLeRepository
    {
        public NgayLeRepository(IDbFactory dbFactory) : base(dbFactory)
        {
        }

       
    }
}
