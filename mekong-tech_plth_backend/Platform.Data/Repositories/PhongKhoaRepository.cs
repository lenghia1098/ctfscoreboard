﻿
using Platform.Data.Infrastructure;
using Platform.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Platform.Data.Repositories
{

    public interface IPhongKhoaRepository : IRepository<PhongKhoa>
    {

    }

    class PhongKhoaRepository : RepositoryBase<PhongKhoa>, IPhongKhoaRepository
    {
        public PhongKhoaRepository(IDbFactory dbFactory) : base(dbFactory)
        {
        }

    }
}