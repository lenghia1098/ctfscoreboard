﻿

using Platform.Data.Infrastructure;
using Platform.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace Platform.Data.Repositories
{

    public interface IDon_SinhVienRepository : IRepository<Don_SinhVien>
    {
        int DemDonDaNop(string mssv);
        Don_SinhVien getID(int maDon);
    }

    class Don_SinhVienRepository : RepositoryBase<Don_SinhVien>, IDon_SinhVienRepository
    {
        public Don_SinhVienRepository(IDbFactory dbFactory) : base(dbFactory)
        {
        }

        public int DemDonDaNop(string mssv)
        {
            var query = from A in DbContext.Don_SinhVien
                        where A.MaSoSV == mssv && A.DaXuLy == false
                        select A;
            int donDanop = query.Count();
            return donDanop;

        }

        public Don_SinhVien getID(int maDon)
        {
            var query = from A in DbContext.Don_SinhVien
                        where A.MaDon.Equals(maDon)
                        select A;
           
            return query.First();
        }
    }
}
