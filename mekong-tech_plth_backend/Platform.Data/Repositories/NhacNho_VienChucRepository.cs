﻿using Platform.Data.Infrastructure;
using Platform.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Platform.Data.Repositories
{

    public interface INhacNho_VienChucRepository : IRepository<NhacNho_VienChuc>
    {
        IQueryable<string> GetMaNhacNhoVC(string msvc);
        IQueryable<NhacNhoVienChucChuaHoanThanh> GetNhacNhoChuaHoanThanh(string msvc);
        IQueryable<ChiTietNhacNhoVienChuc> Getchitietnhacnho(int MaSoNN);

    }

    class NhacNho_VienChucRepository : RepositoryBase<NhacNho_VienChuc>, INhacNho_VienChucRepository
    {
        public NhacNho_VienChucRepository(IDbFactory dbFactory) : base(dbFactory)
        {

        }

        public IQueryable<string> GetMaNhacNhoVC(string msvc)
        {
            var query = from A in DbContext.NhacNho_VienChuc
                        where A.MaSoVC == msvc
                        select A.MaSoNN.ToString();

            return query;
        }

        public IQueryable<ChiTietNhacNhoVienChuc> Getchitietnhacnho(int MaSoNN)
        {
            var query = from A in DbContext.VienChucs
                        join B in DbContext.NhacNho_VienChuc
                        on A.MaSoVC equals B.MaSoVC
                        join C in DbContext.NhacNhos
                        on B.MaSoNN equals C.MaSoNN
                        where C.MaSoNN.Equals(MaSoNN)
                        select new ChiTietNhacNhoVienChuc
                        {

                            NoiDungNhac = C.NoiDungNhac,
                            HoTen = A.HoTen,


                        };
            return query;
        }
        public IQueryable<NhacNhoVienChucChuaHoanThanh> GetNhacNhoChuaHoanThanh(string msvc)
        {
            var query = from A in DbContext.NhacNhos
                        join B in DbContext.NhacNho_VienChuc
                        on A.MaSoNN equals B.MaSoNN
                        where B.MaSoVC.Equals(msvc)
                        select new NhacNhoVienChucChuaHoanThanh()
                        {

                            NoiDungNhac = A.NoiDungNhac,
                            MaSoNN = A.MaSoNN
                        };
            return query;
        }
    }

}
