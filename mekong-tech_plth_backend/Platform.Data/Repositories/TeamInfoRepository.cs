﻿
using Platform.Data.Infrastructure;
using Platform.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Platform.Data.Repositories
{

    public interface ITeamInfoRepository : IRepository<TeamInfo>
    {
      
    }

    class TeamInfoRepository : RepositoryBase<TeamInfo>, ITeamInfoRepository
    {
        public TeamInfoRepository(IDbFactory dbFactory) : base(dbFactory)
        {
        }

       

       
    }



}
