﻿

using Platform.Data.Infrastructure;
using Platform.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Platform.Data.Repositories
{

    public interface ILop_ChuongTrinhDaoTaoRepository : IRepository<Lop_ChuongTrinhDaoTao>
    {

    }

    class Lop_ChuongTrinhDaoTaoRepository : RepositoryBase<Lop_ChuongTrinhDaoTao>, ILop_ChuongTrinhDaoTaoRepository
    {
        public Lop_ChuongTrinhDaoTaoRepository(IDbFactory dbFactory) : base(dbFactory)
        {
        }

    }
}