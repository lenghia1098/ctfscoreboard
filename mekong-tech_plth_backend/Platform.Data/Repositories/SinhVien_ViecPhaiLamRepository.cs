﻿
using Platform.Data.Infrastructure;
using Platform.Model;
using System.Collections.Generic;
using System.Linq;

namespace Platform.Data.Repositories
{

    public interface ISinhVien_ViecPhaiLamRepository : IRepository<SinhVien_ViecPhaiLam>
    {
        int DemViecPhaiLam(string mssv);
        IEnumerable<viecdalam> viecdalam(string mssv);
       SinhVien_ViecPhaiLam getIDViecPhaiLam(int mscv);
      IEnumerable< SinhVien_ViecPhaiLam >getIdAll( string mssv);
    }

    class SinhVien_ViecPhaiLamRepository : RepositoryBase<SinhVien_ViecPhaiLam>, ISinhVien_ViecPhaiLamRepository
    {
        public SinhVien_ViecPhaiLamRepository(IDbFactory dbFactory) : base(dbFactory)
        {
        }
        public int DemViecPhaiLam(string mssv)
        {
            var query = from sv_vpl in DbContext.SinhVien_ViecPhaiLam
                        join vpl in DbContext.ViecPhaiLamSinhViens
                        on sv_vpl.MaSoCongViec equals vpl.MaSoCongViec
                        where sv_vpl.MaSoSV == mssv && sv_vpl.DaHoanThanh == false
                        select sv_vpl;
            int  soViec = query.Count();
            return soViec;
        }

        public IEnumerable<SinhVien_ViecPhaiLam> getIdAll(string mssv)
        {
            var query = from A in DbContext.SinhVien_ViecPhaiLam
                        where A.MaSoSV.Equals(mssv) && A.DaHoanThanh == true 
                        select A;
            return query;
        }

        public SinhVien_ViecPhaiLam getIDViecPhaiLam(int mscv)
        {
            var query = from A in DbContext.SinhVien_ViecPhaiLam
                        where A.MaSoCongViec.Equals(mscv)
                        select A;
            return query.First();
        }

        public IEnumerable<viecdalam> viecdalam(string mssv)
        {
            var query = from A in DbContext.SinhVien_ViecPhaiLam
                        join B in DbContext.SinhViens
                        on A.MaSoSV equals B.MaSoSV
                        join C in DbContext.ViecPhaiLamSinhViens
                        on A.MaSoCongViec equals C.MaSoCongViec
                        where A.MaSoSV.Equals(mssv) && A.DaHoanThanh == true && A.DaXoa==false ||A.DaXoa==null
                        select new viecdalam
                        {
                            MaSoSV = B.MaSoSV,
                            MoTa = C.MoTa,
                            HoLot = B.HoLot,
                            Ten = B.Ten,
                            NgayHoanThanh = A.NgayHoanThanh,
                            MaSoCongViec = C.MaSoCongViec,
                            TrangThai = A.TrangThai,
                            ThoiHanHoanThanh=A.ThoiHanHoanThanh,
                            HanChot=A.HanChot,
                            DaHoanThanh=A.DaHoanThanh,
                            DaXoa=A.DaXoa,
                            ThoiGianXoa = A.ThoiGianXoa,
                            NguoiXoa =A.NguoiXoa
                            
                        };



            return query;
        }
    }
}