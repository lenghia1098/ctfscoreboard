﻿using Platform.Data.Infrastructure;
using Platform.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Platform.Data.Repositories
{
    public interface ICongViecDeXuatRepository : IRepository<CongViecDeXuat>
    {
        IEnumerable<CongViecDeXuat> GetAllById(string Id);
       

    }


    public class CongViecDeXuatRepository : RepositoryBase<CongViecDeXuat>, ICongViecDeXuatRepository
    {
        public CongViecDeXuatRepository(IDbFactory dbFactory) : base(dbFactory)
        {

        }

        public IEnumerable<CongViecDeXuat> GetAllById(string Id)
        {
            var query = from p in DbContext.CongViecDeXuat
                        where p.MaCVDeXuat.Equals(Id)
                        select p;
            return query;
        }

       
    }
}


