﻿
using Platform.Data.Infrastructure;
using Platform.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Platform.Data.Repositories
{

    public interface IPhongKhoa_VienChucRepository : IRepository<PhongKhoa_VienChuc>
    {

    }

    class PhongKhoa_VienChucRepository : RepositoryBase<PhongKhoa_VienChuc>, IPhongKhoa_VienChucRepository
    {
        public PhongKhoa_VienChucRepository(IDbFactory dbFactory) : base(dbFactory)
        {
        }

    }
}