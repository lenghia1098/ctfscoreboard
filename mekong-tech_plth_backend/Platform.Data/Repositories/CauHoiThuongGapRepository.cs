﻿
using Platform.Data.Infrastructure;
using Platform.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Platform.Data.Repositories
{

    public interface ICauHoiThuongGapRepository : IRepository<CauHoiThuongGap>
    {
        IEnumerable<CauHoiThuongGap> GetAllById(string Id);
    }

    class CauHoiThuongGapRepository : RepositoryBase<CauHoiThuongGap>, ICauHoiThuongGapRepository
    {
        public CauHoiThuongGapRepository(IDbFactory dbFactory) : base(dbFactory)
        {
        }

        public IEnumerable<CauHoiThuongGap> GetAllById(string Id)
        {
            var query = from p in DbContext.CauHoiThuongGaps


                        where p.MaSoCauHoi.Equals(Id)
                        
                        select p;

            //totalRow = query.Count();

            //query = query.Skip((pageIndex - 1) * pageSize).Take(pageSize);

            return query;
        }
    }
}
