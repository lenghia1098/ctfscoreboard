﻿
using Platform.Data.Infrastructure;
using Platform.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Platform.Data.Repositories
{

    public interface IDangKyRepository : IRepository<DangKy>
    {
        IQueryable<string> GetMaNhomByMSSV(string mssv, int hocKy);
        IQueryable<DanhSachMonHocTheoMaLopNghanhKhoa> getDKMH(string mssv, string hocKy, string namHoc);
        IQueryable<DanhSachMonHocTheoMaLopNghanhKhoa> xemThongTinMH(string MaMH, string MaNhom, string MaLop);
    }

    class DangKyRepository : RepositoryBase<DangKy>, IDangKyRepository
    {
        public DangKyRepository(IDbFactory dbFactory) : base(dbFactory)
        {
        }

        public IQueryable<DanhSachMonHocTheoMaLopNghanhKhoa> getDKMH(string mssv, string hocKy, string namHoc)
        {
            var query = from p in DbContext.DangKy
                        join A in DbContext.Nhom
                        on p.MaNhom equals A.MaNhom
                        join B in DbContext.MonHocs
                        on A.MaMH equals B.MaMH
                        join C in DbContext.HocPhi
                        on p.MaSoSV equals C.MaSoSV
                        join D in DbContext.ThoiKhoaBieu
                        on A.MaNhom equals D.MaNhom
                        join E in DbContext.SinhVien_Lop
                        on p.MaSoSV equals E.MaSoSV

                        where p.MaSoSV.Equals(mssv) && A.HocKy.Equals(hocKy) && A.NamHoc.Equals(namHoc)&&C.HocKy.Equals(hocKy)&&C.NamHoc.Equals(namHoc)
                        select new DanhSachMonHocTheoMaLopNghanhKhoa
                        {
                            MaMH = B.MaMH,
                            TenMH = B.TenMH,
                            MaNhom = A.MaNhom,
                            SoTinChi = B.SoTinChi,


                            TongSoTienHocPhiHocKy = C.TongSoHocPhi,
                            SoGioThucHanh = B.SoGioThucHanh,

                            Dong = A.Dong,
                            Thu = D.Thu,
                            MaLop=E.MaLop

                        };
            return query;
        }

        public IQueryable<string> GetMaNhomByMSSV(string mssv, int hocKy)
        {


            var query = from A in DbContext.DangKy
                        where A.MaSoSV == mssv
                        select A.MaNhom;
            return query;
        }
        public IQueryable<DanhSachMonHocTheoMaLopNghanhKhoa> xemThongTinMH(string MaMH, string MaNhom, string MaLop)
        {
            var query = from P in DbContext.Lops

                        join D in DbContext.Lop_ChuongTrinhDaoTao
                        on P.MaLop equals D.MaLop
                        join E in DbContext.ChuongTrinhDaoTaos
                        on D.MaCT equals E.MaCT
                        join F in DbContext.ChuongTrinhDaoTao_MonHoc
                        on E.MaCT equals F.MaCT
                        join p in DbContext.MonHocs
                        on F.MaMH equals p.MaMH
                        join A in DbContext.Nhom
                        on p.MaMH equals A.MaMH
                        join B in DbContext.ThoiKhoaBieu
                        on A.MaNhom equals B.MaNhom
                        join C in DbContext.PhongHoc
                        on B.MaPH equals C.MaPH
                        join G in DbContext.DangKy
                        on A.MaNhom equals G.MaNhom
                        join H in DbContext.SinhViens
                        on G.MaSoSV equals H.MaSoSV
                        join K in DbContext.GiangDay
                        on A.MaNhom equals K.MaNhom
                        join L in DbContext.VienChucs
                        on K.MaSoVC equals L.MaSoVC
                        where A.MaMH.Equals(MaMH) && A.MaNhom.Equals(MaNhom) && P.MaLop.Equals(MaLop)
                        select new DanhSachMonHocTheoMaLopNghanhKhoa()
                        {
                            MaMH = p.MaMH,
                            TenMH = p.TenMH,
                            SoTinChi = p.SoTinChi,

                            TenPH = C.TenPH,
                            MaNhom = A.MaNhom,

                            SoGioThucHanh = p.SoGioThucHanh,
                            SoTiet = B.SoTiet,


                            Thu = B.Thu,
                            MaLop = P.MaLop,
                            NgayBatDau = A.NgayBatDau,
                            NgayKetThuc = A.NgayKetThuc,
                            MaSoSV = H.MaSoSV,
                            HoLot = H.HoLot,
                            Ten = H.Ten,
                            HoTen = L.HoTen,
                            Tiet = B.Tiet

                        };
            return query;
        }
    }
}
