﻿
using Platform.Data.Infrastructure;
using Platform.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Platform.Data.Repositories
{

    public interface IMonHocRepository : IRepository<MonHoc>
    {
        IQueryable<ChucNangHocPhi> GetHocPhi(string mssv, string hocKy);
        IQueryable<DanhSachMonHocTheoMaLopNghanhKhoa> getThongTinMonHoc(string tenMH, string hocKy, string namhoc, string mssv, DateTime ngayDangKy);
        IQueryable<DanhSachMonHocTheoMaLopNghanhKhoa> getDSMHTheoLop(string tenLop, string hocKy, string namhoc, string mssv, DateTime ngayDangKy);
        IQueryable<DanhSachMonHocTheoMaLopNghanhKhoa> getDSMHTheoNganh(string tenNganh, string hocKy, string namhoc, string mssv, DateTime ngayDangKy);
        IQueryable<DanhSachMonHocTheoMaLopNghanhKhoa> getDSMHTheoKhoa(string tenKhoa, string hocKy, string namhoc, string mssv, DateTime ngayDangKy);
        IQueryable<DanhSachMonHocTheoMaLopNghanhKhoa> getAllMonHoc(string hocKy,string namhoc);
        IQueryable<DanhSachMonHocTheoMaLopNghanhKhoa> getMonHocTheoMSSV(string mssv,string hocKy, string namhoc);

        //IQueryable<MonHoc> getDanhSachLopNghanhKhoa();
    }

    class MonHocRepository : RepositoryBase<MonHoc>, IMonHocRepository
    {
        public MonHocRepository(IDbFactory dbFactory) : base(dbFactory)
        {
        }

        public IQueryable<ChucNangHocPhi> GetHocPhi(string mssv, string hocKy)
        {
            var query = from A in DbContext.DangKy
                        join B in DbContext.Nhom
                        on A.MaNhom equals B.MaNhom
                        join C in DbContext.MonHocs
                        on B.MaMH equals C.MaMH
                        where A.MaSoSV.Equals(mssv) && B.HocKy.Equals(hocKy)
                        select new ChucNangHocPhi()
                        {
                            SoTinChi = C.SoTinChi,
                        };


            return query ;
        }
        public IQueryable<DanhSachMonHocTheoMaLopNghanhKhoa> getDSMHTheoLop(string tenLop,  string hocKy,string namhoc, string mssv, DateTime ngayDangKy)
        {

            var query = from p in DbContext.Lops
                        join A in DbContext.Lop_ChuongTrinhDaoTao
                        on p.MaLop equals A.MaLop
                        join B in DbContext.ChuongTrinhDaoTaos
                        on A.MaCT equals B.MaCT
                        join C in DbContext.ChuongTrinhDaoTao_MonHoc
                        on B.MaCT equals C.MaCT
                        join D in DbContext.MonHocs
                        on C.MaMH equals D.MaMH
                        join E in DbContext.Nhom_TamThoi
                        on D.MaMH equals E.MaMH
                        join F in DbContext.ThoiKhoaBieu_TamThoi
                        on E.MaNhom equals F.MaNhom
                        join G in DbContext.PhongHoc 
                        on F.MaPH equals G.MaPH
                        where p.TenLop.Equals(tenLop) && E.HocKy.Equals(hocKy) && E.NamHoc.Equals(namhoc)
                        select new DanhSachMonHocTheoMaLopNghanhKhoa
                        {
                            MaMH = D.MaMH,
                            TenMH = D.TenMH,
                            SoTinChi = D.SoTinChi,
                            MaLop = p.MaLop,
                           TenPH=G.TenPH,
                           MaNhom=E.MaNhom,
                           TongSoTienHocPhiHocKy=E.TongSoTienHocPhiHocKy,
                           SoGioThucHanh=D.SoGioThucHanh,
                           SoTiet=F.SoTiet,
                           NgayBatDau=E.NgayBatDau,
                           NgayKetThuc=E.NgayKetThuc,
                           Dong=E.Dong,
                           Thu=F.Thu,
                          MaSoSV=mssv,
                          NgayDangKy=ngayDangKy,
                            Tiet = F.Tiet



                        };
            return query;



        }
        public IQueryable<DanhSachMonHocTheoMaLopNghanhKhoa> getThongTinMonHoc(string tenMH, string hocKy, string namhoc, string mssv, DateTime ngayDangKy)

        {
            var query = from  C in DbContext.Lops
                        join D in DbContext.Lop_ChuongTrinhDaoTao
                        on C.MaLop equals D.MaLop
                        join E in DbContext.ChuongTrinhDaoTaos
                        on D.MaCT equals E.MaCT
                        join F in DbContext.ChuongTrinhDaoTao_MonHoc
                        on E.MaCT equals F.MaCT
                        join G in DbContext.MonHocs
                        on F.MaMH equals G.MaMH
                        join H in DbContext.Nhom_TamThoi
                        on G.MaMH equals H.MaMH
                        join I in DbContext.ThoiKhoaBieu_TamThoi
                        on H.MaNhom equals I.MaNhom
                        join J in DbContext.PhongHoc
                        on I.MaPH equals J.MaPH
                        where (G.TenMH.Contains(tenMH) ||G.MaMH.Contains(tenMH)) && H.HocKy.Equals(hocKy) && H.NamHoc.Equals(namhoc)
                        select new DanhSachMonHocTheoMaLopNghanhKhoa
                        {
                            MaMH = G.MaMH,
                            TenMH = G.TenMH,
                            SoTinChi = G.SoTinChi,
                            MaLop = C.MaLop,
                            TenPH = J.TenPH,
                            MaNhom = H.MaNhom,
                            TongSoTienHocPhiHocKy = H.TongSoTienHocPhiHocKy,
                            SoGioThucHanh = G.SoGioThucHanh,
                            SoTiet = I.SoTiet,
                            NgayBatDau = H.NgayBatDau,
                            NgayKetThuc = H.NgayKetThuc,
                            Dong = H.Dong,
                            Thu = I.Thu,
                            MaSoSV = mssv,
                            NgayDangKy = ngayDangKy,
                            Tiet = I.Tiet



                        };
            return query;
        }

        public IQueryable<DanhSachMonHocTheoMaLopNghanhKhoa> getDSMHTheoNganh(string tenNganh, string hocKy, string namhoc, string mssv, DateTime ngayDangKy)
        {
            var query = from p in DbContext.Nganhs
                        join P in  DbContext.Lops
                        on p.MaNganh equals P.MaNganh
                        join A in DbContext.Lop_ChuongTrinhDaoTao
                        on P.MaLop equals A.MaLop
                        join B in DbContext.ChuongTrinhDaoTaos
                        on A.MaCT equals B.MaCT
                        join C in DbContext.ChuongTrinhDaoTao_MonHoc
                        on B.MaCT equals C.MaCT
                        join D in DbContext.MonHocs
                        on C.MaMH equals D.MaMH
                        join E in DbContext.Nhom_TamThoi
                        on D.MaMH equals E.MaMH
                        join F in DbContext.ThoiKhoaBieu_TamThoi
                        on E.MaNhom equals F.MaNhom
                        join G in DbContext.PhongHoc
                        on F.MaPH equals G.MaPH
                        where p.TenNganh.Equals(tenNganh) && E.HocKy.Equals(hocKy) && E.NamHoc.Equals(namhoc)
                        select new DanhSachMonHocTheoMaLopNghanhKhoa
                        {
                            MaMH = D.MaMH,
                            TenMH = D.TenMH,
                            SoTinChi = D.SoTinChi,
                            MaLop = P.MaLop,
                            TenPH = G.TenPH,
                            MaNhom = E.MaNhom,
                            TongSoTienHocPhiHocKy = E.TongSoTienHocPhiHocKy,
                            SoGioThucHanh = D.SoGioThucHanh,
                            SoTiet = F.SoTiet,
                            NgayBatDau = E.NgayBatDau,
                            NgayKetThuc = E.NgayKetThuc,
                            Dong = E.Dong,
                            Thu = F.Thu,
                            MaSoSV = mssv,
                            NgayDangKy = ngayDangKy,
                            Tiet = F.Tiet



                        };
            return query;
        }

        public IQueryable<DanhSachMonHocTheoMaLopNghanhKhoa> getDSMHTheoKhoa(string tenKhoa, string hocKy, string namhoc, string mssv, DateTime ngayDangKy)
        {
            var query = from a in DbContext.PhongKhoas
                       join p in DbContext.Nganhs
                       on a.MaPK equals p.MaPK
                        join P in DbContext.Lops
                        on p.MaNganh equals P.MaNganh
                        join A in DbContext.Lop_ChuongTrinhDaoTao
                        on P.MaLop equals A.MaLop
                        join B in DbContext.ChuongTrinhDaoTaos
                        on A.MaCT equals B.MaCT
                        join C in DbContext.ChuongTrinhDaoTao_MonHoc
                        on B.MaCT equals C.MaCT
                        join D in DbContext.MonHocs
                        on C.MaMH equals D.MaMH
                        join E in DbContext.Nhom_TamThoi
                        on D.MaMH equals E.MaMH
                        join F in DbContext.ThoiKhoaBieu_TamThoi
                        on E.MaNhom equals F.MaNhom
                        join G in DbContext.PhongHoc
                        on F.MaPH equals G.MaPH
                        where a.TenPhongKhoa.Equals(tenKhoa) && E.HocKy.Equals(hocKy) && E.NamHoc.Equals(namhoc)
                        select new DanhSachMonHocTheoMaLopNghanhKhoa
                        {
                            MaMH = D.MaMH,
                            TenMH = D.TenMH,
                            SoTinChi = D.SoTinChi,
                            MaLop = P.MaLop,
                            TenPH = G.TenPH,
                            MaNhom = E.MaNhom,
                            TongSoTienHocPhiHocKy = E.TongSoTienHocPhiHocKy,
                            SoGioThucHanh = D.SoGioThucHanh,
                            SoTiet = F.SoTiet,
                            NgayBatDau = E.NgayBatDau,
                            NgayKetThuc = E.NgayKetThuc,
                            Dong = E.Dong,
                            Thu = F.Thu,
                            MaSoSV = mssv,
                            NgayDangKy = ngayDangKy,
                            Tiet = F.Tiet



                        };
            return query;
        }

        public IQueryable<DanhSachMonHocTheoMaLopNghanhKhoa> getAllMonHoc(string hocKy,string namhoc)
        {
            var query = from
                         P in DbContext.Lops

                        join D in DbContext.Lop_ChuongTrinhDaoTao
                        on P.MaLop equals D.MaLop
                        join E in DbContext.ChuongTrinhDaoTaos
                        on D.MaCT equals E.MaCT
                        join F in DbContext.ChuongTrinhDaoTao_MonHoc
                        on E.MaCT equals F.MaCT
                       join p in DbContext.MonHocs
                       on F.MaMH equals p.MaMH
                        join A in DbContext.Nhom
                        on p.MaMH equals A.MaMH
                        join B in DbContext.ThoiKhoaBieu
                        on A.MaNhom equals B.MaNhom
                        join C in DbContext.PhongHoc
                        on B.MaPH equals C.MaPH
                        where A.HocKy.Equals(hocKy) && A.NamHoc.Equals(namhoc) 
                        select new DanhSachMonHocTheoMaLopNghanhKhoa
                        {
                            MaMH = p.MaMH,
                            TenMH = p.TenMH,
                            SoTinChi = p.SoTinChi,
                            
                           
                            TenPH = C.TenPH,
                            MaNhom = A.MaNhom,
                           
                            SoGioThucHanh = p.SoGioThucHanh,
                            SoTiet =B.SoTiet,
                           
                          
                            Thu = B.Thu,
                            //MaLop = P.MaLop,
                            NgayBatDau=A.NgayBatDau,
                            NgayKetThuc=A.NgayKetThuc,
                            MaLop=P.MaLop,
                            
                            
                        };
            return query;
        }

        public IQueryable<DanhSachMonHocTheoMaLopNghanhKhoa> getMonHocTheoMSSV(string mssv, string hocKy, string namhoc)
        {
            var query = from A in DbContext.DangKy
                        join B in DbContext.Nhom
                        on A.MaNhom equals B.MaNhom
                        join C in DbContext.ThoiKhoaBieu
                        on B.MaNhom equals C.MaNhom
                        join D in DbContext.PhongHoc
                        on C.MaPH equals D.MaPH
                        join E in DbContext.MonHocs 
                        on B.MaMH equals E.MaMH
                        join F in DbContext.ChuongTrinhDaoTao_MonHoc
                        on E.MaMH equals F.MaMH
                        join G in DbContext.ChuongTrinhDaoTaos 
                        on F.MaCT equals G.MaCT
                        join H in DbContext.Lop_ChuongTrinhDaoTao
                        on G.MaCT equals H.MaCT
                        join I in DbContext.Lops
                        on H.MaLop equals I.MaLop
                        where A.MaSoSV.Equals(mssv) && B.HocKy.Equals(hocKy)&&B.NamHoc.Equals(namhoc)
                        select new DanhSachMonHocTheoMaLopNghanhKhoa()
                        {
                            MaMH = E.MaMH,
                            TenMH = E.TenMH,
                            SoTinChi = E.SoTinChi,


                            TenPH = D.TenPH,
                            MaNhom =B.MaNhom,

                            SoGioThucHanh = E.SoGioThucHanh,
                            SoTiet = C.SoTiet,


                            Thu = C.Thu,
                            MaLop = I.MaLop,
                            NgayBatDau = B.NgayBatDau,
                            NgayKetThuc = B.NgayKetThuc,
                            Tiet = C.Tiet,


                        };
            return query;
        }
    }
}