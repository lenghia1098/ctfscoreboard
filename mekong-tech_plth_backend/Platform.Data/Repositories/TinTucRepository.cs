﻿
using Platform.Data.Infrastructure;
using Platform.Model;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Platform.Data.Repositories
{

    public interface ITinTucRepository : IRepository<TinTuc>
    {
        IEnumerable<TinTuc> GetTinTuc(String mssv);
        IQueryable<TinTuc> ChiTietTT(int MaTinTuc);

    }


    public class TinTucRepository : RepositoryBase<TinTuc>, ITinTucRepository
    {
        public TinTucRepository(IDbFactory dbFactory) : base(dbFactory)
        {

        }

        public IQueryable<TinTuc> ChiTietTT(int MaTinTuc)
        {
            var query = from A in DbContext.TinTuc
                        where A.MaTinTuc == MaTinTuc
                        select A;
            return query;

        }

        public IEnumerable<TinTuc> GetTinTuc(string mssv)
        {
            IEnumerable<TinTuc> query = from p in DbContext.TinTuc
                                         orderby p.Ngay descending
                                         select p;

           

            return query;
        }
    }
}

