﻿using Microsoft.AspNet.Identity.EntityFramework;
using Platform.Model;
using Platform.Model.Models;
using System.Data.Entity;


namespace Platform.Data
{
    public class QLTHDbContext : IdentityDbContext<ApplicationUser>
    {
       public QLTHDbContext() : base("PlatformTH")
        {
            this.Configuration.LazyLoadingEnabled = false;
        }
        public DbSet<ApplicationGroup> ApplicationGroups { set; get; }
        public DbSet<ApplicationRole> ApplicationRoles { set; get; }
        public DbSet<ApplicationRoleGroup> ApplicationRoleGroups { set; get; }
        public DbSet<ApplicationUserGroup> ApplicationUserGroups { set; get; }
        public  DbSet<CauHoiThuongGap> CauHoiThuongGaps { get; set; }
        public  DbSet<ChuongTrinhDaoTao> ChuongTrinhDaoTaos { get; set; }
        public  DbSet<ChuongTrinhDaoTao_MonHoc> ChuongTrinhDaoTao_MonHoc { get; set; }
        public  DbSet<Don_SinhVien> Don_SinhVien { get; set; }
        public  DbSet<Don_VienChuc> Don_VienChuc { get; set; }
        public  DbSet<DonSinhVien> DonSinhViens { get; set; }
        public  DbSet<DonVienChuc> DonVienChucs { get; set; }
        public  DbSet<Lop> Lops { get; set; }
        public  DbSet<Lop_ChuongTrinhDaoTao> Lop_ChuongTrinhDaoTao { get; set; }
        public  DbSet<MonHoc> MonHocs { get; set; }
        public  DbSet<Nganh> Nganhs { get; set; }
        public  DbSet<NhacNho> NhacNhos { get; set; }
        public  DbSet<PhongKhoa> PhongKhoas { get; set; }
        public  DbSet<PhongKhoa_VienChuc> PhongKhoa_VienChuc { get; set; }
        public  DbSet<SinhVien> SinhViens { get; set; }
        public  DbSet<SinhVien_ViecPhaiLam> SinhVien_ViecPhaiLam { get; set; }
        public  DbSet<ThongBao> ThongBaos { get; set; }
        public  DbSet<Truong> Truongs { get; set; }
        public  DbSet<ViecPhaiLamSinhVien> ViecPhaiLamSinhViens { get; set; }
        public  DbSet<ViecPhaiLamVienChuc> ViecPhaiLamVienChucs { get; set; }
        public  DbSet<VienChuc> VienChucs { get; set; }
        public  DbSet<VienChuc_ViecPhaiLam> VienChuc_ViecPhaiLam { get; set; }
        public  DbSet<Loi> Lois { get; set; }
        public DbSet<NhacNho_SinhVien> NhacNho_SinhVien { get; set; }
        public DbSet<DangKy> DangKy { get; set; }
        public DbSet<Nhom> Nhom { get; set; }
        public DbSet<KetQua> KetQuas { get; set; }
        public DbSet<ThoiKhoaBieu> ThoiKhoaBieu { get; set; }
        public DbSet<ThoiHanDangKyMonHoc> ThoiHanDangKyMonHoc { get; set; }
        public DbSet<QuaTrinhDaoTao> QuaTrinhDaoTao { get; set; }
        public DbSet<ThoiGianCongTac> ThoiGianCongTac { get; set; }
        public DbSet<TrinhDoNgoaiNgu> TrinhDoNgoaiNgu { get; set; }
        public DbSet<ApplicationUserRoleNew> ApplicationUserRoleNew { get; set; }

        public virtual DbSet<LichThi> LichThi { get; set; }
        public virtual DbSet<Thi> Thi { get; set; }
        public virtual DbSet<PhongHoc> PhongHoc { get; set; }
        public virtual DbSet<GiangDay> GiangDay { get; set; }
        public virtual DbSet<HocPhi> HocPhi { get; set; }
        public virtual DbSet<SinhVien_Lop> SinhVien_Lop { get; set; }
        public virtual DbSet<CongViecDeXuat> CongViecDeXuat { get; set; }
        public virtual DbSet<Nhom_TamThoi> Nhom_TamThoi { get; set; }
        public virtual DbSet<ThoiKhoaBieu_TamThoi> ThoiKhoaBieu_TamThoi { get; set; }
        public virtual DbSet<HocKyHienTai> HocKyHienTai { get; set; }
        public virtual DbSet<LichGacThi> LichGacThi { get; set; }
        public virtual DbSet<KhungGio> KhungGios { get; set; }
        public virtual DbSet<Flats> Flats { get; set; }
        public virtual DbSet<TeamInfo> TeamInfos { get; set; }
        public DbSet<ServiceCTF> serviceCTFs { get; set; }



        public virtual DbSet<NhacNho_VienChuc> NhacNho_VienChuc { get; set; }
        public virtual DbSet<TinTuc> TinTuc { get; set; }
        public virtual DbSet<DangKy_TamThoi> DangKy_TamThoi { get; set; }
        public virtual DbSet<NgayLe> NgayLe { get; set; }
        public virtual DbSet<NgayLeThayDoi> NgayLeThayDoi { get; set; }
        public virtual DbSet<SinhVienDauVao> SinhVienDauVao { get; set; }
        public DbSet<SinhVienDauVao_MSSV> sinhVienDauVao_MSSVs { get; set; }




        protected override void OnModelCreating(DbModelBuilder builder)
        {
            builder.Entity<IdentityUserRole>().HasKey(i => new { i.UserId, i.RoleId }).ToTable("ApplicationUserRoles");
            builder.Entity<IdentityUserLogin>().HasKey(i => i.UserId).ToTable("ApplicationUserLogins");
            builder.Entity<IdentityRole>().ToTable("ApplicationRoles");
            builder.Entity<IdentityUserClaim>().HasKey(i => i.UserId).ToTable("ApplicationUserClaims");
        }
        public static QLTHDbContext Create()
        {
            return new QLTHDbContext();
        }
    }
}
