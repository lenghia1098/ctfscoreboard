//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Platform.Model
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    [Table("Nhom_TamThoi")]

    public partial class Nhom_TamThoi
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Nhom_TamThoi()
        {
            this.DangKy_TamThoi = new HashSet<DangKy_TamThoi>();
            this.GiangDay_TamThoi = new HashSet<GiangDay_TamThoi>();
            this.ThoiKhoaBieu_TamThoi = new HashSet<ThoiKhoaBieu_TamThoi>();
        }
        [Key]
        public string MaNhom { get; set; }
        public string MaMH { get; set; }
        public string HocKy { get; set; }
        public string NamHoc { get; set; }
        public Nullable<System.DateTime> NgayBatDau { get; set; }
        public Nullable<System.DateTime> NgayKetThuc { get; set; }
        public Nullable<short> SoSinhVienToiDa { get; set; }
        public Nullable<short> SoSinhVienToiThieu { get; set; }
        public Nullable<bool> Dong { get; set; }
        public Nullable<int> TongSoTienHocPhiHocKy { get; set; }
        public Nullable<bool> DaXoa { get; set; }
        public string NguoiXoa { get; set; }
        public Nullable<System.DateTime> ThoiGianXoa { get; set; }
    
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<DangKy_TamThoi> DangKy_TamThoi { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<GiangDay_TamThoi> GiangDay_TamThoi { get; set; }
        public virtual MonHoc MonHoc { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ThoiKhoaBieu_TamThoi> ThoiKhoaBieu_TamThoi { get; set; }
    }
}
