﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Platform.Model
{
   public class LayBangDiemTheoHocKy
    {
        public string MaMH { get; set; }
        public string TenMH { get; set; }
        public Nullable<byte> SoTinChi { get; set; }
        public Nullable<double> Diem { get; set; }
        public string DiemChu { get; set; }
        public string HocKy { get; set; }
        public string NamHoc { get; set; }
        public Nullable<bool> Dau { get; set; }
        public Nullable<double> DiemChuyenCan { get; set; }
        public Nullable<double> DiemKiemTra { get; set; }
        public Nullable<double> DiemThucHanh { get; set; }
        public Nullable<double> DiemSeminar { get; set; }
        public Nullable<double> DiemThi { get; set; }
        public Nullable<double> TyLeThi { get; set; }
        public Nullable<double> TyLeChuyenCan { get; set; }
        public Nullable<double> TyLeKiemTra { get; set; }
        public Nullable<double> TyLeThucHanh { get; set; }
        public Nullable<double> TyLeSeminar { get; set; }
        public Nullable<double> DiemTongKet { get; set; }

    }
}
