﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Platform.Model
{
   public class chucnangchuongtrinhdaotao
    {
        public string MaCT { get; set; }
        public string TenChuong { get; set; }
        public string MaLop { get; set; }
       
      
        public string MaMH { get; set; }
        public string TenMH { get; set; }
        public Nullable<byte> SoTinChi { get; set; }
        public Nullable<short> SoGioLyThuyet { get; set; }
        public Nullable<short> SoGioThucHanh { get; set; }
        public Nullable<bool> Dau { get; set; }
        //public Nullable<bool> DaHoc { get; set; }
        public string HocKy { get; set; }
        public string NamHoc { get; set; }


    }
}
