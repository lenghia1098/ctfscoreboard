﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Platform.Model
{
   public class ChiTietViecDaLamVienChuc
    {
        public long MaSoCongViec { get; set; }
        public string TenCongViec { get; set; }
        public string MoTa { get; set; }
        public string MaSoSV { get; set; }
       
        public string HoTen { get; set; }
        public string TrangThai { get; set; }
        public Nullable<System.DateTime> NgayHoanThanh { get; set; }
    }
}
