﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Platform.Model
{
    public class ChucNangBangDiem
    {
        public string NamHoc { get; set; }
        public string HocKy { get; set; }
     public  IQueryable<LayBangDiemTheoHocKy> LayBangDiemTheoHocKy { get; set; } 
}
}
