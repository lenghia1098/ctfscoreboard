//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Platform.Model
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    [Table("SinhVien_ViecPhaiLam")]

    public partial class SinhVien_ViecPhaiLam
    {
        [Key]
        [Column(Order = 1)]
        public string MaSoSV { get; set; }
        [Key]
        [Column(Order = 2)]
        public int MaSoCongViec { get; set; }
        [Key]
        [Column(Order = 3)]
        public System.DateTime ThoiHanHoanThanh { get; set; }
        public string TrangThai { get; set; }
        public Nullable<System.DateTime> HanChot { get; set; }
        public Nullable<System.DateTime> NgayHoanThanh { get; set; }
        public Nullable<bool> DaHoanThanh { get; set; }
        public Nullable<bool> DaXoa { get; set; }
        public string NguoiXoa { get; set; }
        public Nullable<System.DateTime> ThoiGianXoa { get; set; }
    
        public virtual SinhVien SinhVien { get; set; }
        public virtual ViecPhaiLamSinhVien ViecPhaiLamSinhVien { get; set; }
    }
}
