﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Platform.Model
{
   public class chitietnhacnho
    {
        public long MaSoNN { get; set; }
        public string MaSoSV { get; set; }
        public System.DateTime NgayNhac { get; set; }
        public Nullable<bool> HoanThanh { get; set; }
        public string HoLot { get; set; }
        public string Ten { get; set; }
        public string NoiDungNhac { get; set; }

    }
}
