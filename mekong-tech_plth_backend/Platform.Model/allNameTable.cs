﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Platform.Model
{
    public class allNameTable
    {
        public long MaSoCauHoi { get; set; }
        public string NoiDungCauHoi { get; set; }
        public string TraLoi { get; set; }
        public Nullable<System.DateTime> NgayTao { get; set; }
        public Nullable<System.DateTime> NgayTraLoi { get; set; }
        public Nullable<bool> DaXoa { get; set; }
        public string NguoiXoa { get; set; }
        public Nullable<System.DateTime> ThoiGianXoa { get; set; }
        public string MaCT { get; set; }
        public string TenChuong { get; set; }
      
     
        public string MaMH { get; set; }
        public string HocKy { get; set; }
      

        public virtual ChuongTrinhDaoTao ChuongTrinhDaoTao { get; set; }
        public virtual MonHoc MonHoc { get; set; }
        public long MaCVDeXuat { get; set; }
        public string TenCVDeXuat { get; set; }
      
        public string MaSoSV { get; set; }
        public string MaNhom { get; set; }
        public Nullable<System.DateTime> NgayDangKy { get; set; }
        public Nullable<bool> DaDongTien { get; set; }
        public Nullable<bool> GiaHan { get; set; }
       

        public virtual Nhom Nhom { get; set; }
        public virtual SinhVien SinhVien { get; set; }
        public int ID { get; set; }
      

    
        public virtual Nhom_TamThoi Nhom_TamThoi { get; set; }
        public long MaDon { get; set; }
   
        public System.DateTime NgayNop { get; set; }
        public Nullable<bool> DaXuLy { get; set; }
       

        public virtual DonSinhVien DonSinhVien { get; set; }
      
        public string MaSoVC { get; set; }
       

        public virtual DonVienChuc DonVienChuc { get; set; }
        public virtual VienChuc VienChuc { get; set; }
      
        public string ChuDe { get; set; }
        public string NoiDung { get; set; }
        public string ChuThich { get; set; }
     
        public string MaPK { get; set; }
        public string GhiChu { get; set; }
      
        public virtual PhongKhoa PhongKhoa { get; set; }
      

        
       

      
        public short Tiet { get; set; }
        public string Gio { get; set; }
      
      
        public string NamHoc { get; set; }
        public Nullable<System.DateTime> NgayBatDauHocKy { get; set; }
        public Nullable<System.DateTime> NgayKetThucHocKy { get; set; }
      
        public Nullable<int> TongSoTinChi { get; set; }
        public Nullable<int> TongSoTinChiHocPhi { get; set; }
        public Nullable<int> TongSoHocPhi { get; set; }
        public Nullable<int> SoTienDaDong { get; set; }
        public Nullable<int> SoTienMienGiam { get; set; }
        public Nullable<int> SoTienConNo { get; set; }
     
        public Nullable<double> DiemChuyenCan { get; set; }
        public Nullable<double> DiemKiemTra { get; set; }
        public Nullable<double> DiemThucHanh { get; set; }
        public Nullable<double> DiemSeminar { get; set; }
        public Nullable<double> DiemThi { get; set; }
        public Nullable<double> DiemTongKet { get; set; }
        public string DiemChu { get; set; }
        public Nullable<System.DateTime> NgayNhap { get; set; }
        public Nullable<System.DateTime> NgayDieuChinh { get; set; }
        public string GiangVienNhap { get; set; }
        public string GiangVienDieuChinh { get; set; }
        public Nullable<bool> Dau { get; set; }
        
        public string GioBatDau { get; set; }
        public string GioKetThuc { get; set; }
        public int id { get; set; }
        public string NguoiThem { get; set; }
        public Nullable<System.DateTime> NgayThem { get; set; }
    
        public string MaSoGiangVien { get; set; }
     
        public string NoiLayDeThi { get; set; }
        public long LichThiID { get; set; }
      
        public string KyHieuPhongThi { get; set; }
        public System.DateTime NgayThi { get; set; }
        public string GioThi { get; set; }
        public string MaPH { get; set; }
       
        public string MaLop { get; set; }
        public string TenLop { get; set; }
        public string NienKhoa { get; set; }
        public string MaNganh { get; set; }
      
        public string HocKyApDung { get; set; }
        public Nullable<System.DateTime> NamApDung { get; set; }
       

      
        public string TenMH { get; set; }
        public Nullable<byte> SoTinChi { get; set; }
        public Nullable<short> SoGioLyThuyet { get; set; }
        public Nullable<short> SoGioThucHanh { get; set; }
        public Nullable<double> TyLeChuyenCan { get; set; }
        public Nullable<double> TyLeKiemTra { get; set; }
        public Nullable<double> TyLeThucHanh { get; set; }
        public Nullable<double> TyLeSeminar { get; set; }
        public Nullable<double> TyLeThi { get; set; }
        public Nullable<double> DiemDau { get; set; }
       
      
        public string TenNganh { get; set; }
        public string MaKhoa { get; set; }
      

       
    }
}
