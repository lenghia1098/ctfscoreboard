﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Platform.Model
{
  public  class getLichThiMonHoc
    {
        public string MaMH { get; set; }
        public string MaNhom { get; set; }
        public string TenMH { get; set; }
        public System.DateTime NgayThi { get; set; }
        public string GioThi { get; set; }
        public string TenPH { get; set; }
        public string TenLop { get; set; }
        public string TenChuong { get; set; }
        public string HocKy { get; set; }
        public string NamHoc { get; set; }
        public Nullable<byte> SoTinChi { get; set; }
        public string MaLop { get; set; }
        public Nullable<short> SoGioThucHanh { get; set; }
        public Nullable<System.DateTime> NgayBatDau { get; set; }
        public Nullable<System.DateTime> NgayKetThuc { get; set; }
        public string TenNganh { get; set; }
        public Nullable<int> SoTiet { get; set; }
        public string HoLot { get; set; }
        public string Ten { get; set; }
        public string HoTen { get; set; }
        public string MaSoSV { get; set; }


    }
}
