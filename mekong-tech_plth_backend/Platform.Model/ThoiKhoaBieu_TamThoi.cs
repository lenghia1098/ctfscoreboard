//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Platform.Model
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    [Table("ThoiKhoaBieu_TamThoi")]

    public partial class ThoiKhoaBieu_TamThoi
    {
        [Key]
       
        public string MaNhom { get; set; }
       
        public short Tiet { get; set; }
       
        public string Thu { get; set; }
        public string MaPH { get; set; }
        public Nullable<int> SoTiet { get; set; }
        public string Tuan { get; set; }
        public Nullable<bool> DaXoa { get; set; }
        public string NguoiXoa { get; set; }
        public Nullable<System.DateTime> ThoiGianXoa { get; set; }
    
        public virtual Nhom_TamThoi Nhom_TamThoi { get; set; }
        public virtual PhongHoc PhongHoc { get; set; }
    }
}
