﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Platform.Model
{
  public  class DanhSachLopTheoMaVC
    {
        public string MaMH { get; set; }
        public string TenMH { get; set; }
        public string MaNhom { get; set; }
        public string TenLop { get; set; }
        public string HocKy { get; set; }

    }
}
