﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Platform.Model.Models
{
    [Table("ApplicationUserRoleNew")]
    public class ApplicationUserRoleNew
    {
        [Key]
        [Column(Order = 1)]
        public string UserId { set; get; }

        [Column(Order = 2)]
        [Key]
        public string RoleId { set; get; }
        public string IdentityRole_Id { set; get; }
        public string ApplicationUser_Id { set; get; }

        [ForeignKey("RoleId")]
        public virtual ApplicationRole ApplicationRole { set; get; }

        [ForeignKey("UserId")]
        public virtual ApplicationUser ApplicationUser { set; get; }
    }
}
