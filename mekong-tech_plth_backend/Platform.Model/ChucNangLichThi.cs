﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Platform.Model
{
   public class ChucNangLichThi
    {
        public string MaSoVC { get; set; }
        public string MaMH { get; set; }
        public string TenMH { get; set; }
        public System.DateTime NgayThi { get; set; }
        public string GioThi { get; set; }
        public string TenPH { get; set; }
        public string TenLop { get; set; }
		public string TenChuong { get; set; }
        public string HocKy { get; set; }
        public string NamHoc { get; set; }
        public string MaNhom { get; set; }
        public string TenNganh { get; set; }
        public string NoiLayDeThi { get; set; }
        public string MaLop { get; set; }

    }
}
