﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Platform.Model
{
  public  class ChucNangTKB_GiaoVien
    {
        public string MaSoVC { get; set; }
        public string MaNhom { get; set; }
        public short Tiet { get; set; }
        public string Thu { get; set; }
        public string TenPH { get; set; }
        public Nullable<int> SoTiet { get; set; }
        public string Tuan { get; set; }
        public string TenMH { get; set; }
    }
}
