﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Platform.Model
{
  public  class chitietdondanop
    {
        public long MaDon { get; set; }
        public string ChuDe { get; set; }
        public string NoiDung { get; set; }
        public string ChuThich { get; set; }
        public string MaSoSV { get; set; }
        public string HoLot { get; set; }
        public string Ten { get; set; }
        public System.DateTime NgayNop { get; set; }
        public Nullable<bool> DaXuLy { get; set; }
    }
}
