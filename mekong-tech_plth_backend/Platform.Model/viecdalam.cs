﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Platform.Model
{
  public  class viecdalam
    {
        public long MaSoCongViec { get; set; }
        public string TenCongViec { get; set; }
        public string MoTa { get; set; }
        public string MaSoSV { get; set; }
        public string HoLot { get; set; }
        public string Ten { get; set; }
        public Nullable<System.DateTime> NgayHoanThanh { get; set; }
        public string TrangThai { get; set; }

       
        public System.DateTime ThoiHanHoanThanh { get; set; }
     
        public Nullable<System.DateTime> HanChot { get; set; }
       
        public Nullable<bool> DaHoanThanh { get; set; }
        public Nullable<bool> DaXoa { get; set; }
        public string NguoiXoa { get; set; }
        public Nullable<System.DateTime> ThoiGianXoa { get; set; }


    }
}
