﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Platform.Model
{
    public class DanhSachMonHocTheoMaLopNghanhKhoa
    {
        public string MaMH { get; set; }
        public string TenMH { get; set; }
        public Nullable<byte> SoTinChi { get; set; }
        public Nullable<double> SoTinChiHocPhi { get; set; }
        public string MaLop { get; set; }
        public Nullable<double> TongSoTienHocPhiHocKy { get; set; }
        public string Thu { get; set; }
        public string TenPH { get; set; }
        public string MaNhom { get; set; }
        public Nullable<short> SoGioThucHanh { get; set; }
        public Nullable<int> SoTiet { get; set; }
        public Nullable<System.DateTime> NgayBatDau { get; set; }
        public Nullable<System.DateTime> NgayKetThuc { get; set; }
        public Nullable<bool> Dong { get; set; }
        public string MaSoSV { get; set; }
        public Nullable<System.DateTime> NgayDangKy { get; set; }
        public string NamHoc { get; set; }
        public string HoLot { get; set; }
        public string Ten { get; set; }
        public string HoTen { get; set; }
        public short Tiet { get; set; }
        public int ID { get; set; }
        public string HocKy { get; set; }

        public Nullable<double> DiemChuyenCan { get; set; }
        public Nullable<double> DiemChuyenCanMoi { get; set; }
        public Nullable<double> DiemKiemTra { get; set; }
        public Nullable<double> DiemKiemTraMoi { get; set; }
        public Nullable<double> DiemThucHanh { get; set; }
        public Nullable<double> DiemThucHanhMoi { get; set; }
        public Nullable<double> DiemSeminar { get; set; }
        public Nullable<double> DiemSeminarMoi { get; set; }
        public Nullable<double> DiemThi { get; set; }
        public Nullable<double> DiemThiMoi { get; set; }
        public Nullable<bool> Dau { get; set; }
        public Nullable<double> DiemTongKet { get; set; }

        public Nullable<System.DateTime> NgayNhapGiuaKi { get; set; }
        public Nullable<System.DateTime> NgayNhapCuoiKi { get; set; }
        public Nullable<System.DateTime> NgayDieuChinhGiuaKi { get; set; }
        public Nullable<System.DateTime> NgayDieuChinhCuoiKi { get; set; }
        public string GiangVienNhapGiuaKi { get; set; }
        public string GiangVienNhapCuoiKi { get; set; }
        public string GiangVienDieuChinhGiuaKi { get; set; }
        public string GiangVienDieuChinhCuoiKi { get; set; }


    }
}
