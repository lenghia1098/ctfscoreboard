﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Platform.Model
{
   public class ChiTietNhacNhoVienChuc
    {
        public long MaSoNN { get; set; }
        public string MaSoVC { get; set; }
        public System.DateTime NgayNhac { get; set; }
       
        public string HoTen { get; set; }
        public string NoiDungNhac { get; set; }
    }
}
