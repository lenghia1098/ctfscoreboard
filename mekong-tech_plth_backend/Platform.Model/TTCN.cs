﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Platform.Model
{
   public class TTCN
    {
        
        public string MaSoSV { get; set; }
        public string HoLot { get; set; }
        public string Ten { get; set; }
        public string Email { get; set; }
        public string NienKhoa { get; set; }
        public string DienThoai { get; set; }
        public string TenNganh { get; set; }
        public string TenPhongKhoa { get; set; }
        public string TenChuong { get; set; }
        public string TenLop { get; set; }
        public string NgaySinh { get; set; }
        public string HoKhau { get; set; }
        public string DiaChi { get; set; }
									
											
        public string HeDaoTao { get; set; }
        public string SoTaiKhoan { get; set; }
        public string TenChuTaiKhoan { get; set; }
        public string ChiNhanh { get; set; }

    }
}
