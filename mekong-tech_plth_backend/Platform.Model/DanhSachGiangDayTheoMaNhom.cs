﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Platform.Model
{
    public class DanhSachGiangDayTheoMaNhom
    {
        public string MaSoSV { get; set; }
        public string HoLot { get; set; }
        public string Ten { get; set; }

        public string TenNganh { get; set; }
        public string TenPhongKhoa { get; set; }
        public string Email { get; set; }
        
    
    }
}
