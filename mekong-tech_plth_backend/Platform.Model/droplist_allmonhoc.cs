﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Platform.Model
{
    public class droplist_allmonhoc
    {
        public string MaNhom { get; set; }
        public string TenMH { get; set; }
        public string MaMH { get; set; }

        public Nullable<byte> SoTinChi { get; set; }
        public Nullable<short> SoGioLyThuyet { get; set; }
        public Nullable<short> SoGioThucHanh { get; set; }
        public string HocKy { get; set; }
        public string NamHoc { get; set; }
    }
}
