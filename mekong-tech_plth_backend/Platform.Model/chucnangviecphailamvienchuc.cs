﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Platform.Model
{
   public class chucnangviecphailamvienchuc
    {
        public long MaSoCongViec { get; set; }
        public string TenCongViec { get; set; }
        public string MoTa { get; set; }
		public string HoTen { get; set; }
        public Nullable<System.DateTime> NgayHoanThanh { get; set; }
        public System.DateTime ThoiHanHoanThanh { get; set; }
    }
}
