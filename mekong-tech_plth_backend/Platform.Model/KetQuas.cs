﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Platform.Model
{
    class KetQuas
    {
        public string MaSoSV { get; set; }
        public string MaNhom { get; set; }
        public Nullable<double> Diem { get; set; }
        public string DiemChu { get; set; }
        public Nullable<System.DateTime> NgayNhap { get; set; }
        public Nullable<System.DateTime> NgayDieuChinh { get; set; }
        public string GiangVienNhap { get; set; }
        public string GiangVienDieuChinh { get; set; }

        public virtual Nhom Nhom { get; set; }
        public virtual SinhVien SinhVien { get; set; }
    }
}
