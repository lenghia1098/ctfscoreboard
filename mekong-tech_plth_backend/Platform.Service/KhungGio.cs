﻿using Platform.Data.Infrastructure;
using Platform.Data.Repositories;
using Platform.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Platform.Service
{
    public interface IKhungGioService
    {
        void Add(KhungGio khungGio);
        void Update(KhungGio khungGio);
        void delete(int id);
        void DeleteAll();
        IEnumerable<KhungGio> GetAll();
        IQueryable<KhungGio> AllKhungGio();
        KhungGio GetByID(int id);
        void Save();
        void Commit();

    }
    public class KhungGioService : IKhungGioService
    {
        IKhungGioRepository _khungGioRepository;
        IUnitOfWork _unitOfWork;
        public KhungGioService(IKhungGioRepository khungGioRepository, IUnitOfWork unitOfWork)
        {
            this._khungGioRepository = khungGioRepository;
            this._unitOfWork = unitOfWork;
        }
        public void Add(KhungGio khungGio)
        {
            _khungGioRepository.Add(khungGio);
        }

        public void Commit()
        {
            _unitOfWork.Commit();
        }

        public void delete(int id)
        {
            _khungGioRepository.Delete(id);
        }

        public IEnumerable<KhungGio> GetAll()
        {
            return _khungGioRepository.GetAll();
        }

        public KhungGio GetByID(int id)
        {
            return _khungGioRepository.GetSingleById(id);
        }



        public void Update(KhungGio khungGio)
        {
            _khungGioRepository.Update(khungGio);
        }
        public void Save()
        {
            _unitOfWork.Commit();
        }

        public void DeleteAll()
        {
             _khungGioRepository.DeleteAll();
        }

        public IQueryable<KhungGio> AllKhungGio()
        {
            return _khungGioRepository.AllKhungGio();
        }
    }
}
