﻿using Platform.Data.Infrastructure;
using Platform.Data.Repositories;
using Platform.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Platform.Service
{
    public interface IDonVienChuc_PhongKhoaService
    {
        void Add(DonVienChuc_PhongKhoa donVienChuc_PhongKhoa);
        void Update(DonVienChuc_PhongKhoa donVienChuc_PhongKhoa);
        void delete(int id);
        IEnumerable<DonVienChuc_PhongKhoa> GetAll();
        DonVienChuc_PhongKhoa GetByID(int id);
        void Save();
        void Commit();

    }
    public class DonVienChuc_PhongKhoaService : IDonVienChuc_PhongKhoaService
    {
        IDonVienChuc_PhongKhoaRepository _donVienChuc_PhongKhoaRepository;
        IUnitOfWork _unitOfWork;
        public DonVienChuc_PhongKhoaService(IDonVienChuc_PhongKhoaRepository donVienChuc_PhongKhoaRepository, IUnitOfWork unitOfWork)
        {
            this._donVienChuc_PhongKhoaRepository = donVienChuc_PhongKhoaRepository;
            this._unitOfWork = unitOfWork;
        }
        public void Save()
        {
            _unitOfWork.Commit();
        }
        public void Add(DonVienChuc_PhongKhoa donVienChuc_PhongKhoa)
        {
            _donVienChuc_PhongKhoaRepository.Add(donVienChuc_PhongKhoa);
        }

        public void Commit()
        {
            _unitOfWork.Commit();
        }

        public void delete(int id)
        {
            _donVienChuc_PhongKhoaRepository.Delete(id);
        }

       

        public IEnumerable<DonVienChuc_PhongKhoa> GetAll()
        {
            return _donVienChuc_PhongKhoaRepository.GetAll();
        }

        public DonVienChuc_PhongKhoa GetByID(int id)
        {
            return _donVienChuc_PhongKhoaRepository.GetSingleById(id);
        }

        public void Update(DonVienChuc_PhongKhoa donVienChuc_PhongKhoa)
        {
            _donVienChuc_PhongKhoaRepository.Update(donVienChuc_PhongKhoa);
        }
    }
}
