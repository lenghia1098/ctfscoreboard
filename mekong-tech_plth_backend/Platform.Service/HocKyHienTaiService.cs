﻿using Platform.Data.Infrastructure;
using Platform.Data.Repositories;
using Platform.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Platform.Service
{
    public interface IHocKyHienTaiService
    {
        void Add(HocKyHienTai hocKyHienTai);
        void Update(HocKyHienTai hocKyHienTai);
        HocKyHienTai delete(int id);
        IEnumerable<HocKyHienTai> GetAll();
        HocKyHienTai getID();
        HocKyHienTai GetByID(int id);
        void Save();
        void Commit();

    }
    public class HocKyHienTaiService : IHocKyHienTaiService
    {
        IHocKyHienTaiRepository _hocKyHienTaiRepository;
        IUnitOfWork _unitOfWork;
        public HocKyHienTaiService(IHocKyHienTaiRepository hocKyHienTaiRepository, IUnitOfWork unitOfWork)
        {
            this._hocKyHienTaiRepository = hocKyHienTaiRepository;
            this._unitOfWork = unitOfWork;
        }
        public void Add(HocKyHienTai hocKyHienTai)
        {
            _hocKyHienTaiRepository.Add(hocKyHienTai);
        }

        public void Commit()
        {
            _unitOfWork.Commit();
        }

        public HocKyHienTai delete(int id)
        {
            return _hocKyHienTaiRepository.Delete(id);
        }

        public IEnumerable<HocKyHienTai> GetAll()
        {
            return _hocKyHienTaiRepository.GetAll();
        }

        public HocKyHienTai GetByID(int id)
        {
            return _hocKyHienTaiRepository.GetSingleById(id);
        }

        public void Update(HocKyHienTai hocKyHienTai)
        {
            _hocKyHienTaiRepository.Update(hocKyHienTai);
        }
        public void Save()
        {
            _unitOfWork.Commit();
        }

        public HocKyHienTai getID()
        {
           return _hocKyHienTaiRepository.getID();
        }
    }
}
