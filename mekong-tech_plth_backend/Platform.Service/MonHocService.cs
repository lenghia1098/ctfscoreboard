﻿using Platform.Data.Infrastructure;
using Platform.Data.Repositories;
using Platform.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Platform.Service
{
    public interface IMonHocService
    {
        void Add(MonHoc monHoc);
        void Update(MonHoc monHoc);
        void delete(int id);
        IEnumerable<MonHoc> GetAll();
        MonHoc GetByID(int id);
        MonHoc GetSingleById(string Id);

        IQueryable<ChucNangHocPhi> GetHocPhi(string mssv, string hocKy);
        IQueryable<DanhSachMonHocTheoMaLopNghanhKhoa> getThongTinMonHoc(string tenMH, string hocKy, string namhoc, string mssv, DateTime ngayDangKy);
        IQueryable<DanhSachMonHocTheoMaLopNghanhKhoa> getDSMHTheoLop(string tenLop, string hocKy, string namhoc, string mssv, DateTime ngayDangKy);
        IQueryable<DanhSachMonHocTheoMaLopNghanhKhoa> getDSMHTheoNganh(string tenNganh, string hocKy, string namhoc, string mssv, DateTime ngayDangKy);
        IQueryable<DanhSachMonHocTheoMaLopNghanhKhoa> getDSMHTheoKhoa(string tenKhoa, string hocKy, string namhoc, string mssv, DateTime ngayDangKy);

        IQueryable<DanhSachMonHocTheoMaLopNghanhKhoa> getAllMonHoc(string hocKy, string namhoc);
        IQueryable<DanhSachMonHocTheoMaLopNghanhKhoa> getMonHocTheoMSSV(string mssv, string hocKy, string namhoc);
        void Commit();
        void Save();

    }
    public class MonHocService : IMonHocService
    {
        IMonHocRepository _monHocRepository;
        IUnitOfWork _unitOfWork;
        public MonHocService(IMonHocRepository monHocRepository, IUnitOfWork unitOfWork)
        {
            this._monHocRepository = monHocRepository;
            this._unitOfWork = unitOfWork;
        }
        public void Add(MonHoc monHoc)
        {
            _monHocRepository.Add(monHoc);
        }
        public void Save()
        {
            _unitOfWork.Commit();
        }
        public void Commit()
        {
            _unitOfWork.Commit();
        }

        public void delete(int id)
        {
            _monHocRepository.Delete(id);
        }

        public IEnumerable<MonHoc> GetAll()
        {
            return _monHocRepository.GetAll();
        }

        public MonHoc GetByID(int id)
        {
            return _monHocRepository.GetSingleById(id);
        }

        public IQueryable<ChucNangHocPhi> GetHocPhi(string mssv, string hocKy)
        {
            return _monHocRepository.GetHocPhi(mssv, hocKy);
        }
        public IQueryable<DanhSachMonHocTheoMaLopNghanhKhoa> getDSMHTheoLop(string tenLop, string hocKy, string namhoc, string mssv, DateTime ngayDangKy)

        {
            return _monHocRepository.getDSMHTheoLop( tenLop,  hocKy,  namhoc,  mssv,  ngayDangKy);
        }

        public IQueryable<DanhSachMonHocTheoMaLopNghanhKhoa> getThongTinMonHoc(string tenMH, string hocKy, string namhoc, string mssv, DateTime ngayDangKy)
        {
            return _monHocRepository.getThongTinMonHoc(tenMH,  hocKy,  namhoc,  mssv,  ngayDangKy);
        }
        public void Update(MonHoc monHoc)
        {
            _monHocRepository.Update(monHoc);
        }

        public IQueryable<DanhSachMonHocTheoMaLopNghanhKhoa> getDSMHTheoNganh(string tenNganh, string hocKy, string namhoc, string mssv, DateTime ngayDangKy)
        {
            return _monHocRepository.getDSMHTheoNganh(tenNganh, hocKy, namhoc, mssv, ngayDangKy);
        }

        public IQueryable<DanhSachMonHocTheoMaLopNghanhKhoa> getDSMHTheoKhoa(string tenKhoa, string hocKy, string namhoc, string mssv, DateTime ngayDangKy)
        {
            return _monHocRepository.getDSMHTheoKhoa(tenKhoa, hocKy, namhoc, mssv, ngayDangKy);
        }

        public IQueryable<DanhSachMonHocTheoMaLopNghanhKhoa> getAllMonHoc(string hocKy, string namhoc)
        {
            return _monHocRepository.getAllMonHoc( hocKy, namhoc);
        }

        public IQueryable<DanhSachMonHocTheoMaLopNghanhKhoa> getMonHocTheoMSSV(string mssv, string hocKy, string namhoc)
        {
            return _monHocRepository.getMonHocTheoMSSV(mssv,hocKy, namhoc);
        }

        public MonHoc GetSingleById(string Id)
        {
            return _monHocRepository.GetSingleById(Id);
        }
    }
}
