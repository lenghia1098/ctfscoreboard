﻿using Platform.Data.Infrastructure;
using Platform.Data.Repositories;
using Platform.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Platform.Service
{
    public interface IDonVienChucService
    {
        void Add(DonVienChuc donVienChuc);
        void Update(DonVienChuc donVienChuc);
        void delete(int id);
        IEnumerable<DonVienChuc> GetAll();
      
        IEnumerable<chucnangdondanopvienchuc> GetChucnangdondanopvienchucs(string msvc);
		IQueryable<ChiTietDonDaNopVienChuc> GetChiTietDonDaNopVienChuc(int MaDon);																		  
        string demdondanopvienchuc(string msvc);
        DonVienChuc GetByID(int id);
        

        void Commit();

    }
    public class DonVienChucService : IDonVienChucService
    {
        IDonVienChucRepository _donVienChucRepository;
        IUnitOfWork _unitOfWork;
        public DonVienChucService(IDonVienChucRepository donVienChucRepository, IUnitOfWork unitOfWork)
        {
            this._donVienChucRepository = donVienChucRepository;
            this._unitOfWork = unitOfWork;
        }
        public void Add(DonVienChuc donVienChuc)
        {
            _donVienChucRepository.Add(donVienChuc);
        }

       

        public void Commit()
        {
            _unitOfWork.Commit();
        }

        public void delete(int id)
        {
            _donVienChucRepository.Delete(id);
        }

        public string demdondanopvienchuc(string msvc)
        {
            return _donVienChucRepository.demdondanopvienchuc(msvc);
        }

        public IEnumerable<DonVienChuc> GetAll()
        {
            return _donVienChucRepository.GetAll();
        }

        public DonVienChuc GetByID(int id)
        {
            return _donVienChucRepository.GetSingleById(id);
        }

		 public IQueryable<ChiTietDonDaNopVienChuc> GetChiTietDonDaNopVienChuc(int MaDon)
        {
            return _donVienChucRepository.GetChiTietDonDaNopVienChuc(MaDon);
        }
		public IEnumerable<chucnangdondanopvienchuc> GetChucnangdondanopvienchucs(string msvc)
        {
            return _donVienChucRepository.GetChucnangdondanopvienchucs(msvc);
        }

       

        public void Update(DonVienChuc donVienChuc)
        {
            _donVienChucRepository.Update(donVienChuc);
        }
    }
}
