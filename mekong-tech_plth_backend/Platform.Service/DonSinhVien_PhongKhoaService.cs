﻿using Platform.Data.Infrastructure;
using Platform.Data.Repositories;
using Platform.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Platform.Service
{
    public interface IDonSinhVien_PhongKhoaService
    {
        void Add(DonSinhVien_PhongKhoa donSinhVien_PhongKhoa);
        void Update(DonSinhVien_PhongKhoa donSinhVien_PhongKhoa);
        void delete(int id);
        IEnumerable<DonSinhVien_PhongKhoa> GetAll();
        DonSinhVien_PhongKhoa GetByID(int id);
        void Save();
        void Commit();

    }
    public class DonSinhVien_PhongKhoaService : IDonSinhVien_PhongKhoaService
    {
        IDonSinhVien_PhongKhoaRepository _donSinhVien_PhongKhoaRepository;
        IUnitOfWork _unitOfWork;
        public DonSinhVien_PhongKhoaService(IDonSinhVien_PhongKhoaRepository donSinhVien_PhongKhoaRepository, IUnitOfWork unitOfWork)
        {
            this._donSinhVien_PhongKhoaRepository = donSinhVien_PhongKhoaRepository;
            this._unitOfWork = unitOfWork;
        }
        public void Save()
        {
            _unitOfWork.Commit();
        }
        public void Add(DonSinhVien_PhongKhoa donSinhVien_PhongKhoa)
        {
            _donSinhVien_PhongKhoaRepository.Add(donSinhVien_PhongKhoa);
        }

        public void Commit()
        {
            _unitOfWork.Commit();
        }

        public void delete(int id)
        {
            _donSinhVien_PhongKhoaRepository.Delete(id);
        }

       

        public IEnumerable<DonSinhVien_PhongKhoa> GetAll()
        {
            return _donSinhVien_PhongKhoaRepository.GetAll();
        }

        public DonSinhVien_PhongKhoa GetByID(int id)
        {
            return _donSinhVien_PhongKhoaRepository.GetSingleById(id);
        }

        public void Update(DonSinhVien_PhongKhoa donSinhVien_PhongKhoa)
        {
            _donSinhVien_PhongKhoaRepository.Update(donSinhVien_PhongKhoa);
        }
    }
}
