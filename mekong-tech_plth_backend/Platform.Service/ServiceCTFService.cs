﻿using Platform.Data.Infrastructure;
using Platform.Data.Repositories;
using Platform.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Platform.Service
{
    public interface IServiceCTFService
    {
        void Add(ServiceCTF serviceCTF);
        void Update(ServiceCTF serviceCTF);
        void delete(int id);
        IEnumerable<ServiceCTF> GetAll();
        ServiceCTF GetByID(int id);
        void Save();
        void deleteall();
        void Commit();

    }
    public class ServiceCTFService : IServiceCTFService
    {
        IServiceCTFRepository _serviceCTFRepository;
        IUnitOfWork _unitOfWork;
        public ServiceCTFService(IServiceCTFRepository serviceCTFRepository, IUnitOfWork unitOfWork)
        {
            this._serviceCTFRepository = serviceCTFRepository;
            this._unitOfWork = unitOfWork;
        }
        public void Save()
        {
            _unitOfWork.Commit();
        }
        public void Add(ServiceCTF serviceCTF)
        {
            _serviceCTFRepository.Add(serviceCTF);
        }

        public void Commit()
        {
            _unitOfWork.Commit();
        }

        public void delete(int id)
        {
            _serviceCTFRepository.Delete(id);
        }

        public IEnumerable<ServiceCTF> GetAll()
        {
            return _serviceCTFRepository.GetAll();
        }

        public ServiceCTF GetByID(int id)
        {
            return _serviceCTFRepository.GetSingleById(id);
        }

     

        public void Update(ServiceCTF serviceCTF)
        {
            _serviceCTFRepository.Update(serviceCTF);
        }

        public void deleteall()
        {
            _serviceCTFRepository.deleteall();
        }
    }
}
