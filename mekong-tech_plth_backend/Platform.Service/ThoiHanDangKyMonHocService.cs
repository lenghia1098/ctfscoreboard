﻿using Platform.Data.Infrastructure;
using Platform.Data.Repositories;
using Platform.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Platform.Service
{
    public interface IThoiHanDangKyMonHocService
    {
        void Add(ThoiHanDangKyMonHoc thoiHanDangKyMonHoc);
        void Update(ThoiHanDangKyMonHoc thoiHanDangKyMonHoc);
        void delete(int id);
        IEnumerable<ThoiHanDangKyMonHoc> GetAll();
        ThoiHanDangKyMonHoc GetByID(int id);
        void Save();
        void Commit();

    }
    public class ThoiHanDangKyMonHocService : IThoiHanDangKyMonHocService
    {
        IThoiHanDangKyMonHocRepository _thoiHanDangKyMonHocRepository;
        IUnitOfWork _unitOfWork;
        public ThoiHanDangKyMonHocService(IThoiHanDangKyMonHocRepository thoiHanDangKyMonHocRepository, IUnitOfWork unitOfWork)
        {
            this._thoiHanDangKyMonHocRepository = thoiHanDangKyMonHocRepository;
            this._unitOfWork = unitOfWork;
        }
        public void Add(ThoiHanDangKyMonHoc thoiHanDangKyMonHoc)
        {
            _thoiHanDangKyMonHocRepository.Add(thoiHanDangKyMonHoc);
        }

        public void Commit()
        {
            _unitOfWork.Commit();
        }

        public void delete(int id)
        {
            _thoiHanDangKyMonHocRepository.Delete(id);
        }

        public IEnumerable<ThoiHanDangKyMonHoc> GetAll()
        {
            return _thoiHanDangKyMonHocRepository.GetAll();
        }

        public ThoiHanDangKyMonHoc GetByID(int id)
        {
            return _thoiHanDangKyMonHocRepository.Delete(id);
        }

        public void Update(ThoiHanDangKyMonHoc thoiHanDangKyMonHoc)
        {
            _thoiHanDangKyMonHocRepository.Update(thoiHanDangKyMonHoc);
        }
        public void Save()
        {
            _unitOfWork.Commit();
        }

        
    }
}
