﻿using Platform.Data.Infrastructure;
using Platform.Data.Repositories;
using Platform.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Platform.Service
{
    public interface ISinhVien_ViecPhaiLamService
    {
        void Add(SinhVien_ViecPhaiLam sinhVien_ViecPhaiLam);
        void Update(SinhVien_ViecPhaiLam sinhVien_ViecPhaiLam);
        SinhVien_ViecPhaiLam delete(int id);
        IEnumerable<SinhVien_ViecPhaiLam> GetAll();
        SinhVien_ViecPhaiLam GetByID(int id);

     SinhVien_ViecPhaiLam getIDViecPhaiLam(int mscv);
        IEnumerable<SinhVien_ViecPhaiLam> getIdAll(string mssv);

        int DemViecPhaiLamSv(string mssv);
           IEnumerable<viecdalam> viecdalam(string mssv);
        void Commit();
        void Save();


    }
    public class SinhVien_ViecPhaiLamService : ISinhVien_ViecPhaiLamService
    {
        ISinhVien_ViecPhaiLamRepository _sinhVien_ViecPhaiLamRepository;
        IUnitOfWork _unitOfWork;
        public SinhVien_ViecPhaiLamService(ISinhVien_ViecPhaiLamRepository sinhVien_ViecPhaiLamRepository, IUnitOfWork unitOfWork)
        {
            this._sinhVien_ViecPhaiLamRepository = sinhVien_ViecPhaiLamRepository;
            this._unitOfWork = unitOfWork;
        }
        public void Add(SinhVien_ViecPhaiLam sinhVien_ViecPhaiLam)
        {
            _sinhVien_ViecPhaiLamRepository.Add(sinhVien_ViecPhaiLam);
        }

        public void Commit()
        {
            _unitOfWork.Commit();
        }
		 public SinhVien_ViecPhaiLam delete(int id)
        {
           return _sinhVien_ViecPhaiLamRepository.Delete(id);
        }

        public void Save()
        {
            _unitOfWork.Commit();
        }
       

        public int DemViecPhaiLamSv(string mssv)
        {
            return _sinhVien_ViecPhaiLamRepository.DemViecPhaiLam(mssv);
        }

        public IEnumerable<SinhVien_ViecPhaiLam> GetAll()
        {
            return _sinhVien_ViecPhaiLamRepository.GetAll();
        }

        public SinhVien_ViecPhaiLam GetByID(int id)
        {
            return _sinhVien_ViecPhaiLamRepository.GetSingleById(id);
        }

        public IEnumerable<SinhVien_ViecPhaiLam> getIdAll(string mssv)
        {
            return _sinhVien_ViecPhaiLamRepository.getIdAll(mssv);
        }

        public SinhVien_ViecPhaiLam getIDViecPhaiLam(int mscv)
        {
            return _sinhVien_ViecPhaiLamRepository.getIDViecPhaiLam(mscv);
        }

        public void Update(SinhVien_ViecPhaiLam sinhVien_ViecPhaiLam)
        {
            _sinhVien_ViecPhaiLamRepository.Update(sinhVien_ViecPhaiLam);
        }

        public IEnumerable<viecdalam> viecdalam(string mssv)
        {
          return  _sinhVien_ViecPhaiLamRepository.viecdalam(mssv);
        }
    }
}