﻿using Platform.Data.Infrastructure;
using Platform.Data.Repositories;
using Platform.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Platform.Service
{
    public interface ILopService
    {
        void Add(Lop lop);
        void Update(Lop lop);
        void delete(int id);
        IEnumerable<Lop> GetAll();
        Lop GetByID(int id);
        void Save();

        void Commit();

    }
    public class LopService : ILopService
    {
        ILopRepository _lopRepository;
        IUnitOfWork _unitOfWork;
        public LopService(ILopRepository lopRepository, IUnitOfWork unitOfWork)
        {
            this._lopRepository = lopRepository;
            this._unitOfWork = unitOfWork;
        }
        public void Save()
        {
            _unitOfWork.Commit();
        }
        public void Add(Lop lop)
        {
            _lopRepository.Add(lop);
        }

        public void Commit()
        {
            _unitOfWork.Commit();
        }

        public void delete(int id)
        {
            _lopRepository.Delete(id);
        }

        public IEnumerable<Lop> GetAll()
        {
            return _lopRepository.GetAll();
        }

        public Lop GetByID(int id)
        {
            return _lopRepository.GetSingleById(id);
        }

        public void Update(Lop lop)
        {
            _lopRepository.Update(lop);
        }
    }
}
