﻿using Platform.Data.Infrastructure;
using Platform.Data.Repositories;
using Platform.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Platform.Service
{
    public interface IThoiKhoaBieuService
    {
        void Add(ThoiKhoaBieu thoiKhoaBieu);
        void Update(ThoiKhoaBieu thoiKhoaBieu);
        void delete(int id);
        IEnumerable<ThoiKhoaBieu> GetAll();
        ThoiKhoaBieu GetByID(int id);
        IQueryable<ChucNangTKB_GiaoVien> getTKB(string MaSoVC, string hocKy, string tuan);
        IQueryable<DanhSachMonHocTheoMaLopNghanhKhoa> xemTKBmonhoc(string tenMH, string hocKy, string namhoc);
        IQueryable<DanhSachMonHocTheoMaLopNghanhKhoa> xemThongTinNhom(string maNhom, string maMH, string malop);
        void Commit();
        void Save();


    }
    public class ThoiKhoaBieuService : IThoiKhoaBieuService
    {
        IThoiKhoaBieuRepository _thoiKhoaBieuRepository;
        IUnitOfWork _unitOfWork;
        public ThoiKhoaBieuService(IThoiKhoaBieuRepository thoiKhoaBieuRepository, IUnitOfWork unitOfWork)
        {
            this._thoiKhoaBieuRepository = thoiKhoaBieuRepository;
            this._unitOfWork = unitOfWork;
        }
        public void Add(ThoiKhoaBieu thoiKhoaBieu)
        {
            _thoiKhoaBieuRepository.Add(thoiKhoaBieu);
        }
        public void Save()
        {
            _unitOfWork.Commit();
        }
        public void Commit()
        {
            _unitOfWork.Commit();
        }

        public void delete(int id)
        {
            _thoiKhoaBieuRepository.Delete(id);
        }

        public IEnumerable<ThoiKhoaBieu> GetAll()
        {
            return _thoiKhoaBieuRepository.GetAll();
        }

        public ThoiKhoaBieu GetByID(int id)
        {
            return _thoiKhoaBieuRepository.GetSingleById(id);
        }

        public IQueryable<ChucNangTKB_GiaoVien> getTKB(string MaSoVC, string hocKy, string tuan)
        {
            return _thoiKhoaBieuRepository.getTKB(MaSoVC,  hocKy,  tuan);
        }

        public void Update(ThoiKhoaBieu thoiKhoaBieu)
        {
            _thoiKhoaBieuRepository.Update(thoiKhoaBieu);
        }

        public IQueryable<DanhSachMonHocTheoMaLopNghanhKhoa> xemThongTinNhom(string maNhom, string maMH, string malop)
        {
            return _thoiKhoaBieuRepository.xemThongTinNhom(maNhom, maMH,  malop);
        }

        public IQueryable<DanhSachMonHocTheoMaLopNghanhKhoa> xemTKBmonhoc(string tenMH, string hocKy, string namhoc)
        {
            return _thoiKhoaBieuRepository.xemTKBmonhoc(tenMH, hocKy, namhoc);
        }
    }
}
