﻿using Platform.Data.Infrastructure;
using Platform.Data.Repositories;
using Platform.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Platform.Service
{
    public interface INgayLeService
    {
        void Add(NgayLe ngayLe);
        void Update(NgayLe ngayLe);
        void delete(int id);
        IEnumerable<NgayLe> GetAll();
        NgayLe GetByID(int id);
        void Commit();
        void Save();

    }
    public class NgayLeService : INgayLeService
    {
        INgayLeRepository _ngayLeRepository;
        IUnitOfWork _unitOfWork;
        public NgayLeService(INgayLeRepository ngayLeRepository, IUnitOfWork unitOfWork)
        {
            this._ngayLeRepository = ngayLeRepository;
            this._unitOfWork = unitOfWork;
        }
        public void Save()
        {
            _unitOfWork.Commit();
        }
        public void Add(NgayLe ngayLe)
        {
            _ngayLeRepository.Add(ngayLe);
        }

        public void Commit()
        {
            _unitOfWork.Commit();
        }

        public void delete(int id)
        {
            _ngayLeRepository.Delete(id);
        }

        public IEnumerable<NgayLe> GetAll()
        {
            return _ngayLeRepository.GetAll();
        }

        public NgayLe GetByID(int id)
        {
            return _ngayLeRepository.GetSingleById(id);
        }

       

        public void Update(NgayLe ngayLe)
        {
            _ngayLeRepository.Update(ngayLe);
        }
    }
}
