﻿using Platform.Data.Infrastructure;
using Platform.Data.Repositories;
using Platform.Model;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Platform.Service
{
    public interface ISinhVienDauVaoService
    {
        void Add(SinhVienDauVao sinhVienDauVao);
        void Update(SinhVienDauVao sinhVienDauVao);
        void delete(int id);
        IEnumerable<SinhVienDauVao> GetAll();
        SinhVienDauVao GetByID(int id);
        IEnumerable<SinhVienDauVao> GetByNamTrungTuyen(string namtrungtuyen);
        IEnumerable<SinhVienDauVao> GetByNamTrungTuyenVaSoBaoDanh(string namtrungtuyen,string sobaodanh);
        List<string> GetListDatabase();
        List<string> GetAllTable();
        IQueryable<SinhVienDauVao_MSSV> getallsv();


        void Save();

        void Commit();

    }
    public class SinhVienDauVaoService : ISinhVienDauVaoService
    {
        ISinhVienDauVaoRepository _sinhVienDauVaoRepository;
        IUnitOfWork _unitOfWork;
        public SinhVienDauVaoService(ISinhVienDauVaoRepository sinhVienDauVaoRepository, IUnitOfWork unitOfWork)
        {
            this._sinhVienDauVaoRepository = sinhVienDauVaoRepository;
            this._unitOfWork = unitOfWork;
        }
        public void Add(SinhVienDauVao sinhVienDauVao)
        {
            _sinhVienDauVaoRepository.Add(sinhVienDauVao);
        }

        public void Commit()
        {
            _unitOfWork.Commit();
        }

        public void delete(int id)
        {
            _sinhVienDauVaoRepository.Delete(id);
        }

        public IEnumerable<SinhVienDauVao> GetAll()
        {
            return _sinhVienDauVaoRepository.GetAll();
        }
        public void Save()
        {
            _unitOfWork.Commit();
        }
        public SinhVienDauVao GetByID(int id)
        {
            return _sinhVienDauVaoRepository.GetSingleById(id);
        }
        public void Update(SinhVienDauVao sinhVienDauVao)
        {
            _sinhVienDauVaoRepository.Update(sinhVienDauVao);
        }
        public IEnumerable<SinhVienDauVao> GetByNamTrungTuyen(string namtrungtuyen)
        {
            return _sinhVienDauVaoRepository.GetMulti(x => x.NamTrungTuyen == namtrungtuyen);
        }

        public IEnumerable<SinhVienDauVao> GetByNamTrungTuyenVaSoBaoDanh(string namtrungtuyen, string sobaodanh)
        {
            return _sinhVienDauVaoRepository.GetMulti(x => x.NamTrungTuyen == namtrungtuyen&&x.SoBaoDanh==sobaodanh );
        }

        public List<string> GetListDatabase()
        {
            return _sinhVienDauVaoRepository.GetListDatabase();

        }

        public List<string> GetAllTable()
        {
            return _sinhVienDauVaoRepository.GetAllTable();
        }

        public IQueryable<SinhVienDauVao_MSSV> getallsv()
        {
            return _sinhVienDauVaoRepository.getallsv();
        }
    }
}
