﻿using Platform.Data.Infrastructure;
using Platform.Data.Repositories;
using Platform.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Platform.Service
{
    public interface INhom_TamThoiService
    {
        void Add(Nhom_TamThoi nhom_TamThoi);
        void Update(Nhom_TamThoi nhom_TamThoi);
        void delete(int id);
        IEnumerable<Nhom_TamThoi> GetAll();
       
        Nhom_TamThoi GetByID(int id);
        void Save();

        void Commit();

    }
    public class Nhom_TamThoiService : INhom_TamThoiService
    {
        INhom_TamThoiRepository _nhom_TamThoiRepository;
        IUnitOfWork _unitOfWork;
        public Nhom_TamThoiService(INhom_TamThoiRepository nhom_TamThoiRepository, IUnitOfWork unitOfWork)
        {
            this._nhom_TamThoiRepository = nhom_TamThoiRepository;
            this._unitOfWork = unitOfWork;
        }
        public void Save()
        {
            _unitOfWork.Commit();
        }
        public void Add(Nhom_TamThoi nhom_TamThoi)
        {
            _nhom_TamThoiRepository.Add(nhom_TamThoi);
        }

        public void Commit()
        {
            _unitOfWork.Commit();
        }

        public void delete(int id)
        {
            _nhom_TamThoiRepository.Delete(id);
        }

        public IEnumerable<Nhom_TamThoi> GetAll()
        {
            return _nhom_TamThoiRepository.GetAll();
        }

        public Nhom_TamThoi GetByID(int id)
        {
            return _nhom_TamThoiRepository.GetSingleById(id);
        }

     

        public void Update(Nhom_TamThoi nhom_TamThoi)
        {
            _nhom_TamThoiRepository.Update(nhom_TamThoi);
        }
    }
}
