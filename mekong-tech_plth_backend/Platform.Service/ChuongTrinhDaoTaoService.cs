﻿using Platform.Data.Infrastructure;
using Platform.Data.Repositories;
using Platform.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Platform.Service
{
    public interface IChuongTrinhDaoTaoService
    {
        void Add(ChuongTrinhDaoTao chuongTrinhDaoTao);
        void Update(ChuongTrinhDaoTao chuongTrinhDaoTao);
        void delete(int id);
        IEnumerable<ChuongTrinhDaoTao> GetAll();
        IQueryable<chucnangchuongtrinhdaotao> GetChucnangchuongtrinhdaotaos(string mssv);
        IQueryable<chucnangchuongtrinhdaotao> getMonDaHoc(string mssv);

        ChuongTrinhDaoTao GetByID(int id);
        void Save();
        void Commit();

    }
    public class ChuongTrinhDaoTaoService : IChuongTrinhDaoTaoService
    {
        IChuongTrinhDaoTaoRepository _chuongTrinhDaoTaoRepository;
         IUnitOfWork _unitOfWork;
    public ChuongTrinhDaoTaoService(IChuongTrinhDaoTaoRepository chuongTrinhDaoTaoRepository, IUnitOfWork unitOfWork)
    {
        this._chuongTrinhDaoTaoRepository = chuongTrinhDaoTaoRepository;
        this._unitOfWork = unitOfWork;
    }
    public void Add(ChuongTrinhDaoTao chuongTrinhDaoTao)
    {
            _chuongTrinhDaoTaoRepository.Add(chuongTrinhDaoTao);
    }

    public void Commit()
    {
        _unitOfWork.Commit();
    }
    public void Save()
    {
        _unitOfWork.Commit();
    }
        public void delete(int id)
    {
            _chuongTrinhDaoTaoRepository.Delete(id);
    }

    public IEnumerable<ChuongTrinhDaoTao> GetAll()
    {
        return _chuongTrinhDaoTaoRepository.GetAll();
    }

    public ChuongTrinhDaoTao GetByID(int id)
    {
        return _chuongTrinhDaoTaoRepository.GetSingleById(id);
    }

        public IQueryable<chucnangchuongtrinhdaotao> GetChucnangchuongtrinhdaotaos(string mssv)
        {
            return _chuongTrinhDaoTaoRepository.GetChucnangchuongtrinhdaotaos(mssv);
        }

        public IQueryable<chucnangchuongtrinhdaotao> getMonDaHoc(string mssv)
        {
            return _chuongTrinhDaoTaoRepository.getMonDaHoc(mssv);
        }

        public void Update(ChuongTrinhDaoTao chuongTrinhDaoTao)
    {
            _chuongTrinhDaoTaoRepository.Update(chuongTrinhDaoTao);
    }
}
}
