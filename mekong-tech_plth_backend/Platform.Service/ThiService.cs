﻿using Platform.Data.Infrastructure;
using Platform.Data.Repositories;
using Platform.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Platform.Service
{
    public interface IThiService
    {
        void Add(Thi thi);
        void Update(Thi thi);
        void delete(int id);
        IEnumerable<Thi> GetAll();
        Thi GetByID(int id);
        void Save();

        void Commit();

    }
    public class ThiService : IThiService
    {
        IThiRepository _thiRepository;
        IUnitOfWork _unitOfWork;
        public ThiService(IThiRepository thiRepository, IUnitOfWork unitOfWork)
        {
            this._thiRepository = thiRepository;
            this._unitOfWork = unitOfWork;
        }
        public void Add(Thi thi)
        {
            _thiRepository.Add(thi);
        }
        public void Save()
        {
            _unitOfWork.Commit();
        }
        public void Commit()
        {
            _unitOfWork.Commit();
        }

        public void delete(int id)
        {
            _thiRepository.Delete(id);
        }

        public IEnumerable<Thi> GetAll()
        {
            return _thiRepository.GetAll();
        }

        public Thi GetByID(int id)
        {
            return _thiRepository.GetSingleById(id);
        }

        public void Update(Thi thi)
        {
            _thiRepository.Update(thi);
        }
    }
}
