﻿using Platform.Data.Infrastructure;
using Platform.Data.Repositories;
using Platform.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Platform.Service
{

    public interface INhacNho_SinhVienService
    {
        void Add(NhacNho_SinhVien nhacNho_SinhVien);
        void Update(NhacNho_SinhVien nhacNho_SinhVien);
        void delete(int id);
        IEnumerable<NhacNho_SinhVien> GetAll();
        NhacNho_SinhVien GetByID(int id);
        IQueryable<string> GetMaNhacNhoSV(string mssv);
        IQueryable<NhacNhoSinhVienChuaHoanThanh> GetNhacNhoChuaHoanThanh(string mssv);

        void Save();


        void Commit();

    }
    public class NhacNho_SinhVienService : INhacNho_SinhVienService
    {
        INhacNho_SinhVienRepository _nhacNho_SinhVienRepository;
        IUnitOfWork _unitOfWork;
        public NhacNho_SinhVienService(INhacNho_SinhVienRepository nhacNho_SinhVienRepository, IUnitOfWork unitOfWork)
        {
            this._nhacNho_SinhVienRepository = nhacNho_SinhVienRepository;
            this._unitOfWork = unitOfWork;
        }
        public void Add(NhacNho_SinhVien nhacNho_SinhVien)
        {
            _nhacNho_SinhVienRepository.Add(nhacNho_SinhVien);
        }
        public void Save()
        {
            _unitOfWork.Commit();
        }
        public void Commit()
        {
            _unitOfWork.Commit();
        }

        public void delete(int id)
        {
            _nhacNho_SinhVienRepository.Delete(id);
        }


        public IEnumerable<NhacNho_SinhVien> GetAll()
        {
            return _nhacNho_SinhVienRepository.GetAll();
        }

        public NhacNho_SinhVien GetByID(int id)
        {
            return _nhacNho_SinhVienRepository.GetSingleById(id);
        }

        public IQueryable<string> GetMaNhacNhoSV(string mssv)
        {
            return _nhacNho_SinhVienRepository.GetMaNhacNhoSV(mssv);
        }

        public IQueryable<NhacNhoSinhVienChuaHoanThanh> GetNhacNhoChuaHoanThanh(string mssv)
        {
            return _nhacNho_SinhVienRepository.GetNhacNhoChuaHoanThanh(mssv);
        }

        public void Update(NhacNho_SinhVien nhacNho_SinhVien)
        {
            _nhacNho_SinhVienRepository.Update(nhacNho_SinhVien);
        }
    }
}
