﻿using Platform.Data.Infrastructure;
using Platform.Data.Repositories;
using Platform.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Platform.Service
{
    public interface IGiangDay_TamThoiService
    {
        void Add(GiangDay_TamThoi giangDay_TamThoi);
        void Update(GiangDay_TamThoi giangDay_TamThoi);
        void delete(int id);
        IEnumerable<GiangDay_TamThoi> GetAll();
        GiangDay_TamThoi GetByID(int id);
      
        void Commit();
        void Save();


    }
    public class GiangDay_TamThoiService : IGiangDay_TamThoiService
    {
        IGiangDay_TamThoiRepository _giangDay_TamThoiRepository;
        IUnitOfWork _unitOfWork;
        public GiangDay_TamThoiService(IGiangDay_TamThoiRepository giangDay_TamThoiRepository, IUnitOfWork unitOfWork)
        {
            this._giangDay_TamThoiRepository = giangDay_TamThoiRepository;
            this._unitOfWork = unitOfWork;
        }
        public void Save()
        {
            _unitOfWork.Commit();
        }
        public void Add(GiangDay_TamThoi giangDay_TamThoi)
        {
            _giangDay_TamThoiRepository.Add(giangDay_TamThoi);
        }

        public void Commit()
        {
            _unitOfWork.Commit();
        }

        public void delete(int id)
        {
            _giangDay_TamThoiRepository.Delete(id);
        }

        public IEnumerable<GiangDay_TamThoi> GetAll()
        {
            return _giangDay_TamThoiRepository.GetAll();
        }

       
          

        public GiangDay_TamThoi GetByID(int id)
        {
            return _giangDay_TamThoiRepository.GetSingleById(id);
        }

       

        public void Update(GiangDay_TamThoi giangDay_TamThoi)
        {
            _giangDay_TamThoiRepository.Update(giangDay_TamThoi);
        }

      
    }
}
