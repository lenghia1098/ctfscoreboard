﻿using Platform.Data.Infrastructure;
using Platform.Data.Repositories;
using Platform.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Platform.Service
{
    public interface ILichGacThiService
    {
        void Add(LichGacThi lichGacThi);
        void Update(LichGacThi lichGacThi);
        void delete(int id);
        IEnumerable<LichGacThi> GetAll();
        LichGacThi GetByID(int id);
        void Save();
        void Commit();

    }
    public class LichGacThiService : ILichGacThiService
    {
        ILichGacThiRepository _lichGacThiRepository;
        IUnitOfWork _unitOfWork;
        public LichGacThiService(ILichGacThiRepository lichGacThiRepository, IUnitOfWork unitOfWork)
        {
            this._lichGacThiRepository = lichGacThiRepository;
            this._unitOfWork = unitOfWork;
        }
        public void Save()
        {
            _unitOfWork.Commit();
        }
        public void Add(LichGacThi lichGacThi)
        {
            _lichGacThiRepository.Add(lichGacThi);
        }

        public void Commit()
        {
            _unitOfWork.Commit();
        }

        public void delete(int id)
        {
            _lichGacThiRepository.Delete(id);
        }

       

        public IEnumerable<LichGacThi> GetAll()
        {
            return _lichGacThiRepository.GetAll();
        }

        public LichGacThi GetByID(int id)
        {
            return _lichGacThiRepository.GetSingleById(id);
        }

        public void Update(LichGacThi lichGacThi)
        {
            _lichGacThiRepository.Update(lichGacThi);
        }
    }
}
