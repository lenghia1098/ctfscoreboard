﻿using Platform.Data.Infrastructure;
using Platform.Data.Repositories;
using Platform.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Platform.Service
{
    public interface ITeamInfoService
    {
        void Add(TeamInfo teamInfo);
        void Update(TeamInfo teamInfo);
        void delete(int id);
        IEnumerable<TeamInfo> GetAll();
       
        TeamInfo GetByID(int id);
        void Save();

        void Commit();

    }
    public class TeamInfoService : ITeamInfoService
    {
        ITeamInfoRepository _teamInfoRepository;
        IUnitOfWork _unitOfWork;
        public TeamInfoService(ITeamInfoRepository teamInfoRepository, IUnitOfWork unitOfWork)
        {
            this._teamInfoRepository = teamInfoRepository;
            this._unitOfWork = unitOfWork;
        }
        public void Save()
        {
            _unitOfWork.Commit();
        }
        public void Add(TeamInfo teamInfo)
        {
            _teamInfoRepository.Add(teamInfo);
        }

        public void Commit()
        {
            _unitOfWork.Commit();
        }

        public void delete(int id)
        {
            _teamInfoRepository.Delete(id);
        }

        public IEnumerable<TeamInfo> GetAll()
        {
            return _teamInfoRepository.GetAll();
        }

        public TeamInfo GetByID(int id)
        {
            return _teamInfoRepository.GetSingleById(id);
        }

     

        public void Update(TeamInfo teamInfo)
        {
            _teamInfoRepository.Update(teamInfo);
        }
    }
}
