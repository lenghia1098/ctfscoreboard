﻿using Platform.Data.Infrastructure;
using Platform.Data.Repositories;
using Platform.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Platform.Service
{
    public interface IHocPhiService
    {
        void Add(HocPhi hocPhi);
        void Update(HocPhi hocPhi);
        void delete(int id);
        IEnumerable<HocPhi> GetAll();
        HocPhi GetByID(int id);
        IEnumerable<HocPhi> GetAllById(string mssv, string hocKy, string namHoc);
        IEnumerable<getHocKyHocPhi> GetHocKy(string mssv);
        void Save();
        void Commit();

    }
    public class HocPhiService : IHocPhiService
    {
        IHocPhiRepository _hocPhiRepository;
        IUnitOfWork _unitOfWork;
        public HocPhiService(IHocPhiRepository hocPhiRepository, IUnitOfWork unitOfWork)
        {
            this._hocPhiRepository = hocPhiRepository;
            this._unitOfWork = unitOfWork;
        }
        public void Add(HocPhi hocPhi)
        {
            _hocPhiRepository.Add(hocPhi);
        }
        public void Save()
        {
            _unitOfWork.Commit();
        }
        public void Commit()
        {
            _unitOfWork.Commit();
        }

        public void delete(int id)
        {
            _hocPhiRepository.Delete(id);
        }

        public IEnumerable<HocPhi> GetAll()
        {
            return _hocPhiRepository.GetAll();
        }

        public IEnumerable<HocPhi> GetAllById(string mssv, string hocKy, string namHoc)
        {
            return _hocPhiRepository.GetAllById(mssv,  hocKy,namHoc);
        }

        public HocPhi GetByID(int id)
        {
            return _hocPhiRepository.GetSingleById(id);
        }

        public IEnumerable<getHocKyHocPhi> GetHocKy(string mssv)
        {
            return _hocPhiRepository.GetHocKy(mssv);
        }

        public void Update(HocPhi hocPhi)
        {
            _hocPhiRepository.Update(hocPhi);
        }
    }
}
