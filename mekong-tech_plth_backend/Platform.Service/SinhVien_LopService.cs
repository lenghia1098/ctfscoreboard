﻿using Platform.Data.Infrastructure;
using Platform.Data.Repositories;
using Platform.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Platform.Service
{
    public interface ISinhVien_LopService
    {
        void Add(SinhVien_Lop sinhVien_Lop);
        void Update(SinhVien_Lop sinhVien_Lop);
        void delete(int id);
        IEnumerable<SinhVien_Lop> GetAll();
    
     
        void Save();

        void Commit();

    }
    public class SinhVien_LopService : ISinhVien_LopService
    {
        ISinhVien_LopRepository _sinhVien_LopRepository;
        IUnitOfWork _unitOfWork;
        public SinhVien_LopService(ISinhVien_LopRepository sinhVien_LopRepository, IUnitOfWork unitOfWork)
        {
            this._sinhVien_LopRepository = sinhVien_LopRepository;
            this._unitOfWork = unitOfWork;
        }
        public void Save()
        {
            _unitOfWork.Commit();
        }
        public void Add(SinhVien_Lop sinhVien_Lop)
        {
            _sinhVien_LopRepository.Add(sinhVien_Lop);
        }

        public void Commit()
        {
            _unitOfWork.Commit();
        }

        public void delete(int id)
        {
            _sinhVien_LopRepository.Delete(id);
        }

        public IEnumerable<SinhVien_Lop> GetAll()
        {
            return _sinhVien_LopRepository.GetAll();
        }

        

        public void Update(SinhVien_Lop sinhVien_Lop)
        {
            _sinhVien_LopRepository.Update(sinhVien_Lop);
        }
    }
}
