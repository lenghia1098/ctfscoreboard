﻿using Platform.Data.Infrastructure;
using Platform.Data.Repositories;
using Platform.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Platform.Service
{
    public interface IKetQuaService
    {
        void Add(KetQua ketQua);
        void Update(KetQua ketQua);
        void delete(int id);
        IEnumerable<KetQua> GetAll();
        KetQua GetByID(int id);
        KetQua GetDetail(string mssv, string manhom);
        IQueryable<KetQua> getbyMaNhom(string manhom);

        List<ChucNangBangDiem> getAllBangDiem(string mssv);
        IQueryable<LayBangDiemTheoHocKy> getBangDiemHocKy(string mssv, string hocKy, string namHoc);
        IQueryable<getHocKy> getHocKy(string mssv);
        IQueryable<DanhSachMonHocTheoMaLopNghanhKhoa> getBangDiemMonHoc(string maNhom, string maMH, string malop, string hocKy, string namHoc);
        IQueryable<DanhSachMonHocTheoMaLopNghanhKhoa> getSuaDiem(string mssv, string manhom);
        void Commit();
        void Save();
    }
    public class KetQuaService : IKetQuaService
    {
        IKetQuaRepository _ketQuaRepository;
        IUnitOfWork _unitOfWork;
        public KetQuaService(IKetQuaRepository ketQuaRepository, IUnitOfWork unitOfWork)
        {
            this._ketQuaRepository = ketQuaRepository;
            this._unitOfWork = unitOfWork;
        }
        public void Add(KetQua ketQua)
        {
            _ketQuaRepository.Add(ketQua);
        }
        public void Save()
        {
            _unitOfWork.Commit();
        }
        public void Commit()
        {
            _unitOfWork.Commit();
        }

        public void delete(int id)
        {
            _ketQuaRepository.Delete(id);
        }

        public IEnumerable<KetQua> GetAll()
        {
            return _ketQuaRepository.GetAll();
        }

        public List<ChucNangBangDiem> getAllBangDiem(string mssv)
        {
            
            IQueryable<getHocKy> HocKy = _ketQuaRepository.getHocKy(mssv);
            List<ChucNangBangDiem> ListBangDiemHocKy= new List<ChucNangBangDiem>();
           foreach(var item in HocKy)
            {


                ChucNangBangDiem query = new ChucNangBangDiem();
                 query.LayBangDiemTheoHocKy= _ketQuaRepository.getBangDiemHocKy(mssv,item.HocKy,item.NamHoc);
                query.HocKy = item.HocKy;
                query.NamHoc = item.NamHoc;
                ListBangDiemHocKy.Add(query);
            }
            return ListBangDiemHocKy;
        }

        public IQueryable<LayBangDiemTheoHocKy> getBangDiemHocKy(string mssv, string hocKy, string namHoc)
        {
            return _ketQuaRepository.getBangDiemHocKy(mssv,hocKy, namHoc);
        }

        public KetQua GetByID(int id)
        {
            return _ketQuaRepository.GetSingleById(id);
        }

        public IQueryable<getHocKy> getHocKy(string mssv)
        {
            return _ketQuaRepository.getHocKy(mssv);
        }

        public void Update(KetQua ketQua)
        {
            _ketQuaRepository.Update(ketQua);
        }

        public IQueryable<DanhSachMonHocTheoMaLopNghanhKhoa> getBangDiemMonHoc(string maNhom, string maMH, string malop, string hocKy, string namHoc)
        {
            return _ketQuaRepository.getBangDiemMonHoc( maNhom,  maMH,  malop,  hocKy,  namHoc);
        }

        public KetQua GetDetail(string mssv, string manhom)
        {
            return _ketQuaRepository.GetSingleByCondition(x => x.MaSoSV == mssv & x.MaNhom==manhom);
        }

       

        public IQueryable<KetQua> getbyMaNhom(string manhom)
        {
            return _ketQuaRepository.getbyMaNhom(manhom);
        }

        public IQueryable<DanhSachMonHocTheoMaLopNghanhKhoa> getSuaDiem(string mssv, string manhom)
        {
            return _ketQuaRepository.getSuaDiem(mssv, manhom);
        }
    }
}
