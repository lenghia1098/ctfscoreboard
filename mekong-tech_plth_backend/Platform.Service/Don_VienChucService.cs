﻿using Platform.Data.Infrastructure;
using Platform.Data.Repositories;
using Platform.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Platform.Service
{
    public interface IDon_VienChucService
    {
        void Add(Don_VienChuc don_VienChuc);
        void Update(Don_VienChuc don_VienChuc);
        void delete(int id);
        IEnumerable<Don_VienChuc> GetAll();
        Don_VienChuc GetByID(int id);

        void Commit();
        Don_VienChuc getIdDonDaNop(int maDon);

    }
    public class Don_VienChucService : IDon_VienChucService
    {
        IDon_Don_VienChucRepository _don_VienChucRepository;
        IUnitOfWork _unitOfWork;
        public Don_VienChucService(IDon_Don_VienChucRepository don_VienChucRepository, IUnitOfWork unitOfWork)
        {
            this._don_VienChucRepository = don_VienChucRepository;
            this._unitOfWork = unitOfWork;
        }
        public void Add(Don_VienChuc don_VienChuc)
        {
            _don_VienChucRepository.Add(don_VienChuc);
        }

        public void Commit()
        {
            _unitOfWork.Commit();
        }

        public void delete(int id)
        {
            _don_VienChucRepository.Delete(id);
        }

        public IEnumerable<Don_VienChuc> GetAll()
        {
            return _don_VienChucRepository.GetAll();
        }

        public Don_VienChuc GetByID(int id)
        {
            return _don_VienChucRepository.GetSingleById(id);
        }

        public Don_VienChuc getIdDonDaNop(int maDon)
        {
            return _don_VienChucRepository.getIdDonDaNop(maDon);
        }

        public void Update(Don_VienChuc don_VienChuc)
        {
            _don_VienChucRepository.Update(don_VienChuc);
        }
    }
}
