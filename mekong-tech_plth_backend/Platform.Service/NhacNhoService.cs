﻿using Platform.Data.Infrastructure;
using Platform.Data.Repositories;
using Platform.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Platform.Service
{
    public interface INhacNhoService
    {
        void Add(NhacNho nhacNho);
        void Update(NhacNho nhacNho);
        void delete(int id);
        IEnumerable<NhacNho> GetAll();
        NhacNho GetByID(int id);
        IQueryable<NhacNho> GetNoiDungNhacNho(string maNN);
		IQueryable<chitietnhacnho> Getchitietnhacnho(int MaSoNN);
        void Save();

        void Commit();

    }
    public class NhacNhoService : INhacNhoService
    {
        INhacNhoRepository _nhacNhoRepository;
        IUnitOfWork _unitOfWork;
        public NhacNhoService(INhacNhoRepository nhacNhoRepository, IUnitOfWork unitOfWork)
        {
            this._nhacNhoRepository = nhacNhoRepository;
            this._unitOfWork = unitOfWork;
        }
        public void Save()
        {
            _unitOfWork.Commit();
        }
        public void Add(NhacNho nhacNho)
        {
            _nhacNhoRepository.Add(nhacNho);
        }

        public void Commit()
        {
            _unitOfWork.Commit();
        }

        public void delete(int id)
        {
            _nhacNhoRepository.Delete(id);
        }

        public IEnumerable<NhacNho> GetAll()
        {
            return _nhacNhoRepository.GetAll();
        }

        public NhacNho GetByID(int id)
        {
            return _nhacNhoRepository.GetSingleById(id);
        }

		public IQueryable<chitietnhacnho> Getchitietnhacnho(int MaSoNN)
        {
            return _nhacNhoRepository.Getchitietnhacnho(MaSoNN);
        }

        public IQueryable<NhacNho> GetNoiDungNhacNho(string maNN)
        {
            return _nhacNhoRepository.GetNoiDungNhacNho(maNN);
        }

        public void Update(NhacNho nhacNho)
        {
            _nhacNhoRepository.Update(nhacNho);
        }
    }
}
