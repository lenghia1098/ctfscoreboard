﻿using Platform.Data.Infrastructure;
using Platform.Data.Repositories;
using Platform.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Platform.Service
{

    public interface INhacNho_VienChucService
    {
        void Add(NhacNho_VienChuc nhacNho_VienChuc);
        void Update(NhacNho_VienChuc nhacNho_VienChuc);
        void delete(int id);
        IEnumerable<NhacNho_VienChuc> GetAll();
        NhacNho_VienChuc GetByID(int id);
        IQueryable<string> GetMaNhacNhoVC(string msvc);
        IQueryable<NhacNhoVienChucChuaHoanThanh> GetNhacNhoChuaHoanThanh(string msvc);
        IQueryable<ChiTietNhacNhoVienChuc> Getchitietnhacnho(int MaSoNN);

        void Save();

        void Commit();

    }
    public class NhacNho_VienChucService : INhacNho_VienChucService
    {
        INhacNho_VienChucRepository _nhacNho_VienChucRepository;
        IUnitOfWork _unitOfWork;
        public NhacNho_VienChucService(INhacNho_VienChucRepository nhacNho_VienChucRepository, IUnitOfWork unitOfWork)
        {
            this._nhacNho_VienChucRepository = nhacNho_VienChucRepository;
            this._unitOfWork = unitOfWork;
        }
        public void Add(NhacNho_VienChuc nhacNho_VienChuc)
        {
            _nhacNho_VienChucRepository.Add(nhacNho_VienChuc);
        }
        public void Save()
        {
            _unitOfWork.Commit();
        }
        public void Commit()
        {
            _unitOfWork.Commit();
        }

        public void delete(int id)
        {
            _nhacNho_VienChucRepository.Delete(id);
        }


        public IEnumerable<NhacNho_VienChuc> GetAll()
        {
            return _nhacNho_VienChucRepository.GetAll();
        }

        public NhacNho_VienChuc GetByID(int id)
        {
            return _nhacNho_VienChucRepository.GetSingleById(id);
        }

        public IQueryable<ChiTietNhacNhoVienChuc> Getchitietnhacnho(int MaSoNN)
        {
            return _nhacNho_VienChucRepository.Getchitietnhacnho(MaSoNN);
        }

        public IQueryable<string> GetMaNhacNhoVC(string msvc)
        {
            return _nhacNho_VienChucRepository.GetMaNhacNhoVC(msvc);
        }

        public IQueryable<NhacNhoVienChucChuaHoanThanh> GetNhacNhoChuaHoanThanh(string mssv)
        {
            return _nhacNho_VienChucRepository.GetNhacNhoChuaHoanThanh(mssv);
        }

        public void Update(NhacNho_VienChuc nhacNho_VienChuc)
        {
            _nhacNho_VienChucRepository.Update(nhacNho_VienChuc);
        }
    }
}
