﻿using Platform.Data.Infrastructure;
using Platform.Data.Repositories;
using Platform.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Platform.Service
{
    public interface IQuaTrinhDaoTaoService
    {
        void Add(QuaTrinhDaoTao quaTrinhDaoTao);
        void Update(QuaTrinhDaoTao quaTrinhDaoTao);
        void delete(int id);
        IEnumerable<QuaTrinhDaoTao> GetAll();
        QuaTrinhDaoTao GetByID(int id);
        void Save();
        IEnumerable<QuaTrinhDaoTao> getQuaTrinhDaoTao(string msvc);
        QuaTrinhDaoTao getid(int ID);
        void Commit();

    }
    public class QuaTrinhDaoTaoService : IQuaTrinhDaoTaoService
    {
        IQuaTrinhDaoTaoRepository _quaTrinhDaoTaoRepository;
        IUnitOfWork _unitOfWork;
        public QuaTrinhDaoTaoService(IQuaTrinhDaoTaoRepository quaTrinhDaoTaoRepository, IUnitOfWork unitOfWork)
        {
            this._quaTrinhDaoTaoRepository = quaTrinhDaoTaoRepository;
            this._unitOfWork = unitOfWork;
        }
        public void Add(QuaTrinhDaoTao quaTrinhDaoTao)
        {
            _quaTrinhDaoTaoRepository.Add(quaTrinhDaoTao);
        }
        public void Save()
        {
            _unitOfWork.Commit();
        }
        public void Commit()
        {
            _unitOfWork.Commit();
        }

        public void delete(int id)
        {
            _quaTrinhDaoTaoRepository.Delete(id);
        }

        public IEnumerable<QuaTrinhDaoTao> GetAll()
        {
            return _quaTrinhDaoTaoRepository.GetAll();
        }

        public QuaTrinhDaoTao GetByID(int id)
        {
            return _quaTrinhDaoTaoRepository.GetSingleById(id);
        }

        public void Update(QuaTrinhDaoTao quaTrinhDaoTao)
        {
            _quaTrinhDaoTaoRepository.Update(quaTrinhDaoTao);
        }

        public IEnumerable<QuaTrinhDaoTao> getQuaTrinhDaoTao(string msvc)
        {
            return _quaTrinhDaoTaoRepository.getQuaTrinhDaoTao(msvc);
        }

        public QuaTrinhDaoTao getid(int ID)
        {
            return _quaTrinhDaoTaoRepository.getid(ID);
        }
    }
}
