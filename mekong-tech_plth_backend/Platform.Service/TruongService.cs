﻿using Platform.Data.Infrastructure;
using Platform.Data.Repositories;
using Platform.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Platform.Service
{
    public interface ITruongService
    {
        void Add(Truong truong);
        void Update(Truong truong);
        void delete(int id);
        IEnumerable<Truong> GetAll();
        Truong GetByID(int id);
        void Save();

        void Commit();

    }
    public class TruongService : ITruongService
    {
        ITruongRepository _truongRepository;
        IUnitOfWork _unitOfWork;
        public TruongService(ITruongRepository truongRepository, IUnitOfWork unitOfWork)
        {
            this._truongRepository = truongRepository;
            this._unitOfWork = unitOfWork;
        }
        public void Add(Truong truong)
        {
            _truongRepository.Add(truong);
        }
        public void Save()
        {
            _unitOfWork.Commit();
        }
        public void Commit()
        {
            _unitOfWork.Commit();
        }

        public void delete(int id)
        {
            _truongRepository.Delete(id);
        }

        public IEnumerable<Truong> GetAll()
        {
            return _truongRepository.GetAll();
        }

        public Truong GetByID(int id)
        {
            return _truongRepository.GetSingleById(id);
        }

        public void Update(Truong truong)
        {
            _truongRepository.Update(truong);
        }
    }
}
