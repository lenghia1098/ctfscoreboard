﻿using Platform.Data.Infrastructure;
using Platform.Data.Repositories;
using Platform.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Platform.Service
{
    public interface ILichThiService
    {
        void Add(LichThi lichThi);
        void Update(LichThi lichThi);
        void delete(int id);
        IEnumerable<LichThi> GetAll();
        IQueryable<ChucNangLichThi> GetLichThi(string mssv, string hocKy,string namhoc);
        IQueryable<ChucNangLichThi> GetLichThiGV(string msvc);

        IQueryable<droplist_allmonhoc> getAllMonHoc(string NamHoc, string hocKy);
        IQueryable<getLichThiMonHoc> getLichThiMH(string tenMH, string hocKy, string namhoc);

        IQueryable<getLichThiMonHoc> getTTMH(string maMH, string maNhom, string malop);
        LichThi GetByID(int id);
        IQueryable<HocKyHienTai> getHocKyHienTai();
        void Save();
        void Commit();

    }
    public class LichThiService : ILichThiService
    {
        ILichThiRepository _lichThiRepository;
        IUnitOfWork _unitOfWork;
        public LichThiService(ILichThiRepository lichThiRepository, IUnitOfWork unitOfWork)
        {
            this._lichThiRepository = lichThiRepository;
            this._unitOfWork = unitOfWork;
        }
        public void Save()
        {
            _unitOfWork.Commit();
        }
        public void Add(LichThi lichThi)
        {
            _lichThiRepository.Add(lichThi);
        }

        public void Commit()
        {
            _unitOfWork.Commit();
        }

        public void delete(int id)
        {
            _lichThiRepository.Delete(id);
        }

        public IEnumerable<LichThi> GetAll()
        {
            return _lichThiRepository.GetAll();
        }

        public IQueryable<droplist_allmonhoc> getAllMonHoc(string NamHoc, string hocKy)
        {
            return _lichThiRepository.getAllMonHoc(NamHoc,hocKy);
        }

        public LichThi GetByID(int id)
        {
            return _lichThiRepository.GetSingleById(id);
        }

        public IQueryable<HocKyHienTai> getHocKyHienTai()
        {
            return _lichThiRepository.getHocKyHienTai();
        }

        public IQueryable<ChucNangLichThi> GetLichThi(string mssv, string hocKy, string namhoc)
        {
            return _lichThiRepository.GetLichThi(mssv, hocKy,  namhoc);
        }

        public IQueryable<ChucNangLichThi> GetLichThiGV(string msvc)
        {
            return _lichThiRepository.GetLichThiGV(msvc);
        }

        public IQueryable<getLichThiMonHoc> getLichThiMH(string tenMH, string hocKy, string namhoc)
        {
            return _lichThiRepository.getLichThiMH(tenMH,hocKy,namhoc);
        }

        public IQueryable<getLichThiMonHoc> getTTMH(string maMH, string maNhom, string malop)
        {
            return _lichThiRepository.getTTMH(maMH, maNhom,  malop);
        }

        public void Update(LichThi lichThi)
        {
            _lichThiRepository.Update(lichThi);
        }
    }
}
