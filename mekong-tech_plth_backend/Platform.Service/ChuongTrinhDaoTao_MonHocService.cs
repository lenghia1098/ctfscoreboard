﻿using Platform.Data.Infrastructure;
using Platform.Data.Repositories;
using Platform.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Platform.Service
{
    public interface IChuongTrinhDaoTao_MonHocService
    {
        void Add(ChuongTrinhDaoTao_MonHoc chuongTrinhDaoTao_MonHoc);
        void Update(ChuongTrinhDaoTao_MonHoc chuongTrinhDaoTao_MonHoc);
        void delete(int id);
        void deleteById(string Id);
        IEnumerable<ChuongTrinhDaoTao_MonHoc> GetAll();
        ChuongTrinhDaoTao_MonHoc GetByID(int id);
        IEnumerable<ChuongTrinhDaoTao_MonHoc> GetAllByMACT(string Id);
        IEnumerable<ChuongTrinhDaoTao_MonHoc> GetAllByKey(string maCT, string maMH);
        void Commit();
        void Save();

    }
    public class ChuongTrinhDaoTao_MonHocService : IChuongTrinhDaoTao_MonHocService
    {
        IChuongTrinhDaoTao_MonHocRepository _chuongTrinhDaoTao_MonHocRepository;
        IUnitOfWork _unitOfWork;
        public ChuongTrinhDaoTao_MonHocService(IChuongTrinhDaoTao_MonHocRepository chuongTrinhDaoTao_MonHocRepository, IUnitOfWork unitOfWork)
        {
            this._chuongTrinhDaoTao_MonHocRepository = chuongTrinhDaoTao_MonHocRepository;
            this._unitOfWork = unitOfWork;
        }
        public void Add(ChuongTrinhDaoTao_MonHoc chuongTrinhDaoTao_MonHoc)
        {
            _chuongTrinhDaoTao_MonHocRepository.Add(chuongTrinhDaoTao_MonHoc);
        }
        public void Save()
        {
            _unitOfWork.Commit();
        }
        public void Commit()
        {
            _unitOfWork.Commit();
        }

        public void delete(int id)
        {
            _chuongTrinhDaoTao_MonHocRepository.Delete(id);
        }

        public void deleteById(string Id)
        {
            _chuongTrinhDaoTao_MonHocRepository.deleteById(Id);
        }

        public IEnumerable<ChuongTrinhDaoTao_MonHoc> GetAll()
        {
            return _chuongTrinhDaoTao_MonHocRepository.GetAll();
        }

        public IEnumerable<ChuongTrinhDaoTao_MonHoc> GetAllByKey(string maCT, string maMH)
        {
            return _chuongTrinhDaoTao_MonHocRepository.GetAllByKey(maCT,maMH);
        }

        public IEnumerable<ChuongTrinhDaoTao_MonHoc> GetAllByMACT(string Id)
        {
            return _chuongTrinhDaoTao_MonHocRepository.GetAllByMACT(Id);
        }

        public ChuongTrinhDaoTao_MonHoc GetByID(int id)
        {
            return _chuongTrinhDaoTao_MonHocRepository.GetSingleById(id);
        }

        public void Update(ChuongTrinhDaoTao_MonHoc chuongTrinhDaoTao_MonHoc)
        {
            _chuongTrinhDaoTao_MonHocRepository.Update(chuongTrinhDaoTao_MonHoc);
        }
    }
}
