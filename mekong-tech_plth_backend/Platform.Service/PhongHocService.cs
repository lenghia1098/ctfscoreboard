﻿using Platform.Data.Infrastructure;
using Platform.Data.Repositories;
using Platform.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Platform.Service
{
    public interface IPhongHocService
    {
        void Add(PhongHoc phongHoc);
        void Update(PhongHoc phongHoc);
        PhongHoc delete(string id);
        IEnumerable<PhongHoc> GetAll();
        PhongHoc GetByID(int id);
        void Save();

        void Commit();

    }
    public class PhongHocService : IPhongHocService
    {
        IPhongHocRepository _phongHocRepository;
        IUnitOfWork _unitOfWork;
        public PhongHocService(IPhongHocRepository phongHocRepository, IUnitOfWork unitOfWork)
        {
            this._phongHocRepository = phongHocRepository;
            this._unitOfWork = unitOfWork;
        }
        public void Add(PhongHoc phongHoc)
        {
            _phongHocRepository.Add(phongHoc);
        }
        public void Save()
        {
            _unitOfWork.Commit();
        }
        public void Commit()
        {
            _unitOfWork.Commit();
        }

        public PhongHoc delete(string id)
        {
          return  _phongHocRepository.Delete(id);
        }

        public IEnumerable<PhongHoc> GetAll()
        {
            return _phongHocRepository.GetAll();
        }

        public PhongHoc GetByID(int id)
        {
            return _phongHocRepository.GetSingleById(id);
        }

        public void Update(PhongHoc phongHoc)
        {
            _phongHocRepository.Update(phongHoc);
        }
    }
}
