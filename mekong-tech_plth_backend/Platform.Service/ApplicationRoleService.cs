﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Platform.Common.Exceptions;
using Platform.Data.Infrastructure;
using Platform.Data.Repositories;
using Platform.Model.Models;

namespace Platform.Service
{
    public interface IApplicationRoleService
    {
        ApplicationRole GetDetail(string id);

        IEnumerable<ApplicationRole> GetAll(int page, int pageSize, out int totalRow, string filter);

        IEnumerable<ApplicationRole> GetAll();

        ApplicationRole Add(ApplicationRole appRole);

        void Update(ApplicationRole AppRole);

        void Delete(string id); 
        //IEnumerable<ApplicationUser> GetUsers();

        //Add roles to a sepcify group
        bool AddRolesToGroup(IEnumerable<ApplicationRoleGroup> roleGroups, int groupId);
        bool AddUserToUserRols(IEnumerable<ApplicationUserRoleNew> userrole, string userId);


        //Get list role by group id
        IEnumerable<ApplicationRole> GetListRoleByGroupId(int groupId);
        IEnumerable<ApplicationRole> GetListRoleByUserId(string userId);
        IEnumerable<ApplicationRole> GetListRoleByUserIdv2(string userId);


        void Save();
    }

    public class ApplicationRoleService : IApplicationRoleService
    {
        private IApplicationRoleRepository _appRoleRepository;
        private IApplicationRoleGroupRepository _appRoleGroupRepository;
        private IApplicationUserRoleNewRepository _appUserRoleNewRepository;
        private IUnitOfWork _unitOfWork;
        private IApplicationUserRepository _applicationUserRepository;

        public ApplicationRoleService(IUnitOfWork unitOfWork,
            IApplicationRoleRepository appRoleRepository, IApplicationRoleGroupRepository appRoleGroupRepository, IApplicationUserRoleNewRepository appUserRoleNewRepository, IApplicationUserRepository applicationUserRepository)
        {
            this._appRoleRepository = appRoleRepository;
            this._appRoleGroupRepository = appRoleGroupRepository;
            this._appUserRoleNewRepository = appUserRoleNewRepository;
            this._unitOfWork = unitOfWork;
            this._applicationUserRepository = applicationUserRepository;
        }

        public ApplicationRole Add(ApplicationRole appRole)
        {
            if (_appRoleRepository.CheckContains(x => x.Description == appRole.Description))
                throw new NameDuplicatedException("Tên không được trùng");
            return _appRoleRepository.Add(appRole);
        }

        public bool AddRolesToGroup(IEnumerable<ApplicationRoleGroup> roleGroups, int groupId)
        {
            _appRoleGroupRepository.DeleteMulti(x => x.GroupId == groupId);
            foreach (var roleGroup in roleGroups)
            {
                _appRoleGroupRepository.Add(roleGroup);
            }
            return true;
        }

        public void Delete(string id)
        {
            _appRoleRepository.DeleteMulti(x => x.Id == id);
        }

        public IEnumerable<ApplicationRole> GetAll()
        {
            return _appRoleRepository.GetAll();
        }

        public IEnumerable<ApplicationRole> GetAll(int page, int pageSize, out int totalRow, string filter = null)
        {
            var query = _appRoleRepository.GetAll();
            if (!string.IsNullOrEmpty(filter))
                query = query.Where(x => x.Description.Contains(filter));

            totalRow = query.Count();
            return query.OrderBy(x => x.Description).Skip(page * pageSize).Take(pageSize);
        }

        public ApplicationRole GetDetail(string id)
        {
            return _appRoleRepository.GetSingleByCondition(x => x.Id == id);
        }

        public void Save()
        {
            _unitOfWork.Commit();
        }

        public void Update(ApplicationRole AppRole)
        {
            if (_appRoleRepository.CheckContains(x => x.Description == AppRole.Description && x.Id != AppRole.Id))
                throw new NameDuplicatedException("Tên không được trùng");
            _appRoleRepository.Update(AppRole);
        }

        public IEnumerable<ApplicationRole> GetListRoleByGroupId(int groupId)
        {
            return _appRoleRepository.GetListRoleByGroupId(groupId);
        }

        public IEnumerable<ApplicationRole> GetListRoleByUserId(string userId)
        {
            return _appRoleRepository.GetListRoleByUserId(userId);
        }
        public IEnumerable<ApplicationRole> GetListRoleByUserIdv2(string userId)
        {
            return _appRoleRepository.GetListRoleByUserIdv2(userId);
        }

        public bool AddUserToUserRols(IEnumerable<ApplicationUserRoleNew> userrole, string userId)
        {
            var duplicates = userrole.GroupBy(r => r.RoleId).Select(y => y.First()).ToList();
            //userrole = userrole.Except(duplicates).ToArray();

            _appUserRoleNewRepository.DeleteMulti(x => x.UserId == userId);

            foreach (var item in duplicates)
            {
                IEnumerable<ApplicationUserRoleNew> a = _appUserRoleNewRepository.GetAll();
                _appUserRoleNewRepository.Add(item);
              
               
            }
            return true;
        }

        //public IEnumerable<ApplicationUser> GetUsers()
        //{
        //    return _applicationUserRepository.GetMulti(x => x.DaXoa == false);
        //}
    }
}
