﻿using Platform.Data.Infrastructure;
using Platform.Data.Repositories;
using Platform.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Platform.Service
{
    public interface ICongViecDeXuatService
    {
        void Add(CongViecDeXuat congViecDeXuat);
        void Update(CongViecDeXuat congViecDeXuat);
        void delete(int id);
        IEnumerable<CongViecDeXuat> GetAll();
        CongViecDeXuat GetByID(int id);
        IEnumerable<CongViecDeXuat> GetAllById(string Id);
        void Save();


        void Commit();

    }
    public class CongViecDeXuatService : ICongViecDeXuatService
    {
        ICongViecDeXuatRepository _congViecDeXuatRepository;
        IUnitOfWork _unitOfWork;
        public CongViecDeXuatService(ICongViecDeXuatRepository congViecDeXuatRepository, IUnitOfWork unitOfWork)
        {
            this._congViecDeXuatRepository = congViecDeXuatRepository;
            this._unitOfWork = unitOfWork;
        }
        public void Add(CongViecDeXuat congViecDeXuat)
        {
            _congViecDeXuatRepository.Add(congViecDeXuat);
        }
        public void Save()
        {
            _unitOfWork.Commit();
        }
        public void Commit()
        {
            _unitOfWork.Commit();
        }

        public void delete(int id)
        {
            _congViecDeXuatRepository.Delete(id);
        }

        public IEnumerable<CongViecDeXuat> GetAll()
        {
            return _congViecDeXuatRepository.GetAll();
        }

        public IEnumerable<CongViecDeXuat> GetAllById(string Id)
        {
            return _congViecDeXuatRepository.GetAllById(Id);
        }

        public CongViecDeXuat GetByID(int id)
        {
            return _congViecDeXuatRepository.GetSingleById(id);
        }

        

        public void Update(CongViecDeXuat congViecDeXuat)
        {
            _congViecDeXuatRepository.Update(congViecDeXuat);
        }
    }
}
