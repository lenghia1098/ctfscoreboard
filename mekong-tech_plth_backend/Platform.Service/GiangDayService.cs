﻿using Platform.Data.Infrastructure;
using Platform.Data.Repositories;
using Platform.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Platform.Service
{
    public interface IGiangDayService
    {
        void Add(GiangDay giangDay);
        void Update(GiangDay giangDay);
        void delete(int id);
        IEnumerable<GiangDay> GetAll();
        GiangDay GetByID(int id);
        IQueryable<DanhSachLopTheoMaVC> getDanhSachLop(string msvc, string hocKy);
        IQueryable getDanhSachHocKy(string msvc);
        IQueryable<DanhSachGiangDayTheoMaNhom> getDanhSachGiangDay(string maNhom);
        void Commit();
        void Save();
        IQueryable<LichGiangDay> GetLichGiangDay(string msvc, string hocky, string namhoc);												  


    }
    public class GiangDayService : IGiangDayService
    {
        IGiangDayRepository _giangDayRepository;
        IUnitOfWork _unitOfWork;
        public GiangDayService(IGiangDayRepository giangDayRepository, IUnitOfWork unitOfWork)
        {
            this._giangDayRepository = giangDayRepository;
            this._unitOfWork = unitOfWork;
        }
        public void Save()
        {
            _unitOfWork.Commit();
        }
        public void Add(GiangDay giangDay)
        {
            _giangDayRepository.Add(giangDay);
        }

        public void Commit()
        {
            _unitOfWork.Commit();
        }

        public void delete(int id)
        {
            _giangDayRepository.Delete(id);
        }

        public IEnumerable<GiangDay> GetAll()
        {
            return _giangDayRepository.GetAll();
        }

        public IQueryable<DanhSachLopTheoMaVC> getDanhSachLop(string msvc, string hocKy)
        {
            return _giangDayRepository.getDanhSachLop(msvc,  hocKy);
        }

        public GiangDay GetByID(int id)
        {
            return _giangDayRepository.GetSingleById(id);
        }

       

        public void Update(GiangDay giangDay)
        {
            _giangDayRepository.Update(giangDay);
        }

        public IQueryable getDanhSachHocKy(string msvc)
        {
            return _giangDayRepository.getDanhSachHocKy(msvc);
        }

        public IQueryable<DanhSachGiangDayTheoMaNhom> getDanhSachGiangDay(string maNhom)
        {
            return _giangDayRepository.getDanhSachGiangDay(maNhom);
        }


        public IQueryable<LichGiangDay> GetLichGiangDay(string msvc, string hocky, string namhoc)

        {
            return _giangDayRepository.GetLichGiangDay(msvc,hocky,namhoc);
        }
    }
}
