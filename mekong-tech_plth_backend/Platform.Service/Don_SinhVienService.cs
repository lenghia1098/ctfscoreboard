﻿using Platform.Data.Infrastructure;
using Platform.Data.Repositories;
using Platform.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Platform.Service
{
    public interface IDon_SinhVienService
    {
        void Add(Don_SinhVien don_SinhVien);
        void Update(Don_SinhVien don_SinhVien);
        void delete(int id);
        IEnumerable<Don_SinhVien> GetAll();
        int DemDonDaNop(string mssv);
        Don_SinhVien GetByID(int id);
        void Save();
        Don_SinhVien getID(int maDon);
        void Commit();

    }
    public class Don_SinhVienService : IDon_SinhVienService
    {
        IDon_SinhVienRepository _don_SinhVienRepository;
        IUnitOfWork _unitOfWork;
        public Don_SinhVienService(IDon_SinhVienRepository don_SinhVienRepository, IUnitOfWork unitOfWork)
        {
            this._don_SinhVienRepository = don_SinhVienRepository;
            this._unitOfWork = unitOfWork;
        }
        public void Save()
        {
            _unitOfWork.Commit();
        }
        public void Add(Don_SinhVien don_SinhVien)
        {
            _don_SinhVienRepository.Add(don_SinhVien);
        }

        public void Commit()
        {
            _unitOfWork.Commit();
        }

        public void delete(int id)
        {
            _don_SinhVienRepository.Delete(id);
        }

        public int DemDonDaNop(string mssv)
        {
            return _don_SinhVienRepository.DemDonDaNop(mssv);
        }

        public IEnumerable<Don_SinhVien> GetAll()
        {
            return _don_SinhVienRepository.GetAll();
        }

        public Don_SinhVien GetByID(int id)
        {
            return _don_SinhVienRepository.GetSingleById(id);
        }

        public Don_SinhVien getID(int maDon)
        {
            return _don_SinhVienRepository.getID(maDon);
        }

        public void Update(Don_SinhVien don_SinhVien)
        {
            _don_SinhVienRepository.Update(don_SinhVien);
        }
    }
}
