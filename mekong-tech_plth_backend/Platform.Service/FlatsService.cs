﻿using Platform.Data.Infrastructure;
using Platform.Data.Repositories;
using Platform.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Platform.Service
{
    public interface IFlatsService
    {
        void Add(Flats flats);
        void Update(Flats flats);
        void delete(int id);
        IEnumerable<Flats> GetAll();
       
        Flats GetByID(int id);
        Flats GetByID(string id);
        void Save();

        void Commit();

    }
    public class FlatsService : IFlatsService
    {
        IFlatsRepository _flatsRepository;
        IUnitOfWork _unitOfWork;
        public FlatsService(IFlatsRepository flatsRepository, IUnitOfWork unitOfWork)
        {
            this._flatsRepository = flatsRepository;
            this._unitOfWork = unitOfWork;
        }
        public void Save()
        {
            _unitOfWork.Commit();
        }
        public void Add(Flats flats)
        {
            _flatsRepository.Add(flats);
        }

        public void Commit()
        {
            _unitOfWork.Commit();
        }

        public void delete(int id)
        {
            _flatsRepository.Delete(id);
        }

        public IEnumerable<Flats> GetAll()
        {
            return _flatsRepository.GetAll();
        }

        public Flats GetByID(int id)
        {
            return _flatsRepository.GetSingleById(id);
        }

     

        public void Update(Flats flats)
        {
            _flatsRepository.Update(flats);
        }

        public Flats GetByID(string id)
        {
            return _flatsRepository.GetSingleById(id);
        }
    }
}
