﻿using Platform.Data.Infrastructure;
using Platform.Data.Repositories;
using Platform.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Platform.Service
{
    public interface IDangKy_TamThoiService
    {
        void Add(DangKy_TamThoi dangKy_TamThoi);
        void Update(DangKy_TamThoi dangKy_TamThoi);
        DangKy_TamThoi Delete(int ID);
        IEnumerable<DangKy_TamThoi> GetAll();
        DangKy_TamThoi GetByID(int id);
        IQueryable<DanhSachMonHocTheoMaLopNghanhKhoa> getDangKy_TamThoi(string mssv);
        IQueryable<DanhSachMonHocTheoMaLopNghanhKhoa> getDKMHTH(string mssv, string hocKy, string namHoc);
        IEnumerable<chucnangtinhhocphi> getDangKyTanThoi_TinhHocPhi();
       
        void Commit();
        void Save();

    }
    public class DangKy_TamThoiService : IDangKy_TamThoiService
    {
        IDangKy_TamThoiRepository _dangKy_TamThoiRepository;
        IUnitOfWork _unitOfWork;
        public DangKy_TamThoiService(IDangKy_TamThoiRepository dangKy_TamThoiRepository, IUnitOfWork unitOfWork)
        {
            this._dangKy_TamThoiRepository = dangKy_TamThoiRepository;
            this._unitOfWork = unitOfWork;
        }
        public void Add(DangKy_TamThoi dangKy_TamThoi)
        {
            _dangKy_TamThoiRepository.Add(dangKy_TamThoi);
        }

        public void Commit()
        {
            _unitOfWork.Commit();
        }

        public DangKy_TamThoi Delete(int ID)
        {
          return  _dangKy_TamThoiRepository.Delete(ID);
        }

        public IEnumerable<DangKy_TamThoi> GetAll()
        {
            return _dangKy_TamThoiRepository.GetAll();
        }

        public DangKy_TamThoi GetByID(int id)
        {
            return _dangKy_TamThoiRepository.GetSingleById(id);
        }

        public IEnumerable<chucnangtinhhocphi> getDangKyTanThoi_TinhHocPhi()
        {
            return _dangKy_TamThoiRepository.getDangKyTanThoi_TinhHocPhi();
        }

        public IQueryable<DanhSachMonHocTheoMaLopNghanhKhoa> getDangKy_TamThoi(string mssv)
        {
            return _dangKy_TamThoiRepository.getDangKy_TamThoi(mssv);
        }

        public IQueryable<DanhSachMonHocTheoMaLopNghanhKhoa> getDKMHTH(string mssv, string hocKy, string namHoc)
        {
            return _dangKy_TamThoiRepository.getDKMHTH(mssv,hocKy,namHoc);
        }

        public void Save()
        {
            _unitOfWork.Commit();
        }

        public void Update(DangKy_TamThoi dangKy_TamThoi)
        {
            _dangKy_TamThoiRepository.Update(dangKy_TamThoi);
        }

       
    }
}
