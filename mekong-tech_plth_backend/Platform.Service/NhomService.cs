﻿using Platform.Data.Infrastructure;
using Platform.Data.Repositories;
using Platform.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Platform.Service
{
    public interface INhomService
    {
        void Add(Nhom nhom);
        void Update(Nhom nhom);
        void delete(int id);
        IEnumerable<Nhom> GetAll();
        IQueryable<ChucNangTKB> getNhom(string mssv, string hocKy, string tuan);
        IQueryable<getHocKy> getHocKy(string mssv);
        IQueryable<getHocKy> getHocKyTH(string mssv);
        IQueryable<ChucNangTKB> getNgayBatDauVaKetThuc(string mssv);
        Nhom GetByID(int id);
        void Save();

        void Commit();

    }
    public class NhomService : INhomService
    {
        INhomRepository _nhomRepository;
        IUnitOfWork _unitOfWork;
        public NhomService(INhomRepository nhomRepository, IUnitOfWork unitOfWork)
        {
            this._nhomRepository = nhomRepository;
            this._unitOfWork = unitOfWork;
        }
        public void Save()
        {
            _unitOfWork.Commit();
        }
        public void Add(Nhom nhom)
        {
            _nhomRepository.Add(nhom);
        }

        public void Commit()
        {
            _unitOfWork.Commit();
        }

        public void delete(int id)
        {
            _nhomRepository.Delete(id);
        }

        public IEnumerable<Nhom> GetAll()
        {
            return _nhomRepository.GetAll();
        }

        public Nhom GetByID(int id)
        {
            return _nhomRepository.GetSingleById(id);
        }

        public IQueryable<getHocKy> getHocKy(string mssv)
        {
            return _nhomRepository.getHocKy(mssv);
        }

        public IQueryable<getHocKy> getHocKyTH(string mssv)
        {
            return _nhomRepository.getHocKyTH(mssv);
        }

        public IQueryable<ChucNangTKB> getNgayBatDauVaKetThuc(string mssv)
        {
            return _nhomRepository.getNgayBatDauVaKetThuc(mssv);
        }

        public IQueryable<ChucNangTKB> getNhom(string mssv, string hocKy, string tuan)
        {
            return _nhomRepository.getNhom(mssv,  hocKy,tuan);
        }

        public void Update(Nhom nhom)
        {
            _nhomRepository.Update(nhom);
        }
    }
}
