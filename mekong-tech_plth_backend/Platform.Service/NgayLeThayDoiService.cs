﻿using Platform.Data.Infrastructure;
using Platform.Data.Repositories;
using Platform.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Platform.Service
{
    public interface INgayLeThayDoiService
    {
        void Add(NgayLeThayDoi ngayLeThayDoi);
        void Update(NgayLeThayDoi ngayLeThayDoi);
        NgayLeThayDoi delete(int id);
        IEnumerable<NgayLeThayDoi> GetAll();
        NgayLeThayDoi GetByID(int id);
        IEnumerable<NgayLeThayDoi> GetByNamHoc(string namhoc);
        void Save();
        void Commit();

    }
    public class NgayLeThayDoiService : INgayLeThayDoiService
    {
        INgayLeThayDoiRepository _ngayLeThayDoiRepository;
        IUnitOfWork _unitOfWork;
        public NgayLeThayDoiService(INgayLeThayDoiRepository ngayLeThayDoiRepository, IUnitOfWork unitOfWork)
        {
            this._ngayLeThayDoiRepository = ngayLeThayDoiRepository;
            this._unitOfWork = unitOfWork;
        }
        public void Add(NgayLeThayDoi ngayLeThayDoi)
        {
            _ngayLeThayDoiRepository.Add(ngayLeThayDoi);
        }

        public void Commit()
        {
            _unitOfWork.Commit();
        }

        public NgayLeThayDoi delete(int id)
        {
           return _ngayLeThayDoiRepository.Delete(id);
        }

        public IEnumerable<NgayLeThayDoi> GetAll()
        {
            return _ngayLeThayDoiRepository.GetAll();
        }

        public NgayLeThayDoi GetByID(int id)
        {
            return _ngayLeThayDoiRepository.GetSingleById(id);
        }

        public void Update(NgayLeThayDoi ngayLeThayDoi)
        {
            _ngayLeThayDoiRepository.Update(ngayLeThayDoi);
        }
        public void Save()
        {
            _unitOfWork.Commit();
        }

        public IEnumerable<NgayLeThayDoi> GetByNamHoc(string namhoc)
        {
            return _ngayLeThayDoiRepository.GetMulti(x=>x.NamHoc==namhoc);
        }
    }
}
