﻿using Platform.Data.Infrastructure;
using Platform.Data.Repositories;
using Platform.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Platform.Service
{
    public interface ITrinhDoNgoaiNguService
    {
        void Add(TrinhDoNgoaiNgu trinhDoNgoaiNgu);
        void Update(TrinhDoNgoaiNgu trinhDoNgoaiNgu);
        void delete(int id);
        IEnumerable<TrinhDoNgoaiNgu> GetAll();
        TrinhDoNgoaiNgu GetByID(int id);
        void Save();
        IEnumerable<TrinhDoNgoaiNgu> GetTrinhDoNgoaiNgu(string msvc);
        TrinhDoNgoaiNgu GetId(int ID);
        void Commit();

    }
    public class TrinhDoNgoaiNguService : ITrinhDoNgoaiNguService
    {
        ITrinhDoNgoaiNguRepository _trinhDoNgoaiNguRepository;
        IUnitOfWork _unitOfWork;
        public TrinhDoNgoaiNguService(ITrinhDoNgoaiNguRepository trinhDoNgoaiNguRepository, IUnitOfWork unitOfWork)
        {
            this._trinhDoNgoaiNguRepository = trinhDoNgoaiNguRepository;
            this._unitOfWork = unitOfWork;
        }
        public void Add(TrinhDoNgoaiNgu trinhDoNgoaiNgu)
        {
            _trinhDoNgoaiNguRepository.Add(trinhDoNgoaiNgu);
        }
        public void Save()
        {
            _unitOfWork.Commit();
        }
        public void Commit()
        {
            _unitOfWork.Commit();
        }

        public void delete(int id)
        {
            _trinhDoNgoaiNguRepository.Delete(id);
        }

        public IEnumerable<TrinhDoNgoaiNgu> GetAll()
        {
            return _trinhDoNgoaiNguRepository.GetAll();
        }

        public TrinhDoNgoaiNgu GetByID(int id)
        {
            return _trinhDoNgoaiNguRepository.GetSingleById(id);
        }

        public void Update(TrinhDoNgoaiNgu trinhDoNgoaiNgu)
        {
            _trinhDoNgoaiNguRepository.Update(trinhDoNgoaiNgu);
        }

        public IEnumerable<TrinhDoNgoaiNgu> GetTrinhDoNgoaiNgu(string msvc)
        {
            return _trinhDoNgoaiNguRepository.GetTrinhDoNgoaiNgu(msvc);
        }

        public TrinhDoNgoaiNgu GetId(int ID)
        {
            return _trinhDoNgoaiNguRepository.GetId(ID);
        }
    }
}
