﻿using Platform.Data.Infrastructure;
using Platform.Data.Repositories;
using Platform.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Platform.Service
{
    public interface IPhongKhoaService
    {
        void Add(PhongKhoa phongKhoa);
        void Update(PhongKhoa phongKhoa);
        void delete(int id);
        IEnumerable<PhongKhoa> GetAll();
        PhongKhoa GetByID(int id);
        void Save();

        void Commit();

    }
    public class PhongKhoaService : IPhongKhoaService
    {
        IPhongKhoaRepository _phongKhoaRepository;
        IUnitOfWork _unitOfWork;
        public PhongKhoaService(IPhongKhoaRepository phongKhoaRepository, IUnitOfWork unitOfWork)
        {
            this._phongKhoaRepository = phongKhoaRepository;
            this._unitOfWork = unitOfWork;
        }
        public void Add(PhongKhoa phongKhoa)
        {
            _phongKhoaRepository.Add(phongKhoa);
        }
        public void Save()
        {
            _unitOfWork.Commit();
        }
        public void Commit()
        {
            _unitOfWork.Commit();
        }

        public void delete(int id)
        {
            _phongKhoaRepository.Delete(id);
        }

        public IEnumerable<PhongKhoa> GetAll()
        {
            return _phongKhoaRepository.GetAll();
        }

        public PhongKhoa GetByID(int id)
        {
            return _phongKhoaRepository.GetSingleById(id);
        }

        public void Update(PhongKhoa phongKhoa)
        {
            _phongKhoaRepository.Update(phongKhoa);
        }
    }
}
