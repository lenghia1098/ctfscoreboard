﻿using Platform.Data.Infrastructure;
using Platform.Data.Repositories;
using Platform.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Platform.Service
{
    public interface IVienChucService
    {
        void Add(VienChuc vienChuc);
        void Update(VienChuc vienChuc);
        void delete(int id);
        IEnumerable<VienChuc> GetAll();
        VienChuc GetByID(int id);
        IQueryable<TTCNVienChuc> GetTTCN(string Id);
        void Save();
       VienChuc GetLyLichVienChuc(string msvc);
        void Commit();

    }
    public class VienChucService : IVienChucService
    {
        IVienChucRepository _vienChucRepository;
        IUnitOfWork _unitOfWork;
        public VienChucService(IVienChucRepository vienChucRepository, IUnitOfWork unitOfWork)
        {
            this._vienChucRepository = vienChucRepository;
            this._unitOfWork = unitOfWork;
        }
        public void Add(VienChuc vienChuc)
        {
            _vienChucRepository.Add(vienChuc);
        }

        public void Save()
        {
            _unitOfWork.Commit();
        }
        public void Commit()
        {
            _unitOfWork.Commit();
        }

        public void delete(int id)
        {
            _vienChucRepository.Delete(id);
        }

        public IEnumerable<VienChuc> GetAll()
        {
            return _vienChucRepository.GetAll();
        }

        public VienChuc GetByID(int id)
        {
            return _vienChucRepository.GetSingleById(id);
        }

        public IQueryable<TTCNVienChuc> GetTTCN(string Id)
        {
            return _vienChucRepository.GetTTCN(Id);
        }

        public void Update(VienChuc vienChuc)
        {
            _vienChucRepository.Update(vienChuc);
        }

        public VienChuc GetLyLichVienChuc(string msvc)
        {
           return _vienChucRepository.GetLyLichVienChuc(msvc);
        }
    }
}
