﻿using Platform.Data.Infrastructure;
using Platform.Data.Repositories;
using Platform.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Platform.Service
{
    public interface ILop_ChuongTrinhDaoTaoService
    {
        void Add(Lop_ChuongTrinhDaoTao lop_ChuongTrinhDaoTao);
        void Update(Lop_ChuongTrinhDaoTao lop_ChuongTrinhDaoTao);
        void delete(int id);
        IEnumerable<Lop_ChuongTrinhDaoTao> GetAll();
        Lop_ChuongTrinhDaoTao GetByID(int id);
        void Save();

        void Commit();

    }
    public class Lop_ChuongTrinhDaoTaoService : ILop_ChuongTrinhDaoTaoService
    {
        ILop_ChuongTrinhDaoTaoRepository _lop_ChuongTrinhDaoTaoRepository;
        IUnitOfWork _unitOfWork;
        public Lop_ChuongTrinhDaoTaoService(ILop_ChuongTrinhDaoTaoRepository lop_ChuongTrinhDaoTaoRepository, IUnitOfWork unitOfWork)
        {
            this._lop_ChuongTrinhDaoTaoRepository = lop_ChuongTrinhDaoTaoRepository;
            this._unitOfWork = unitOfWork;
        }
        public void Save()
        {
            _unitOfWork.Commit();
        }
        public void Add(Lop_ChuongTrinhDaoTao lop_ChuongTrinhDaoTao)
        {
            _lop_ChuongTrinhDaoTaoRepository.Add(lop_ChuongTrinhDaoTao);
        }

        public void Commit()
        {
            _unitOfWork.Commit();
        }

        public void delete(int id)
        {
            _lop_ChuongTrinhDaoTaoRepository.Delete(id);
        }

        public IEnumerable<Lop_ChuongTrinhDaoTao> GetAll()
        {
            return _lop_ChuongTrinhDaoTaoRepository.GetAll();
        }

        public Lop_ChuongTrinhDaoTao GetByID(int id)
        {
            return _lop_ChuongTrinhDaoTaoRepository.GetSingleById(id);
        }

        public void Update(Lop_ChuongTrinhDaoTao lop_ChuongTrinhDaoTao)
        {
            _lop_ChuongTrinhDaoTaoRepository.Update(lop_ChuongTrinhDaoTao);
        }
    }
}
