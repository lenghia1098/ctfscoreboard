﻿using Platform.Data.Infrastructure;
using Platform.Data.Repositories;
using Platform.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Platform.Service
{
    public interface IViecPhaiLamSinhVienService
    {
        void Add(ViecPhaiLamSinhVien viecPhaiLamSinhVien);
        void Update(ViecPhaiLamSinhVien viecPhaiLamSinhVien);
        void delete(int id);
        IEnumerable<ViecPhaiLamSinhVien> GetAll();
        IEnumerable<chucnangvieclamsinhvien> getviecphailam(string mssv);
        int getviecdalam(string mssv);
        IQueryable<chitietviecdalam> getchitietviecdalam(int MaSoCongViec);
        string DemViecPhaiLamSinhVien(string mssv);
        ViecPhaiLamSinhVien GetByID(int id);
        void Commit();
        void Save();

    }
    public class ViecPhaiLamSinhVienService : IViecPhaiLamSinhVienService
    {
        IViecPhaiLamSinhVienRepository _viecPhaiLamSinhVienRepository;
        IUnitOfWork _unitOfWork;
        public ViecPhaiLamSinhVienService(IViecPhaiLamSinhVienRepository viecPhaiLamSinhVienRepository, IUnitOfWork unitOfWork)
        {
            this._viecPhaiLamSinhVienRepository = viecPhaiLamSinhVienRepository;
            this._unitOfWork = unitOfWork;
        }
        public void Add(ViecPhaiLamSinhVien viecPhaiLamSinhVien)
        {
            _viecPhaiLamSinhVienRepository.Add(viecPhaiLamSinhVien);
        }
        public void Save()
        {
            _unitOfWork.Commit();
        }
        public void Commit()
        {
            _unitOfWork.Commit();
        }

        public void delete(int id)
        {
            _viecPhaiLamSinhVienRepository.Delete(id);
        }

        public IEnumerable<ViecPhaiLamSinhVien> GetAll()
        {
            return _viecPhaiLamSinhVienRepository.GetAll();
        }

        public IEnumerable<chucnangvieclamsinhvien> getviecphailam(string mssv)
        {
            return _viecPhaiLamSinhVienRepository.getviecphailam(mssv);
        }

        public ViecPhaiLamSinhVien GetByID(int id)
        {
            return _viecPhaiLamSinhVienRepository.GetSingleById(id);
        }

        public void Update(ViecPhaiLamSinhVien viecPhaiLamSinhVien)
        {
            _viecPhaiLamSinhVienRepository.Update(viecPhaiLamSinhVien);
        }

        public string DemViecPhaiLamSinhVien(string mssv)
        {
            return _viecPhaiLamSinhVienRepository.DemViecPhaiLamSinhVien(mssv);
        }

        public int getviecdalam(string mssv)
        {
            return _viecPhaiLamSinhVienRepository.getviecdalam(mssv);
        }

        public IQueryable<chitietviecdalam> getchitietviecdalam(int MaSoCongViec)
        {
            return _viecPhaiLamSinhVienRepository.getchitietviecdalam(MaSoCongViec);
        }

    }
}
