﻿using Platform.Data.Infrastructure;
using Platform.Data.Repositories;
using Platform.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Platform.Service
{
 
        public interface ICauHoiThuongGapService
        {
            void Add(CauHoiThuongGap cauHoiThuongGap);
            void Update(CauHoiThuongGap cauHoiThuongGap);
            void delete(int id);
            IEnumerable<CauHoiThuongGap> GetAll();
            CauHoiThuongGap GetByID(int id);
            IEnumerable<CauHoiThuongGap> GetAllById(string Id);
            void Commit();
            void Save();

    }
        public class CauHoiThuongGapService : ICauHoiThuongGapService
        {
            ICauHoiThuongGapRepository _cauHoiThuongGapRepository;
            IUnitOfWork _unitOfWork;
            public CauHoiThuongGapService(ICauHoiThuongGapRepository cauHoiThuongGapRepository, IUnitOfWork unitOfWork)
            {
                this._cauHoiThuongGapRepository = cauHoiThuongGapRepository;
                this._unitOfWork = unitOfWork;
            }
            public void Add(CauHoiThuongGap cauHoiThuongGap)
            {
                _cauHoiThuongGapRepository.Add(cauHoiThuongGap);
            }

            public void Commit()
            {
                _unitOfWork.Commit();
            }
            public void Save()
            {
                _unitOfWork.Commit();
            }
             public void delete(int id)
            {
                _cauHoiThuongGapRepository.Delete(id);
            }

            public IEnumerable<CauHoiThuongGap> GetAll()
            {
                return _cauHoiThuongGapRepository.GetAll();
            }

        public IEnumerable<CauHoiThuongGap> GetAllById(string Id)
        {
            return _cauHoiThuongGapRepository.GetAllById(Id);
        }

        public CauHoiThuongGap GetByID(int id)
            {
                return _cauHoiThuongGapRepository.GetSingleById(id);
            }

            public void Update(CauHoiThuongGap cauHoiThuongGap)
            {
                _cauHoiThuongGapRepository.Update(cauHoiThuongGap);
            }
        }
    }

