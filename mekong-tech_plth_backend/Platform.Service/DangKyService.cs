﻿using Platform.Data.Infrastructure;
using Platform.Data.Repositories;
using Platform.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Platform.Service
{
    public interface IDangKyService
    {
        void Add(DangKy dangKy);
        void Update(DangKy dangKy);
        void delete(int id);
        IEnumerable<DangKy> GetAll();
        DangKy GetByID(int id);
        IQueryable<string> GetMaNhomByMSSV(string mssv, int hocKy);
        IQueryable<DanhSachMonHocTheoMaLopNghanhKhoa> getDKMH(string mssv, string namHoc, string hocKy);
        IQueryable<DanhSachMonHocTheoMaLopNghanhKhoa> xemThongTinMH(string MaMH, string MaNhom,string MaLop);
        void Commit();
        void Save();

    }
    public class DangKyService : IDangKyService
    {
        IDangKyRepository _dangKyRepository;
        IUnitOfWork _unitOfWork;
        public DangKyService(IDangKyRepository dangKyRepository, IUnitOfWork unitOfWork)
        {
            this._dangKyRepository = dangKyRepository;
            this._unitOfWork = unitOfWork;
        }
        public void Add(DangKy dangKy)
        {
            _dangKyRepository.Add(dangKy);
        }
        public void Save()
        {
            _unitOfWork.Commit();
        }
        public void Commit()
        {
            _unitOfWork.Commit();
        }

        public void delete(int id)
        {
            _dangKyRepository.Delete(id);
        }

        public IEnumerable<DangKy> GetAll()
        {
            return _dangKyRepository.GetAll();
        }

        public DangKy GetByID(int id)
        {
            return _dangKyRepository.GetSingleById(id);
        }

        public IQueryable<DanhSachMonHocTheoMaLopNghanhKhoa> getDKMH(string mssv, string hocKy, string namHoc)
        {
            return _dangKyRepository.getDKMH(mssv, hocKy, namHoc);
        }

        public IQueryable<string> GetMaNhomByMSSV(string mssv, int hocKy)
        {
            return _dangKyRepository.GetMaNhomByMSSV(mssv, hocKy);
        }

        public void Update(DangKy dangKy)
        {
            _dangKyRepository.Update(dangKy);
        }

        public IQueryable<DanhSachMonHocTheoMaLopNghanhKhoa> xemThongTinMH(string MaMH, string MaNhom,string MaLop)
        {
            return _dangKyRepository.xemThongTinMH(MaMH,  MaNhom,  MaLop);
        }
    }
}
