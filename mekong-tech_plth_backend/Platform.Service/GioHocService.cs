﻿using Platform.Data.Infrastructure;
using Platform.Data.Repositories;
using Platform.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Platform.Service
{
    public interface IGioHocService
    {
        void Add(GioHoc gioHoc);
        void Update(GioHoc gioHoc);
        void delete(int id);
        IEnumerable<GioHoc> GetAll();
        GioHoc GetByID(int id);
        void Save();
        void Commit();

    }
    public class GioHocService : IGioHocService
    {
        IGioHocRepository _gioHocRepository;
        IUnitOfWork _unitOfWork;
        public GioHocService(IGioHocRepository gioHocRepository, IUnitOfWork unitOfWork)
        {
            this._gioHocRepository = gioHocRepository;
            this._unitOfWork = unitOfWork;
        }
        public void Add(GioHoc gioHoc)
        {
            _gioHocRepository.Add(gioHoc);
        }
        public void Save()
        {
            _unitOfWork.Commit();
        }
        public void Commit()
        {
            _unitOfWork.Commit();
        }

        public void delete(int id)
        {
            _gioHocRepository.Delete(id);
        }

        public IEnumerable<GioHoc> GetAll()
        {
            return _gioHocRepository.GetAll();
        }

        public GioHoc GetByID(int id)
        {
            return _gioHocRepository.GetSingleById(id);
        }

        public void Update(GioHoc gioHoc)
        {
            _gioHocRepository.Update(gioHoc);
        }
    }
}
