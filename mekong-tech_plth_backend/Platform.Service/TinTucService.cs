﻿using Platform.Data.Infrastructure;
using Platform.Data.Repositories;
using Platform.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Platform.Service
{
    public interface ITinTucService
    {
        void Add(TinTuc tinTuc);
        void Update(TinTuc tinTuc);
        void delete(int id);
        IEnumerable<TinTuc> GetAll();
        TinTuc GetByID(int id);
        IEnumerable<TinTuc> GetTinTuc(string mssv);
        IQueryable<TinTuc> ChiTietTT(int MaTinTuc);
        void Commit();
        void Save();

    }
    public class TinTucService : ITinTucService
    {
        ITinTucRepository _tinTucRepository;
        IUnitOfWork _unitOfWork;
        public TinTucService(ITinTucRepository tinTucRepository, IUnitOfWork unitOfWork)
        {
            this._tinTucRepository = tinTucRepository;
            this._unitOfWork = unitOfWork;
        }
        public void Add(TinTuc tinTuc)
        {
            _tinTucRepository.Add(tinTuc);
        }
        public void Save()
        {
            _unitOfWork.Commit();
        }

        public IQueryable<TinTuc> ChiTietTT(int MaTinTuc)
        {
            return _tinTucRepository.ChiTietTT(MaTinTuc);
        }

        public void Commit()
        {
            _unitOfWork.Commit();
        }

        public void delete(int id)
        {
            _tinTucRepository.Delete(id);
        }

        public IEnumerable<TinTuc> GetAll()
        {
            return _tinTucRepository.GetAll();
        }

        public TinTuc GetByID(int id)
        {
            return _tinTucRepository.GetSingleById(id);
        }

        public IEnumerable<TinTuc> GetTinTuc(string mssv)
        {
            return _tinTucRepository.GetTinTuc(mssv);
        }

        public void Update(TinTuc tinTuc)
        {
            _tinTucRepository.Update(tinTuc);
        }
    }
}
