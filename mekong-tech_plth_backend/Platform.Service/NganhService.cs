﻿using Platform.Data.Infrastructure;
using Platform.Data.Repositories;
using Platform.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Platform.Service
{
    public interface INganhService
    {
        void Add(Nganh nganh);
        void Update(Nganh nganh);
        void delete(int id);
        IEnumerable<Nganh> GetAll();
        Nganh GetByID(int id);
        void Save();

        void Commit();

    }
    public class NganhService : INganhService
    {
        INganhRepository _nganhRepository;
        IUnitOfWork _unitOfWork;
        public NganhService(INganhRepository nganhRepository, IUnitOfWork unitOfWork)
        {
            this._nganhRepository = nganhRepository;
            this._unitOfWork = unitOfWork;
        }
        public void Add(Nganh nganh)
        {
            _nganhRepository.Add(nganh);
        }
        public void Save()
        {
            _unitOfWork.Commit();
        }
        public void Commit()
        {
            _unitOfWork.Commit();
        }

        public void delete(int id)
        {
            _nganhRepository.Delete(id);
        }

        public IEnumerable<Nganh> GetAll()
        {
            return _nganhRepository.GetAll();
        }

        public Nganh GetByID(int id)
        {
            return _nganhRepository.GetSingleById(id);
        }

        public void Update(Nganh nganh)
        {
            _nganhRepository.Update(nganh);
        }
    }
}
