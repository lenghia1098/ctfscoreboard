﻿using Platform.Data.Infrastructure;
using Platform.Data.Repositories;
using Platform.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Platform.Service
{
    public interface ISinhVienService
    {
        void Add(SinhVien sinhVien);
        void Update(SinhVien sinhVien);
        void delete(int id);
        IEnumerable<SinhVien> GetAll();
    
       SinhVien GetByStringId(string Id);
        IQueryable<TTCN> GetTTCN(string Id);
        IQueryable<SinhVien> ThongTinSinhVien(string Id);
        IEnumerable<chucnangxemthongtinsinhvien_dauvao> getthongtinsinhvien_dauvao(string mssv);
        void Save();

        void Commit();

    }
    public class SinhVienService : ISinhVienService
    {
        ISinhVienRepository _sinhVienRepository;
        IUnitOfWork _unitOfWork;
        public SinhVienService(ISinhVienRepository sinhVienRepository, IUnitOfWork unitOfWork)
        {
            this._sinhVienRepository = sinhVienRepository;
            this._unitOfWork = unitOfWork;
        }
        public void Save()
        {
            _unitOfWork.Commit();
        }
        public void Add(SinhVien sinhVien)
        {
            _sinhVienRepository.Add(sinhVien);
        }

        public void Commit()
        {
            _unitOfWork.Commit();
        }

        public void delete(int id)
        {
            _sinhVienRepository.Delete(id);
        }

        public IEnumerable<SinhVien> GetAll()
        {
            return _sinhVienRepository.GetAll();
        }

        public SinhVien GetByStringId(string Id)
        {
            return _sinhVienRepository.GetByStringId(Id);
        }

       

        public IQueryable<TTCN> GetTTCN(string Id)
        {
            return _sinhVienRepository.GetTTCN(Id);
        }

        public IQueryable<SinhVien> ThongTinSinhVien(string Id)
        {
            return _sinhVienRepository.ThongTinSinhVien(Id);
        }

        public void Update(SinhVien sinhVien)
        {
            _sinhVienRepository.Update(sinhVien);
        }

        public IEnumerable<chucnangxemthongtinsinhvien_dauvao> getthongtinsinhvien_dauvao(string mssv)
        {
            return _sinhVienRepository.getthongtinsinhvien_dauvao(mssv);
        }
    }
}
