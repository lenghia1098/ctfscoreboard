﻿using Platform.Data.Infrastructure;
using Platform.Data.Repositories;
using Platform.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Platform.Service
{
    public interface IViecPhaiLamVienChucService
    {
        void Add(ViecPhaiLamVienChuc viecPhaiLamVienChuc);
        void Update(ViecPhaiLamVienChuc viecPhaiLamVienChuc);
        void delete(int id);
        IEnumerable<ViecPhaiLamVienChuc> GetAll();
        IQueryable<chucnangviecphailamvienchuc> GetViecPhaiLamVienChucs(string msvc);
        string DemViecPhaiLamVienChuc(string MaSoVienChuc);
        ViecPhaiLamVienChuc GetByID(int id);
        IEnumerable<ViecDaLamVienChuc> ViecDaLamVienChuc(string msvc);
        IQueryable<ChiTietViecDaLamVienChuc> getChiTietViecDaLamVienChuc(int MaSoCongViec);


       

        void Commit();

    }
    public class ViecPhaiLamVienChucService : IViecPhaiLamVienChucService
    {
        IViecPhaiLamVienChucRepository _viecPhaiLamVienChucRepository;
        IUnitOfWork _unitOfWork;
        public ViecPhaiLamVienChucService(IViecPhaiLamVienChucRepository viecPhaiLamVienChucRepository, IUnitOfWork unitOfWork)
        {
            this._viecPhaiLamVienChucRepository = viecPhaiLamVienChucRepository;
            this._unitOfWork = unitOfWork;
        }
        public void Add(ViecPhaiLamVienChuc viecPhaiLamVienChuc)
        {
            _viecPhaiLamVienChucRepository.Add(viecPhaiLamVienChuc);
        }

        public void Commit()
        {
            _unitOfWork.Commit();
        }

        public void delete(int id)
        {
            _viecPhaiLamVienChucRepository.Delete(id);
        }

        public string DemViecPhaiLamVienChuc(string MaSoVienChuc)
        {
             return _viecPhaiLamVienChucRepository.DemViecPhaiLamVienChuc(MaSoVienChuc);
        }

        public IEnumerable<ViecPhaiLamVienChuc> GetAll()
        {
            return _viecPhaiLamVienChucRepository.GetAll();
        }

        public ViecPhaiLamVienChuc GetByID(int id)
        {
            return _viecPhaiLamVienChucRepository.GetSingleById(id);
        }

		public IQueryable<ChiTietViecDaLamVienChuc> getChiTietViecDaLamVienChuc(int MaSoCongViec)
        {
            return _viecPhaiLamVienChucRepository.getChiTietViecDaLamVienChuc(MaSoCongViec);
        }

       

        public IQueryable<chucnangviecphailamvienchuc> GetViecPhaiLamVienChucs(string msvc)
        {
            return _viecPhaiLamVienChucRepository.GetViecPhaiLamVienChucs(msvc);
        }

        public void Update(ViecPhaiLamVienChuc viecPhaiLamVienChuc)
        {
            _viecPhaiLamVienChucRepository.Update(viecPhaiLamVienChuc);
        }
        public IEnumerable<ViecDaLamVienChuc> ViecDaLamVienChuc(string msvc)
        {
            return _viecPhaiLamVienChucRepository.ViecDaLamVienChuc(msvc);
        }
    }
}
