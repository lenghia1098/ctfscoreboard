﻿using Platform.Data.Infrastructure;
using Platform.Data.Repositories;
using Platform.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Platform.Service
{
    public interface IPhongKhoa_VienChucService
    {
        void Add(PhongKhoa_VienChuc phongKhoa_VienChuc);
        void Update(PhongKhoa_VienChuc phongKhoa_VienChuc);
        void delete(int id);
        IEnumerable<PhongKhoa_VienChuc> GetAll();
        PhongKhoa_VienChuc GetByID(int id);
        void Save();

        void Commit();

    }
    public class PhongKhoa_VienChucService : IPhongKhoa_VienChucService
    {
        IPhongKhoa_VienChucRepository _phongKhoa_VienChucRepository;
        IUnitOfWork _unitOfWork;
        public PhongKhoa_VienChucService(IPhongKhoa_VienChucRepository phongKhoa_VienChucRepository, IUnitOfWork unitOfWork)
        {
            this._phongKhoa_VienChucRepository = phongKhoa_VienChucRepository;
            this._unitOfWork = unitOfWork;
        }
        public void Add(PhongKhoa_VienChuc phongKhoa_VienChuc)
        {
            _phongKhoa_VienChucRepository.Add(phongKhoa_VienChuc);
        }
        public void Save()
        {
            _unitOfWork.Commit();
        }
        public void Commit()
        {
            _unitOfWork.Commit();
        }

        public void delete(int id)
        {
            _phongKhoa_VienChucRepository.Delete(id);
        }

        public IEnumerable<PhongKhoa_VienChuc> GetAll()
        {
            return _phongKhoa_VienChucRepository.GetAll();
        }

        public PhongKhoa_VienChuc GetByID(int id)
        {
            return _phongKhoa_VienChucRepository.GetSingleById(id);
        }

        public void Update(PhongKhoa_VienChuc phongKhoa_VienChuc)
        {
            _phongKhoa_VienChucRepository.Update(phongKhoa_VienChuc);
        }
    }
}
