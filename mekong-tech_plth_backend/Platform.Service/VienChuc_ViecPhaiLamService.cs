﻿using Platform.Data.Infrastructure;
using Platform.Data.Repositories;
using Platform.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Platform.Service
{
    public interface IVienChuc_ViecPhaiLamService
    {
        void Add(VienChuc_ViecPhaiLam vienChuc_ViecPhaiLam);
        void Update(VienChuc_ViecPhaiLam vienChuc_ViecPhaiLam);
        void delete(int id);
        IEnumerable<VienChuc_ViecPhaiLam> GetAll();
        VienChuc_ViecPhaiLam GetByID(int id);
        VienChuc_ViecPhaiLam getIDViecPhaiLam(int mscv);
        IEnumerable<VienChuc_ViecPhaiLam> getIdAll(string msvc);
        void Commit();

    }
    public class VienChuc_ViecPhaiLamService : IVienChuc_ViecPhaiLamService
    {
        IVienChuc_ViecPhaiLamRepository _vienChuc_ViecPhaiLamRepository;
        IUnitOfWork _unitOfWork;
        public VienChuc_ViecPhaiLamService(IVienChuc_ViecPhaiLamRepository vienChuc_ViecPhaiLamRepository, IUnitOfWork unitOfWork)
        {
            this._vienChuc_ViecPhaiLamRepository = vienChuc_ViecPhaiLamRepository;
            this._unitOfWork = unitOfWork;
        }
        public void Add(VienChuc_ViecPhaiLam vienChuc_ViecPhaiLam)
        {
            _vienChuc_ViecPhaiLamRepository.Add(vienChuc_ViecPhaiLam);
        }

        public void Commit()
        {
            _unitOfWork.Commit();
        }

        public void delete(int id)
        {
            _vienChuc_ViecPhaiLamRepository.Delete(id);
        }

        public IEnumerable<VienChuc_ViecPhaiLam> GetAll()
        {
            return _vienChuc_ViecPhaiLamRepository.GetAll();
        }

        public VienChuc_ViecPhaiLam getIDViecPhaiLam(int mscv)
        {
            return _vienChuc_ViecPhaiLamRepository.getIDViecPhaiLam(mscv);
        }
        public VienChuc_ViecPhaiLam GetByID(int id)
        {
            return _vienChuc_ViecPhaiLamRepository.GetSingleById(id);
        }

        public void Update(VienChuc_ViecPhaiLam vienChuc_ViecPhaiLam)
        {
            _vienChuc_ViecPhaiLamRepository.Update(vienChuc_ViecPhaiLam);
        }

        public IEnumerable<VienChuc_ViecPhaiLam> getIdAll(string msvc)
        {
            return _vienChuc_ViecPhaiLamRepository.getIdAll(msvc);
        }
    }
}
