﻿using Platform.Data.Infrastructure;
using Platform.Data.Repositories;
using Platform.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Platform.Service
{
    public interface IDonSinhVienService
    {
        void Add(DonSinhVien donSinhVien);
        void Update(DonSinhVien donSinhVien);
        void delete(int id);
        IEnumerable<DonSinhVien> GetAll();
        IEnumerable<chucnangdondanop> GetChucnangdondanops(string mssv);
       
        string demdonsinhvien(string mssv);
        DonSinhVien GetByID(int id);
        IQueryable<chitietdondanop> ChiTietDonDaNop(int MaDon);
        void Save();
        void Commit();

    }
    public class DonSinhVienService : IDonSinhVienService
    {
        IDonSinhVienRepository _donSinhVienRepository;
        IUnitOfWork _unitOfWork;
        public DonSinhVienService(IDonSinhVienRepository donSinhVienRepository, IUnitOfWork unitOfWork)
        {
            this._donSinhVienRepository = donSinhVienRepository;
            this._unitOfWork = unitOfWork;
        }
        public void Save()
        {
            _unitOfWork.Commit();
        }
        public void Add(DonSinhVien donSinhVien)
        {
            _donSinhVienRepository.Add(donSinhVien);
        }

        public IQueryable<chitietdondanop> ChiTietDonDaNop(int MaDon)
        {
            return _donSinhVienRepository.ChiTietDonDaNop(MaDon);
        }

        public void Commit()
        {
            _unitOfWork.Commit();
        }

        public void delete(int id)
        {
            _donSinhVienRepository.Delete(id);
        }

        public string demdonsinhvien(string mssv)
        {
            return _donSinhVienRepository.demdonsinhvien(mssv);
        }

        public IEnumerable<DonSinhVien> GetAll()
        {
            return _donSinhVienRepository.GetAll();
        }

        public DonSinhVien GetByID(int id)
        {
            return _donSinhVienRepository.GetSingleById(id);
        }

       

        public IEnumerable<chucnangdondanop> GetChucnangdondanops(string mssv)
        {
            return _donSinhVienRepository.GetChucnangdondanops(mssv);
        }

        public void Update(DonSinhVien donSinhVien)
        {
            _donSinhVienRepository.Update(donSinhVien);
        }
    }
}
