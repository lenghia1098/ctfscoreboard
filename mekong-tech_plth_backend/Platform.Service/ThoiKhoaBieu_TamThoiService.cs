﻿using Platform.Data.Infrastructure;
using Platform.Data.Repositories;
using Platform.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Platform.Service
{
    public interface IThoiKhoaBieu_TamThoiService
    {
        void Add(ThoiKhoaBieu_TamThoi thoiKhoaBieu_TamThoi);
        void Update(ThoiKhoaBieu_TamThoi thoiKhoaBieu_TamThoi);
        void delete(int id);
        IEnumerable<ThoiKhoaBieu_TamThoi> GetAll();
        ThoiKhoaBieu_TamThoi GetByID(int id);
        void Save();

        void Commit();

    }
    public class ThoiKhoaBieu_TamThoiService : IThoiKhoaBieu_TamThoiService
    {
        IThoiKhoaBieu_TamThoiRepository _thoiKhoaBieu_TamThoiRepository;
        IUnitOfWork _unitOfWork;
        public ThoiKhoaBieu_TamThoiService(IThoiKhoaBieu_TamThoiRepository thoiKhoaBieu_TamThoiRepository, IUnitOfWork unitOfWork)
        {
            this._thoiKhoaBieu_TamThoiRepository = thoiKhoaBieu_TamThoiRepository;
            this._unitOfWork = unitOfWork;
        }
        public void Add(ThoiKhoaBieu_TamThoi thoiKhoaBieu_TamThoi)
        {
            _thoiKhoaBieu_TamThoiRepository.Add(thoiKhoaBieu_TamThoi);
        }
        public void Save()
        {
            _unitOfWork.Commit();
        }
        public void Commit()
        {
            _unitOfWork.Commit();
        }

        public void delete(int id)
        {
            _thoiKhoaBieu_TamThoiRepository.Delete(id);
        }

        public IEnumerable<ThoiKhoaBieu_TamThoi> GetAll()
        {
            return _thoiKhoaBieu_TamThoiRepository.GetAll();
        }

        public ThoiKhoaBieu_TamThoi GetByID(int id)
        {
            return _thoiKhoaBieu_TamThoiRepository.GetSingleById(id);
        }

        public void Update(ThoiKhoaBieu_TamThoi thoiKhoaBieu_TamThoi)
        {
            _thoiKhoaBieu_TamThoiRepository.Update(thoiKhoaBieu_TamThoi);
        }
    }
}
