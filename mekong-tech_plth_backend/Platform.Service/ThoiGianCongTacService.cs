﻿using Platform.Data.Infrastructure;
using Platform.Data.Repositories;
using Platform.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Platform.Service
{
    public interface IThoiGianCongTacService
    {
        void Add(ThoiGianCongTac thoiGianCongTac);
        void Update(ThoiGianCongTac thoiGianCongTac);
        void delete(int id);
        IEnumerable<ThoiGianCongTac> GetAll();
        ThoiGianCongTac GetByID(int id);
        void Save();
        IQueryable<ThoiGianCongTac> getThoiGianCongTac(string msvc);
        ThoiGianCongTac getid(int ID);
        void Commit();

    }
    public class ThoiGianCongTacService : IThoiGianCongTacService
    {
        IThoiGianCongTacRepository _thoiGianCongTacRepository;
        IUnitOfWork _unitOfWork;
        public ThoiGianCongTacService(IThoiGianCongTacRepository thoiGianCongTacRepository, IUnitOfWork unitOfWork)
        {
            this._thoiGianCongTacRepository = thoiGianCongTacRepository;
            this._unitOfWork = unitOfWork;
        }
        public void Add(ThoiGianCongTac thoiGianCongTac)
        {
            _thoiGianCongTacRepository.Add(thoiGianCongTac);
        }
        public void Save()
        {
            _unitOfWork.Commit();
        }
        public void Commit()
        {
            _unitOfWork.Commit();
        }

        public void delete(int id)
        {
            _thoiGianCongTacRepository.Delete(id);
        }

        public IEnumerable<ThoiGianCongTac> GetAll()
        {
            return _thoiGianCongTacRepository.GetAll();
        }

        public ThoiGianCongTac GetByID(int id)
        {
            return _thoiGianCongTacRepository.GetSingleById(id);
        }

        public void Update(ThoiGianCongTac thoiGianCongTac)
        {
            _thoiGianCongTacRepository.Update(thoiGianCongTac);
        }

        public IQueryable<ThoiGianCongTac> getThoiGianCongTac(string msvc)
        {
            return _thoiGianCongTacRepository.getThoiGianCongTac(msvc);
        }

        public ThoiGianCongTac getid(int ID)
        {
            return _thoiGianCongTacRepository.getid(ID);
        }
    }
}
