﻿/// <reference path="../../assets/giaovien/libs/angular/angular.js" />



(function () {
    angular.module('platformTH_GV',
        ['platformTH_GV.common',
            'platformTH_GV.chitietdondanop',
			 'platformTH_GV.chitietnhacnho',
            'platformTH_GV.dondanop',
            'platformTH_GV.tintuc',
            'platformTH_GV.thongbao',
            'platformTH_GV.viecphailam',
            'platformTH_GV.chitietviecdalam',
            'platformTH_GV.thongkevangmat',
            'platformTH_GV.viecdalam',
            'platformTH_GV.dangkycongviec',
            'platformTH_GV.dangkytrucbomon',
            'platformTH_GV.thongtincanhan',
            'platformTH_GV.inbangdiem',
            'platformTH_GV.giangday',
            'platformTH_GV.xemthoikhoabieu',
            'platformTH_GV.xemdangkymonhoc',
            'platformTH_GV.xembangdiem',
            'platformTH_GV.xemlichthigacthi',
            'platformTH_GV.xemhocphi',
            'platformTH_GV.xemlichgacthicanhan',
            'platformTH_GV.xemlichday',
            'platformTH_GV.xemlichdaycanhan',
            'platformTH_GV.thaydoikhunggiohoc',
            'platformTH_GV.tinhhocphi',
            'ngSanitize',
            'ui.bootstrap',
            'platformTH_GV.modangkymonhoc',
            'platformTH_GV.datngaycuahocky',
            'platformTH_GV.datngaynghi',
            'platformTH_GV.xemlichday',
            'platformTH_GV.xemlichdaycanhan',
            'platformTH_GV.xemthongtinsinhvientrungtuyen',
            'platformTH_GV.nhapsinhvientrungtuyen',
            'platformTH_GV.nhapnoidungdatabase',
            "checklist-model",
            'platformTH_GV.application_users',
            'platformTH_GV.application_roles',
            'platformTH_GV.application_groups',
            'platformTH_GV.nhapdiemtructuyen',
            'platformTH_GV.nhapdiem',
            'platformTH_GV.suadiem',
            'platformTH_GV.themthongtinvienchuc',
            'platformTH_GV.xemdanhsachphonghoc',
            'platformTH_GV.xemthongtinsinhvien',


            'platformTH_GV.dangkymonhoc',
            'platformTH_GV.viecphailam_sv',
            'platformTH_GV.dondanop_sv',
            'platformTH_GV.chitietnhacnho_sv',
            'platformTH_GV.thongbao_sv',
            'platformTH_GV.tintuc_sv',
            'platformTH_GV.viecdalam_sv',
            'platformTH_GV.chitietdondanop_sv',
            'platformTH_GV.thoikhoabieu_sv',
            'platformTH_GV.lichthi_sv',
            'platformTH_GV.bangdiem',
            'platformTH_GV.hocphi',
            'platformTH_GV.chuongtrinhdaotao',
            'platformTH_GV.nopdon',
            'platformTH_GV.thongtincanhan_sv',
            'platformTH_GV.doimatkhau',
            'platformTH_GV.taolichthi',
            'platformTH_GV.taothoikhoabieu',
            'platformTH_GV.ctf',
        ])

        .config(config)
        .config(configAuthentication);

    config.$inject = ['$stateProvider', '$urlRouterProvider'];
    window.appVersion = angular.element(document.getElementsByTagName('html')).attr('data-ver');

    function config($stateProvider, $urlRouterProvider) {
        $stateProvider.state('base', {
            url: '',
            templateUrl: '/app_giaovien/shared/views/baseView.html?v=' + window.appVersion,
            abstract: true
        }).state('dangnhap', {
            url: "/dangnhap",
            templateUrl: "app_giaovien/conponents/dangnhap/dangNhapView.html?v=" + window.appVersion,
            controller: "dangNhapController"
        }).state('trangchu_sv', {
            url: "/trangchu_sv",
            parent: 'base',
            templateUrl: "app_giaovien/conponents_sv/trangchu/trangChu_svView.html?v=" + window.appVersion,
            controller: "trangChu_svController"
        }).state('trangchu', {
            url: "/trangchu",
            parent: 'base',
            templateUrl: "app_giaovien/conponents/trangchu/trangChuView.html?v=" + window.appVersion,
            controller: "trangChuController"
        });
        $urlRouterProvider.otherwise('/dangnhap');

    }

    function configAuthentication($httpProvider) {
        $httpProvider.interceptors.push(function ($q, $location) {
            return {
                request: function (config) {

                    return config;
                },
                requestError: function (rejection) {

                    return $q.reject(rejection);
                },
                response: function (response) {
                    if (response.status == "401") {
                        $location.path('/dangnhap');
                    }
                    //the same response/modified/or a new one need to be returned.
                    return response;
                },
                responseError: function (rejection) {

                    if (rejection.status == "401") {
                        $location.path('/dangnhap');
                    }
                    return $q.reject(rejection);
                }
            };
        });
    }
})();