﻿/// <reference path="../../../assets/sinhvien/libs/angular/angular.js" />


(function () {
    angular.module('platformTH.inchuongtrinhdaotao', ['platformTH.common']).config(config);

    config.$inject = ['$stateProvider', '$urlRouterProvider'];
    window.appVersion = angular.element(document.getElementsByTagName('html')).attr('data-ver');

    function config($stateProvider, $urlRouterProvider) {
        $stateProvider.state('inchuongtrinhdaotao', {
            url: "/inchuongtrinhdaotao",
            parent: 'base',
            templateUrl: "app/conponents/inchuongtrinhdaotao/inChuongTrinhDaoTaoView.html?v=" + window.appVersion,
            controller: "inChuongTrinhDaoTaoController"
        });
        $urlRouterProvider.otherwise('/trangchu');
    }
})();