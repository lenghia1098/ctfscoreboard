﻿(function (app) {
    app.controller('lichThi_svController', lichThi_svController);
    lichThi_svController.$inject = ['$scope', 'apiService', '$stateParams', 'loginService', '$injector', 'notificationService'];
    function lichThi_svController($scope, apiService, $stateParams, loginService, $injector, notificationService) {
        if ($scope.kiemtrarole('SinhVien') === 'SinhVien') {

            //#region
            $scope.thongTincaNhan = [];
            $scope.lichThi = [];

            $scope.mssv = $scope.authentication.userName;;

            $scope.hocKy = "";


            $scope.getThongTinCaNhan = getThongTinCaNhan;
            $scope.getLichThi = getLichThi;

            function getThongTinCaNhan() {
                var config = {
                    params: {
                        Id: $scope.mssv,

                    }
                }
                apiService.get('/api/sinhvien/getTTCN/', config, function (result) {
                    $scope.thongTincaNhan = result.data;
                }, function () {
                    console.log('Load thông tin failed.');
                });

            }

            function getLichThi() {
                var config = {
                    params: {
                        mssv: $scope.mssv,
                        hocKy: $scope.hocKy,
                        namhoc: $scope.namhoc

                    }
                }
                apiService.get('/api/lichthi/getLichThi/', config, function (result) {
                    $scope.lichThi = result.data;
                }, function () {
                    console.log('Load thông tin failed.');
                });

            }
            $scope.getThongTinCaNhan();
            $scope.getLichThi();

            $scope.hocKyHienTai = [];
            $scope.getHocKyHienTai = getHocKyHienTai;
            function getHocKyHienTai() {
                apiService.get('/api/hockyhientai/getall/', null, function (result) {

                    $scope.hocKyHienTai = result.data[result.data.length - 1];
                    $scope.hocKy = result.data[result.data.length - 1].HocKy;
                    $scope.namhoc = result.data[result.data.length - 1].NamHoc;
                    $scope.getLichThi();
                }, function () {
                    console.log('Load thông tin failed.');
                });

            }
            $scope.getHocKyHienTai();
            //#endregion
        }
        else {
            loginService.logOut();
            var stateService = $injector.get('$state');
            stateService.go('dangnhap');
            notificationService.displayError("Bạn không có quyền truy cập trang này");

        }
       
    }
})(angular.module('platformTH_GV.lichthi_sv'));