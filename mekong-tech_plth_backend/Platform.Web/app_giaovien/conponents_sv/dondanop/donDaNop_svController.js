﻿(function (app) {
    app.controller('donDaNop_svController', donDaNop_svController);

    function donDaNop_svController($scope, apiService, $stateParams, $ngBootbox, notificationService, loginService, $injector) {
        if ($scope.kiemtrarole('SinhVien') === 'SinhVien') {

            //#region
            $scope.donDaNop = [];
            $scope.mssv = $scope.authentication.userName;;
            $scope.selected = [];



            $scope.exist = function (item) {
                return $scope.selected.indexOf(item) > -1;

            }

            $scope.toge = function (item) {
                var idx = $scope.selected.indexOf(item);
                if (idx > -1) {
                    $scope.selected.splice(idx, 1);

                }
                else {
                    $scope.selected.push(item);
                    console.log($scope.selected);
                }
            }


            $scope.checkAll = function () {
                if ($scope.selectedAll) {
                    angular.forEach($scope.viecDaLam, function (item) {
                        idx = $scope.selected.indexOf(item);
                        if (idx >= 0) {
                            return true;
                        } else {
                            $scope.selected.push(item);
                        }
                    })
                }
                else {
                    $scope.selected = [];
                }
            }


            $scope.DeleteAllDonDaNop = DeleteAllDonDaNop;


            function DeleteAllDonDaNop() {

                $ngBootbox.confirm('Bạn chắc muốn xóa không').then(function () {
                    angular.forEach($scope.selected, function (item) {
                        item.NguoiXoa = $scope.authentication.userName

                        apiService.put('api/don-sinhvien/updateAll', item, function (result) {

                            //notificationService.displaySuccess("Xóa thành công");
                            $scope.getDonDaNop();


                        }, function () {
                            notificationService.displaySuccess('Xóa không thành công');
                        });
                    });
                });

            };







            $scope.getDonDaNop = getDonDaNop;
            function getDonDaNop(page) {
                page = page || 0;
                var config = {
                    params: {
                        page: page,
                        mssv: $scope.mssv,
                        pageSize: 10,
                    }
                }
                apiService.get('/api/donsinhvien/GetChucnangdondanops/', config, function (result) {

                    $scope.donDaNop = result.data.Items;
                    $scope.page = result.data.Page;
                    $scope.pagesCount = result.data.TotalPages;
                    $scope.TotalCount = result.data.TotalCount;

                }, function () {
                    console.log('Load thông tin failed.');
                });

            }
            $scope.getDonDaNop();
            //#endregion
        }
        else {
            loginService.logOut();
            var stateService = $injector.get('$state');
            stateService.go('dangnhap');
            notificationService.displayError("Bạn không có quyền truy cập trang này");

        }
        

    }
})(angular.module('platformTH_GV.dondanop_sv'));