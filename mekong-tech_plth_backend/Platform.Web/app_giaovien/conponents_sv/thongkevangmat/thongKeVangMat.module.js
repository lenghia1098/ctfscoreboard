﻿/// <reference path="../../../assets/sinhvien/libs/angular/angular.js" />


(function () {
    angular.module('platformTH.thongkevangmat', ['platformTH.common']).config(config);

    config.$inject = ['$stateProvider', '$urlRouterProvider'];
    window.appVersion = angular.element(document.getElementsByTagName('html')).attr('data-ver');

    function config($stateProvider, $urlRouterProvider) {
        $stateProvider.state('thongkevangmat', {
            url: "/thongkevangmat",
            parent: 'base',
            templateUrl: "app/conponents/thongkevangmat/thongKeVangMatView.html?v=" + window.appVersion,
            controller: "thongKeVangMatController"
        });

    }
})();