﻿(function (app) {
    app.controller('chiTietDonDaNop_svController', chiTietDonDaNop_svController);

    function chiTietDonDaNop_svController($scope, apiService, $stateParams, loginService, $injector, notificationService) {
        if ($scope.kiemtrarole('SinhVien') === 'SinhVien') {

            //#region
            $scope.chiTietDonDaNop = [];
            $scope.MaDon = $stateParams.MaDon;


            $scope.getDonDaNop = getDonDaNop;


            function getDonDaNop() {
                var config = {
                    params: {
                        MaDon: $scope.MaDon
                    }
                }
                apiService.get('/api/donsinhvien/chitietdon/', config, function (result) {
                    $scope.chiTietDonDaNop = result.data;
                }, function () {
                    console.log('Load thông tin failed.');
                });

            }
            $scope.getDonDaNop();
            //#endregion
        }
        else {
            loginService.logOut();
            var stateService = $injector.get('$state');
            stateService.go('dangnhap');
            notificationService.displayError("Bạn không có quyền truy cập trang này");

        }
        

    }
})(angular.module('platformTH_GV.chitietdondanop_sv'));