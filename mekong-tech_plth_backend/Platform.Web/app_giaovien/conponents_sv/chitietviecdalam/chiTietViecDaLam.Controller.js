﻿(function (app) {
    app.controller('chiTietViecDaLamController', chiTietViecDaLamController);

    function chiTietViecDaLamController($scope, apiService, $stateParams, loginService, $injector, notificationService) {
        if ($scope.kiemtrarole('SinhVien') === 'SinhVien') {

            //#region
            $scope.chiTietViecDaLam = [];
            $scope.MaSoCongViec = $stateParams.MaSoCongViec;


            $scope.getChiTietViecDaLam = getChiTietViecDaLam;


            function getChiTietViecDaLam() {
                var config = {
                    params: {
                        MaSoCongViec: $scope.MaSoCongViec
                    }
                }
                apiService.get('/api/ViecPhaiLamSinhVien/getchitietviecdalam/', config, function (result) {
                    $scope.chiTietViecDaLam = result.data;
                }, function () {
                    console.log('Load thông tin failed.');
                });

            }
            $scope.getChiTietViecDaLam();
            //#endregion
        }
        else {
            loginService.logOut();
            var stateService = $injector.get('$state');
            stateService.go('dangnhap');
            notificationService.displayError("Bạn không có quyền truy cập trang này");

        }
       

    }
})(angular.module('platformTH.chitietviecdalam'));