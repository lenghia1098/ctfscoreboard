﻿/// <reference path="../../../assets/sinhvien/libs/angular/angular.js" />


(function () {
    angular.module('platformTH.chitietviecdalam', ['platformTH.common']).config(config);
    window.appVersion = angular.element(document.getElementsByTagName('html')).attr('data-ver');

    config.$inject = ['$stateProvider', '$urlRouterProvider'];

    function config($stateProvider, $urlRouterProvider) {
        $stateProvider.state('chitietviecdalam', {
            url: "/chitietviecdalam/:MaSoCongViec",
            parent: 'base',
            templateUrl: "app/conponents/chitietviecdalam/chiTietViecDaLamView.html?v=" + window.appVersion,
            controller: "chiTietViecDaLamController"
        });

    }
})();