﻿(function (app) {
    app.controller('tinTuc_svController', tinTuc_svController);

    function tinTuc_svController($scope, apiService, $stateParams, loginService, $injector, notificationService) {
        if ($scope.kiemtrarole('SinhVien') === 'SinhVien') {

            //#region
            $scope.tintuc = [];
            $scope.MaTinTuc = $stateParams.MaTinTuc;


            $scope.getchitiettintuc = getchitiettintuc;


            function getchitiettintuc() {
                var config = {
                    params: {
                        MaTinTuc: $scope.MaTinTuc
                    }
                }
                apiService.get('/api/tintuc/chitiettt/', config, function (result) {
                    $scope.tintuc = result.data;
                }, function () {
                    console.log('load thông tin failed.');
                });

            }
            $scope.getchitiettintuc();
            //#endregion
        }
        else {
            loginService.logOut();
            var stateService = $injector.get('$state');
            stateService.go('dangnhap');
            notificationService.displayError("Bạn không có quyền truy cập trang này");

        }
       

    }
})(angular.module('platformTH_GV.tintuc_sv'));