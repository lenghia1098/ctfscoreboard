﻿/// <reference path="../../../assets/sinhvien/libs/angular/angular.js" />


(function () {
    angular.module('platformTH_GV.chuongtrinhdaotao', ['platformTH_GV.common']).config(config);

    config.$inject = ['$stateProvider', '$urlRouterProvider'];
    window.appVersion = angular.element(document.getElementsByTagName('html')).attr('data-ver');

    function config($stateProvider, $urlRouterProvider) {
        $stateProvider.state('chuongtrinhdaotao', {
            url: "/chuongtrinhdaotao",
            parent: 'base',
            templateUrl: "app_giaovien/conponents_sv/chuongtrinhdaotao/chuongTrinhDaoTaoView.html?v=" + window.appVersion,
            controller: "chuongTrinhDaoTaoController"
        });
        $urlRouterProvider.otherwise('/trangchu');
    }
})();