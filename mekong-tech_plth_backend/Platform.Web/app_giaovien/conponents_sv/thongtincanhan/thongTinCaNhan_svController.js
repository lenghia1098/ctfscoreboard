﻿
(function (app) {
    app.controller('thongTinCaNhan_svController', thongTinCaNhan_svController);

    thongTinCaNhan_svController.$inject = ['$scope', 'apiService', '$stateParams', '$ngBootbox', 'notificationService', '$window', 'loginService', '$injector'];

    function thongTinCaNhan_svController($scope, apiService, $stateParams, $ngBootbox, notificationService, $window, loginService, $injector) {
        if ($scope.kiemtrarole('SinhVien') === 'SinhVien') {

            //#region
            $scope.thongTinCaNhan = [];
            $scope.moTa;
            $scope.soGioLamViec;
            $scope.mssv = $stateParams.Id;
            $scope.getThongTinCaNhan = getThongTinCaNhan;
            $scope.thongtin = {
                Status: true
            }
            $scope.update = update;

            function getThongTinCaNhan() {
                var config = {
                    params: {
                        Id: $scope.mssv
                    }
                }
                apiService.get('/api/sinhvien/getTTCN/', config, function (result) {
                    $scope.thongTinCaNhan = result.data;
                }, function () {
                    console.log('Load thông tin failed.');
                });

            }





            $scope.getThongTin = getThongTin;

            function getThongTin() {
                var config = {
                    params: {
                        Id: $scope.mssv
                    }
                }
                apiService.get('/api/sinhvien/getbyid', config, function (result) {
                    $scope.thongtin = result.data;
                }, function () {
                    console.log('Load thông tin failed.');
                });

            }

            function update() {
                $ngBootbox.confirm('Bạn chắc muốn Sửa  không').then(function () {
                    apiService.put('/api/sinhvien/update', $scope.thongtin, function (result) {
                        notificationService.displaySuccess("Sửa thành công");
                        $scope.getThongTin();
                        $('.modal').modal('hide');

                        //$window.location.reload();
                        console.log('Sửa thành công.');
                    }, function () {
                        console.log('sửa thông tin không thành công.');
                    });
                });

            }



            $scope.getThongTin();
            $scope.getThongTinCaNhan();

            $scope.showAlert = function () {

                $scope.showall = true;
            };
            //#endregion
        }
        else {
            loginService.logOut();
            var stateService = $injector.get('$state');
            stateService.go('dangnhap');
            notificationService.displayError("Bạn không có quyền truy cập trang này");

        }
        

       

    }
})(angular.module('platformTH_GV.thongtincanhan_sv'));