﻿/// <reference path="../../../assets/sinhvien/libs/angular/angular.js" />


(function () {
    angular.module('platformTH.inhocphi', ['platformTH.common']).config(config);

    config.$inject = ['$stateProvider', '$urlRouterProvider'];
    window.appVersion = angular.element(document.getElementsByTagName('html')).attr('data-ver');

    function config($stateProvider, $urlRouterProvider) {
        $stateProvider.state('inhocphi', {
            url: "/inhocphi",
            parent: 'base',
            templateUrl: "app/conponents/inhocphi/inHocPhiView.html?v=" + window.appVersion,
            controller: "inHocPhiController"
        });
        $urlRouterProvider.otherwise('/trangchu');
    }
})();