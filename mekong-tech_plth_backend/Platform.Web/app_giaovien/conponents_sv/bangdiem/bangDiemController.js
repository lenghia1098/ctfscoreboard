﻿(function (app) {
    app.controller('bangDiemController', bangDiemController);
    bangDiemController.$inject = ['$scope', 'apiService', '$stateParams', 'loginService', '$injector', 'notificationService'];

    function bangDiemController($scope, apiService, $stateParams, loginService, $injector, notificationService) {
        if ($scope.kiemtrarole('SinhVien') === 'SinhVien') {
            //#region
            //$scope.bangDiem = [];
            $scope.hockynamhoc = [];
            $scope.mssv = $scope.authentication.userName;;
            $scope.hocKy = [];
            $scope.HocKyNamHoc = [];
            $scope.allbangdiem = [];
            //$scope.getBangDiem = getBangDiem;
            $scope.thongTincaNhan = [];
            $scope.getThongTinCaNhan = getThongTinCaNhan;
            $scope.getHockynamhoc = getHockynamhoc;
            $scope.getAllBangDiem = getAllBangDiem;
            $scope.getHocKy = getHocKy;



            function getAllBangDiem() {
                $scope.showbangdiem = false;
                var config = {
                    params: {
                        mssv: $scope.mssv

                    }   
                }
                apiService.get('/api/ketqua/getAllBangDiem', config, function (result) {
                    $scope.allbangdiem = result.data;
                    $scope.showbangdiemtatca = true;

                }, function () {
                    console.log('Load thông tin failed.');

                });

            }

            function getHocKy() {
                var config = {
                    params: {
                        mssv: $scope.mssv

                    }
                }
                apiService.get('/api/ketqua/getHocKy', config, function (result) {
                    $scope.HocKyNamHoc = result.data;
                    $scope.showbangdiemtatca = false;

                }, function () {
                    console.log('Load thông tin failed.');

                });

            }


            function getHockynamhoc() {
                var config = {
                    params: {
                        mssv: $scope.mssv,
                        hocKy: $scope.hocKy.HocKy,
                        namHoc: $scope.hocKy.NamHoc



                    }
                }
                apiService.get('/api/ketqua/getBangDiemHocKy', config, function (result) {

                    $scope.hockynamhoc = result.data;
                    var sum = 0;
                    var sumTBHK = 0;
                    angular.forEach($scope.hockynamhoc, function (item) {
                        sum += item.SoTinChi
                    })
                    angular.forEach($scope.hockynamhoc, function (item) {
                        sumTBHK += (item.DiemTongKet * item.SoTinChi) / sum
                    })
                    $scope.sumTBHocKy = sumTBHK /*parseFloat(sumTBHK).toFixed(2);*/
                    $scope.sumTinChiHK = sum 



                }, function () {
                    console.log('Load thông tin failed.');
                });
                $scope.allbangdiem = [];


            }
            $scope.clickbangdiem = function () {

                $scope.showbangdiem = true;
                $scope.showbangdiemtatca = false;

            }


            function getThongTinCaNhan() {
                var config = {
                    params: {
                        Id: $scope.mssv
                    }
                }
                apiService.get('/api/sinhvien/getTTCN/', config, function (result) {
                    $scope.thongTincaNhan = result.data;
                }, function () {
                    console.log('Load thông tin failed.');
                });

            }

            $scope.clickbangdiem = function () {

                $scope.showbangdiem = true;
            }






            $scope.sumtchk = function (hocky, namhoc) {
                var sum = 0;
                var sumtbhk = 0;
                var myArray = [];

                $scope.allbangdiem.filter(function (item) {
                    if (item.HocKy == hocky && item.NamHoc == namhoc) {

                        myArray.push(item.LayBangDiemTheoHocKy);
                        for (var i = 0; i <= myArray[0].length - 1; i++) {

                            sum += item.LayBangDiemTheoHocKy[i].SoTinChi;

                            sumtbhk += (item.LayBangDiemTheoHocKy[i].DiemTongKet * item.LayBangDiemTheoHocKy[i].SoTinChi);
                        }

                    }
                })
                return sum;

            }


            $scope.sumtbhk = function (hocky, namhoc, sumtchk) {

                var sum = 0;
                var myArray = [];

                $scope.allbangdiem.filter(function (item) {

                    if (item.HocKy == hocky && item.NamHoc == namhoc) {

                        myArray.push(item.LayBangDiemTheoHocKy);
                        for (var i = 0; i <= myArray[0].length - 1; i++) {

                            //  sum += item.LayBangDiemTheoHocKy[i].SoTinChi;

                            sum += (item.LayBangDiemTheoHocKy[i].DiemTongKet * item.LayBangDiemTheoHocKy[i].SoTinChi) / sumtchk;
                        }

                    }
                })
                return sum;

            }


            $scope.sumTC = "";
            $scope.sumtchkHocKy = sumtchkHocKy;

            function sumtchkHocKy(item) {
                var sum = 0;
                var sumtbhk = 0;
                var myArray = [];
                myArray.push($scope.hockynamhoc);
                for (var i = 0; i <= myArray[0].length - 1; i++) {

                    sum += $scope.hockynamhoc[i].SoTinChi;

                    //sumtbhk += (item.HocKyNamHoc[i].DiemTongKet * item.HocKyNamHoc[i].SoTinChi);
                }



                return sum;
                $scope.sumTC = sum;
            }


            $scope.sumtbhkHocKy = function (hocky, namhoc, sumtchkHocKy) {

                var sum = 0;
                var myArray = [];

                $scope.HocKyNamHoc.filter(function (item) {

                    if (item.HocKy == hocky && item.NamHoc == namhoc) {

                        myArray.push(item.HocKyNamHoc);
                        for (var i = 0; i <= myArray[0].length - 1; i++) {

                            //  sum += item.LayBangDiemTheoHocKy[i].SoTinChi;

                            sum += (item.HocKyNamHoc[i].DiemTongKet * item.HocKyNamHoc[i].SoTinChi) / sumtchkHocKy;
                        }

                    }
                })
                return sum;

            }







            $scope.sumtc = function () {
                var sum = 0;


                $scope.allbangdiem.filter(function (item) {
                    if ($scope.allbangdiem.length > 0) {

                        item.LayBangDiemTheoHocKy.filter(function (item1) {

                            sum += item1.SoTinChi;
                        }

                        )
                    }

                })


                return sum;
            }
            $scope.sumtb = function (tong) {

                var sum = 0;
                var sumtc = 0;

                $scope.allbangdiem.filter(function (item) {
                    if ($scope.allbangdiem.length > 0) {

                        item.LayBangDiemTheoHocKy.filter(function (item1) {
                            sumtc += item1.SoTinChi
                            sum += (item1.DiemTongKet * item1.SoTinChi) / tong;
                        }

                        )
                    }

                })


                return sum.toFixed(2);
            }











            $scope.getThongTinCaNhan();
            //$scope.getBangDiem();
            //$scope.getHockynamhoc();
            //$scope.getAllBangDiem();
            $scope.getHocKy();

            //#endregion
        }
        else {
            loginService.logOut();
            var stateService = $injector.get('$state');
            stateService.go('dangnhap');
            notificationService.displayError("Bạn không có quyền truy cập trang này");

        }
        

    }
})(angular.module('platformTH_GV.bangdiem'));