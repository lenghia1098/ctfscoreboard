﻿/// <reference path="../../../assets/sinhvien/libs/angular/angular.js" />


(function () {
    angular.module('platformTH_GV.doimatkhau', ['platformTH_GV.common']).config(config);
    window.appVersion = angular.element(document.getElementsByTagName('html')).attr('data-ver');

    config.$inject = ['$stateProvider', '$urlRouterProvider'];

    function config($stateProvider, $urlRouterProvider) {
        $stateProvider.state('doimatkhau', {
            url: "/doimatkhau",
            templateUrl: "app_giaovien/conponents_sv/doimatkhau/doiMatKhauView.html?v=" + window.appVersion,
            controller: "doiMatKhauController"
        });
      

    }
})();