﻿(function (app) {
    app.controller('doiMatKhauController', doiMatKhauController);
    doiMatKhauController.$inject = ['$scope', 'apiService', '$stateParams', '$ngBootbox', 'notificationService', '$window', '$location'];

    function doiMatKhauController($scope, apiService, $stateParams, $ngBootbox, notificationService, $window, $location) {
        $scope.ChangePassword = {}
        $scope.update = update
        function update() {
            $ngBootbox.confirm('Bạn chắc muốn đổi mật khẩu không ?').then(function () {
                apiService.post('/api/Manage/DoiMatKhau', $scope.ChangePassword, addSuccessed, addFailed);
            });

        }
        function addSuccessed() {
            notificationService.displaySuccess('Đổi mật khẩu thành công');

            $location.url('trangchu');
        }
        function addFailed(response) {
           // $scope.errors = parseErrors(response);
            if (!response.data.ModelState) {
                notificationService.displayError(response.data.Message);

            }
            $scope.errors = [];
            for (var key in response.data.ModelState) {
                for (var i = 0; i < response.data.ModelState[key].length; i++) {
                    $scope.errors.push(response.data.ModelState[key][i]);
                    notificationService.displayError(response.data.ModelState[key][i]);
                }
            } 
            
        }
        
    }
})(angular.module('platformTH_GV.doimatkhau'));