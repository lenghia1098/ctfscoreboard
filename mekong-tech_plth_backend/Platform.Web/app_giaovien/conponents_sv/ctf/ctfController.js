﻿(function (app) {
    app.controller('ctfController', ctfController);
    ctfController.$inject = ['$scope', 'apiService', '$stateParams', 'loginService', '$injector', 'notificationService','$timeout','$q'];
    function ctfController($scope, apiService, $stateParams, loginService, $injector, notificationService, $timeout, $q) {
        $scope.ServiceScore = [];
        $scope.Service1ATScoreTeam1 = 0;
        $scope.Service1DFScoreTeam1 = 0;
        $scope.Service2ATScoreTeam1 = 0;
        $scope.Service2DFScoreTeam1 = 0;
        $scope.Service1ATScoreTeam2 = 0;
        $scope.Service1DFScoreTeam2 = 0;
        $scope.Service2ATScoreTeam2 = 0;
        $scope.Service2DFScoreTeam2 = 0;
        $scope.totalScoreAT1 = 0;
        $scope.totalScoreDF1 = 0;
        $scope.totalScoreAT2 = 0;
        $scope.totalScoreDF2 = 0;
        $scope.counter = 10;
        $scope.Round = 1;
        $scope.stringrandom1 = '';
        $scope.stringrandom2 = '';
        var nameservice = 1;
        $scope.onTimeout = function () {
            if ($scope.counter > 0) $scope.counter--;
            getService();
            $scope.mytimeout = $timeout($scope.onTimeout, 1000);
            if ($scope.counter == 0) {
                $scope.counter = 10;
                var service2 = {
                    ServiceName: "Service" + $scope.Round
                }
                apiService.post('/api/ServiceCTF/create/', service2, function (result) {
                }, function () {
                });
                $scope.radom();
                apiService.get('/api/Flags/getall/', null, function (result) {
                    result.data[0].Flat = 'SVATTT{'+$scope.stringrandom1+'}';
                    result.data[1].Flat = 'SVATTT{' + $scope.stringrandom2 + '}';

                    var promises = [];
                    angular.forEach(result.data, function (item) {
                        var defer = $q.defer();
                        //kiem tra khong tan cong
                        if (item.IsAttack == false) {
                            var service = {
                                DefenseScore: "1000",
                                TeamID: item.TeamID,
                            }
                            apiService.post('/api/ServiceCTF/createhoa/', service, function (result) {
                                defer.resolve(item);
                            }, function () {
                            });
                        }
                        apiService.put('/api/Flags/update/', item, function (result) {
                            console.log('Thanh cong');
                            
                        }, function () {
                            console.log('Load thông tin failed.');
                        });
                        promises.push(defer.promise);

                    });
                    ////
                    //$q.all(promises).then(function () {
                    //    nameservice += 1;
                    //    var service1 = {
                    //        ServiceName: "Service" + $scope.Round
                    //    }
                    //    apiService.post('/api/ServiceCTF/create/', service1, function (result) {
                    //    }, function () {
                    //    });


                    //});
                   

                   
                    
                }, function () {
                    console.log('Load thông tin failed.');
                });

               
                $scope.Round++;


            }
        }
        $scope.mytimeout = function () {
            $timeout($scope.onTimeout, 1000);
            $scope.showstart = false;
            $scope.showstop = true;
            $scope.showresum = false;
        }
        $scope.getService = getService;
        function getService() {


            apiService.get('/api/ServiceCTF/getall', null, function (result) {
                $scope.ServiceScore = result.data;

                angular.forEach(result.data, function (item) {
                    //if ($scope.Round == 2) {
                    //    $scope.Service1ATScoreTeam1 = 0;
                    //    $scope.Service1DFScoreTeam1 = 0;
                    //    $scope.Service1ATScoreTeam2 = 0;
                    //    $scope.Service1DFScoreTeam2 = 0;
                    //}
                    //if ($scope.Round == 3) {
                    //    $scope.Service2ATScoreTeam1 = 0;
                    //    $scope.Service2DFScoreTeam1 = 0;
                    //    $scope.Service2ATScoreTeam2 = 0;
                    //    $scope.Service2DFScoreTeam2 = 0;
                    //}
                    if ($scope.Round == 2) {

                        if (item.ServiceName == "Service1") {
                            if (item.TeamID == "1") {
                                if (item.AttackScore != null) {
                                    $scope.Service1ATScoreTeam1 = Number(item.AttackScore);
                                }
                                if (item.DefenseScore != null) {
                                    $scope.Service1DFScoreTeam1 = Number(item.DefenseScore);
                                }
                                

                            }
                            else if (item.TeamID == "2") {
                                if (item.AttackScore != null) {
                                    $scope.Service1ATScoreTeam2 = Number(item.AttackScore);
                                }
                                if (item.DefenseScore != null) {
                                    $scope.Service1DFScoreTeam2 = Number(item.DefenseScore);
                                }

                                
                                
                            }

                        }
                    }
                    else if ($scope.Round == 3) {



                          if (item.ServiceName == "Service2") {
                              if (item.TeamID == "1") {
                                  if (item.AttackScore != null) {
                                      $scope.Service2ATScoreTeam1 = Number(item.AttackScore);
                                  }
                                  if (item.DefenseScore != null) {
                                      $scope.Service2DFScoreTeam1 = Number(item.DefenseScore);
                                  }
                                
                                
                            }
                              else if (item.TeamID == "2") {
                                  if (item.AttackScore != null) {
                                      $scope.Service2ATScoreTeam2 = Number(item.AttackScore);
                                  }
                                  if (item.DefenseScore != null) {
                                      $scope.Service2DFScoreTeam2 = Number(item.DefenseScore);
                                  }
                                
                                
                            }
                        }
                    }
                    $scope.totalScoreAT1 = $scope.Service1ATScoreTeam1 + $scope.Service2ATScoreTeam1;
                    $scope.totalScoreDF1 = $scope.Service1DFScoreTeam1 + $scope.Service2DFScoreTeam1;
                    $scope.totalScoreAT2 = $scope.Service1ATScoreTeam2 + $scope.Service2ATScoreTeam2;
                    $scope.totalScoreDF2 = $scope.Service1DFScoreTeam2 + $scope.Service2DFScoreTeam2;

                });
            }, function () {
                console.log('Load thông tin failed.');
            });
        }
        $scope.deleteService = deleteService;
        function deleteService() {


            apiService.del('/api/ServiceCTF/deleteall', null, function (result) {
                $scope.Service1ATScoreTeam1 = 0;
                $scope.Service1DFScoreTeam1 = 0;
                $scope.Service2ATScoreTeam1 = 0;
                $scope.Service2DFScoreTeam1 = 0;
                $scope.Service1ATScoreTeam2 = 0;
                $scope.Service1DFScoreTeam2 = 0;
                $scope.Service2ATScoreTeam2 = 0;
                $scope.Service2DFScoreTeam2 = 0;
                $scope.totalScoreAT1 = 0;
                $scope.totalScoreDF1 = 0;
                $scope.totalScoreAT2 = 0;
                $scope.totalScoreDF2 = 0;

                $scope.counter = 10;
                $scope.Round = 1;
            }, function () {
                console.log('Load thông tin failed.');
            });
        }
        $scope.taoservice = taoservice;
        function taoservice() {
            var service = {
                ServiceName: "Service" + $scope.Round
            }
            apiService.post('/api/ServiceCTF/create/', service, function (result) {
            }, function () {
            });
        }
        $scope.radom = function () {
            $scope.stringrandom1 = '';
            $scope.stringrandom2 = '';
            var characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
            var charactersLength = characters.length;
            for (var i = 0; i < 7; i++) {
                $scope.stringrandom1 += characters.charAt(Math.floor(Math.random() * charactersLength));
                $scope.stringrandom2 += characters.charAt(Math.floor(Math.random() * charactersLength));
            }
           
            //return result;
           
        };
        
        $scope.info = true


        $scope.showstart = true;
        $scope.showstop = false;
        $scope.showresum = false;
        $scope.stop = function () {
            $scope.showstart = false;
            $scope.showstop = false;
            $scope.showresum = true;
            $timeout.cancel($scope.mytimeout);
        }
        $scope.resum = function () {
            $scope.showstart = false;
            $scope.showstop = true;
            $scope.showresum = false;
            $timeout($scope.onTimeout, 1000);
        }
        deleteService();
        taoservice();
    }
})(angular.module('platformTH_GV.ctf'));