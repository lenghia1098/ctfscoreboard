﻿/// <reference path="../../../assets/sinhvien/libs/angular/angular.js" />


(function () {
    angular.module('platformTH_GV.ctf', ['platformTH_GV.common']).config(config);

    config.$inject = ['$stateProvider', '$urlRouterProvider'];
    window.appVersion = angular.element(document.getElementsByTagName('html')).attr('data-ver');

    function config($stateProvider, $urlRouterProvider) {
        $stateProvider.state('ctf', {
            url: "/ctf",
            parent: 'base',
            templateUrl: "app_giaovien/conponents_sv/ctf/ctfView.html?v=" + window.appVersion,
            controller: "ctfController"
        });
    }
})();