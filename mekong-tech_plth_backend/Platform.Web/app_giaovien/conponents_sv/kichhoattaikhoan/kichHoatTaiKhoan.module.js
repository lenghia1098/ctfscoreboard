﻿/// <reference path="../../../assets/sinhvien/libs/angular/angular.js" />


(function () {
    angular.module('platformTH.kichhoattaikhoan', ['platformTH.common']).config(config);

    config.$inject = ['$stateProvider', '$urlRouterProvider'];
    window.appVersion = angular.element(document.getElementsByTagName('html')).attr('data-ver');

    function config($stateProvider, $urlRouterProvider) {
        $stateProvider.state('kichhoattaikhoan', {
            url: "/kichhoattaikhoan",
            templateUrl: "app/conponents/kichhoattaikhoan/kichHoatTaiKhoanView.html?v=" + window.appVersion,
            controller: "kichHoatTaiKhoanController"
        });
      

    }
})();