﻿/// <reference path="../../../assets/sinhvien/libs/angular/angular.js" />


(function () {
    angular.module('platformTH_GV.chitietnhacnho_sv', ['platformTH_GV.common']).config(config);

    config.$inject = ['$stateProvider', '$urlRouterProvider'];
    window.appVersion = angular.element(document.getElementsByTagName('html')).attr('data-ver');

    function config($stateProvider, $urlRouterProvider) {
        $stateProvider.state('chitietnhacnho_sv', {
            url: "/chitietnhacnho_sv/:MaSoNN",
            parent: 'base',
            templateUrl: "app_giaovien/conponents_sv/chitietnhacnho/chiTietNhacNho_svView.html?v=" + window.appVersion,
            controller: "chiTietNhacNho_svController"
        });

    }
})();