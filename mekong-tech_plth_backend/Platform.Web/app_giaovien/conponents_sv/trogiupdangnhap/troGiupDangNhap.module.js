﻿/// <reference path="../../../assets/sinhvien/libs/angular/angular.js" />


(function () {
    angular.module('platformTH.trogiupdangnhap', ['platformTH.common']).config(config);
    window.appVersion = angular.element(document.getElementsByTagName('html')).attr('data-ver');

    config.$inject = ['$stateProvider', '$urlRouterProvider'];

    function config($stateProvider, $urlRouterProvider) {
        $stateProvider.state('trogiupdangnhap', {
            url: "/trogiupdangnhap",
            templateUrl: "app/conponents/trogiupdangnhap/troGiupDangNhapView.html?v=" + window.appVersion,
            controller: "troGiupDangNhapController"
        });


    }
})();