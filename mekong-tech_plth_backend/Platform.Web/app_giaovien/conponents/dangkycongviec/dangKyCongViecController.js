﻿(function (app) {
    app.controller('dangKyCongViecController', dangKyCongViecController);
    dangKyCongViecController.$inject = ['$scope', 'apiService', '$stateParams', 'loginService', '$injector', 'notificationService'];

    function dangKyCongViecController($scope, apiService, $stateParams, loginService, $injector, notificationService) {
        if ($scope.kiemtrarole('SinhVien') === 'SinhVien' && $scope.kiemtrarole('Admin') != 'Admin') {
            loginService.logOut();
            var stateService = $injector.get('$state');
            stateService.go('dangnhap');
            notificationService.displayError("Bạn không có quyền truy cập trang này");
        }
        else {
            //#region
            $scope.congViecDeXuat = [];
            $scope.getCongViecDeXuat = getCongViecDeXuat;
            function getCongViecDeXuat() {
                var config = {

                }
                apiService.get('/api/congviecdexuat/getall/', null, function (result) {
                    $scope.congViecDeXuat = result.data;
                }, function () {
                    console.log('Load thông tin failed.');
                });

            }
            $scope.getCongViecDeXuat();
            //#endregion

        }
       
    }
})(angular.module('platformTH_GV.dangkycongviec'));