﻿/// <reference path="../../../assets/giaovien/libs/angular/angular.js" />



(function () {
    angular.module('platformTH_GV.thongtincanhan', ['platformTH_GV.common']).config(config);

    config.$inject = ['$stateProvider', '$urlRouterProvider'];
    window.appVersion = angular.element(document.getElementsByTagName('html')).attr('data-ver');

    function config($stateProvider, $urlRouterProvider) {

        $stateProvider.state('thongtincanhan', {
            url: "/thongtincanhan",
            parent: 'base',
            templateUrl: "app_giaovien/conponents/thongtincanhan/thongTinCaNhanView.html?v=" + window.appVersion,
            controller: "thongTinCaNhanController"
        }).state('lylichcanhan', {
            url: "/lylichcanhan",
            parent: 'base',
            templateUrl: "app_giaovien/conponents/thongtincanhan/lyLichCaNhanView.html?v=" + window.appVersion,
                controller: "lyLichCaNhanController"
           }).state('trinhdongoaingu', {
               url: "/trinhdongoaingu",
               parent: 'base',
               templateUrl: "app_giaovien/conponents/thongtincanhan/trinhDoNgoaiNguView.html?v=" + window.appVersion,
                controller: "trinhDoNgoaiNguController"
            }).state('thoigiancongtac', {
                url: "/thoigiancongtac",
                parent: 'base',
                templateUrl: "app_giaovien/conponents/thongtincanhan/thoiGianCongTacView.html?v=" + window.appVersion,
                controller: "thoiGianCongTacController"
            }).state('quatrinhdaotao', {
                url: "/quatrinhdaotao",
                parent: 'base',
                templateUrl: "app_giaovien/conponents/thongtincanhan/quaTrinhDaoTaoView.html?v=" + window.appVersion,
                controller: "quaTrinhDaoTaoController"
            });

    }
})();