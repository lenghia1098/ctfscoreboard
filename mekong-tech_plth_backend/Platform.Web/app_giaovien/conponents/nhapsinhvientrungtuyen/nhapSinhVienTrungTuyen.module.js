﻿/// <reference path="../../../assets/giaovien/libs/angular/angular.js" />


(function () {
    angular.module('platformTH_GV.nhapsinhvientrungtuyen', ['platformTH_GV.common']).config(config);

    config.$inject = ['$stateProvider', '$urlRouterProvider'];
    window.appVersion = angular.element(document.getElementsByTagName('html')).attr('data-ver');

    function config($stateProvider, $urlRouterProvider) {
        $stateProvider.state('nhapsinhvientrungtuyen', {
            url: "/nhapsinhvientrungtuyen",
            parent: 'base',
            templateUrl: "app_giaovien/conponents/nhapsinhvientrungtuyen/nhapSinhVienTrungTuyenView.html?v=" + window.appVersion,
            controller: "nhapSinhVienTrungTuyenController"
        });

    }
})();