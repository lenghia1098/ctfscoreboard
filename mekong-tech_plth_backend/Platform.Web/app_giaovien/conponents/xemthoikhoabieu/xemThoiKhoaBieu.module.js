﻿/// <reference path="../../../assets/giaovien/libs/angular/angular.js" />


(function () {
    angular.module('platformTH_GV.xemthoikhoabieu', ['platformTH_GV.common']).config(config);
    window.appVersion = angular.element(document.getElementsByTagName('html')).attr('data-ver');

    config.$inject = ['$stateProvider', '$urlRouterProvider'];

    function config($stateProvider, $urlRouterProvider) {
        $stateProvider.state('xemthoikhoabieu', {
            url: "/xemthoikhoabieu",

            parent: 'base',
            templateUrl: "app_giaovien/conponents/xemthoikhoabieu/xemThoiKhoaBieuView.html?v=" + window.appVersion,
            controller: "xemThoiKhoaBieuController"
        });

    }
})();