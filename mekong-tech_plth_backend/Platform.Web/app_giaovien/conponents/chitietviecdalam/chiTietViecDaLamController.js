﻿(function (app) {
    app.controller('chiTietViecDaLamController', chiTietViecDaLamController);

    function chiTietViecDaLamController($scope, apiService, $stateParams, loginService, $injector, notificationService) {
        if ($scope.kiemtrarole('SinhVien') === 'SinhVien' && $scope.kiemtrarole('Admin') != 'Admin') {
            loginService.logOut();
            var stateService = $injector.get('$state');
            stateService.go('dangnhap');
            notificationService.displayError("Bạn không có quyền truy cập trang này");
        }
        else {
            //#region
            $scope.chiTietViecDaLam = [];
            $scope.MaSoCongViec = $stateParams.MaSoCongViec;


            $scope.getChiTietViecDaLam = getChiTietViecDaLam;


            function getChiTietViecDaLam() {
                var config = {
                    params: {
                        MaSoCongViec: $scope.MaSoCongViec
                    }
                }
                apiService.get('/api/ViecPhaiLamVienChuc/getChiTietViecDaLamVienChuc/', config, function (result) {
                    $scope.chiTietViecDaLam = result.data;
                }, function () {
                    console.log('Load thông tin failed.');
                });

            }
            $scope.getChiTietViecDaLam();
            //#endregion

        }
        

    }
})(angular.module('platformTH_GV.chitietviecdalam'));