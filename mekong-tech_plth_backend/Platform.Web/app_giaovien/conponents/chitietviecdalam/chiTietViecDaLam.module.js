﻿/// <reference path="../../../assets/giaovien/libs/angular/angular.js" />


(function () {
    angular.module('platformTH_GV.chitietviecdalam', ['platformTH_GV.common']).config(config);
    window.appVersion = angular.element(document.getElementsByTagName('html')).attr('data-ver');

    config.$inject = ['$stateProvider', '$urlRouterProvider'];

    function config($stateProvider, $urlRouterProvider) {
        $stateProvider.state('chitietviecdalam', {
            url: "/chitietviecdalam/:MaSoCongViec",
            parent: 'base',
            templateUrl: "app_giaovien/conponents/chitietviecdalam/chiTietViecDaLamView.html?v=" + window.appVersion,
            controller: "chiTietViecDaLamController"
        });

    }
})();