﻿(function (app) {
    app.controller('moDangKyMonHocController', moDangKyMonHocController);
    moDangKyMonHocController.$inject = ['$scope', 'apiService', '$stateParams', '$window', 'notificationService', '$ngBootbox', 'loginService', '$injector'];
    function moDangKyMonHocController($scope, apiService, $stateParams, $window, notificationService, $ngBootbox, loginService, $injector) {
        if ($scope.kiemtrarole('SinhVien') === 'SinhVien' && $scope.kiemtrarole('Admin') != 'Admin') {
            loginService.logOut();
            var stateService = $injector.get('$state');
            stateService.go('dangnhap');
            notificationService.displayError("Bạn không có quyền truy cập trang này");
        }
        else {
            //#region
            $scope.tenVienChuc = $scope.authentication.userName;
            $scope.thoiHanDangKyMonHoc = {
                NguoiTao: $scope.tenVienChuc,

                ChoPhep: false
            }

            $scope.AddThoiHanDangKyMonHoc = AddThoiHanDangKyMonHoc;
            function AddThoiHanDangKyMonHoc() {
                if ($scope.thoiHanDangKyMonHoc.NgayCuoi - $scope.thoiHanDangKyMonHoc.NgayDau == 0) {
                    alert('Ngày cuối phải cách ngày đầu ít nhất một ngày')
                }
                else if ($scope.thoiHanDangKyMonHoc.NgayDau - $scope.thoiHanDangKyMonHoc.NgayCuoi > 0) {
                    alert('Ngày cuối phải tính từ ngày đầu sau 1 ngày')
                }
                else {
                    apiService.post('api/thoihandangkymonhoc/create', $scope.thoiHanDangKyMonHoc,
                        function (result) {
                            notificationService.displaySuccess("Mở đăng ký môn học thành công")
                            $scope.thoiHanDangKyMonHoc = {
                                NguoiTao: $scope.tenVienChuc,

                                ChoPhep: false
                            };

                            $scope.getThoiHanDangKyMonHoc();
                        }, function () {
                            console.log('Thêm mới không thành công.');
                        })
                        ;
                }
            }
            $scope.thoiHanDangKyMonHocAll = [];


            $scope.getThoiHanDangKyMonHoc = getThoiHanDangKyMonHoc;

            function getThoiHanDangKyMonHoc() {

                apiService.get('/api/thoihandangkymonhoc/getall', null, function (result) {

                    $scope.thoiHanDangKyMonHocAll = result.data;

                }, function () {
                    console.log('Load thoihandangkymonhoc failed.');
                });
            }
            $scope.delete = [];


            $scope.deleteDangKyMH = deleteDangKyMH;

            function deleteDangKyMH(ID) {
                $ngBootbox.confirm('Bạn có chắc muốn xóa?').then(function () {
                    var config = {
                        params: {
                            ID: ID
                        }
                    }
                    apiService.del('api/thoihandangkymonhoc/delete', config, function () {
                        notificationService.displaySuccess("Đã xóa thành công")
                        $scope.getThoiHanDangKyMonHoc();
                    }, function () {
                        notificationService.displayWarning("Xóa không thành công")

                    })
                });
            }
            $scope.mobang = function () {
                $scope.moDKMH = true
            }
            $scope.getThoiHanDangKyMonHoc();
            //#endregion
        }
        

    }
})(angular.module('platformTH_GV.xemdangkymonhoc'));


