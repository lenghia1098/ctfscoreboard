﻿/// <reference path="../../../assets/giaovien/libs/angular/angular.js" />


(function () {
    angular.module('platformTH_GV.modangkymonhoc', ['platformTH_GV.common']).config(config);
    window.appVersion = angular.element(document.getElementsByTagName('html')).attr('data-ver');

    config.$inject = ['$stateProvider', '$urlRouterProvider'];

    function config($stateProvider, $urlRouterProvider) {
        $stateProvider.state('modangkymonhoc', {
            url: "/modangkymonhoc",

            parent: 'base',
            templateUrl: "app_giaovien/conponents/modangkymonhoc/moDangKyMonHocView.html?v=" + window.appVersion,
            controller: "moDangKyMonHocController"
        });

    }
})();