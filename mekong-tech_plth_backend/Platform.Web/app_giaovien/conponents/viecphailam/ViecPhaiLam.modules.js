﻿/// <reference path="../../../assets/giaovien/libs/angular/angular.js" />

(function () {
    angular.module('platformTH_GV.viecphailam', ['platformTH_GV.common']).config(config);

    config.$inject = ['$stateProvider', '$urlRouterProvider'];
    window.appVersion = angular.element(document.getElementsByTagName('html')).attr('data-ver');

    function config($stateProvider, $urlRouterProvider) {
        $stateProvider.state('viecphailam', {
            url: "/viecphailam",
            parent: 'base',
            templateUrl: "app_giaovien/conponents/viecphailam/viecPhaiLamView.html?v=" + window.appVersion,
            controller: "viecPhaiLamController"
        });

    }
})();