﻿(function (app) {
    app.controller('viecPhaiLamController', viecPhaiLamController);

    function viecPhaiLamController($scope, apiService, $stateParams, loginService, $injector, notificationService) {
        if ($scope.kiemtrarole('SinhVien') === 'SinhVien' && $scope.kiemtrarole('Admin') != 'Admin') {
            loginService.logOut();
            var stateService = $injector.get('$state');
            stateService.go('dangnhap');
            notificationService.displayError("Bạn không có quyền truy cập trang này");
        }
        else {
            //#region
            $scope.viecPhaiLam = [];
            $scope.msvc = $scope.authentication.userName;
            $scope.getViecPhaiLam = getViecPhaiLam;




            function getViecPhaiLam() {
                var config = {
                    params: {
                        msvc: $scope.msvc
                    }
                }
                apiService.get('/api/ViecPhaiLamVienChuc/GetViecPhaiLamVienChucs/', config, function (result) {
                    $scope.viecPhaiLam = result.data;
                }, function () {
                    console.log('Load thông tin failed.');
                });

            }
            $scope.getViecPhaiLam();
            //#endregion

        }
        

    }
})(angular.module('platformTH_GV.viecphailam'));


