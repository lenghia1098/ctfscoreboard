﻿/// <reference path="../../../assets/giaovien/libs/angular/angular.js" />
(function () {
    angular.module('platformTH_GV.giangday', ['platformTH_GV.common']).config(config);
    window.appVersion = angular.element(document.getElementsByTagName('html')).attr('data-ver');

    config.$inject = ['$stateProvider', '$urlRouterProvider'];

    function config($stateProvider, $urlRouterProvider) {
        $stateProvider.state('giangday', {
            parent: 'base',
            url: "/giangday",
            templateUrl: "app_giaovien/conponents/giangday/giangDayView.html?v="+ window.appVersion,
            controller: "giangDayController"
        }).state('danhsachlop', {
            url: "/danhsachlop",
            parent: 'base',
            templateUrl: "app_giaovien/conponents/giangday/danhSachLopView.html?v=" + window.appVersion,
                controller: "danhSachLopController"
            }).state('dangkynghiday_daybu', {
                url: "/dangkynghiday_daybu",
                parent: 'base',
                templateUrl: "app_giaovien/conponents/giangday/dangKyNghiDay_DayBuView.html?v=" + window.appVersion,
                controller: "dangKyNghiDay_DayBuController"
            }).state('khoiluonggiangday', {
                url: "/khoiluonggiangday",
                parent: 'base',
                templateUrl: "app_giaovien/conponents/giangday/khoiLuongGiangDayView.html?v=" + window.appVersion,
                controller: "khoiLuongGiangDayController"
            }).state('lichthi', {
                url: "/lichthi",
                parent: 'base',
                templateUrl: "app_giaovien/conponents/giangday/lichThiView.html?v=" + window.appVersion,
                controller: "lichThiController"
            }).state('nhapdiem2', {
                url: "/nhapdiem",
                parent: 'base',
                templateUrl: "app_giaovien/conponents/giangday/nhapDiemView.html?v=" + window.appVersion,
                controller: "nhapDiemController"
            }).state('thoikhoabieu', {
                url: "/thoikhoabieu",
                parent: 'base',
                templateUrl: "app_giaovien/conponents/giangday/thoiKhoaBieuView.html?v=" + window.appVersion,
                controller: "thoiKhoaBieuController"
            }).state('huynghiday', {
                url: "/huynghiday",
                parent: 'base',
                templateUrl: "app_giaovien/conponents/giangday/huyNghiDayView.html?v=" + window.appVersion,
                controller: "huyNghiDayController"
            }).state('huydaybu', {
                url: "/huydaybu",
                parent: 'base',
                templateUrl: "app_giaovien/conponents/giangday/huyDayBuView.html?v=" + window.appVersion,
                controller: "huyDayBuController"
            });

    }
})();