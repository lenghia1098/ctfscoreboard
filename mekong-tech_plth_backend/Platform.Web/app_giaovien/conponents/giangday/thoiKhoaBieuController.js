﻿(function (app) {
    app.controller('thoiKhoaBieuController', thoiKhoaBieuController);
    thoiKhoaBieuController.$inject = ['$scope', 'apiService'];
    function thoiKhoaBieuController($scope, apiService) {
        $scope.thoikhoabieu = [];
        $scope.getThoikhoabieu = getThoikhoabieu;
        $scope.MaSoVC = $scope.authentication.userName;
        $scope.hocKy = "2";
        $scope.tuan = "1";
        function getThoikhoabieu() {
            var config = {
                params: {
                    MaSoVC: $scope.MaSoVC,
                    hocKy: $scope.hocKy,
                    tuan: $scope.tuan
                }
            }
            apiService.get('/api/thoikhoabieu/getTKB', config, function (result) {

                $scope.thoikhoabieu = result.data;

            }, function () {
                console.log('Load product failed.');
            });
        }
        $scope.getThoikhoabieu();
    }
})(angular.module('platformTH_GV.giangday'));