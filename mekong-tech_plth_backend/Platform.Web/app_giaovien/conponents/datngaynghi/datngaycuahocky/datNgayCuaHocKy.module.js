﻿/// <reference path="../../../assets/giaovien/libs/angular/angular.js" />

(function () {
    angular.module('platformTH_GV.datngaycuahocky', ['platformTH_GV.common']).config(config);
    window.appVersion = angular.element(document.getElementsByTagName('html')).attr('data-ver');

    config.$inject = ['$stateProvider', '$urlRouterProvider'];

    function config($stateProvider, $urlRouterProvider) {
        $stateProvider.state('datngaycuahocky', {
            url: "/datngaycuahocky/",
            parent: 'base',
            templateUrl: "app_giaovien/conponents/datngaycuahocky/datNgayCuaHocKyView.html?v=" + window.appVersion,
            controller: "datNgayCuaHocKyController"
        });

    }
})();