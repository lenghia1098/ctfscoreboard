﻿(function (app) {
    app.controller('datNgayCuaHocKyController', datNgayCuaHocKyController);
    datNgayCuaHocKyController.$inject = ['$scope', 'apiService', '$window', '$ngBootbox', 'notificationService'];
    function datNgayCuaHocKyController($scope, apiService, $window, $ngBootbox, notificationService) {     
        $scope.addHocKyHienTai = function () {
            var year;
            if ($scope.HocKy==1) {
                var year1 = new Date($scope.NgayBatDauHocKy).getFullYear();
                var year2 = new Date($scope.NgayBatDauHocKy).getFullYear()+1;
            }
            else if ($scope.HocKy == 2 || $scope.HocKy == 3) {
                var year1 = new Date($scope.NgayBatDauHocKy).getFullYear() - 1;
                var year2 = new Date($scope.NgayBatDauHocKy).getFullYear();
            }
            
               
                year = year1 + "-" + year2;
              
            
            $scope.hocKyHienTai = {
                HocKy: $scope.HocKy,
                NamHoc: year,
                NgayBatDauHocKy: $scope.NgayBatDauHocKy,
                NgayKetThucHocKy: $scope.NgayKetThucHocKy,
            }
           
            if ($scope.NgayKetThucHocKy <= $scope.NgayBatDauHocKy) {
                alert("Ngày kết thúc phải lớn hơn ngày bắt đầu")
            }
            else {
                apiService.post('api/hockyhientai/create', $scope.hocKyHienTai,
                    function (result) {
                        notificationService.displaySuccess("Đã thêm thành công")
                        $scope.getHocKy();
                        $scope.hocKyHienTai = null;
                        $scope.themhocky = false;

                    }, function () {
                        notificationService.displayWarning("Thêm không thành công")
                    })
                    ;
            }  


            
        }
        $scope.themhocky = false;
        $scope.mobang = function () {
            $scope.themhocky = !$scope.themhocky
        } 

        $scope.hocKy = []; 
        $scope.getHocKy = getHocKy;
        function getHocKy() {
            apiService.get('/api/hockyhientai/getall/', null, function (result) {

                $scope.hocKy = result.data;
               

               
            }, function () {
                console.log('Load thông tin failed.');
            });


        }
        $scope.getHocKy();



        $scope.deleteHocKy = deleteHocKy;

        function deleteHocKy(ID) {
            $ngBootbox.confirm('Bạn có chắc muốn xóa?').then(function () {
                var config = {
                    params: {
                        ID: ID
                    }
                }
                apiService.del('api/hockyhientai/delete', config, function () {
                    notificationService.displaySuccess("Đã xóa thành công")
                    $scope.getHocKy();
                }, function () {
                    notificationService.displayWarning("Xóa không thành công")

                })
            });
        }
    }
})(angular.module('platformTH_GV.datngaycuahocky'));