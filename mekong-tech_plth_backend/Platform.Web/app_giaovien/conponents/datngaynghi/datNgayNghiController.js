﻿(function (app) {
    app.controller('datNgayNghiController', datNgayNghiController);
    datNgayNghiController.$inject = ['$scope', 'apiService', '$window', '$ngBootbox', 'notificationService', 'loginService', '$injector'];
    function datNgayNghiController($scope, apiService, $window, $ngBootbox, notificationService, loginService, $injector) {
        if ($scope.kiemtrarole('SinhVien') === 'SinhVien' && $scope.kiemtrarole('Admin') != 'Admin') {
            loginService.logOut();
            var stateService = $injector.get('$state');
            stateService.go('dangnhap');
            notificationService.displayError("Bạn không có quyền truy cập trang này");
        }
        else {
            //#region
            $scope.namhoc;

            $scope.getHocKyHienTai = getHocKyHienTai;
            function getHocKyHienTai() {
                apiService.get('/api/hockyhientai/getall/', null, function (result) {

                    $scope.hocKyHienTai = result.data[result.data.length - 1];
                    $scope.hocKy = result.data[result.data.length - 1].HocKy;
                    $scope.namhoc = result.data[result.data.length - 1].NamHoc;

                    $scope.getNgayLeThayDoi();
                }, function () {
                    console.log('Load thông tin failed.');
                });


            }
            $scope.getHocKyHienTai();



            $scope.NgayLeThayDoi = [];
            $scope.getNgayLeThayDoi = getNgayLeThayDoi;
            function getNgayLeThayDoi() {
                var config = {
                    params: {
                        namhoc: $scope.namhoc

                    }
                }

                apiService.get('/api/ngaylethaydoi/GetByNamHoc/', config, function (result) {

                    $scope.NgayLeThayDoi = result.data;

                }, function () {
                    console.log('Load thông tin failed.');
                });

            }



            $scope.themngaynghi = false;
            $scope.mobang = function () {
                $scope.themngaynghi = !$scope.themngaynghi
            }
            $scope.addNgayLe = function () {
                $scope.NgayLe = {
                    NamHoc: $scope.namhoc,
                    TenNgayNghi: $scope.TenNgayNghi,
                    NgayBatDauNghi: $scope.NgayBatDauNghi,
                    NgayKetThucNghi: $scope.NgayKetThucNghi,

                }
                //var ngaybatdauhientai = (new Date($scope.NgayBatDauNghi).getDate() + new Date($scope.NgayBatDauNghi).getMonth() + new Date($scope.NgayBatDauNghi).getFullYear());
                //var ngayketthuchientai = (new Date($scope.NgayKetThucNghi).getDate() + new Date($scope.NgayKetThucNghi).getMonth() + new Date($scope.NgayKetThucNghi).getFullYear());

                //var ngaybatdau;
                //var ngayketthuc;
                //$scope.ketqua = false;
                //$scope.NgayLeThayDoi.filter(function (item) {

                //    ngaybatdau = new Date(item.NgayBatDauNghi).getDate() + new Date(item.NgayBatDauNghi).getMonth() + new Date(item.NgayBatDauNghi).getFullYear();
                //    ngayketthuc = new Date(item.NgayKetThucNghi).getDate() + new Date(item.NgayKetThucNghi).getMonth() + new Date(item.NgayKetThucNghi).getFullYear();
                //    if (ngaybatdauhientai == ngaybatdau && ngayketthuchientai == ngayketthuc) {
                //        $scope.ketqua = true;
                //    }
                //    else {
                //        $scope.ketqua = false;
                //    }
                //    return $scope.ketqua;
                //})




                if ($scope.NgayKetThucNghi < $scope.NgayBatDauNghi) {

                    alert("Ngày kết thúc phải lớn hơn ngày bắt đầu")
                }
                //else if ($scope.ketqua == true) {
                //    notificationService.displayWarning("Ngày bắt đầu và ngày kết thúc đã có sẵn, vui lòng xóa hoặc thêm ngày khác!")
                //}
                else {
                    apiService.post('api/ngaylethaydoi/create', $scope.NgayLe,
                        function (result) {
                            notificationService.displaySuccess("Thêm ngày lễ thành công")
                            $scope.getNgayLeThayDoi();
                            $scope.TenNgayNghi = "";
                            $scope.NgayBatDauNghi = "";
                            $scope.NgayKetThucNghi = "";
                            //$window.location.reload();
                        }, function () {
                            notificationService.displayWarning("Thêm mới không thành công")

                        })
                        ;
                }
            }


            $scope.deleteNgayLe = deleteNgayLe;

            function deleteNgayLe(ID) {
                $ngBootbox.confirm('Bạn có chắc muốn xóa?').then(function () {
                    var config = {
                        params: {
                            ID: ID
                        }
                    }
                    apiService.del('api/ngaylethaydoi/delete', config, function () {
                        notificationService.displaySuccess("Đã xóa thành công")
                        $scope.getNgayLeThayDoi();
                    }, function () {
                        notificationService.displayWarning("Xóa không thành công")

                    })
                });
            }
            //#endregion

        }

        
    }
})(angular.module('platformTH_GV.datngaynghi'));