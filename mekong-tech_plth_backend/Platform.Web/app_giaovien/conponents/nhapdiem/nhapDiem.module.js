﻿/// <reference path="../../../assets/giaovien/libs/angular/angular.js" />


(function () {
    angular.module('platformTH_GV.nhapdiem', ['platformTH_GV.common']).config(config);
    window.appVersion = angular.element(document.getElementsByTagName('html')).attr('data-ver');

    config.$inject = ['$stateProvider', '$urlRouterProvider'];

    function config($stateProvider, $urlRouterProvider) {
        $stateProvider.state('nhapdiem', {
            url: "/nhapdiem/",
            parent: 'base',
            templateUrl: "app_giaovien/conponents/nhapdiem/nhapDiemView.html?v=" + window.appVersion,
            controller: "nhapDiemController"
        })
            .state('nhapdiemgiuaki', {
            url: "/nhapdiemgiuaki/:MaNhom,:MaMH,:MaLop",
            parent: 'base',
                templateUrl: "app_giaovien/conponents/nhapdiem/nhapDiemGiuaKiView.html?v=" + window.appVersion,
            controller: "nhapDiemGiuaKiController"
            }).state('nhapdiemcuoiki', {
                url: "/nhapdiemcuoiki/:MaNhom,:MaMH,:MaLop",
            parent: 'base',
                templateUrl: "app_giaovien/conponents/nhapdiem/nhapDiemCuoiKiView.html?v=" + window.appVersion,
            controller: "nhapDiemCuoiKiController"
        })

    }
})();