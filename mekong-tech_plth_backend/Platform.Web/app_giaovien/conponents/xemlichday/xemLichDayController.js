﻿(function (app) {
    app.controller('xemLichDayController', xemLichDayController);

    xemLichDayController.$inject = ['$scope', 'apiService', '$stateParams', 'loginService', '$injector', 'notificationService'];
    function xemLichDayController($scope, apiService, $stateParams, loginService, $injector, notificationService) {
        if ($scope.kiemtrarole('SinhVien') === 'SinhVien' && $scope.kiemtrarole('Admin') != 'Admin') {
            loginService.logOut();
            var stateService = $injector.get('$state');
            stateService.go('dangnhap');
            notificationService.displayError("Bạn không có quyền truy cập trang này");
        }
        else {
            //#region
            $scope.TTCN = [];
            $scope.LichGiangDay = [];

            $scope.msvc = "";
            $scope.getHocKyHienTai = getHocKyHienTai;
            $scope.hocKy;
            $scope.namhoc;
            function getHocKyHienTai() {
                apiService.get('/api/hockyhientai/getall/', null, function (result) {

                    $scope.hocKyHienTai = result.data[result.data.length - 1];
                    $scope.hocKy = result.data[result.data.length - 1].HocKy;
                    $scope.namhoc = result.data[result.data.length - 1].NamHoc;

                }, function () {
                    console.log('Load thông tin failed.');
                });

            }
            $scope.getHocKyHienTai();




            $scope.getlichGiangDay = getlichGiangDay;
            function getlichGiangDay() {
                var config = {
                    params: {
                        msvc: $scope.msvc,
                        hocky: $scope.hocKy,
                        namhoc: $scope.namhoc


                    }
                }
                apiService.get('/api/giangday/GetLichGiangDay/', config, function (result) {
                    $scope.LichGiangDay = result.data;
                }, function () {
                    console.log('Load thông tin failed.');
                });

            }
            $scope.showAll = function () {
                $scope.showlichday = true;
            }

            $scope.getThongTinCaNhan = getThongTinCaNhan;
            function getThongTinCaNhan() {
                var config = {
                    params: {
                        Id: $scope.msvc

                    }
                }
                apiService.get('/api/vienchuc/GetTTCN/', config, function (result) {
                    $scope.TTCN = result.data;
                }, function () {
                    console.log('Load thông tin failed.');
                });

            }

            //#endregion

        }

       
    }
  
})(angular.module('platformTH_GV.xemlichday'));