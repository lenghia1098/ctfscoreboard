﻿(function (app) {
    app.controller('xemBangDiemController', xemBangDiemController);
    xemBangDiemController.$inject = ['$scope', 'apiService', '$stateParams', 'loginService', '$injector', 'notificationService'];
    function xemBangDiemController($scope, apiService, $stateParams, loginService, $injector, notificationService) {
        if ($scope.kiemtrarole('SinhVien') === 'SinhVien' && $scope.kiemtrarole('Admin') != 'Admin') {
            loginService.logOut();
            var stateService = $injector.get('$state');
            stateService.go('dangnhap');
            notificationService.displayError("Bạn không có quyền truy cập trang này");
        }
        else {
            //#region
            //$scope.bangDiem = [];
            $scope.hockynamhoc = [];
            $scope.mssv = "";

            $scope.hocKy = [];
            $scope.getHocKyNamHoc = "";
            $scope.allbangdiem = [];
            //$scope.getBangDiem = getBangDiem;
            $scope.thongTincaNhan = [];
            $scope.getThongTinCaNhan = getThongTinCaNhan;
            $scope.getHockynamhoc = getHockynamhoc;
            $scope.getAllBangDiem = getAllBangDiem;
            $scope.getHocKy = getHocKy;

            $scope.showbangdiemtatca = false


            function getAllBangDiem() {
                $scope.showbangdiemtatca = true
                var config = {
                    params: {
                        mssv: $scope.mssv

                    }
                }
                apiService.get('/api/ketqua/getAllBangDiem', config, function (result) {
                    $scope.hockynamhoc = "";
                    $scope.allbangdiem = "";
                    $scope.allbangdiem = result.data;


                }, function () {
                    console.log('Load thông tin failed.');

                });
            }

            function getHocKy() {
                var config = {
                    params: {
                        mssv: $scope.mssv
                    }
                }
                apiService.get('/api/ketqua/getHocKy', config, function (result) {

                    $scope.getHocKyNamHoc = result.data;
                }, function () {
                    console.log('Load thông tin failed.');

                });

            }


            function getHockynamhoc() {
                var config = {
                    params: {
                        mssv: $scope.mssv,
                        hocKy: $scope.hocKy.HocKy,
                        namHoc: $scope.hocKy.NamHoc



                    }
                }
                apiService.get('/api/ketqua/getBangDiemHocKy', config, function (result) {
                    $scope.hockynamhoc = "";
                    $scope.hockynamhoc = result.data;
                    $scope.allbangdiem = "";
                    var sum = 0;
                    var sumTBHK = 0;
                    angular.forEach($scope.hockynamhoc, function (item) {
                        sum += item.SoTinChi
                    })
                    angular.forEach($scope.hockynamhoc, function (item) {
                        sumTBHK += (item.DiemTongKet * item.SoTinChi) / sum
                    })
                    $scope.sumTBHocKy = sumTBHK /*parseFloat(sumTBHK).toFixed(2);*/
                    $scope.sumTinChiHK = sum 


                }, function () {
                    console.log('Load thông tin failed.');
                });



            }

            function getThongTinCaNhan() {
                var config = {
                    params: {
                        Id: $scope.mssv
                    }
                }
                apiService.get('/api/sinhvien/getTTCN/', config, function (result) {
                    $scope.thongTincaNhan = result.data;
                }, function () {
                    console.log('Load thông tin failed.');
                });

            }


            $scope.clickHocKy = function () {
                $scope.allHocKy = true;
                $scope.showbangdiemtatca = false;
            }
            $scope.clickallbangdiem = function () {
                $scope.allHocKy = false;
            }

            
            $scope.sumtchk = function (hocky, namhoc) {
                var sum = 0;
                var sumtbhk = 0;
                var myArray = [];

                $scope.allbangdiem.filter(function (item) {
                    if (item.HocKy == hocky && item.NamHoc == namhoc) {

                        myArray.push(item.LayBangDiemTheoHocKy);
                        for (var i = 0; i <= myArray[0].length - 1; i++) {

                            sum += item.LayBangDiemTheoHocKy[i].SoTinChi;

                            sumtbhk += (item.LayBangDiemTheoHocKy[i].DiemTongKet * item.LayBangDiemTheoHocKy[i].SoTinChi);
                        }

                    }
                })
                return sum;

            }


            $scope.sumtbhk = function (hocky, namhoc, sumtchk) {

                var sum = 0;
                var myArray = [];

                $scope.allbangdiem.filter(function (item) {

                    if (item.HocKy == hocky && item.NamHoc == namhoc) {

                        myArray.push(item.LayBangDiemTheoHocKy);
                        for (var i = 0; i <= myArray[0].length - 1; i++) {

                            //  sum += item.LayBangDiemTheoHocKy[i].SoTinChi;

                            sum += (item.LayBangDiemTheoHocKy[i].DiemTongKet * item.LayBangDiemTheoHocKy[i].SoTinChi) / sumtchk;
                        }

                    }
                })
                return sum;

            }










            $scope.sumtc = function () {
                var sum = 0;


                $scope.allbangdiem.filter(function (item) {
                    if ($scope.allbangdiem.length > 0) {

                        item.LayBangDiemTheoHocKy.filter(function (item1) {

                            sum += item1.SoTinChi;
                        }

                        )
                    }

                })


                return sum;
            }
            $scope.sumtb = function (tong) {

                var sum = 0;
                var sumtc = 0;

                $scope.allbangdiem.filter(function (item) {
                    if ($scope.allbangdiem.length > 0) {

                        item.LayBangDiemTheoHocKy.filter(function (item1) {
                            sumtc += item1.SoTinChi
                            sum += (item1.DiemTongKet * item1.SoTinChi) / tong;
                        }

                        )
                    }

                })


                return sum.toFixed(2);
            }
            //#endregion

        }
       
        
    }
})(angular.module('platformTH_GV.xembangdiem'));