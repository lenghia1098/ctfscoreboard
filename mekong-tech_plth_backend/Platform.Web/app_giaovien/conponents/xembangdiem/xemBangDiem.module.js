﻿/// <reference path="../../../assets/giaovien/libs/angular/angular.js" />


(function () {
    angular.module('platformTH_GV.xembangdiem', ['platformTH_GV.common']).config(config);
    window.appVersion = angular.element(document.getElementsByTagName('html')).attr('data-ver');

    config.$inject = ['$stateProvider', '$urlRouterProvider'];

    function config($stateProvider, $urlRouterProvider) {
        $stateProvider.state('xembangdiem', {
            url: "/xembangdiem",
            parent: 'base',
            templateUrl: "app_giaovien/conponents/xembangdiem/xemBangDiemView.html?v=" + window.appVersion,
            controller: "xemBangDiemController"
        });

    }
})();