﻿/// <reference path="../../../assets/giaovien/libs/angular/angular.js" />


(function () {
    angular.module('platformTH_GV.xemhocphi', ['platformTH_GV.common']).config(config);

    config.$inject = ['$stateProvider', '$urlRouterProvider'];
    window.appVersion = angular.element(document.getElementsByTagName('html')).attr('data-ver');

    function config($stateProvider, $urlRouterProvider) {
        $stateProvider.state('xemhocphi', {
            url: "/xemhocphi",
            parent: 'base',
            templateUrl: "app_giaovien/conponents/xemhocphi/xemHocPhiView.html?v=" + window.appVersion,
            controller: "xemHocPhiController"
        });

    }
})();