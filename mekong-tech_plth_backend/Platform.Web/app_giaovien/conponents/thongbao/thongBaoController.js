﻿(function (app) {
    app.controller('thongBaoController', thongBaoController);

    function thongBaoController($scope, apiService, $stateParams, notificationService, loginService, $injector) {

        if ($scope.kiemtrarole('SinhVien') === 'SinhVien' && $scope.kiemtrarole('Admin') != 'Admin') {
            loginService.logOut();
            var stateService = $injector.get('$state');
            stateService.go('dangnhap');
            notificationService.displayError("Bạn không có quyền truy cập trang này");
        }
        else {
            //#region
            $scope.thongBao = [];
            $scope.MaSoTB = $stateParams.MaSoTB;


            $scope.getChiTietThongBao = getChiTietThongBao;


            function getChiTietThongBao() {
                var config = {
                    params: {
                        MaSoTB: $scope.MaSoTB
                    }
                }
                apiService.get('/api/ThongBao/chitietTB/', config, function (result) {
                    $scope.thongBao = result.data;
                }, function () {
                    console.log('Load thông tin failed.');
                });

            }
            $scope.getChiTietThongBao();
            //#endregion

        }
       

    }
})(angular.module('platformTH_GV.thongbao'));