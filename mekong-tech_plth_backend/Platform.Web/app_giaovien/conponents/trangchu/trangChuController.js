﻿
(function (app) {
    app.controller('trangChuController', trangChuController);
    trangChuController.$inject = ['$scope', 'apiService', '$stateParams', 'loginService', '$injector', 'notificationService'];
    function trangChuController($scope, apiService, $stateParams, loginService, $injector, notificationService) {
        if ($scope.kiemtrarole('SinhVien') === 'SinhVien' && $scope.kiemtrarole('Admin') != 'Admin') {
            var stateService = $injector.get('$state');
            stateService.go('trangchu_sv');
        }
        else {
            //#region
            $scope.demvieclamvienchuc = "0";
            $scope.demdonvienchuc = "0";
            $scope.msvc = $scope.authentication.userName;
            $scope.nhacNho = [];
            $scope.thongBao = [];



            $scope.tinTuc = [];



            $scope.getNhacNho = getNhacNho;
            $scope.getThongBao = getThongBao;
            $scope.getdondanopVienChuc = getdondanopVienChuc;
            $scope.getViecPhaiLamVienChuc = getViecPhaiLamVienChuc;
            $scope.getTinTuc = getTinTuc;


            function getViecPhaiLamVienChuc() {
                var config = {
                    params: {
                        MaSoVienChuc: $scope.msvc
                    }
                }
                apiService.get('/api/ViecPhaiLamVienChuc/DemViecPhaiLamVienChuc/', config, function (result) {
                    $scope.demvieclamvienchuc = result.data;
                }, function () {
                    console.log('Load thông tin failed.');
                });

            }
            function getdondanopVienChuc() {
                var config = {
                    params: {
                        msvc: $scope.msvc
                    }
                }
                apiService.get('/api/donvienchuc/demdondanopvienchuc/', config, function (result) {
                    $scope.demdonvienchuc = result.data;
                }, function () {
                    console.log('Load thông tin failed.');
                });

            }

            function getNhacNho(page) {
                page = page || 0;
                var config = {
                    params: {
                        page: page,
                        msvc: $scope.msvc,
                        pageSize: 5,
                    }
                }
                apiService.get('/api/nhacnho-vc/getNhacNhoChuaHoanThanh/', config, function (result) {
                    $scope.nhacNho = result.data.Items;
                    $scope.page = result.data.Page;
                    $scope.pagesCount = result.data.TotalPages;
                    $scope.TotalCount = result.data.TotalCount;

                }, function () {
                    console.log('Load thông tin failed.');
                });

            };


            function getThongBao(page) {
                page = page || 0;
                var config = {
                    params: {
                        page: page,
                        mssv: $scope.msvc,
                        pageSize: 5,
                    }
                }
                apiService.get('/api/ThongBao/getTB/', config, function (result) {
                    $scope.thongBao = result.data.Items;
                    $scope.page = result.data.Page;
                    $scope.pagesCount = result.data.TotalPages;
                    $scope.TotalCount = result.data.TotalCount;


                }, function () {
                    console.log('Load thông tin failed.');
                });

            }
            function getTinTuc(page) {
                page = page || 0;
                var config = {
                    params: {
                        page: page,
                        mssv: $scope.msvc,
                        pageSize: 5,
                    }
                }
                apiService.get('/api/TinTuc/getTT/', config, function (result) {
                    $scope.tinTuc = result.data.Items;
                    $scope.page = result.data.Page;
                    $scope.pagesCount = result.data.TotalPages;
                    $scope.TotalCount = result.data.TotalCount;
                }, function () {
                    console.log('Load thông tin failed.');
                });

            };







            $scope.getNhacNho();
            $scope.getThongBao();
            $scope.getTinTuc();
            $scope.getViecPhaiLamVienChuc();
            $scope.getdondanopVienChuc();
            //#endregion

        }


    }
})(angular.module('platformTH_GV'));
