﻿(function (app) {
    'use strict';

    app.controller('applicationUserAddController', applicationUserAddController);

    applicationUserAddController.$inject = ['$scope', 'apiService', 'notificationService', '$location', 'loginService', '$injector'];

    function applicationUserAddController($scope, apiService, notificationService, $location, loginService, $injector) {
        if ($scope.kiemtrarole('SinhVien') === 'SinhVien' && $scope.kiemtrarole('Admin') != 'Admin') {
            loginService.logOut();
            var stateService = $injector.get('$state');
            stateService.go('dangnhap');
            notificationService.displayError("Bạn không có quyền truy cập trang này");
        }
        else {
            //#region
            $scope.account = {
                Password: 'Nghia1097@',
                Groups: []
            }

            $scope.addAccount = addAccount;

            function addAccount() {
                apiService.post('/api/applicationUser/add', $scope.account, addSuccessed, addFailed);
            }

            function addSuccessed() {
                notificationService.displaySuccess($scope.account.Name + ' đã được thêm mới.');

                $location.url('application_users');
            }
            function addFailed(response) {
                notificationService.displayError(response.data.Message);
                notificationService.displayErrorValidation(response);
            }

            function loadGroups() {
                apiService.get('/api/applicationGroup/getlistall',
                    null,
                    function (response) {
                        $scope.groups = response.data;
                    }, function (response) {
                        notificationService.displayError('Không tải được danh sách nhóm.');
                    });

            }
            function loadRoles() {
                apiService.get('/api/applicationRole/getlistall',
                    null,
                    function (response) {
                        $scope.roles = response.data;
                    }, function (response) {
                        notificationService.displayError('Không tải được danh sách quyền.');
                    });
            }
            $scope.loadVienChuc = loadVienChuc;
            $scope.VienChuc = [];
            function loadVienChuc() {
                var config = {
                    params: {
                        msvc: $scope.msvc,
                       
                    }
                }
                apiService.get('/api/vienchuc/GetLyLichVienChuc',
                    config,
                    function (response) {
                        $scope.VienChuc = response.data;
                       
                        $scope.account.FullName = $scope.VienChuc.HoTen;
                        $scope.account.UserName = $scope.VienChuc.MaSoVC;
                        $scope.account.Email = $scope.VienChuc.Email;
                        $scope.account.PhoneNumber = $scope.VienChuc.DienThoai;
                        $scope.account.BirthDay = $scope.VienChuc.NgaySinh;
                    }, function (response) {
                        notificationService.displayError('Không tải được danh sách viên chức.');
                    });
            }
            $scope.LocHienThi = function (GiangDay) {
                var name;
                angular.forEach($scope.roles, function (item) {
                    if (GiangDay=item.Name) {
                        name = item.Name
                    }
                })
                return name
            }
            
            loadGroups();
            loadRoles();
            //#endregion

        }
       
    }
})(angular.module('platformTH_GV.application_users'));