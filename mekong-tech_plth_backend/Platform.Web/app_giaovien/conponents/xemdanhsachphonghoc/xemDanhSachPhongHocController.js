﻿(function (app) {
    app.controller('xemDanhSachPhongHocController', xemDanhSachPhongHocController);
    xemDanhSachPhongHocController.$inject = ['$scope', 'apiService', '$window', '$ngBootbox', 'notificationService', 'loginService', '$injector'];
    function xemDanhSachPhongHocController($scope, apiservice, $window, $ngBootbox, notificationService, loginService, $injector) {
        if ($scope.kiemtrarole('SinhVien') === 'SinhVien' && $scope.kiemtrarole('Admin') != 'Admin') {
            loginService.logOut();
            var stateService = $injector.get('$state');
            stateService.go('dangnhap');
            notificationService.displayError("Bạn không có quyền truy cập trang này");
        }
        else {
            //#region
            $scope.AllPhongHoc = [];
            $scope.getAllPhongHoc = getAllPhongHoc;
            function getAllPhongHoc() {


                apiservice.get('/api/PhongHoc/getall/', null, function (result) {

                    $scope.AllPhongHoc = result.data;

                }, function () {
                    console.log('load thông tin failed.');
                });

            }

            $scope.getAllPhongHoc();
            $scope.mobang = function () {
                $scope.themphonghoc = !$scope.themphonghoc
            }

            $scope.Huy = function () {
                $scope.themphonghoc = !$scope.themphonghoc
                $scope.GhiChu = "";
                $scope.SoCho = "";
                $scope.TenPH = "";
                $scope.MaPH = "";

            }
            $scope.addPhongHoc = function () {
                $scope.PhongHoc = {

                    MaPH: $scope.MaPH,
                    TenPH: $scope.TenPH,
                    SoCho: $scope.SoCho,
                    GhiChu: $scope.GhiChu,

                }
                apiservice.post('api/PhongHoc/create', $scope.PhongHoc,
                    function (result) {
                        notificationService.displaySuccess("Thêm phòng học thành công")
                        $scope.getAllPhongHoc();
                        //$window.location.reload();
                    }, function () {
                        console.log("thêm mới thất bại")

                    })
                    ;
            }

            $scope.validate = function () {
                angular.forEach($scope.AllPhongHoc, function (item) {
                    if (item.MaPH == $scope.MaPH) {
                        notificationService.displayWarning("Mã phòng học đã tồn tại")
                    }

                }, function () {
                    $scope.addPhongHoc();

                })

            }



            $scope.deletePhongHoc = deletePhongHoc;

            function deletePhongHoc(ID) {
                $ngBootbox.confirm('Bạn có chắc muốn xóa?').then(function () {
                    var config = {
                        params: {
                            ID: ID
                        }
                    }

                    apiservice.del('api/PhongHoc/delete', config, function () {
                        notificationService.displaySuccess("Đã xóa thành công")
                        $scope.getAllPhongHoc();
                    }, function () {

                        notificationService.displayWarning("Xóa không thành công")
                    })

                });
            }
            //#endregion

        }

        
      
       
    }
})(angular.module('platformTH_GV.xemdanhsachphonghoc'));