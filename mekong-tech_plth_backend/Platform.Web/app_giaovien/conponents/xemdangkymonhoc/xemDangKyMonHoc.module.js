﻿/// <reference path="../../../assets/giaovien/libs/angular/angular.js" />


(function () {
    angular.module('platformTH_GV.xemdangkymonhoc', ['platformTH_GV.common']).config(config);

    config.$inject = ['$stateProvider', '$urlRouterProvider'];
    window.appVersion = angular.element(document.getElementsByTagName('html')).attr('data-ver');

    function config($stateProvider, $urlRouterProvider) {
        $stateProvider.state('xemdangkymonhoc', {
            url: "/xemdangkymonhoc",

            parent: 'base',
            templateUrl: "app_giaovien/conponents/xemdangkymonhoc/xemDangKyMonHocView.html?v=" + window.appVersion,
            controller: "xemDangKyMonHocController"
        });

    }
})();