﻿
(function (app) {
    app.controller('dangNhapController', ['$scope', 'loginService', '$injector', 'notificationService',
        function ($scope, loginService, $injector,notificationService) {

            $scope.loginData = {
                userName: "",
                password: ""
            };

            $scope.loginSubmit = function () {
                
                loginService.login($scope.loginData.userName, $scope.loginData.password).then(function (response) {
                    if (response !== null && response.data.error !== undefined) {
                        notificationService.displayError(response.data.error_description);

                    }
                    else {
                        
                        if ($scope.kiemtrarole('SinhVien') === 'SinhVien' && $scope.kiemtrarole('Admin') != 'Admin') {
                                var stateService = $injector.get('$state');
                            stateService.go('trangchu_sv');
                        }
                        else {
                        //#region
                            var stateService = $injector.get('$state');
                            stateService.go('trangchu');
                         //#endregion

                        }
                        
                       
                    }
                });
            }
        }]);
})(angular.module('platformTH_GV'));