﻿/// <reference path="../../../assets/giaovien/libs/angular/angular.js" />


(function () {
    angular.module('platformTH_GV.xemthongtinsinhvientrungtuyen', ['platformTH_GV.common']).config(config);
    window.appVersion = angular.element(document.getElementsByTagName('html')).attr('data-ver');

    config.$inject = ['$stateProvider', '$urlRouterProvider'];

    function config($stateProvider, $urlRouterProvider) {
        $stateProvider.state('xemthongtinsinhvientrungtuyen', {
            url: "/xemthongtinsinhvientrungtuyen",
            parent: 'base',
            templateUrl: "app_giaovien/conponents/xemthongtinsinhvientrungtuyen/xemThongTinSinhVienTrungTuyenView.html?v=" + window.appVersion,
            controller: "xemThongTinSinhVienTrungTuyenController"
        });

    }
})();