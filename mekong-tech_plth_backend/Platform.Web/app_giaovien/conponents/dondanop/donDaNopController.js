﻿(function (app) {
    app.controller('donDaNopController', donDaNopController);


    function donDaNopController($scope, apiService, $stateParams, $ngBootbox, notificationService, loginService, $injector, notificationService) {

        if ($scope.kiemtrarole('SinhVien') === 'SinhVien' && $scope.kiemtrarole('Admin') != 'Admin') {
            loginService.logOut();
            var stateService = $injector.get('$state');
            stateService.go('dangnhap');
            notificationService.displayError("Bạn không có quyền truy cập trang này");
        }
        else {
            //#region
            $scope.donDaNop = [];
            $scope.msvc = $scope.authentication.userName;
            $scope.selected = [];

            $scope.exist = function (item) {
                return $scope.selected.indexOf(item) > -1;

            }

            $scope.toge = function (item) {
                var idx = $scope.selected.indexOf(item);
                if (idx > -1) {
                    $scope.selected.splice(idx, 1);

                }
                else {
                    $scope.selected.push(item);
                    console.log($scope.selected);
                }
            }


            $scope.checkAll = function () {
                if ($scope.selectedAll) {
                    angular.forEach($scope.donDaNop, function (item) {

                        idx = $scope.selected.indexOf(item);
                        if (idx >= 0) {
                            return true;
                        } else {
                            $scope.selected.push(item);
                        }
                    })
                }
                else {
                    $scope.selected = [];
                }
            }









            $scope.getDonDaNop = getDonDaNop;


            function getDonDaNop(page) {
                page = page || 0
                var config = {
                    params: {
                        page: page,
                        msvc: $scope.msvc,
                        pageSize: 5
                    }
                }
                apiService.get('/api/donvienchuc/GetChucnangdondanopvienchucs/', config, function (result) {
                    $scope.donDaNop = result.data.Items;
                    $scope.page = result.data.Page;
                    $scope.pagesCount = result.data.TotalPages;
                    $scope.TotalCount = result.data.TotalCount;
                }, function () {
                    console.log('Load thông tin failed.');
                });

            }




            $scope.DeleteAllDonDaNop = DeleteAllDonDaNop;


            function DeleteAllDonDaNop() {
                if ($scope.selected == "") {
                    alert("Bạn chưa chọn");
                } else {

                    $ngBootbox.confirm('Bạn chắc muốn xóa không').then(function () {
                        angular.forEach($scope.selected, function (item) {
                            item.NguoiXoa = $scope.authentication.userName;
                            apiService.put('api/don-vienchuc/updateAll', item, function (result) {

                                notificationService.displaySuccess("Xóa thành công");
                                $scope.getDonDaNop();


                            }, function () {
                                notificationService.displaySuccess('Xóa không thành công');
                            });
                        });
                    });

                }
            };


            $scope.getDonDaNop();

            //#endregion

        }
       
    }
})(angular.module('platformTH_GV.dondanop'));