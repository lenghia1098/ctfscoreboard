﻿(function (app) {
    app.controller('inBangDiemController', inBangDiemController);
    inBangDiemController.$inject = ['$scope', 'apiService', '$stateParams', 'loginService', '$injector', 'notificationService'];
    function inBangDiemController($scope, apiService, $stateParams, loginService, $injector, notificationService) {
        if ($scope.kiemtrarole('SinhVien') === 'SinhVien' && $scope.kiemtrarole('Admin') != 'Admin') {
            loginService.logOut();
            var stateService = $injector.get('$state');
            stateService.go('dangnhap');
            notificationService.displayError("Bạn không có quyền truy cập trang này");
        }
        else {
            //#region
            $scope.bangDiem = [];
            $scope.mssv = "";
            //$scope.hocKy = "1";
            $scope.getBangDiem = getBangDiem;

            function getBangDiem() {
                var config = {
                    params: {
                        mssv: $scope.mssv,

                    }
                }
                apiService.get('/api/ketqua/getAllBangDiem', config, function (result) {
                    $scope.bangDiem = result.data;
                }, function () {
                    console.log('Load thông tin failed.');
                });

            }
            $scope.getBangDiem();
            $scope.thongTincaNhan = [];
            $scope.getThongTinCaNhan = getThongTinCaNhan;


            function getThongTinCaNhan() {
                var config = {
                    params: {
                        Id: $scope.mssv,

                    }
                }
                apiService.get('/api/sinhvien/getTTCN/', config, function (result) {
                    $scope.thongTincaNhan = result.data;
                }, function () {
                    console.log('Load thông tin failed.');
                });

            }

            $scope.getThongTinCaNhan();



            $scope.sumtchk = function (hocky, namhoc) {
                var sum = 0;
                var sumtbhk = 0;
                var myArray = [];

                $scope.bangDiem.filter(function (item) {
                    if (item.HocKy == hocky && item.NamHoc == namhoc) {

                        myArray.push(item.LayBangDiemTheoHocKy);
                        for (var i = 0; i <= myArray[0].length - 1; i++) {

                            sum += item.LayBangDiemTheoHocKy[i].SoTinChi;

                            sumtbhk += (item.LayBangDiemTheoHocKy[i].DiemTongKet * item.LayBangDiemTheoHocKy[i].SoTinChi);
                        }

                    }
                })
                return sum;

            }


            $scope.sumtbhk = function (hocky, namhoc, sumtchk) {

                var sum = 0;
                var myArray = [];

                $scope.bangDiem.filter(function (item) {

                    if (item.HocKy == hocky && item.NamHoc == namhoc) {

                        myArray.push(item.LayBangDiemTheoHocKy);
                        for (var i = 0; i <= myArray[0].length - 1; i++) {

                            //  sum += item.LayBangDiemTheoHocKy[i].SoTinChi;

                            sum += (item.LayBangDiemTheoHocKy[i].DiemTongKet * item.LayBangDiemTheoHocKy[i].SoTinChi) / sumtchk;
                        }

                    }
                })
                return sum;

            }





            $scope.sumtc = function () {
                var sum = 0;


                $scope.bangDiem.filter(function (item) {
                    if ($scope.bangDiem.length > 0) {

                        item.LayBangDiemTheoHocKy.filter(function (item1) {

                            sum += item1.SoTinChi;
                        }

                        )
                    }

                })


                return sum;
            }
            $scope.sumtb = function (tong) {

                var sum = 0;
                var sumtc = 0;

                $scope.bangDiem.filter(function (item) {
                    if ($scope.bangDiem.length > 0) {

                        item.LayBangDiemTheoHocKy.filter(function (item1) {
                            sumtc += item1.SoTinChi
                            sum += (item1.DiemTongKet * item1.SoTinChi) / tong;
                        }

                        )
                    }

                })


                return sum.toFixed(2);
            }


            $scope.bang1 = false;
            $scope.showbang = function () {
                $scope.bang1 = true;
            };

            $scope.hocKyHienTai = [];
            $scope.getHocKyHienTai = getHocKyHienTai;
            function getHocKyHienTai() {
                apiService.get('/api/hockyhientai/getall/', null, function (result) {

                    $scope.hocKyHienTai = result.data[result.data.length - 1];
                    $scope.hocKy = result.data[result.data.length - 1].HocKy;
                    $scope.namhoc = result.data[result.data.length - 1].NamHoc;
                    $scope.getBangDiem();
                }, function () {
                    console.log('Load thông tin failed.');
                });

            }
            $scope.getHocKyHienTai();



            var ngay = new Date().getDate();
            var thang = new Date().getMonth() + 1;
            var nam = new Date().getFullYear();

            $scope.ngaythangnam = "Ngày " + ngay + " tháng " + thang + " năm " + nam
            //#endregion

        }
        
    }
})(angular.module('platformTH_GV.inbangdiem'));