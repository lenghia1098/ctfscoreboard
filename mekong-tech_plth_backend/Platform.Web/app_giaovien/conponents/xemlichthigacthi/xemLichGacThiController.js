﻿(function (app) {
    app.controller('xemLichGacThiController', xemLichGacThiController);

    xemLichGacThiController.$inject = ['$scope', 'apiService', '$stateParams', 'loginService', '$injector', 'notificationService'];
    function xemLichGacThiController($scope, apiService, $stateParams, loginService, $injector, notificationService) {
        if ($scope.kiemtrarole('SinhVien') === 'SinhVien' && $scope.kiemtrarole('Admin') != 'Admin') {
            loginService.logOut();
            var stateService = $injector.get('$state');
            stateService.go('dangnhap');
            notificationService.displayError("Bạn không có quyền truy cập trang này");
        }
        else {
            //#region
            $scope.TTCN = [];
            $scope.LichGacThi = [];

            $scope.msvc = "";


            $scope.getThongTinCaNhan = getThongTinCaNhan;
            function getThongTinCaNhan() {
                var config = {
                    params: {
                        Id: $scope.msvc

                    }
                }
                apiService.get('/api/vienchuc/GetTTCN/', config, function (result) {
                    $scope.TTCN = result.data;
                }, function () {
                    console.log('Load thông tin failed.');
                });

            }
            $scope.showAll = function () {

                $scope.all = true;
            };


            $scope.getlichGacThi = getlichGacThi;
            function getlichGacThi() {
                var config = {
                    params: {
                        msvc: $scope.msvc,


                    }
                }
                apiService.get('/api/lichthi/getLichThiGV/', config, function (result) {
                    $scope.LichGacThi = result.data;
                }, function () {
                    console.log('Load thông tin failed.');
                });

            }
            //#endregion

        }

       
        }
  
})(angular.module('platformTH_GV.xemlichthigacthi'));