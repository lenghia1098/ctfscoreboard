﻿(function (app) {
    app.controller('suaDiemController', suaDiemController);

    suaDiemController.$inject = ['$scope', 'apiService', '$stateParams', '$injector', '$ngBootbox', 'notificationService', 'loginService', '$injector'];
    function suaDiemController($scope, apiService, $stateParams, $injector, $ngBootbox, notificationService,loginService, $injector) {
        if ($scope.kiemtrarole('SinhVien') === 'SinhVien' && $scope.kiemtrarole('Admin') != 'Admin') {
            loginService.logOut();
            var stateService = $injector.get('$state');
            stateService.go('dangnhap');
            notificationService.displayError("Bạn không có quyền truy cập trang này");
        }
        else {
            //#region
            $scope.khung1 = false;
            $scope.khung2 = false;
            $scope.bang1 = false;
            $scope.bang2 = false;

            $scope.showkhung1 = function () {
                $scope.khung1 = true;
                $scope.khung2 = false;
                $scope.bang2 = false;
            };
            $scope.showkhung2 = function () {
                $scope.khung1 = false;
                $scope.khung2 = true;
                $scope.bang1 = false;

            };
            $scope.hienthongbao = false;
            $scope.SuaDiemGiuaKi = {};
            $scope.getSuaDiemGiuaKi = function (mssv, manhom) {
                $scope.hienthongbao = false;
                $scope.bang1 = true;
                var config = {
                    params: {
                        mssv: mssv,
                        manhom: manhom
                    }
                }
                apiService.get('/api/ketqua/getidsuadiem/', config, function (result) {

                    if (result.data[0] != null) {
                        if (result.data[0].GiangVienDieuChinhGiuaKi == null) {
                            $scope.SuaDiemGiuaKi = result.data;
                            $scope.getTTMonHoc($scope.SuaDiemGiuaKi[0].MaMH);
                        }
                        else {
                            alert("Bạn đã sửa điểm giữa kỳ sinh viên này");
                            $scope.SuaDiemGiuaKi = [];
                            $scope.hienthongbao = true
                        }

                    }
                    else {
                        $scope.hienthongbao = true

                    }

                }, function () {
                    console.log('Load thông tin failed.');
                    $scope.hienthongbao = true
                });
            }
            $scope.getSuaDiemCuoiKi = function (mssv, manhom) {
                $scope.hienthongbao = false;
                $scope.bang2 = true;
                var config = {
                    params: {
                        mssv: mssv,
                        manhom: manhom
                    }
                }
                apiService.get('/api/ketqua/getidsuadiem/', config, function (result) {
                    if (result.data[0] != null) {
                        if (result.data[0].GiangVienDieuChinhCuoiKi == null) {
                            $scope.SuaDiemGiuaKi = result.data;
                            $scope.getTTMonHoc($scope.SuaDiemGiuaKi[0].MaMH);
                        }
                        else {
                            $scope.SuaDiemGiuaKi = [];

                            alert("Bạn đã sửa điểm cuối kỳ sinh viên này");

                            $scope.hienthongbao = true
                        }

                    }
                    else {
                        $scope.hienthongbao = true

                    }


                }, function () {
                    console.log('Load thông tin failed.');
                    $scope.hienthongbao = true
                });
            }
            $scope.TTMonHoc = {};
            $scope.getTTMonHoc = getTTMonHoc;
            function getTTMonHoc(MaMH) {
                var config = {
                    params: {
                        Id: MaMH
                    }
                }
                apiService.get('/api/monhoc/getbyid/', config, function (result) {

                    $scope.TTMonHoc = result.data;

                }, function () {
                    console.log('Load thông tin failed.');
                });

            }

            $scope.updateDiemCuoiKi = updateDiemCuoiKi;
            function updateDiemCuoiKi() {
                $ngBootbox.confirm('Xác nhận thêm điểm').then(function () {

                    angular.forEach($scope.SuaDiemGiuaKi, function (item) {
                        item.DiemTongKet = ((item.DiemChuyenCanMoi * $scope.TTMonHoc.TyLeChuyenCan) + (item.DiemKiemTra * $scope.TTMonHoc.TyLeKiemTra) + (item.DiemThucHanhMoi * $scope.TTMonHoc.TyLeThucHanh) + (item.DiemSeminarMoi * $scope.TTMonHoc.TyLeSeminar) + (item.DiemThiMoi * $scope.TTMonHoc.TyLeThi)) / 100
                        if ((((item.DiemChuyenCanMoi * $scope.TTMonHoc.TyLeChuyenCan) + (item.DiemKiemTra * $scope.TTMonHoc.TyLeKiemTra) + (item.DiemThucHanhMoi * $scope.TTMonHoc.TyLeThucHanh) + (item.DiemSeminarMoi * $scope.TTMonHoc.TyLeSeminar) + (item.DiemThiMoi * $scope.TTMonHoc.TyLeThi)) / 100) >= $scope.TTMonHoc.DiemDau) {
                            item.Dau = true;
                        }
                        if ((((item.DiemChuyenCanMoi * $scope.TTMonHoc.TyLeChuyenCan) + (item.DiemKiemTra * $scope.TTMonHoc.TyLeKiemTra) + (item.DiemThucHanhMoi * $scope.TTMonHoc.TyLeThucHanh) + (item.DiemSeminarMoi * $scope.TTMonHoc.TyLeSeminar) + (item.DiemThiMoi * $scope.TTMonHoc.TyLeThi)) / 100) < $scope.TTMonHoc.DiemDau) {
                            item.Dau = false;
                        }
                        item.DiemThucHanh = item.DiemThucHanhMoi;
                        item.DiemChuyenCan = item.DiemChuyenCanMoi;
                        item.DiemSeminar = item.DiemSeminarMoi;
                        item.DiemThi = item.DiemThiMoi;
                        item.GiangVienDieuChinhCuoiKi = $scope.authentication.userName;
                        var d = new Date()
                        item.NgayDieuChinhCuoiKi = d.toLocaleTimeString();
                        apiService.put('api/ketqua/themdiemcuoiki', item, function (result) {


                            notificationService.displaySuccess("Sửa điểm cuối kì thành công cho sinh viên " + item.MaSoSV);
                            $scope.bang2 = false;



                        }, function () {
                            notificationService.displayWarning('Thêm không thành công');
                        });

                    });


                });


            }
            $scope.updateDiemGiuaKi = updateDiemGiuaKi;
            function updateDiemGiuaKi() {
                $ngBootbox.confirm('Xác nhận thêm điểm').then(function () {

                    angular.forEach($scope.SuaDiemGiuaKi, function (item) {
                        item.DiemKiemTra = item.DiemKiemTraMoi;
                        item.GiangVienDieuChinhGiuaKi = $scope.authentication.userName;
                        var d = new Date()
                        item.NgayDieuChinhGiuaKi = d.toLocaleTimeString();
                        apiService.put('api/ketqua/themdiemcuoiki', item, function (result) {


                            notificationService.displaySuccess("Sửa điểm giữa kì thành công cho sinh viên " + item.MaSoSV);
                            $scope.bang1 = false;



                        }, function () {
                            notificationService.displayWarning('Thêm không thành công');
                        });

                    });


                });


            }
            $scope.huy = function () {
                $scope.khung1 = false;
                $scope.khung2 = false;
                $scope.bang1 = false;
                $scope.bang2 = false;
            }
            //#endregion

        }

        
    }
  
})(angular.module('platformTH_GV.suadiem'));