﻿(function (app) {
    app.controller('chiTietNhacNhoController', chiTietNhacNhoController);

    function chiTietNhacNhoController($scope, apiService, $stateParams, loginService, $injector, notificationService) {
        if ($scope.kiemtrarole('SinhVien') === 'SinhVien' && $scope.kiemtrarole('Admin') != 'Admin') {
            loginService.logOut();
            var stateService = $injector.get('$state');
            stateService.go('dangnhap');
            notificationService.displayError("Bạn không có quyền truy cập trang này");
        }
        else {
            //#region
            $scope.chiTietNhacNho = [];
            $scope.MaSoNN = $stateParams.MaSoNN;


            $scope.getChiTietNhacNho = getChiTietNhacNho;


            function getChiTietNhacNho() {
                var config = {
                    params: {
                        MaSoNN: $scope.MaSoNN
                    }
                }
                apiService.get('/api/nhacnho-vc/Getchitietnhacnho/', config, function (result) {
                    $scope.chiTietNhacNho = result.data;
                }, function () {
                    console.log('Load thông tin failed.');
                });

            }
            $scope.getChiTietNhacNho();
            //#endregion

        }
      

    }
})(angular.module('platformTH_GV.chitietnhacnho'));