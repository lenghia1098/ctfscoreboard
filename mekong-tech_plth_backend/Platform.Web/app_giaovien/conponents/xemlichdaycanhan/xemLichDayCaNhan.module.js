﻿/// <reference path="../../../assets/giaovien/libs/angular/angular.js" />


(function () {
    angular.module('platformTH_GV.xemlichdaycanhan', ['platformTH_GV.common']).config(config);

    config.$inject = ['$stateProvider', '$urlRouterProvider'];
    window.appVersion = angular.element(document.getElementsByTagName('html')).attr('data-ver');

    function config($stateProvider, $urlRouterProvider) {
        $stateProvider.state('xemlichdaycanhan', {
            url: "/xemlichdaycanhan/:msvc",
            parent: 'base',
            templateUrl: "app_giaovien/conponents/xemlichdaycanhan/xemLichDayCaNhanView.html?v=" + window.appVersion,
            controller: "xemLichDayCaNhanController"
        });

    }
})();