﻿using Platform.Model;
using Platform.Model.Models;
using Platform.Web.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Platform.Web.infratructure.extensions
{
    public static class EntityExtensions
    {
        public static void UpdateSinhVien(this SinhVien sinhVien, SinhVienViewModel sinhVienVm)
        {

            sinhVien.MaSoSV = sinhVienVm.MaSoSV;
            sinhVien.HoLot = sinhVienVm.HoLot;
            sinhVien.Ten = sinhVienVm.Ten;
            sinhVien.NgaySinh = sinhVienVm.NgaySinh;
            sinhVien.HoKhau = sinhVienVm.HoKhau;
            sinhVien.DiaChi = sinhVienVm.DiaChi;
            sinhVien.Email = sinhVienVm.Email;
            sinhVien.DienThoai = sinhVienVm.DienThoai;
            sinhVien.Hinh = sinhVienVm.Hinh;
            sinhVien.SoTaiKhoan = sinhVienVm.SoTaiKhoan;
            sinhVien.TenChuTaiKhoan = sinhVienVm.TenChuTaiKhoan;
            sinhVien.ChiNhanh = sinhVienVm.ChiNhanh;
            sinhVien.HeDaoTao = sinhVienVm.HeDaoTao;
            sinhVien.DaXoa = sinhVienVm.DaXoa;
            sinhVien.NguoiXoa = sinhVienVm.NguoiXoa;
            sinhVien.ThoiGianXoa = sinhVienVm.ThoiGianXoa;

        }
        public static void UpdateCauHoiThuongGap(this CauHoiThuongGap cauHoiThuongGap, CauHoiThuongGapViewModel cauHoiThuongGapVm)
        {

            cauHoiThuongGap.MaSoCauHoi = cauHoiThuongGapVm.MaSoCauHoi;
            cauHoiThuongGap.NoiDungCauHoi = cauHoiThuongGapVm.NoiDungCauHoi;
            cauHoiThuongGap.TraLoi = cauHoiThuongGapVm.TraLoi;
            cauHoiThuongGap.NgayTao = cauHoiThuongGapVm.NgayTao;
            cauHoiThuongGap.NgayTraLoi = cauHoiThuongGapVm.NgayTraLoi;
        }
        public static void UpdateChuongTrinhDaoTao_MonHoc(this ChuongTrinhDaoTao_MonHoc chuongTrinhDaoTao_MonHoc, ChuongTrinhDaoTao_MonHocViewModel chuongTrinhDaoTao_MonHocVm)
        {
            chuongTrinhDaoTao_MonHoc.MaCT = chuongTrinhDaoTao_MonHocVm.MaCT;
            chuongTrinhDaoTao_MonHoc.MaMH = chuongTrinhDaoTao_MonHocVm.MaMH;
            chuongTrinhDaoTao_MonHoc.HocKy = chuongTrinhDaoTao_MonHocVm.HocKy;
        }

        public static void UpdateChuongTrinhDaoTao(this ChuongTrinhDaoTao chuongTrinhDaoTao, ChuongTrinhDaoTaoViewModel chuongTrinhDaoTaoVm)
        {
            chuongTrinhDaoTao.MaCT = chuongTrinhDaoTaoVm.MaCT;
            chuongTrinhDaoTao.TenChuong = chuongTrinhDaoTaoVm.TenChuong;
        }
        public static void UpdateDon_SinhVien(this Don_SinhVien don_SinhVien, Don_SinhVienViewModel don_SinhVienVm)
        {
            don_SinhVien.MaDon = don_SinhVienVm.MaDon;
            don_SinhVien.MaSoSV = don_SinhVienVm.MaSoSV;
            don_SinhVien.NgayNop = don_SinhVienVm.NgayNop;
        }
        public static void UpdateDon_VienChuc(this Don_VienChuc don_VienChuc, Don_VienChucViewModel don_VienChucVm)
        {
            don_VienChuc.MaDon = don_VienChucVm.MaDon;
            don_VienChuc.MaSoVC = don_VienChucVm.MaSoVC;
            don_VienChuc.NgayNop = don_VienChucVm.NgayNop;
        }
        public static void UpdateDonSinhVien(this DonSinhVien donSinhVien, DonSinhVienViewModel donSinhVienVm)
        {
            donSinhVien.MaDon = donSinhVienVm.MaDon;
            donSinhVien.ChuDe = donSinhVienVm.ChuDe;
            donSinhVien.NoiDung = donSinhVienVm.NoiDung;
            donSinhVien.ChuThich = donSinhVienVm.ChuThich;
        }
        public static void UpdateDonVienChuc(this DonVienChuc donVienChuc, DonVienChucViewModel donVienChucVm)
        {
            donVienChuc.MaDon = donVienChucVm.MaDon;
            donVienChuc.ChuDe = donVienChucVm.ChuDe;
            donVienChuc.NoiDung = donVienChucVm.NoiDung;
            donVienChuc.ChuThich = donVienChucVm.ChuThich;

        }
        public static void UpdateLoi(this Loi loi, LoiViewModel loiVm)
        {
            loi.MaLoi = loiVm.MaLoi;
            loi.ThongBao = loiVm.ThongBao;
            loi.NgayGio = loiVm.NgayGio;
            loi.GhiChu = loiVm.GhiChu;
        }
        public static void UpdateLop_ChuongTrinhDaoTao(this Lop_ChuongTrinhDaoTao lop_ChuongTrinhDaoTao, Lop_ChuongTrinhDaoTaoViewModel lop_ChuongTrinhDaoTaoVm)
        {
            lop_ChuongTrinhDaoTao.MaLop = lop_ChuongTrinhDaoTaoVm.MaLop;
            lop_ChuongTrinhDaoTao.MaCT = lop_ChuongTrinhDaoTaoVm.MaCT;
            lop_ChuongTrinhDaoTao.HocKyApDung = lop_ChuongTrinhDaoTaoVm.HocKyApDung;
            lop_ChuongTrinhDaoTao.NamApDung = lop_ChuongTrinhDaoTaoVm.NamApDung;
        }

        public static void UpdateLop(this Lop lop, LopViewModel lopVm)
        {
            lop.MaLop = lopVm.MaLop;
            lop.TenLop = lopVm.TenLop;
            lop.NienKhoa = lopVm.NienKhoa;
            lop.MaNganh = lopVm.MaNganh;
        }

        public static void UpdateMonHoc(this MonHoc monHoc, MonHocViewModel monHocVm)
        {
            monHoc.MaMH = monHocVm.MaMH;
            monHoc.TenMH = monHocVm.TenMH;
            monHoc.SoTinChi = monHocVm.SoTinChi;
            monHoc.SoTinChiHocPhi = monHocVm.SoTinChiHocPhi;
            monHoc.SoGioLyThuyet = monHocVm.SoGioLyThuyet;
            monHoc.SoGioThucHanh = monHocVm.SoGioThucHanh;
        }

        public static void UpdateNganh(this Nganh nganh, NganhViewModel nganhVm)
        {
            nganh.MaNganh = nganhVm.MaNganh;
            nganh.TenNganh = nganhVm.TenNganh;
            nganh.MaPK = nganhVm.MaPK;
        }
        public static void UpdateNhacNho(this NhacNho nhacNho, NhacNhoViewModel nhacNhoVm)
        {
            nhacNho.MaSoNN = nhacNhoVm.MaSoNN;
            nhacNho.NoiDungNhac = nhacNhoVm.NoiDungNhac;
        }
        public static void UpdateNhacNho_SinhVien(this NhacNho_SinhVien nhacNho_SinhVien, NhacNho_SinhVienViewModel nhacNho_SinhVienVm)
        {
            nhacNho_SinhVien.MaSoNN = nhacNho_SinhVienVm.MaSoNN;
            nhacNho_SinhVien.MaSoSV = nhacNho_SinhVienVm.MaSoSV;
            nhacNho_SinhVien.NgayNhac = nhacNho_SinhVienVm.NgayNhac;
            nhacNho_SinhVien.HoanThanh = nhacNho_SinhVienVm.HoanThanh;
        }
        public static void UpdateVienChuc(this VienChuc vienChuc, VienChucViewModel vienChucVm)
        {
            vienChuc.MaSoVC = vienChucVm.MaSoVC;
            vienChuc.HoTen = vienChucVm.HoTen;
            vienChuc.NgaySinh = vienChucVm.NgaySinh;
            vienChuc.DiaChi = vienChucVm.DiaChi;
            vienChuc.DienThoai = vienChucVm.DienThoai;
            vienChuc.Email = vienChucVm.Email;
            vienChuc.Hinh = vienChucVm.Hinh;
            vienChuc.DaXoa = vienChucVm.NghiViec;
            vienChuc.NguoiXoa = vienChucVm.NguoiXoa;
            vienChuc.ThoiGianXoa = vienChucVm.ThoiGianXoa;
            vienChuc.MaPK = vienChucVm.MaPK;
            vienChuc.Phai = vienChucVm.Phai;
            vienChuc.SHCC = vienChucVm.SHCC;
            vienChuc.CMND = vienChucVm.CMND;
            vienChuc.NgayCap = vienChucVm.NgayCap;
            vienChuc.NoiCap = vienChucVm.NoiCap;
            vienChuc.MST = vienChucVm.MST;
            vienChuc.QuocTich = vienChucVm.QuocTich;
            vienChuc.DanToc = vienChucVm.DanToc;
            vienChuc.SoTaiKhoan = vienChucVm.SoTaiKhoan;
            vienChuc.NganHang = vienChucVm.NganHang;
            vienChuc.ChiNhanh = vienChucVm.ChiNhanh;
            vienChuc.ChucVu = vienChucVm.ChucVu;
            vienChuc.CQCongTac = vienChucVm.CQCongTac;
            vienChuc.DiaChiCQ = vienChucVm.DiaChiCQ;
            vienChuc.WebsiteCQ = vienChucVm.WebsiteCQ;
            vienChuc.LanhDaoCQ = vienChucVm.LanhDaoCQ;
            vienChuc.DienThoaiCQ = vienChucVm.DienThoaiCQ;
            vienChuc.DienThoaiLanhDao = vienChucVm.DienThoaiLanhDao;
            vienChuc.NamBatDauCongTac = vienChucVm.NamBatDauCongTac;
            vienChuc.NamNghiHuu = vienChucVm.NamNghiHuu;
            vienChuc.HocVu = vienChucVm.HocVu;
            vienChuc.NamDat = vienChucVm.NamDat;
            vienChuc.NuocTotNghiep = vienChucVm.NuocTotNghiep;
            vienChuc.ChuyenNganh = vienChucVm.ChuyenNganh;
            vienChuc.ChuyenMonHienTai = vienChucVm.ChuyenMonHienTai;
            vienChuc.HocHam = vienChucVm.HocHam;
            vienChuc.NamCongNhan = vienChucVm.NamCongNhan;
            vienChuc.ChucDanh = vienChucVm.ChucDanh;
        }


        public static void UpdateVienChuc_ViecPhaiLam(this VienChuc_ViecPhaiLam vienChuc_ViecPhaiLam, VienChuc_ViecPhaiLamViewModel vienChuc_ViecPhaiLamVm)
        {
            vienChuc_ViecPhaiLam.MaSoVC = vienChuc_ViecPhaiLamVm.MaSoVC;
            vienChuc_ViecPhaiLam.MaSoCongViec = vienChuc_ViecPhaiLamVm.MaSoCongViec;
            vienChuc_ViecPhaiLam.ThoiHanHoanThanh = vienChuc_ViecPhaiLamVm.ThoiHanHoanThanh;
            vienChuc_ViecPhaiLam.TrangThai = vienChuc_ViecPhaiLamVm.TrangThai;
            vienChuc_ViecPhaiLam.HanChot = vienChuc_ViecPhaiLamVm.HanChot;
            vienChuc_ViecPhaiLam.NgayHoanThanh = vienChuc_ViecPhaiLamVm.NgayHoanThanh;
            vienChuc_ViecPhaiLam.DaHoanThanh = vienChuc_ViecPhaiLamVm.DaHoanThanh;
        }


        public static void UpdateViecPhaiLamVienChuc(this ViecPhaiLamVienChuc viecPhaiLamVienChuc, ViecPhaiLamVienChucViewModel viecPhaiLamVienChucVm)
        {
            viecPhaiLamVienChuc.MaSoCongViec = viecPhaiLamVienChucVm.MaSoCongViec;
            viecPhaiLamVienChuc.TenCongViec = viecPhaiLamVienChucVm.TenCongViec;
            viecPhaiLamVienChuc.MoTa = viecPhaiLamVienChucVm.MoTa;

        }



        public static void UpdateViecPhaiLamSinhVien(this ViecPhaiLamSinhVien viecPhaiLamSinhVien, ViecPhaiLamSinhVienViewModel viecPhaiLamSinhVienVm)
        {
            viecPhaiLamSinhVien.MaSoCongViec = viecPhaiLamSinhVienVm.MaSoCongViec;
            viecPhaiLamSinhVien.TenCongViec = viecPhaiLamSinhVienVm.TenCongViec;
            viecPhaiLamSinhVien.MoTa = viecPhaiLamSinhVienVm.MoTa;

        }


        public static void UpdateTruong(this Truong truong, TruongViewModel truongVm)
        {
            truong.MaTruong = truongVm.MaTruong;
            truong.Ten = truongVm.Ten;
            truong.DonViChuQuan = truongVm.DonViChuQuan;
            truong.DiaChi = truongVm.DiaChi;
            truong.SoDienThoai = truongVm.SoDienThoai;

        }


        public static void UpdateThongBao(this ThongBao thongBao, ThongBaoViewModel thongBaoVm)
        {
            thongBao.MaSoTB = thongBaoVm.MaSoTB;
            thongBao.NoiDung = thongBaoVm.NoiDung;
            thongBao.NgayThongBao = thongBaoVm.NgayThongBao;

        }

        public static void UpdateSinhVien_ViecPhaiLam(this SinhVien_ViecPhaiLam sinhVien_ViecPhaiLam, SinhVien_ViecPhaiLamViewModel sinhVien_ViecPhaiLamVm)
        {
            sinhVien_ViecPhaiLam.MaSoSV = sinhVien_ViecPhaiLamVm.MaSoSV;
            sinhVien_ViecPhaiLam.MaSoCongViec = sinhVien_ViecPhaiLamVm.MaSoCongViec;
            sinhVien_ViecPhaiLam.ThoiHanHoanThanh = sinhVien_ViecPhaiLamVm.ThoiHanHoanThanh;
            sinhVien_ViecPhaiLam.TrangThai = sinhVien_ViecPhaiLamVm.TrangThai;
            sinhVien_ViecPhaiLam.HanChot = sinhVien_ViecPhaiLamVm.HanChot;
            sinhVien_ViecPhaiLam.NgayHoanThanh = sinhVien_ViecPhaiLamVm.NgayHoanThanh;
            sinhVien_ViecPhaiLam.DaHoanThanh = sinhVien_ViecPhaiLamVm.DaHoanThanh;

        }


        public static void UpdatePhongKhoa(this PhongKhoa phongKhoa, PhongKhoaViewModel phongKhoaVm)
        {
            phongKhoa.MaPK = phongKhoaVm.MaPK;
            phongKhoa.TenPhongKhoa = phongKhoaVm.TenPhongKhoa;
            phongKhoa.MaTruong = phongKhoaVm.MaTruong;

        }


        public static void UpdatePhongKhoa_VienChuc(this PhongKhoa_VienChuc phongKhoa_VienChuc, PhongKhoa_VienChucViewModel phongKhoa_VienChucVm)
        {
            phongKhoa_VienChuc.MaPK = phongKhoa_VienChucVm.MaPK;
            phongKhoa_VienChuc.MaSoVC = phongKhoa_VienChucVm.MaSoVC;
            phongKhoa_VienChuc.NgayBatDau = phongKhoa_VienChucVm.NgayBatDau;

        }
        public static void UpdateDangKy(this DangKy dangKy, DangKyViewModel dangKyVm)
        {
            dangKy.MaSoSV = dangKyVm.MaSoSV;
            dangKy.MaNhom = dangKyVm.MaNhom;
            dangKy.NgayDangKy = dangKyVm.NgayDangKy;
            dangKy.DaDongTien = dangKyVm.DaDongTien;
            dangKy.GiaHan = dangKyVm.GiaHan;

        }
        public static void UpdateGioHoc(this GioHoc gioHoc, GioHocViewModel gioHocVm)
        {
            gioHoc.Tiet = gioHocVm.Tiet;
            gioHoc.Gio = gioHocVm.Gio;

        }
        public static void UpdateKetQua(this KetQua ketQua, KetQuaViewModel ketQuaVm)
        {
            ketQua.MaSoSV = ketQuaVm.MaSoSV;
            ketQua.MaNhom = ketQuaVm.MaNhom;
            ketQua.DiemChuyenCan = ketQuaVm.DiemChuyenCan;
            ketQua.DiemKiemTra = ketQuaVm.DiemKiemTra;
            ketQua.DiemThucHanh = ketQuaVm.DiemThucHanh;
            ketQua.DiemSeminar = ketQuaVm.DiemSeminar;
            ketQua.DiemThi = ketQuaVm.DiemThi;
            ketQua.DiemTongKet = ketQuaVm.DiemTongKet;
            ketQua.DiemChu = ketQuaVm.DiemChu;
            ketQua.NgayNhapGiuaKi = ketQuaVm.NgayNhapGiuaKi;
            ketQua.NgayNhapCuoiKi = ketQuaVm.NgayNhapCuoiKi;
            ketQua.NgayDieuChinhGiuaKi = ketQuaVm.NgayDieuChinhGiuaKi;
            ketQua.NgayDieuChinhCuoiKi = ketQuaVm.NgayDieuChinhCuoiKi;
            ketQua.GiangVienNhapGiuaKi = ketQuaVm.GiangVienNhapGiuaKi;
            ketQua.GiangVienNhapCuoiKi = ketQuaVm.GiangVienNhapCuoiKi;
            ketQua.GiangVienDieuChinhGiuaKi = ketQuaVm.GiangVienDieuChinhGiuaKi;
            ketQua.GiangVienDieuChinhCuoiKi = ketQuaVm.GiangVienDieuChinhCuoiKi;
            ketQua.Dau = ketQuaVm.Dau;

        }
        public static void UpdateLichThi(this LichThi lichThi, LichThiViewModel lichThiVm)
        {
            lichThi.LichThiID = lichThiVm.LichThiID;
            lichThi.MaNhom = lichThiVm.MaNhom;
            lichThi.KyHieuPhongThi = lichThiVm.KyHieuPhongThi;
            lichThi.NgayThi = lichThiVm.NgayThi;
            lichThi.GioThi = lichThiVm.GioThi;
            lichThi.MaPH = lichThiVm.MaPH;

        }

        public static void UpdatNhom(this Nhom nhom, NhomViewModel nhomHocVm)
        {
            nhom.MaNhom = nhomHocVm.MaNhom;
            nhom.MaMH = nhomHocVm.MaMH;
            nhom.HocKy = nhomHocVm.HocKy;
            nhom.NamHoc = nhomHocVm.NamHoc;
            nhom.NgayBatDau = nhomHocVm.NgayBatDau;
            nhom.NgayKetThuc = nhomHocVm.NgayKetThuc;
            nhom.SoSinhVienToiDa = nhomHocVm.SoSinhVienToiDa;
            nhom.SoSinhVienToiThieu = nhomHocVm.SoSinhVienToiThieu;
            nhom.Dong = nhomHocVm.Dong;
        }
        public static void UpdateNhom_TamThoi(this Nhom_TamThoi nhom_TamThoi, Nhom_TamThoiViewModel nhom_TamThoiVm)
        {
            nhom_TamThoi.MaNhom = nhom_TamThoiVm.MaNhom;
            nhom_TamThoi.MaMH = nhom_TamThoiVm.MaMH;
            nhom_TamThoi.HocKy = nhom_TamThoiVm.HocKy;
            nhom_TamThoi.NamHoc = nhom_TamThoiVm.NamHoc;
            nhom_TamThoi.NgayBatDau = nhom_TamThoiVm.NgayBatDau;
            nhom_TamThoi.NgayKetThuc = nhom_TamThoiVm.NgayKetThuc;
            nhom_TamThoi.SoSinhVienToiDa = nhom_TamThoiVm.SoSinhVienToiDa;
            nhom_TamThoi.SoSinhVienToiThieu = nhom_TamThoiVm.SoSinhVienToiThieu;
            nhom_TamThoi.TongSoTienHocPhiHocKy = nhom_TamThoiVm.TongSoTienHocPhiHocKy;
            nhom_TamThoi.DaXoa = nhom_TamThoiVm.DaXoa;
            nhom_TamThoi.NguoiXoa = nhom_TamThoiVm.NguoiXoa;
            nhom_TamThoi.ThoiGianXoa = nhom_TamThoiVm.ThoiGianXoa;


        }
        public static void UpdatePhongHoc(this PhongHoc phongHoc, PhongHocViewModel phongHocVm)
        {
            phongHoc.MaPH = phongHocVm.MaPH;
            phongHoc.TenPH = phongHocVm.TenPH;
            phongHoc.SoCho = phongHocVm.SoCho;
            phongHoc.DaXoa = phongHocVm.DaXoa;
            phongHoc.GhiChu = phongHocVm.GhiChu;
            phongHoc.NguoiXoa = phongHocVm.NguoiXoa;
            phongHoc.ThoiGianXoa = phongHocVm.ThoiGianXoa;

        }

        public static void UpdateThoiKhoaBieu(this ThoiKhoaBieu thoiKhoaBieu, ThoiKhoaBieuViewModel thoiKhoaBieuVm)
        {
            thoiKhoaBieu.MaNhom = thoiKhoaBieuVm.MaNhom;
            thoiKhoaBieu.Tiet = thoiKhoaBieuVm.Tiet;
            thoiKhoaBieu.Thu = thoiKhoaBieuVm.Thu;
            thoiKhoaBieu.MaPH = thoiKhoaBieuVm.MaPH;
            thoiKhoaBieu.SoTiet = thoiKhoaBieuVm.SoTiet;
            thoiKhoaBieu.Tuan = thoiKhoaBieuVm.Tuan;

        }
        public static void UpdateDangKy_TamThoi(this DangKy_TamThoi dangKy_TamThoi, DangKy_TamThoiViewModel dangKy_TamThoiVm)
        {
            dangKy_TamThoi.MaSoSV = dangKy_TamThoiVm.MaSoSV;
            dangKy_TamThoi.MaNhom = dangKy_TamThoiVm.MaNhom;
            dangKy_TamThoi.NgayDangKy = dangKy_TamThoiVm.NgayDangKy;
            dangKy_TamThoi.DaDongTien = dangKy_TamThoiVm.DaDongTien;
            dangKy_TamThoi.GiaHan = dangKy_TamThoiVm.GiaHan;
            dangKy_TamThoi.DaXoa = dangKy_TamThoiVm.DaXoa;
            dangKy_TamThoi.NguoiXoa = dangKy_TamThoiVm.NguoiXoa;
            dangKy_TamThoi.ThoiGianXoa = dangKy_TamThoiVm.ThoiGianXoa;
            dangKy_TamThoi.ID = dangKy_TamThoiVm.ID;

        }
        public static void TinTuc(this TinTuc tinTuc, TinTucViewModel tintucVm)
        {
            tinTuc.MaTinTuc = tintucVm.MaTinTuc;
            tinTuc.NoiDung = tintucVm.NoiDung;
            tinTuc.Ngay = tintucVm.Ngay;
            tinTuc.GhiChu = tintucVm.GhiChu;
            tinTuc.DaXoa = tintucVm.DaXoa;

            tinTuc.NguoiXoa = tintucVm.NguoiXoa;
            tinTuc.ThoiGianXoa = tintucVm.ThoiGianXoa;



        }
        public static void UpdateNhacNho_VienChuc(this NhacNho_VienChuc nhacNho_VienChuc, NhacNho_VienChucViewModel nhacnho_vienchucVm)
        {
            nhacNho_VienChuc.MaSoNN = nhacnho_vienchucVm.MaSoNN;
            nhacNho_VienChuc.MaSoVC = nhacnho_vienchucVm.MaSoVC;
            nhacNho_VienChuc.GhiChu = nhacnho_vienchucVm.GhiChu;


        }
        public static void UpdateThoiHanDangKyMonHoc(this ThoiHanDangKyMonHoc thoiHanDangKyMonHoc, ThoiHanDangKyMonHocViewModel thoiHanDangKyMonHocVm)
        {
            thoiHanDangKyMonHoc.NgayDau = thoiHanDangKyMonHocVm.NgayDau;
            thoiHanDangKyMonHoc.NgayCuoi = thoiHanDangKyMonHocVm.NgayCuoi;
            thoiHanDangKyMonHoc.ChoPhep = thoiHanDangKyMonHocVm.ChoPhep;
            thoiHanDangKyMonHoc.NguoiTao = thoiHanDangKyMonHocVm.NguoiTao;




        }
        public static void UpdateHocKyHienTai(this HocKyHienTai hocKyHienTai, HocKyHienTaiViewModel hocKyHienTaiVm)
        {
            hocKyHienTai.NamHoc = hocKyHienTaiVm.NamHoc;
            hocKyHienTai.NgayBatDauHocKy = hocKyHienTaiVm.NgayBatDauHocKy;
            hocKyHienTai.NgayKetThucHocKy = hocKyHienTaiVm.NgayKetThucHocKy;
            hocKyHienTai.HocKy = hocKyHienTaiVm.HocKy;
            hocKyHienTai.DaTinhHocPhi = hocKyHienTaiVm.DaTinhHocPhi;
        }
        public static void UpdateNgayLeThayDoi(this NgayLeThayDoi ngayLeThayDoi, NgayLeThayDoiViewModel ngayLeThayDoiVm)
        {
            ngayLeThayDoi.NamHoc = ngayLeThayDoiVm.NamHoc;
            ngayLeThayDoi.TenNgayNghi = ngayLeThayDoiVm.TenNgayNghi;
            ngayLeThayDoi.NgayBatDauNghi = ngayLeThayDoiVm.NgayBatDauNghi;
            ngayLeThayDoi.NgayKetThucNghi = ngayLeThayDoiVm.NgayKetThucNghi;
        }   
    public static void LichGacThi(this LichGacThi lichGacThi, LichGacThiViewModel lichGacThiVm)
        {
            lichGacThi.ID = lichGacThiVm.ID;
            lichGacThi.MaSoGiangVien = lichGacThiVm.MaSoGiangVien;
            lichGacThi.MaNhom = lichGacThiVm.MaNhom;
            lichGacThi.NoiLayDeThi = lichGacThiVm.NoiLayDeThi;


        }
         public static void HocKyHienTai(this HocKyHienTai hocKyHienTai, HocKyHienTaiViewModel hocKyHienTaiVm)
        {
            hocKyHienTai.ID = hocKyHienTaiVm.ID;
            hocKyHienTai.NamHoc = hocKyHienTaiVm.NamHoc;
            hocKyHienTai.NgayBatDauHocKy = hocKyHienTaiVm.NgayBatDauHocKy;
            hocKyHienTai.NgayKetThucHocKy = hocKyHienTaiVm.NgayKetThucHocKy;


        }

        public static void UpdateSinhVienDauVao(this SinhVienDauVao sinhVienDauVao, SinhVienDauVaoViewModel sinhVienDauVaoVm)
        {

            sinhVienDauVao.SoBaoDanh = sinhVienDauVaoVm.SoBaoDanh;
            sinhVienDauVao.HoLot = sinhVienDauVaoVm.HoLot;
            sinhVienDauVao.Ten = sinhVienDauVaoVm.Ten;
            sinhVienDauVao.Phai = sinhVienDauVaoVm.Phai;
            sinhVienDauVao.NgaySinh = sinhVienDauVaoVm.NgaySinh;
            sinhVienDauVao.NoiSinh = sinhVienDauVaoVm.NoiSinh;
            sinhVienDauVao.dm1 = sinhVienDauVaoVm.dm1;
            sinhVienDauVao.dm2 = sinhVienDauVaoVm.dm2;
            sinhVienDauVao.dm3 = sinhVienDauVaoVm.dm3;
            sinhVienDauVao.dtc0 = sinhVienDauVaoVm.dtc0;
            sinhVienDauVao.dtc = sinhVienDauVaoVm.dtc;
            sinhVienDauVao.NamTrungTuyen = sinhVienDauVaoVm.NamTrungTuyen;
            sinhVienDauVao.Huyen = sinhVienDauVaoVm.Huyen;
            sinhVienDauVao.DaDongTienNhapHoc = sinhVienDauVaoVm.DaDongTienNhapHoc;
            sinhVienDauVao.DanToc = sinhVienDauVaoVm.DanToc;
            sinhVienDauVao.Tongiao = sinhVienDauVaoVm.Tongiao;
            sinhVienDauVao.SoDienThoai1 = sinhVienDauVaoVm.SoDienThoai1;
            sinhVienDauVao.SoDienThoai2 = sinhVienDauVaoVm.SoDienThoai2;
            sinhVienDauVao.email = sinhVienDauVaoVm.email;
            sinhVienDauVao.HoKhau = sinhVienDauVaoVm.HoKhau;
            sinhVienDauVao.Anh34 = sinhVienDauVaoVm.Anh34;
            sinhVienDauVao.TenCha = sinhVienDauVaoVm.TenCha;
            sinhVienDauVao.NgheNghiepCha = sinhVienDauVaoVm.NgheNghiepCha;
            sinhVienDauVao.TenMe = sinhVienDauVaoVm.TenMe;
            sinhVienDauVao.NgheNghiepMe = sinhVienDauVaoVm.NgheNghiepMe;
            sinhVienDauVao.NamTotNghiepTHPT = sinhVienDauVaoVm.NamTotNghiepTHPT;
            sinhVienDauVao.NgayNhapHoc = sinhVienDauVaoVm.NgayNhapHoc;
            sinhVienDauVao.NamTotNghiep = sinhVienDauVaoVm.NamTotNghiep;
            sinhVienDauVao.NoiLamViec = sinhVienDauVaoVm.NoiLamViec;
            sinhVienDauVao.DoiTuong = sinhVienDauVaoVm.DoiTuong;
            sinhVienDauVao.KhuVuc = sinhVienDauVaoVm.KhuVuc;
            sinhVienDauVao.Ghichu = sinhVienDauVaoVm.Ghichu;
            sinhVienDauVao.Xoa = sinhVienDauVaoVm.Xoa;
            sinhVienDauVao.DaTotNghiep = sinhVienDauVaoVm.DaTotNghiep;
            sinhVienDauVao.TayNamBo = sinhVienDauVaoVm.TayNamBo;
        }



        public static void UpdateKhungGio(this KhungGio khungGio, KhungGioViewModel khungGioVm)
        {
            khungGio.Tiet = khungGioVm.Tiet;
            khungGio.GioBatDau = khungGioVm.GioBatDau;
            khungGio.GioKetThuc = khungGioVm.GioKetThuc;
            khungGio.id = khungGioVm.id;
            khungGio.NguoiThem = khungGioVm.NguoiThem;
            khungGio.NgayThem = khungGioVm.NgayThem;
        }
        public static void UpdateCongViecDeXuat(this CongViecDeXuat congViecDeXuat, CongViecDeXuatViewModel congViecDeXuatVm)
        {
            congViecDeXuat.MaCVDeXuat = congViecDeXuatVm.MaCVDeXuat;
            congViecDeXuat.TenCVDeXuat = congViecDeXuatVm.TenCVDeXuat;
            congViecDeXuat.DaXoa = congViecDeXuatVm.DaXoa;
            congViecDeXuat.NguoiXoa = congViecDeXuatVm.NguoiXoa;
            congViecDeXuat.ThoiGianXoa  = congViecDeXuatVm.ThoiGianXoa;
        }
        public static void UpdateDonSinhVien_PhongKhoa(this DonSinhVien_PhongKhoa donSinhVien_PhongKhoa, DonSinhVien_PhongKhoaViewModel donSinhVien_PhongKhoaVm)
        {
            donSinhVien_PhongKhoa.MaDon = donSinhVien_PhongKhoaVm.MaDon;
            donSinhVien_PhongKhoa.MaPK = donSinhVien_PhongKhoaVm.MaPK;
            donSinhVien_PhongKhoa.GhiChu = donSinhVien_PhongKhoaVm.GhiChu;
            donSinhVien_PhongKhoa.DaXoa = donSinhVien_PhongKhoaVm.DaXoa;
            donSinhVien_PhongKhoa.NguoiXoa = donSinhVien_PhongKhoaVm.NguoiXoa;
            donSinhVien_PhongKhoa.ThoiGianXoa = donSinhVien_PhongKhoaVm.ThoiGianXoa;
        }
        public static void UpdateDonVienChuc_PhongKhoa(this DonVienChuc_PhongKhoa donVienChuc_PhongKhoa, DonVienChuc_PhongKhoaViewModel donVienChuc_PhongKhoaVm)
        {
            donVienChuc_PhongKhoa.MaDon = donVienChuc_PhongKhoaVm.MaDon;
            donVienChuc_PhongKhoa.MaPK = donVienChuc_PhongKhoaVm.MaPK;
            donVienChuc_PhongKhoa.GhiChu = donVienChuc_PhongKhoaVm.GhiChu;
            donVienChuc_PhongKhoa.DaXoa = donVienChuc_PhongKhoaVm.DaXoa;
            donVienChuc_PhongKhoa.NguoiXoa = donVienChuc_PhongKhoaVm.NguoiXoa;
            donVienChuc_PhongKhoa.ThoiGianXoa = donVienChuc_PhongKhoaVm.ThoiGianXoa;
        }
        public static void UpdateGiangDay(this GiangDay giangDay, GiangDayViewModel giangDayVm)
        {
            giangDay.MaSoVC = giangDayVm.MaSoVC;
            giangDay.MaNhom = giangDayVm.MaNhom;
            giangDay.GhiChu = giangDayVm.GhiChu;
            giangDay.DaXoa = giangDayVm.DaXoa;
            giangDay.NguoiXoa = giangDayVm.NguoiXoa;
            giangDay.ThoiGianXoa = giangDayVm.ThoiGianXoa;
        }
        public static void UpdateGiangDay_TamThoi(this GiangDay_TamThoi giangDay_TamThoi, GiangDay_TamThoiViewModel giangDay_TamThoiVm)
        {
            giangDay_TamThoi.MaSoVC = giangDay_TamThoiVm.MaSoVC;
            giangDay_TamThoi.MaNhom = giangDay_TamThoiVm.MaNhom;
            giangDay_TamThoi.GhiChu = giangDay_TamThoiVm.GhiChu;
            giangDay_TamThoi.DaXoa = giangDay_TamThoiVm.DaXoa;
            giangDay_TamThoi.NguoiXoa = giangDay_TamThoiVm.NguoiXoa;
            giangDay_TamThoi.ThoiGianXoa = giangDay_TamThoiVm.ThoiGianXoa;
        }
        public static void UpdateHocPhi(this HocPhi hocPhi, HocPhiViewModel hocPhiVm)
        {
            hocPhi.MaSoSV = hocPhiVm.MaSoSV;
            hocPhi.TongSoTinChi = hocPhiVm.TongSoTinChi;
            hocPhi.TongSoTinChiHocPhi = hocPhiVm.TongSoTinChiHocPhi;
            hocPhi.SoTienConNo = hocPhiVm.SoTienConNo;
            hocPhi.HocKy = hocPhiVm.HocKy;
            hocPhi.NamHoc = hocPhiVm.NamHoc;
            hocPhi.TongSoHocPhi = hocPhiVm.TongSoHocPhi;
            hocPhi.SoTienDaDong = hocPhiVm.SoTienDaDong;
            hocPhi.SoTienMienGiam = hocPhiVm.SoTienMienGiam;

            hocPhi.DaXoa = hocPhiVm.DaXoa;
            hocPhi.NguoiXoa = hocPhiVm.NguoiXoa;
            hocPhi.ThoiGianXoa = hocPhiVm.ThoiGianXoa;
        }
        public static void UpdateLichGacThi(this LichGacThi lichGacThi, LichGacThiViewModel lichGacThiVm)
        {
            lichGacThi.ID = lichGacThiVm.ID;
            lichGacThi.MaSoGiangVien = lichGacThiVm.MaSoGiangVien;
            lichGacThi.MaNhom = lichGacThiVm.MaNhom;
            lichGacThi.NoiLayDeThi = lichGacThiVm.NoiLayDeThi;
          
        }
        public static void UpdateNgayLe(this NgayLe ngayLe, NgayLeViewModel ngayLeVm)
        {
            ngayLe.MaNgayLe = ngayLeVm.MaNgayLe;
            ngayLe.TenNgayLe = ngayLeVm.TenNgayLe;
            ngayLe.Ngay = ngayLeVm.Ngay;
            ngayLe.TrangThai = ngayLeVm.TrangThai;
        }
        public static void UpdateQuaTrinhDaoTao(this QuaTrinhDaoTao quaTrinhDaoTao, QuaTrinhDaoTaoViewModel quaTrinhDaoTaoVm)
        {
            quaTrinhDaoTao.ID = quaTrinhDaoTaoVm.ID;
            quaTrinhDaoTao.MaSoVC = quaTrinhDaoTaoVm.MaSoVC;
            quaTrinhDaoTao.BacDaoTao = quaTrinhDaoTaoVm.BacDaoTao;
            quaTrinhDaoTao.HeDaoTao = quaTrinhDaoTaoVm.HeDaoTao;
            quaTrinhDaoTao.NganhDaoTao = quaTrinhDaoTaoVm.NganhDaoTao;
            quaTrinhDaoTao.NoiDaoTao = quaTrinhDaoTaoVm.NoiDaoTao;
            quaTrinhDaoTao.QuocGia = quaTrinhDaoTaoVm.QuocGia;
            quaTrinhDaoTao.TenLuanAn = quaTrinhDaoTaoVm.TenLuanAn;
            quaTrinhDaoTao.NamBD = quaTrinhDaoTaoVm.NamBD;
            quaTrinhDaoTao.NamTN = quaTrinhDaoTaoVm.NamTN;
            quaTrinhDaoTao.DaXoa = quaTrinhDaoTaoVm.DaXoa;
            quaTrinhDaoTao.NguoiXoa = quaTrinhDaoTaoVm.NguoiXoa;
            quaTrinhDaoTao.ThoiGianXoa = quaTrinhDaoTaoVm.ThoiGianXoa;
            quaTrinhDaoTao.XepLoaiTotNghiep = quaTrinhDaoTaoVm.XepLoaiTotNghiep;
        }
        public static void UpdateSinhVien_Lop(this SinhVien_Lop sinhVien_Lop, SinhVien_LopViewModel sinhVien_LopVm)
        {
            sinhVien_Lop.MaSoSV = sinhVien_LopVm.MaSoSV;
            sinhVien_Lop.MaLop = sinhVien_LopVm.MaLop;
            sinhVien_Lop.GhiChu = sinhVien_LopVm.GhiChu;
            sinhVien_Lop.DaXoa = sinhVien_LopVm.DaXoa;
            sinhVien_Lop.NguoiXoa = sinhVien_LopVm.NguoiXoa;
            sinhVien_Lop.ThoiGianXoa = sinhVien_LopVm.ThoiGianXoa;
        }
        public static void UpdateThi(this Thi thi, ThiViewModel thiVm)
        {
            thi.LichThiID = thiVm.LichThiID;
            thi.MaSoSV = thiVm.MaSoSV;
            thi.GhiChu = thiVm.GhiChu;
            thi.DaXoa = thiVm.DaXoa;
            thi.NguoiXoa = thiVm.NguoiXoa;
            thi.ThoiGianXoa = thiVm.ThoiGianXoa;
        }
        public static void UpdateThoiGianCongTac(this ThoiGianCongTac thoiGianCongTac, ThoiGianCongTacViewModel thoiGianCongTacVm)
        {
            thoiGianCongTac.ID = thoiGianCongTacVm.ID;
            thoiGianCongTac.MaSoVC = thoiGianCongTacVm.MaSoVC;
            thoiGianCongTac.ThoiGian = thoiGianCongTacVm.ThoiGian;
            thoiGianCongTac.NoiCongTac = thoiGianCongTacVm.NoiCongTac;
            thoiGianCongTac.ChucVu = thoiGianCongTacVm.ChucVu;
            thoiGianCongTac.ChuyenMon = thoiGianCongTacVm.ChuyenMon;
            thoiGianCongTac.DiaChi = thoiGianCongTacVm.DiaChi;
            thoiGianCongTac.DaXoa = thoiGianCongTacVm.DaXoa;
            thoiGianCongTac.NguoiXoa = thoiGianCongTacVm.NguoiXoa;
            thoiGianCongTac.ThoiGianXoa = thoiGianCongTacVm.ThoiGianXoa;
        }
        public static void UpdateThoiKhoaBieu_TamThoi(this ThoiKhoaBieu_TamThoi thoiKhoaBieu_TamThoi, ThoiKhoaBieu_TamThoiViewModel thoiKhoaBieu_TamThoiVm)
        {
            thoiKhoaBieu_TamThoi.MaNhom = thoiKhoaBieu_TamThoiVm.MaNhom;
            thoiKhoaBieu_TamThoi.Tiet = thoiKhoaBieu_TamThoiVm.Tiet;
            thoiKhoaBieu_TamThoi.Thu = thoiKhoaBieu_TamThoiVm.Thu;
            thoiKhoaBieu_TamThoi.MaPH = thoiKhoaBieu_TamThoiVm.MaPH;
            thoiKhoaBieu_TamThoi.SoTiet = thoiKhoaBieu_TamThoiVm.SoTiet;
            thoiKhoaBieu_TamThoi.Tuan = thoiKhoaBieu_TamThoiVm.Tuan;
            thoiKhoaBieu_TamThoi.DaXoa = thoiKhoaBieu_TamThoiVm.DaXoa;
            thoiKhoaBieu_TamThoi.NguoiXoa = thoiKhoaBieu_TamThoiVm.NguoiXoa;
            thoiKhoaBieu_TamThoi.ThoiGianXoa = thoiKhoaBieu_TamThoiVm.ThoiGianXoa;

        }
        public static void UpdateTrinhDoNgoaiNgu(this TrinhDoNgoaiNgu trinhDoNgoaiNgu, TrinhDoNgoaiNguViewModel trinhDoNgoaiNguVm)
        {
            trinhDoNgoaiNgu.ID = trinhDoNgoaiNguVm.ID;
            trinhDoNgoaiNgu.MaSoVC = trinhDoNgoaiNguVm.MaSoVC;
            trinhDoNgoaiNgu.TenNgoaiNgu = trinhDoNgoaiNguVm.TenNgoaiNgu;

            trinhDoNgoaiNgu.Nghe = trinhDoNgoaiNguVm.Nghe;
            trinhDoNgoaiNgu.Noi = trinhDoNgoaiNguVm.Noi;
            trinhDoNgoaiNgu.Viet = trinhDoNgoaiNguVm.Viet;
            trinhDoNgoaiNgu.Doc = trinhDoNgoaiNguVm.Doc;
            trinhDoNgoaiNgu.GhiChu = trinhDoNgoaiNguVm.GhiChu;

            trinhDoNgoaiNgu.DaXoa = trinhDoNgoaiNguVm.DaXoa;
            trinhDoNgoaiNgu.NguoiXoa = trinhDoNgoaiNguVm.NguoiXoa;
            trinhDoNgoaiNgu.ThoiGianXoa = trinhDoNgoaiNguVm.ThoiGianXoa;
        }

        public static void UpdateApplicationGroup(this ApplicationGroup appGroup, ApplicationGroupViewModel appGroupViewModel)
        {
            appGroup.ID = appGroupViewModel.ID;
            appGroup.Name = appGroupViewModel.Name;
        }

        public static void UpdateApplicationRole(this ApplicationRole appRole, ApplicationRoleViewModel appRoleViewModel, string action = "add")
        {
            if (action == "update")
                appRole.Id = appRoleViewModel.Id;
            else
                appRole.Id = Guid.NewGuid().ToString();
            appRole.Name = appRoleViewModel.Name;
            appRole.Description = appRoleViewModel.Description;
        }
        public static void UpdateUser(this ApplicationUser appUser, ApplicationUserViewModel appUserViewModel, string action = "add")
        {

            appUser.Id = appUserViewModel.Id;
            appUser.FullName = appUserViewModel.FullName;
            appUser.BirthDay = appUserViewModel.BirthDay;
            appUser.Email = appUserViewModel.Email;
            appUser.UserName = appUserViewModel.UserName;
            appUser.PhoneNumber = appUserViewModel.PhoneNumber;
        }
        public static void UpdateSinhVienDauVao_MSSV(this SinhVienDauVao_MSSV sinhVienDauVao_mssv, SinhVienDauVao_MSSVViewModel sinhVienDauVao_MSSVVM)
        {
            sinhVienDauVao_mssv.SoBaoDanh = sinhVienDauVao_MSSVVM.SoBaoDanh;
            sinhVienDauVao_mssv.MaSoSV = sinhVienDauVao_MSSVVM.MaSoSV;

        }
        public static void UpdateFlats(this Flats flats, FlatsViewModel flatsVm)
        {
            flats.FlatID = flatsVm.FlatID;
            flats.Flat = flatsVm.Flat;
            flats.TeamID = flatsVm.TeamID;
            flats.IsAttack = flatsVm.IsAttack;
        }
        public static void UpdateTeamInfo(this TeamInfo teamInfo, TeamInfoViewModel teamInfoVm)
        {
            teamInfo.TeamID = teamInfoVm.TeamID;
            teamInfo.Name = teamInfoVm.Name;
            teamInfo.AttackScore = teamInfoVm.AttackScore;
            teamInfo.DefenseScore = teamInfoVm.DefenseScore;
         }
        public static void UpdateServiceCTF(this ServiceCTF serviceCTF, ServiceCTFViewModel serviceCTFVm)
        {
            serviceCTF.ID = serviceCTFVm.ID;
            serviceCTF.ServiceName = serviceCTFVm.ServiceName;
            serviceCTF.TeamID = serviceCTFVm.TeamID;
            serviceCTF.AttackScore = serviceCTFVm.AttackScore;
            serviceCTF.DefenseScore = serviceCTFVm.DefenseScore;
            
    }
    }
}

