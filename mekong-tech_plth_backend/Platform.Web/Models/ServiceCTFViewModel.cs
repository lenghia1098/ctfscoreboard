﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Platform.Web.Models
{
    public class ServiceCTFViewModel
    {
        public int ID { get; set; }
        public string ServiceName { get; set; }
        public string TeamID { get; set; }
        public string AttackScore { get; set; }
        public string DefenseScore { get; set; }
    }
}