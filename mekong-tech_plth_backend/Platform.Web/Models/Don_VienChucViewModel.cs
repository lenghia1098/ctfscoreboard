﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Platform.Web.Models
{
    public class Don_VienChucViewModel
    {
        public long MaDon { get; set; }
        public string MaSoVC { get; set; }
        public System.DateTime NgayNop { get; set; }
        public Nullable<bool> DaXuLy { get; set; }
        public Nullable<bool> DaXoa { get; set; }
        public string NguoiXoa { get; set; }
        public Nullable<System.DateTime> ThoiGianXoa { get; set; }


        public virtual DonVienChucViewModel DonVienChuc { get; set; }
        public virtual VienChucViewModel VienChuc { get; set; }
    }
}