﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Platform.Web.Models
{
    public class PhongKhoaViewModel
    {
        public string MaPK { get; set; }
        public string TenPhongKhoa { get; set; }
        public string MaTruong { get; set; }

        public virtual ICollection<NganhViewModel> Nganhs { get; set; }
        public virtual TruongViewModel Truong { get; set; }
        public virtual ICollection<PhongKhoa_VienChucViewModel> PhongKhoa_VienChuc { get; set; }
        public virtual ICollection<DonSinhVienViewModel> DonSinhViens { get; set; }
        public virtual ICollection<DonVienChucViewModel> DonVienChucs { get; set; }
    }
}