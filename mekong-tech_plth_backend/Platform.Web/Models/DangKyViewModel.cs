﻿using Platform.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Platform.Web.Models
{
    public class DangKyViewModel
    {
        public string MaSoSV { get; set; }
        public string MaNhom { get; set; }
        public Nullable<System.DateTime> NgayDangKy { get; set; }
        public Nullable<bool> DaDongTien { get; set; }
        public Nullable<bool> GiaHan { get; set; }
        public int HocKy { get; set; }
        public virtual Nhom Nhom { get; set; }
        public virtual SinhVien SinhVien { get; set; }
    }
}