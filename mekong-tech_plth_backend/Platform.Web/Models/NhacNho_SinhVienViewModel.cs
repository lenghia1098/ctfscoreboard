﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Platform.Web.Models
{
    public class NhacNho_SinhVienViewModel
    {
        public long MaSoNN { get; set; }
        public string MaSoSV { get; set; }
        public System.DateTime NgayNhac { get; set; }
        public Nullable<bool> HoanThanh { get; set; }
    }
}