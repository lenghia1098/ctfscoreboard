﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Platform.Web.Models
{
    public class CauHoiThuongGapViewModel
    {
        public long MaSoCauHoi { get; set; }
        public string NoiDungCauHoi { get; set; }
        public string TraLoi { get; set; }
        public Nullable<System.DateTime> NgayTao { get; set; }
        public Nullable<System.DateTime> NgayTraLoi { get; set; }
    }
}