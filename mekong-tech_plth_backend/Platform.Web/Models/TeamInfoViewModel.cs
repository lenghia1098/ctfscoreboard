﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Platform.Web.Models
{
    public class TeamInfoViewModel
    {
        public string TeamID { get; set; }
        public string Name { get; set; }
        public string AttackScore { get; set; }
        public string DefenseScore { get; set; }
    }
}