﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Platform.Web.Models
{
    public class NgayLeViewModel
    {
        public string MaNgayLe { get; set; }
        public string TenNgayLe { get; set; }
        public string Ngay { get; set; }
        public Nullable<bool> TrangThai { get; set; }
    }
}