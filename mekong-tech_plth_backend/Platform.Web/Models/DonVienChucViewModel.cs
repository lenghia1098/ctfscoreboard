﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Platform.Web.Models
{
    public class DonVienChucViewModel
    {
        public long MaDon { get; set; }
        public string ChuDe { get; set; }
        public string NoiDung { get; set; }
        public string ChuThich { get; set; }

        public virtual ICollection<Don_VienChucViewModel> Don_VienChuc { get; set; }
        public virtual ICollection<PhongKhoaViewModel> PhongKhoas { get; set; }
    }
}
