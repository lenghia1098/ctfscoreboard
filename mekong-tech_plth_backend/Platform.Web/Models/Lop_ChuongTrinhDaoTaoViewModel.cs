﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Platform.Web.Models
{
    public class Lop_ChuongTrinhDaoTaoViewModel
    {
        public string MaLop { get; set; }
        public string MaCT { get; set; }
        public string HocKyApDung { get; set; }
        public Nullable<System.DateTime> NamApDung { get; set; }

        public virtual ChuongTrinhDaoTaoViewModel ChuongTrinhDaoTao { get; set; }
        public virtual LopViewModel Lop { get; set; }
    }
}