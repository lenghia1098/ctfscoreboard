﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Platform.Web.Models
{
    public class ThoiHanDangKyMonHocViewModel
    {
       
            public int ID { get; set; }
            public Nullable<System.DateTime> NgayDau { get; set; }
            public Nullable<System.DateTime> NgayCuoi { get; set; }
            public Nullable<bool> ChoPhep { get; set; }
            public string NguoiTao { get; set; }
            public Nullable<bool> DaXoa { get; set; }
            public string NguoiXoa { get; set; }
            public Nullable<System.DateTime> ThoiGianXoa { get; set; }
        
    }
}