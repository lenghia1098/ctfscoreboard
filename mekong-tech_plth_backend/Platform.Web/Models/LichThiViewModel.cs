﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Platform.Web.Models
{
    public class LichThiViewModel
    {
        public long LichThiID { get; set; }
        public string MaNhom { get; set; }
        public string KyHieuPhongThi { get; set; }
        public System.DateTime NgayThi { get; set; }
        public string GioThi { get; set; }
        public string MaPH { get; set; }
    }
}