﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Platform.Web.Models
{
    public class PhongKhoa_VienChucViewModel
    {
        public string MaPK { get; set; }
        public string MaSoVC { get; set; }
        public Nullable<System.DateTime> NgayBatDau { get; set; }

        public virtual PhongKhoaViewModel PhongKhoa { get; set; }
        public virtual VienChucViewModel VienChuc { get; set; }
    }
}