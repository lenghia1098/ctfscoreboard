﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Platform.Web.Models
{
    public class TrinhDoNgoaiNguViewModel
    {
        public long ID { get; set; }

        public string MaSoVC { get; set; }
        public string TenNgoaiNgu { get; set; }
        public string Nghe { get; set; }
        public string Noi { get; set; }
        public string Doc { get; set; }
        public string Viet { get; set; }
        public string GhiChu { get; set; }
        public Nullable<bool> DaXoa { get; set; }
        public string NguoiXoa { get; set; }
        public Nullable<System.DateTime> ThoiGianXoa { get; set; }
    }
}