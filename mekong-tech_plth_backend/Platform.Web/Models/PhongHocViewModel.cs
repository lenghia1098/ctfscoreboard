﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Platform.Web.Models
{
    public class PhongHocViewModel
    {
        public string MaPH { get; set; }
        public string TenPH { get; set; }
        public Nullable<short> SoCho { get; set; }
        public string GhiChu { get; set; }
        public Nullable<bool> DaXoa { get; set; }
        public string NguoiXoa { get; set; }
        public Nullable<System.DateTime> ThoiGianXoa { get; set; }
    }
}