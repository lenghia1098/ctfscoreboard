﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Platform.Web.Models
{
    public class MonHocViewModel
    {
        public string MaMH { get; set; }
        public string TenMH { get; set; }
        public Nullable<byte> SoTinChi { get; set; }
        public Nullable<double> SoTinChiHocPhi { get; set; }
        public Nullable<short> SoGioLyThuyet { get; set; }
        public Nullable<short> SoGioThucHanh { get; set; }
        public Nullable<double> TyLeChuyenCan { get; set; }
        public Nullable<double> TyLeKiemTra { get; set; }
        public Nullable<double> TyLeThucHanh { get; set; }
        public Nullable<double> TyLeSeminar { get; set; }
        public Nullable<double> TyLeThi { get; set; }
        public Nullable<double> DiemDau { get; set; }
        public Nullable<bool> DaXoa { get; set; }
        public string NguoiXoa { get; set; }
        public Nullable<System.DateTime> ThoiGianXoa { get; set; }

        public virtual ICollection<ChuongTrinhDaoTao_MonHocViewModel> ChuongTrinhDaoTao_MonHoc { get; set; }
    }
}