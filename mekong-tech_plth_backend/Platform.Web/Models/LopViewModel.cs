﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Platform.Web.Models
{
    public class LopViewModel
    {
        public string MaLop { get; set; }
        public string TenLop { get; set; }
        public string NienKhoa { get; set; }
        public string MaNganh { get; set; }

        public virtual ICollection<Lop_ChuongTrinhDaoTaoViewModel> Lop_ChuongTrinhDaoTao { get; set; }
        public virtual NganhViewModel Nganh { get; set; }
        public virtual ICollection<SinhVienViewModel> SinhViens { get; set; }
    }
}