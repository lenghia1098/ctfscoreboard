﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Platform.Web.Models
{
    public class GioHocViewModel
    {
        public short Tiet { get; set; }
        public string Gio { get; set; }
    }
}