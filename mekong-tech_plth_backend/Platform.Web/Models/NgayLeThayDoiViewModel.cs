﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Platform.Web.Models
{
    public class NgayLeThayDoiViewModel
    {
        public int ID { get; set; }
        public string NamHoc { get; set; }
        public string TenNgayNghi { get; set; }
        public Nullable<System.DateTime> NgayBatDauNghi { get; set; }
        public Nullable<System.DateTime> NgayKetThucNghi { get; set; }
    }
}