﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Platform.Web.Models
{
    public class QuaTrinhDaoTaoViewModel
    {
        public long ID { get; set; }
        public string MaSoVC { get; set; }
        public string BacDaoTao { get; set; }
        public string HeDaoTao { get; set; }
        public string NganhDaoTao { get; set; }
        public string NoiDaoTao { get; set; }
        public string QuocGia { get; set; }
        public string TenLuanAn { get; set; }
        public Nullable<System.DateTime> NamBD { get; set; }
        public Nullable<System.DateTime> NamTN { get; set; }
        public Nullable<bool> DaXoa { get; set; }
        public string NguoiXoa { get; set; }
        public Nullable<System.DateTime> ThoiGianXoa { get; set; }
        public string XepLoaiTotNghiep { get; set; }

    }
}