﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Platform.Web.Models
{
    public class TruongViewModel
    {
        public string MaTruong { get; set; }
        public string Ten { get; set; }
        public string DonViChuQuan { get; set; }
        public string DiaChi { get; set; }
        public string SoDienThoai { get; set; }

        public virtual ICollection<PhongKhoaViewModel> PhongKhoas { get; set; }
    }
}