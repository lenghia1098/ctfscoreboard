﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Platform.Web.Models
{
    public class SinhVienDauVao_MSSVViewModel
    {
        public string SoBaoDanh { get; set; }
        public string MaSoSV { get; set; }
    }
}