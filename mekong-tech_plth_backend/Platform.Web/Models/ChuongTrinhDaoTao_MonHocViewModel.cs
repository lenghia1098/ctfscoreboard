﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Platform.Web.Models
{
    public class ChuongTrinhDaoTao_MonHocViewModel
    {
        public string MaCT { get; set; }
        public string MaMH { get; set; }
        public string HocKy { get; set; }

        public virtual ChuongTrinhDaoTaoViewModel ChuongTrinhDaoTao { get; set; }
        public virtual MonHocViewModel MonHoc { get; set; }
    }
}