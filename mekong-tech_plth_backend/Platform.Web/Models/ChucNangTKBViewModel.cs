﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Platform.Web.Models
{
    public class ChucNangTKBViewModel
    {
        public string MaSoSV { get; set; }
        public string MaNhom { get; set; }
        public short Tiet { get; set; }
        public string Thu { get; set; }
        public string MaPH { get; set; }
        public Nullable<int> SoTiet { get; set; }
        public string Tuan { get; set; }
        public string TenMH { get; set; }
    }
}