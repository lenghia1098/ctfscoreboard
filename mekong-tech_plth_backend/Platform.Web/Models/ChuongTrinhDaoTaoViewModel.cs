﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Platform.Web.Models
{
    public class ChuongTrinhDaoTaoViewModel
    {
        public string MaCT { get; set; }
        public string TenChuong { get; set; }

        public virtual ICollection<ChuongTrinhDaoTao_MonHocViewModel> ChuongTrinhDaoTao_MonHoc { get; set; }
       
        public virtual ICollection<Lop_ChuongTrinhDaoTaoViewModel> Lop_ChuongTrinhDaoTao { get; set; }
    }
}
