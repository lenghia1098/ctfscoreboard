﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Platform.Web.Models
{
    public class viecdalamViewModel
    {
        public long MaSoCongViec { get; set; }
        public string TenCongViec { get; set; }
        public string MoTa { get; set; }
        public string MaSoSV { get; set; }
        public string HoLot { get; set; }
        public string Ten { get; set; }
        public Nullable<System.DateTime> NgayHoanThanh { get; set; }
        public string TrangThai { get; set; }
    }
}