﻿using Platform.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Platform.Web.Models
{
    public class HocPhiViewModel
    {
        public string MaSoSV { get; set; }

        public Nullable<double> TongSoTinChi { get; set; }
        public Nullable<double> TongSoTinChiHocPhi { get; set; }
        public Nullable<double> TongSoHocPhi { get; set; }
        public Nullable<double> SoTienDaDong { get; set; }
        public Nullable<double> SoTienMienGiam { get; set; }
        public Nullable<double> SoTienConNo { get; set; }
        public Nullable<bool> DaXoa { get; set; }
        public string NguoiXoa { get; set; }
        public Nullable<System.DateTime> ThoiGianXoa { get; set; }
        public string HocKy { get; set; }
        public string NamHoc { get; set; }

       

       
    }
}