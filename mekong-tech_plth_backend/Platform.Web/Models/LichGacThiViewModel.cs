﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Platform.Web.Models
{
    public class LichGacThiViewModel
    {
        public int ID { get; set; }
        public string MaSoGiangVien { get; set; }
        public string MaNhom { get; set; }
        public string NoiLayDeThi { get; set; }
    }
}