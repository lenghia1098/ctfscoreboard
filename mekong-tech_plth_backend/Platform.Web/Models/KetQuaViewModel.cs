﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Platform.Web.Models
{
    public class KetQuaViewModel
    {
        public string MaSoSV { get; set; }
        public string MaNhom { get; set; }
       
        public Nullable<double> DiemChuyenCan { get; set; }
        public Nullable<double> DiemKiemTra { get; set; }
        public Nullable<double> DiemThucHanh { get; set; }
        public Nullable<double> DiemSeminar { get; set; }
        public Nullable<double> DiemThi { get; set; }
        public Nullable<double> DiemTongKet { get; set; }
        public string DiemChu { get; set; }
        public Nullable<System.DateTime> NgayNhapGiuaKi { get; set; }
        public Nullable<System.DateTime> NgayNhapCuoiKi { get; set; }
        public Nullable<System.DateTime> NgayDieuChinhGiuaKi { get; set; }
        public Nullable<System.DateTime> NgayDieuChinhCuoiKi { get; set; }
        public string GiangVienNhapGiuaKi { get; set; }
        public string GiangVienNhapCuoiKi { get; set; }
        public string GiangVienDieuChinhGiuaKi { get; set; }
        public string GiangVienDieuChinhCuoiKi { get; set; }
        public Nullable<bool> Dau { get; set; }
        public Nullable<bool> DaXoa { get; set; }
        public string NguoiXoa { get; set; }
        public Nullable<System.DateTime> ThoiGianXoa { get; set; }
    }
}