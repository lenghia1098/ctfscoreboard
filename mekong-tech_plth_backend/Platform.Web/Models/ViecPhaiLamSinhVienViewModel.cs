﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Platform.Web.Models
{
    public class ViecPhaiLamSinhVienViewModel
    {
        public int MaSoCongViec { get; set; }
        public string TenCongViec { get; set; }
        public string MoTa { get; set; }

        public virtual ICollection<SinhVien_ViecPhaiLamViewModel> SinhVien_ViecPhaiLam { get; set; }
    }
}