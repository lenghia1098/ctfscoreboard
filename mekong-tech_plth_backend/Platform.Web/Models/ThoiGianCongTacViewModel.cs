﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Platform.Web.Models
{
    public class ThoiGianCongTacViewModel
    {
        public long ID { get; set; }
        public string MaSoVC { get; set; }
       
        public Nullable<System.DateTime> ThoiGian { get; set; }
        public string NoiCongTac { get; set; }
        public string ChucVu { get; set; }
        public string ChuyenMon { get; set; }
        public string DiaChi { get; set; }
        public Nullable<bool> DaXoa { get; set; }
        public string NguoiXoa { get; set; }
       
        public Nullable<System.DateTime> ThoiGianXoa { get; set; }

    }
}