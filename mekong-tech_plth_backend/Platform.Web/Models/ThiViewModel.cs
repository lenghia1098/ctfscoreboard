﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Platform.Web.Models
{
    public class ThiViewModel
    {
        public long LichThiID { get; set; }
        public string MaSoSV { get; set; }
        public string GhiChu { get; set; }
        public Nullable<bool> DaXoa { get; set; }
        public string NguoiXoa { get; set; }
        public Nullable<System.DateTime> ThoiGianXoa { get; set; }
    }
}