﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Platform.Web.Models
{
    public class NhacNhoViewModel
    {
        public long MaSoNN { get; set; }
        public string NoiDungNhac { get; set; }

        public virtual ICollection<SinhVienViewModel> SinhViens { get; set; }
        public virtual ICollection<VienChucViewModel> VienChucs { get; set; }
    }
}