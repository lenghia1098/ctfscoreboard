﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Platform.Web.Models
{
    public class NganhViewModel
    {
        public string MaNganh { get; set; }
        public string TenNganh { get; set; }
        public string MaPK { get; set; }

        public virtual ICollection<LopViewModel> Lops { get; set; }
        public virtual PhongKhoaViewModel PhongKhoa { get; set; }
    }
}