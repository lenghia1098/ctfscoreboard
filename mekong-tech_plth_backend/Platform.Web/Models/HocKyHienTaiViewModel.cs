﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Platform.Web.Models
{
    public class HocKyHienTaiViewModel
    {
        public int ID { get; set; }
        public string NamHoc { get; set; }
        public Nullable<System.DateTime> NgayBatDauHocKy { get; set; }
        public Nullable<System.DateTime> NgayKetThucHocKy { get; set; }
        public Nullable<int> HocKy { get; set; }
        public Nullable<bool> DaTinhHocPhi { get; set; }


    }
}