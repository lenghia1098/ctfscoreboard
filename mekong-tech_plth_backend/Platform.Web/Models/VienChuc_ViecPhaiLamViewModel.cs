﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Platform.Web.Models
{
    public class VienChuc_ViecPhaiLamViewModel
    {
        public string MaSoVC { get; set; }
        public int MaSoCongViec { get; set; }
        public System.DateTime ThoiHanHoanThanh { get; set; }
        public string TrangThai { get; set; }
        public Nullable<System.DateTime> HanChot { get; set; }
        public Nullable<System.DateTime> NgayHoanThanh { get; set; }
        public Nullable<bool> DaHoanThanh { get; set; }
        public Nullable<bool> DaXoa { get; set; }
        public string NguoiXoa { get; set; }
        public Nullable<System.DateTime> ThoiGianXoa { get; set; }


        public virtual ViecPhaiLamVienChucViewModel ViecPhaiLamVienChuc { get; set; }
        public virtual VienChucViewModel VienChuc { get; set; }
    }
}