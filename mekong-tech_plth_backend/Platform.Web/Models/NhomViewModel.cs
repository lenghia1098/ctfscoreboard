﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Platform.Web.Models
{
    public class NhomViewModel
    {
        public string MaNhom { get; set; }
        public string MaMH { get; set; }
        public string HocKy { get; set; }
        public string NamHoc { get; set; }
        public Nullable<System.DateTime> NgayBatDau { get; set; }
        public Nullable<System.DateTime> NgayKetThuc { get; set; }
        public Nullable<short> SoSinhVienToiDa { get; set; }
        public Nullable<short> SoSinhVienToiThieu { get; set; }
        public Nullable<bool> Dong { get; set; }
    }
}