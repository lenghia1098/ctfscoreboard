﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Platform.Web.Models
{
    public class FlatsViewModel
    {
        public string FlatID { get; set; }
        public string Flat { get; set; }
        public string TeamID { get; set; }
        public Nullable<bool> IsAttack { get; set; }

    }
}