﻿using Platform.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Platform.Web.Models
{
    public class DangKy_TamThoiViewModel
    {
        public string MaSoSV { get; set; }
        public string MaNhom { get; set; }
        public Nullable<System.DateTime> NgayDangKy { get; set; }
        public Nullable<bool> DaDongTien { get; set; }
        public Nullable<bool> GiaHan { get; set; }
        public string HocKy { get; set; }
        public Nullable<bool> DaXoa { get; set; }
        public string NguoiXoa { get; set; }
        public Nullable<System.DateTime> ThoiGianXoa { get; set; }

        public int ID { get; set; }


    }
}