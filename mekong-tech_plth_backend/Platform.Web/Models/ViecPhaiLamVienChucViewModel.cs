﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Platform.Web.Models
{
    public class ViecPhaiLamVienChucViewModel
    {
        public int MaSoCongViec { get; set; }
        public string TenCongViec { get; set; }
        public string MoTa { get; set; }

        public virtual ICollection<VienChuc_ViecPhaiLamViewModel> VienChuc_ViecPhaiLam { get; set; }
    }
}