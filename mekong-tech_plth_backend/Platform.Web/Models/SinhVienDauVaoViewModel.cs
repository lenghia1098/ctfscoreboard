﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Platform.Web.Models
{
    public class SinhVienDauVaoViewModel
    {
      
        public string SoBaoDanh { get; set; }
        public string HoLot { get; set; }
        public string Ten { get; set; }
        public string Phai { get; set; }
        public string NgaySinh { get; set; }
        public string NoiSinh { get; set; }
        public string dm1 { get; set; }
        public string dm2 { get; set; }
        public string dm3 { get; set; }
        public string dtc0 { get; set; }
        public string dtc { get; set; }
        public string NamTrungTuyen { get; set; }
        public string Huyen { get; set; }
        public Nullable<bool> DaDongTienNhapHoc { get; set; }
        public string DanToc { get; set; }
        public string Tongiao { get; set; }
        public string SoDienThoai1 { get; set; }
        public string SoDienThoai2 { get; set; }
        public string email { get; set; }
        public string HoKhau { get; set; }
        public string DiaChiLienHe { get; set; }
        public string Anh34 { get; set; }
        public string TenCha { get; set; }
        public string NgheNghiepCha { get; set; }
        public string TenMe { get; set; }
        public string NgheNghiepMe { get; set; }
        public string NamTotNghiepTHPT { get; set; }
        public Nullable<System.DateTime> NgayNhapHoc { get; set; }
        public string NamTotNghiep { get; set; }
        public string NoiLamViec { get; set; }
        public string DoiTuong { get; set; }
        public string KhuVuc { get; set; }
        public string Ghichu { get; set; }
        public Nullable<bool> Xoa { get; set; }
        public Nullable<bool> DaTotNghiep { get; set; }
        public Nullable<bool> TayNamBo { get; set; }
    }
}