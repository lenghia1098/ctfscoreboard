﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Platform.Web.Models
{
    public class KhungGioViewModel
    {
        public Nullable<int> Tiet { get; set; }
        public string GioBatDau { get; set; }
        public string GioKetThuc { get; set; }
        public int id { get; set; }
        public string NguoiThem { get; set; }
        public Nullable<System.DateTime> NgayThem { get; set; }
    }
}