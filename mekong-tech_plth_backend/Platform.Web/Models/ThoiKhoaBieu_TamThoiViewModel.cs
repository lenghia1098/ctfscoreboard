﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Platform.Web.Models
{
    public class ThoiKhoaBieu_TamThoiViewModel
    {

        public string MaNhom { get; set; }
        public short Tiet { get; set; }
        public string Thu { get; set; }
        public string MaPH { get; set; }
        public Nullable<int> SoTiet { get; set; }
        public string Tuan { get; set; }
        public Nullable<bool> DaXoa { get; set; }
        public string NguoiXoa { get; set; }
        public Nullable<System.DateTime> ThoiGianXoa { get; set; }

    }
}