﻿using AutoMapper;
using Platform.Model;
using Platform.Service;
using Platform.Web.infratructure.core;
using Platform.Web.infratructure.extensions;
using Platform.Web.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;

namespace Platform.Web.Api
{
    [RoutePrefix("api/TeamInfo")]
    [Authorize]
    public class TeamInfoController : ApiControllerBase
    {
        ITeamInfoService _teamInfoService;
        IDangKyService _dangKyService;
        public TeamInfoController(ILoiService loiService, ITeamInfoService teamInfoService, IDangKyService dangKyService) : base(loiService)
        {
            this._teamInfoService = teamInfoService;
            this._dangKyService = dangKyService;
        }
        [Route("createExcel")]
        [HttpPost]
        public HttpResponseMessage Create(HttpRequestMessage request, IEnumerable<TeamInfoViewModel>teamInfoVm)
        {


            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                if (!ModelState.IsValid)
                {
                    response = request.CreateResponse(HttpStatusCode.BadRequest, ModelState);
                }
                else
                {
                    foreach (var item in teamInfoVm)
                    {
                        var newTeamInfo = new TeamInfo();
                        newTeamInfo.UpdateTeamInfo(item);

                        _teamInfoService.Add(newTeamInfo);
                        _teamInfoService.Save();
                    }


                    //var responseData = Mapper.Map<DangKy_TamThoi, DangKy_TamThoiViewModel>(newDangKy_TamThoi);
                    response = request.CreateResponse(HttpStatusCode.OK);
                }

                return response;
            });
        }
       
        [Route("getall")]
        public HttpResponseMessage Get(HttpRequestMessage request)
        {
            return CreateHttpResponse(request, () =>
            {


                var listCategory = _teamInfoService.GetAll();
                var listHocPhiVm = Mapper.Map<List<TeamInfoViewModel>>(listCategory);

                HttpResponseMessage response = request.CreateResponse(HttpStatusCode.OK, listHocPhiVm);


                return response;
            });
        }
    }


}