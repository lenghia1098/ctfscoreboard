﻿using Platform.Model;
using Platform.Service;
using Platform.Web.infratructure.core;
using Platform.Web.infratructure.extensions;
using Platform.Web.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace Platform.Web.Api
{
    [RoutePrefix("api/ctdt")]
    [Authorize]
    public class ChuongTrinhDaoTaoController : ApiControllerBase
    {
        IChuongTrinhDaoTaoService _chuongTrinhDaoTaoService;

        public ChuongTrinhDaoTaoController(ILoiService loiService, IChuongTrinhDaoTaoService chuongTrinhDaoTaoService) : base(loiService)
        {
            this._chuongTrinhDaoTaoService = chuongTrinhDaoTaoService;
        }

        [Route("createExcel")]
        [HttpPost]
        // [AllowAnonymous]
        public HttpResponseMessage Create(HttpRequestMessage request, IEnumerable<ChuongTrinhDaoTaoViewModel> chuongTrinhDaoTaoVM)
        {


            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                if (!ModelState.IsValid)
                {
                    response = request.CreateResponse(HttpStatusCode.BadRequest, ModelState);
                }
                else
                {
                    foreach (var item in chuongTrinhDaoTaoVM)
                    {
                        var newChuongTrinhDaoTao = new ChuongTrinhDaoTao();
                        newChuongTrinhDaoTao.UpdateChuongTrinhDaoTao(item);

                        _chuongTrinhDaoTaoService.Add(newChuongTrinhDaoTao);
                        _chuongTrinhDaoTaoService.Save();
                    }


                    //var responseData = Mapper.Map<DangKy_TamThoi, DangKy_TamThoiViewModel>(newDangKy_TamThoi);
                    response = request.CreateResponse(HttpStatusCode.OK);
                }

                return response;
            });
        }
            public HttpResponseMessage Create(HttpRequestMessage request, ChuongTrinhDaoTao chuongTrinhDaoTao)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                if (ModelState.IsValid)
                {
                    request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
                }
                else
                {
                    _chuongTrinhDaoTaoService.Add(chuongTrinhDaoTao);
                    _chuongTrinhDaoTaoService.Commit();
                    response = request.CreateResponse(HttpStatusCode.Created, chuongTrinhDaoTao);
                }
                return response;
            });

        }
        [Route("getall")]
        public HttpResponseMessage Get(HttpRequestMessage request)
        {
            return CreateHttpResponse(request, () =>
            {


                var listCategory = _chuongTrinhDaoTaoService.GetAll();

                HttpResponseMessage response = request.CreateResponse(HttpStatusCode.OK, listCategory);


                return response;
            });
        }
        [Route("GetChucnangchuongtrinhdaotaos")]
        public HttpResponseMessage GetChucnangchuongtrinhdaotaos(HttpRequestMessage request, string mssv)
        {
            return CreateHttpResponse(request, () =>
            {


                var Chucnangchuongtrinhdaotao = _chuongTrinhDaoTaoService.GetChucnangchuongtrinhdaotaos(mssv);

                HttpResponseMessage response = request.CreateResponse(HttpStatusCode.OK, Chucnangchuongtrinhdaotao);


                return response;
            });
        }
        [Route("getMonDaHoc")]
        public HttpResponseMessage getMonDaHoc(HttpRequestMessage request, string mssv)
        {
            return CreateHttpResponse(request, () =>
            {


                var Chucnangchuongtrinhdaotao = _chuongTrinhDaoTaoService.getMonDaHoc(mssv);

                HttpResponseMessage response = request.CreateResponse(HttpStatusCode.OK, Chucnangchuongtrinhdaotao);


                return response;
            });
        }

        public HttpResponseMessage Post(HttpRequestMessage request, ChuongTrinhDaoTao chuongTrinhDaoTao)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                if (ModelState.IsValid)
                {
                    request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
                }
                else
                {
                    _chuongTrinhDaoTaoService.Add(chuongTrinhDaoTao);
                    _chuongTrinhDaoTaoService.Commit();

                    response = request.CreateResponse(HttpStatusCode.Created);

                }
                return response;
            });
        }

        public HttpResponseMessage Put(HttpRequestMessage request, ChuongTrinhDaoTao chuongTrinhDaoTao)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                if (ModelState.IsValid)
                {
                    request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
                }
                else
                {
                    _chuongTrinhDaoTaoService.Update(chuongTrinhDaoTao);
                    _chuongTrinhDaoTaoService.Commit();

                    response = request.CreateResponse(HttpStatusCode.OK);

                }
                return response;
            });
        }

        public HttpResponseMessage Delete(HttpRequestMessage request, int id)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                if (ModelState.IsValid)
                {
                    request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
                }
                else
                {
                    _chuongTrinhDaoTaoService.delete(id);
                    _chuongTrinhDaoTaoService.Commit();

                    response = request.CreateResponse(HttpStatusCode.OK);

                }
                return response;
            });
        }
    }
}
