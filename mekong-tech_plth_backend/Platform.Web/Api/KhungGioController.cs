﻿using AutoMapper;
using Platform.Model;
using Platform.Service;
using Platform.Web.infratructure.core;
using Platform.Web.infratructure.extensions;
using Platform.Web.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity.Core.Metadata.Edm;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace Platform.Web.Api
{
    [RoutePrefix("api/khunggio")]
    public class KhungGioController : ApiControllerBase
    {
        IKhungGioService _khungGioService;

        public KhungGioController(ILoiService loiService, IKhungGioService khungGioService) : base(loiService)
        {
            this._khungGioService = khungGioService;
        }
       
        [Route("create")]
        [HttpPost]
       // [AllowAnonymous]
        public HttpResponseMessage Create(HttpRequestMessage request, IEnumerable<KhungGioViewModel> khungGioVm)
            {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                if (!ModelState.IsValid)
                {
                    response = request.CreateResponse(HttpStatusCode.BadRequest, ModelState);
                }
                else
                {// int t =0 là thêm số tiết 
                    int t = 0;
                   
                    _khungGioService.DeleteAll();
                    foreach (var item in khungGioVm)
                    {
                        item.Tiet = t+1;//gán số tiết bằng t +1
                        t++;
                        var newKhungGio = new KhungGio();
                        newKhungGio.UpdateKhungGio(item);
                        newKhungGio.NgayThem = DateTime.Now;
                       
                        _khungGioService.Add(newKhungGio);
                        _khungGioService.Save();
                    }
                   

                   
                    response = request.CreateResponse(HttpStatusCode.OK);
                }

                return response;
            });
        }
       
        
        [Route("getall")]
        public HttpResponseMessage Get(HttpRequestMessage request )
        {
            return CreateHttpResponse(request, () =>
            {


                var listCategory = _khungGioService.GetAll();
              //  var responseData = Mapper.Map<KhungGio, KhungGioViewModel>(khungGioVm);

                HttpResponseMessage response = request.CreateResponse(HttpStatusCode.OK, listCategory);


                return response;
            });
        }
        
        

        public HttpResponseMessage Post(HttpRequestMessage request, KhungGio khungGio)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                if (ModelState.IsValid)
                {
                    request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
                }
                else
                {
                    _khungGioService.Add(khungGio);
                    _khungGioService.Commit();

                    response = request.CreateResponse(HttpStatusCode.Created);

                }
                return response;
            });
        }
        [Route ("Update")]
        public HttpResponseMessage Put(HttpRequestMessage request, KhungGio khungGio)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                if (ModelState.IsValid)
                {
                    request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
                }
                else
                {
                    _khungGioService.Update(khungGio);
                    _khungGioService.Commit();

                    response = request.CreateResponse(HttpStatusCode.OK);

                }
                return response;
            });
        }

        public HttpResponseMessage Delete(HttpRequestMessage request, int id)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                if (ModelState.IsValid)
                {
                    request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
                }
                else
                {
                    _khungGioService.delete(id);
                    _khungGioService.Commit();

                    response = request.CreateResponse(HttpStatusCode.OK);

                }
                return response;
            });
        }

    }
}