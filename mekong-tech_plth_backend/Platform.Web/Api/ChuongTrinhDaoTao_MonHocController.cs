﻿using AutoMapper;
using Platform.Model;
using Platform.Service;
using Platform.Web.infratructure.core;
using Platform.Web.infratructure.extensions;
using Platform.Web.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace Platform.Web.Api
{
    [RoutePrefix("api/ctdt-monhoc")]
    [Authorize]
    public class ChuongTrinhDaoTao_MonHocController : ApiControllerBase
    {
        IChuongTrinhDaoTao_MonHocService _chuongTrinhDaoTao_MonHocService;

        public ChuongTrinhDaoTao_MonHocController(ILoiService loiService, IChuongTrinhDaoTao_MonHocService chuongTrinhDaoTao_MonHocService) : base(loiService)
        {
            this._chuongTrinhDaoTao_MonHocService = chuongTrinhDaoTao_MonHocService;
        }
        [Route("createExcel")]
        [HttpPost]
        // [AllowAnonymous]
        public HttpResponseMessage Create(HttpRequestMessage request, IEnumerable<ChuongTrinhDaoTao_MonHocViewModel> chuongTrinhDaoTao_MonHocVm)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                if (!ModelState.IsValid)
                {
                    response = request.CreateResponse(HttpStatusCode.BadRequest, ModelState);
                }
                else
                {
                    foreach (var item in chuongTrinhDaoTao_MonHocVm)
                    {
                        var newChuongTrinhDaoTao = new ChuongTrinhDaoTao_MonHoc();
                        newChuongTrinhDaoTao.UpdateChuongTrinhDaoTao_MonHoc(item);

                        _chuongTrinhDaoTao_MonHocService.Add(newChuongTrinhDaoTao);
                        _chuongTrinhDaoTao_MonHocService.Save();
                    }


                    //var responseData = Mapper.Map<DangKy_TamThoi, DangKy_TamThoiViewModel>(newDangKy_TamThoi);
                    response = request.CreateResponse(HttpStatusCode.OK);
                }

                return response;
            });
        }
        public HttpResponseMessage Create(HttpRequestMessage request, ChuongTrinhDaoTao_MonHoc chuongTrinhDaoTao_MonHoc)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                if (ModelState.IsValid)
                {
                    request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
                }
                else
                {
                    _chuongTrinhDaoTao_MonHocService.Add(chuongTrinhDaoTao_MonHoc);
                    _chuongTrinhDaoTao_MonHocService.Commit();
                    response = request.CreateResponse(HttpStatusCode.Created, chuongTrinhDaoTao_MonHoc);
                }
                return response;
            });

        }
        [Route("getbykey/vitri")]
        [HttpGet]
        public HttpResponseMessage GetByMACT(HttpRequestMessage request, string maCT,string maMH)
        {
            return CreateHttpResponse(request, () =>
            {
                var model = _chuongTrinhDaoTao_MonHocService.GetAllByKey(maCT, maMH);
                var responseData = Mapper.Map<List<ChuongTrinhDaoTao_MonHocViewModel>>(model);

                var response = request.CreateResponse(HttpStatusCode.OK, responseData);

                return response;
            });
        }
        [Route("getbyMACT/{Id}")]
        [HttpGet]
        public HttpResponseMessage GetByMACT(HttpRequestMessage request, string Id)
        {
            return CreateHttpResponse(request, () =>
            {
                var model = _chuongTrinhDaoTao_MonHocService.GetAllByMACT(Id);

                var responseData = Mapper.Map<List<ChuongTrinhDaoTao_MonHocViewModel>>(model);

                var response = request.CreateResponse(HttpStatusCode.OK, responseData);

                return response;
            });
        }
        [Route("getall")]
        public HttpResponseMessage Get(HttpRequestMessage request)
        {
            return CreateHttpResponse(request, () =>
            {


                var listCategory = _chuongTrinhDaoTao_MonHocService.GetAll();

                HttpResponseMessage response = request.CreateResponse(HttpStatusCode.OK, listCategory);


                return response;
            });
        }

        public HttpResponseMessage Post(HttpRequestMessage request, ChuongTrinhDaoTao_MonHoc chuongTrinhDaoTao_MonHoc)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                if (ModelState.IsValid)
                {
                    request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
                }
                else
                {
                    _chuongTrinhDaoTao_MonHocService.Add(chuongTrinhDaoTao_MonHoc);
                    _chuongTrinhDaoTao_MonHocService.Commit();

                    response = request.CreateResponse(HttpStatusCode.Created);

                }
                return response;
            });
        }

        public HttpResponseMessage Put(HttpRequestMessage request, ChuongTrinhDaoTao_MonHoc chuongTrinhDaoTao_MonHoc)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                if (ModelState.IsValid)
                {
                    request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
                }
                else
                {
                    _chuongTrinhDaoTao_MonHocService.Update(chuongTrinhDaoTao_MonHoc);
                    _chuongTrinhDaoTao_MonHocService.Commit();

                    response = request.CreateResponse(HttpStatusCode.OK);

                }
                return response;
            });
        }

        public HttpResponseMessage Delete(HttpRequestMessage request, int id)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                if (ModelState.IsValid)
                {
                    request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
                }
                else
                {
                   
                    _chuongTrinhDaoTao_MonHocService.delete(id);
                    _chuongTrinhDaoTao_MonHocService.Commit();

                    response = request.CreateResponse(HttpStatusCode.OK);

                }
                return response;
            });
        }
        [Route("deletebyid")]
  
        public HttpResponseMessage DeleteById(HttpRequestMessage request, string id)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                if (ModelState.IsValid)
                {
                    request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
                }
                else
                {
                    id = "01";
                    _chuongTrinhDaoTao_MonHocService.deleteById(id);
                    _chuongTrinhDaoTao_MonHocService.Commit();

                    response = request.CreateResponse(HttpStatusCode.OK);

                }
                return response;
            });
        }
    }
}
