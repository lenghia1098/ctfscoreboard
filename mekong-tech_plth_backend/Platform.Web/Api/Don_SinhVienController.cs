﻿using AutoMapper;
using Platform.Model;
using Platform.Service;
using Platform.Web.infratructure.core;
using Platform.Web.infratructure.extensions;
using Platform.Web.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace Platform.Web.Api
{
    [RoutePrefix("api/don-sinhvien")]
    [Authorize]
    public class Don_SinhVienController : ApiControllerBase
    {
        IDon_SinhVienService _don_SinhVienService;

        public Don_SinhVienController(ILoiService loiService, IDon_SinhVienService don_SinhVienService) : base(loiService)
        {
            this._don_SinhVienService = don_SinhVienService;
        }

        [Route("createExcel")]
        [HttpPost]
        // [AllowAnonymous]
        public HttpResponseMessage Create(HttpRequestMessage request, IEnumerable<Don_SinhVienViewModel> don_SinhVienVm)
        {


            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                if (!ModelState.IsValid)
                {
                    response = request.CreateResponse(HttpStatusCode.BadRequest, ModelState);
                }
                else
                {
                    foreach (var item in don_SinhVienVm)
                    {
                        var newDon_SinhVien = new Don_SinhVien();
                        newDon_SinhVien.UpdateDon_SinhVien(item);

                        _don_SinhVienService.Add(newDon_SinhVien);
                        _don_SinhVienService.Save();
                    }


                    //var responseData = Mapper.Map<DangKy_TamThoi, DangKy_TamThoiViewModel>(newDangKy_TamThoi);
                    response = request.CreateResponse(HttpStatusCode.OK);
                }

                return response;
            });
        }
		
		 [Route("getID")]
        [HttpGet]

        public HttpResponseMessage getIdDonDaNop(HttpRequestMessage request, int maDon)
        {
            return CreateHttpResponse(request, () =>
            {
                var model = _don_SinhVienService.getID(maDon);

                var responseData = Mapper.Map<Don_SinhVien, Don_SinhVienViewModel>(model);

                var response = request.CreateResponse(HttpStatusCode.OK, responseData);
				 

                return response;
            });
        }

        public HttpResponseMessage Create(HttpRequestMessage request, Don_SinhVien don_SinhVien)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                if (ModelState.IsValid)
                {
                    request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
                }
                else
                {
                    _don_SinhVienService.Add(don_SinhVien);
                    _don_SinhVienService.Commit();
                    response = request.CreateResponse(HttpStatusCode.Created, don_SinhVien);
                }
                return response;
            });

        }
		[Route("updateAll")]
        [HttpPut]
        //    [AllowAnonymous]
        public HttpResponseMessage UpdateAll(HttpRequestMessage request, Don_SinhVienViewModel don_SinhVienVM)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                if (!ModelState.IsValid)
                {
                    request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
                }
                else
                {

                    var dbsinhvien = _don_SinhVienService.getID(Convert.ToInt32(don_SinhVienVM.MaDon));

                    dbsinhvien.UpdateDon_SinhVien(don_SinhVienVM);


                    dbsinhvien.DaXoa = true;
                    dbsinhvien.ThoiGianXoa = DateTime.Now;
                    dbsinhvien.NguoiXoa = don_SinhVienVM.NguoiXoa;
                    _don_SinhVienService.Update(dbsinhvien);
                    _don_SinhVienService.Commit();

                    var responseData = Mapper.Map<Don_SinhVien, Don_SinhVienViewModel>(dbsinhvien);

                    response = request.CreateResponse(HttpStatusCode.Created, responseData);

                }
                return response;
            });

        }





        [Route("getall")]
        public HttpResponseMessage Get(HttpRequestMessage request)
        {
            return CreateHttpResponse(request, () =>
            {


                var listCategory = _don_SinhVienService.GetAll();

                HttpResponseMessage response = request.CreateResponse(HttpStatusCode.OK, listCategory);


                return response;
            });
        }
        [Route("getDonSV")]
        public HttpResponseMessage GetDonSV(HttpRequestMessage request,string mssv)
        {
            return CreateHttpResponse(request, () =>
            {


                int donSV = _don_SinhVienService.DemDonDaNop(mssv);

                HttpResponseMessage response = request.CreateResponse(HttpStatusCode.OK, donSV);


                return response;
            });
        }
        public HttpResponseMessage Post(HttpRequestMessage request, Don_SinhVien don_SinhVien)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                if (ModelState.IsValid)
                {
                    request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
                }
                else
                {
                    _don_SinhVienService.Add(don_SinhVien);
                    _don_SinhVienService.Commit();

                    response = request.CreateResponse(HttpStatusCode.Created);

                }
                return response;
            });
        }

        public HttpResponseMessage Put(HttpRequestMessage request, Don_SinhVien don_SinhVien)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                if (ModelState.IsValid)
                {
                    request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
                }
                else
                {
                    _don_SinhVienService.Update(don_SinhVien);
                    _don_SinhVienService.Commit();

                    response = request.CreateResponse(HttpStatusCode.OK);

                }
                return response;
            });
        }

        public HttpResponseMessage Delete(HttpRequestMessage request, int id)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                if (ModelState.IsValid)
                {
                    request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
                }
                else
                {
                    _don_SinhVienService.delete(id);
                    _don_SinhVienService.Commit();

                    response = request.CreateResponse(HttpStatusCode.OK);

                }
                return response;
            });
        }

    }
}
