﻿using AutoMapper;
using Platform.Model;
using Platform.Service;
using Platform.Web.infratructure.core;
using Platform.Web.infratructure.extensions;
using Platform.Web.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace Platform.Web.Api
{
    [RoutePrefix("api/cauhoi")]
    [Authorize]
    public class CauHoiThuongGapController : ApiControllerBase
    {
        ICauHoiThuongGapService _cauHoiThuongGapService;

        public CauHoiThuongGapController(ILoiService loiService, ICauHoiThuongGapService cauHoiThuongGapService) : base(loiService)
        {
            this._cauHoiThuongGapService = cauHoiThuongGapService;
        }
        [Route("createExcel")]
        [HttpPost]
        // [AllowAnonymous]
        public HttpResponseMessage Create(HttpRequestMessage request, IEnumerable<CauHoiThuongGapViewModel> cauHoiThuongGapVm)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                if (!ModelState.IsValid)
                {
                    response = request.CreateResponse(HttpStatusCode.BadRequest, ModelState);
                }
                else
                {
                    foreach (var item in cauHoiThuongGapVm)
                    {
                        var newCauHoiThuongGap = new CauHoiThuongGap();
                        newCauHoiThuongGap.UpdateCauHoiThuongGap(item);

                        _cauHoiThuongGapService.Add(newCauHoiThuongGap);
                        _cauHoiThuongGapService.Save();
                    }


                    //var responseData = Mapper.Map<DangKy_TamThoi, DangKy_TamThoiViewModel>(newDangKy_TamThoi);
                    response = request.CreateResponse(HttpStatusCode.OK);
                }

                return response;
            });
        }
        [Route("getbyid/{id:int}")]
        [HttpGet]
        public HttpResponseMessage GetById(HttpRequestMessage request,int Id)
        {
            return CreateHttpResponse(request, () =>
            {
                var model = _cauHoiThuongGapService.GetAllById(string.Format(Convert.ToString(Id)));

                var responseData = Mapper.Map<IEnumerable<CauHoiThuongGap>, CauHoiThuongGapViewModel>(model);

                var response = request.CreateResponse(HttpStatusCode.OK, responseData);

                return response;
            });
        }
        [Route("getall")]
        public HttpResponseMessage Get(HttpRequestMessage request)
        {
            return CreateHttpResponse(request, () =>
            {


                var listCategory = _cauHoiThuongGapService.GetAll();

                HttpResponseMessage response = request.CreateResponse(HttpStatusCode.OK, listCategory);


                return response;
            });
        }

        public HttpResponseMessage Post(HttpRequestMessage request, CauHoiThuongGap cauHoiThuongGap)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                if (ModelState.IsValid)
                {
                    request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
                }
                else
                {
                    _cauHoiThuongGapService.Add(cauHoiThuongGap);
                    _cauHoiThuongGapService.Commit();

                    response = request.CreateResponse(HttpStatusCode.Created);

                }
                return response;
            });
        }

        public HttpResponseMessage Put(HttpRequestMessage request, CauHoiThuongGap cauHoiThuongGap)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                if (ModelState.IsValid)
                {
                    request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
                }
                else
                {
                    _cauHoiThuongGapService.Update(cauHoiThuongGap);
                    _cauHoiThuongGapService.Commit();

                    response = request.CreateResponse(HttpStatusCode.OK);

                }
                return response;
            });
        }

        public HttpResponseMessage Delete(HttpRequestMessage request, int id)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                if (ModelState.IsValid)
                {
                    request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
                }
                else
                {
                    _cauHoiThuongGapService.delete(id);
                    _cauHoiThuongGapService.Commit();

                    response = request.CreateResponse(HttpStatusCode.OK);

                }
                return response;
            });
        }
    }
}
