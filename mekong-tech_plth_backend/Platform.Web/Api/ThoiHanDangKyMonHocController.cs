﻿using AutoMapper;
using Platform.Model;
using Platform.Service;
using Platform.Web.infratructure.core;
using Platform.Web.Models;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Platform.Web.infratructure.extensions;


namespace Platform.Web.Api
{
    [RoutePrefix("api/thoihandangkymonhoc")]
    [Authorize]
    public class ThoiHanDangKyMonHocController : ApiControllerBase
    {
        IThoiHanDangKyMonHocService _thoiHanDangKyMonHocService;

        public ThoiHanDangKyMonHocController(ILoiService loiService, IThoiHanDangKyMonHocService thoiHanDangKyMonHocService) : base(loiService)
        {
            this._thoiHanDangKyMonHocService = thoiHanDangKyMonHocService;
        }
        [Route("createExcel")]
        [HttpPost]
        // [AllowAnonymous]
        public HttpResponseMessage CreateExcel(HttpRequestMessage request, IEnumerable<ThoiHanDangKyMonHocViewModel> thoiHanDangKyMonHocVm)
        {


            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                if (!ModelState.IsValid)
                {
                    response = request.CreateResponse(HttpStatusCode.BadRequest, ModelState);
                }
                else
                {
                    foreach (var item in thoiHanDangKyMonHocVm)
                    {
                        var newThoiHanDangKyMonHoc = new ThoiHanDangKyMonHoc();
                        newThoiHanDangKyMonHoc.UpdateThoiHanDangKyMonHoc(item);

                        _thoiHanDangKyMonHocService.Add(newThoiHanDangKyMonHoc);
                        _thoiHanDangKyMonHocService.Save();
                    }


                    //var responseData = Mapper.Map<DangKy_TamThoi, DangKy_TamThoiViewModel>(newDangKy_TamThoi);
                    response = request.CreateResponse(HttpStatusCode.OK);
                }

                return response;
            });
        }
        [Route("create")]
        [HttpPost]
       // [AllowAnonymous]
        public HttpResponseMessage Create(HttpRequestMessage request, ThoiHanDangKyMonHocViewModel thoiHanDangKyMonHocVm)
            {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                if (!ModelState.IsValid)
                {
                    response = request.CreateResponse(HttpStatusCode.BadRequest, ModelState);
                }
                else
                {
                   
                        var newThoiHanDangKyMonHoc = new ThoiHanDangKyMonHoc();
                        newThoiHanDangKyMonHoc.UpdateThoiHanDangKyMonHoc(thoiHanDangKyMonHocVm);

                        _thoiHanDangKyMonHocService.Add(newThoiHanDangKyMonHoc);
                        _thoiHanDangKyMonHocService.Save();
                    
                   

                    //var responseData = Mapper.Map<ThoiHanDangKyMonHoc, ThoiHanDangKyMonHocViewModel>(newThoiHanDangKyMonHoc);
                    response = request.CreateResponse(HttpStatusCode.OK);
                }

                return response;
            });
        }
       
        [Route("getall")]
        public HttpResponseMessage Get(HttpRequestMessage request)
        {
            return CreateHttpResponse(request, () =>
            {


                var listCategory = _thoiHanDangKyMonHocService.GetAll();
                  var listThoiHanDangKyMonHocVm = Mapper.Map<List<ThoiHanDangKyMonHocViewModel>>(listCategory);

                 HttpResponseMessage response = request.CreateResponse(HttpStatusCode.OK, listThoiHanDangKyMonHocVm);
              //  HttpResponseMessage response = request.CreateResponse(HttpStatusCode.OK, listCategory);

                return response;
            });
        }

     
        [Route("getID")]
        public HttpResponseMessage GetById(HttpRequestMessage request)
        {
            return CreateHttpResponse(request, () =>
            {


                var sinhvien = _thoiHanDangKyMonHocService.GetByID(1);
                var ThoiHanDangKyMonHocVm = Mapper.Map<ThoiHanDangKyMonHocViewModel>(sinhvien);

                HttpResponseMessage response = request.CreateResponse(HttpStatusCode.OK, ThoiHanDangKyMonHocVm);


                return response;
            });
        }
       

        public HttpResponseMessage Post(HttpRequestMessage request, ThoiHanDangKyMonHocViewModel thoiHanDangKyMonHocVm)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                if (ModelState.IsValid)
                {
                    request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
                }
                else
                {
                    ThoiHanDangKyMonHoc newThoiHanDangKyMonHoc = new ThoiHanDangKyMonHoc();
                    newThoiHanDangKyMonHoc.UpdateThoiHanDangKyMonHoc(thoiHanDangKyMonHocVm);
                    _thoiHanDangKyMonHocService.Add(newThoiHanDangKyMonHoc);
                    _thoiHanDangKyMonHocService.Commit();

                    response = request.CreateResponse(HttpStatusCode.Created);

                }
                return response;
            });
        }

        [Route("delete")]
        [HttpDelete]
        [HttpGet]
        // [AllowAnonymous]
        public HttpResponseMessage delete(HttpRequestMessage request, string ID)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                if (!ModelState.IsValid)
                {
                    response = request.CreateResponse(HttpStatusCode.BadRequest, ModelState);
                }
                else
                {
                    var oldProductCategory = _thoiHanDangKyMonHocService.GetByID(int.Parse(ID));
                    _thoiHanDangKyMonHocService.Save();

                    var responseData = Mapper.Map<ThoiHanDangKyMonHoc, ThoiHanDangKyMonHocViewModel>(oldProductCategory);
                    response = request.CreateResponse(HttpStatusCode.Created, responseData);
                }

                return response;
            });
        }







    }
}