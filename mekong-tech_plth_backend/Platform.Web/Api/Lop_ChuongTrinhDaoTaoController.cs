﻿using Platform.Model;
using Platform.Service;
using Platform.Web.infratructure.core;
using Platform.Web.infratructure.extensions;
using Platform.Web.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace Platform.Web.Api
{
    [RoutePrefix("api/lop_ctdt")]
    [Authorize]
    public class Lop_ChuongTrinhDaoTaoController : ApiControllerBase
    {
        ILop_ChuongTrinhDaoTaoService _lop_ChuongTrinhDaoTaoService;

        public Lop_ChuongTrinhDaoTaoController(ILoiService loiService, ILop_ChuongTrinhDaoTaoService lop_ChuongTrinhDaoTaoService) : base(loiService)
        {
            this._lop_ChuongTrinhDaoTaoService = lop_ChuongTrinhDaoTaoService;
        }
        [Route("createExcel")]
        [HttpPost]
        public HttpResponseMessage Create(HttpRequestMessage request, IEnumerable<Lop_ChuongTrinhDaoTaoViewModel> lop_ChuongTrinhDaoTaoVm)
        {


            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                if (!ModelState.IsValid)
                {
                    response = request.CreateResponse(HttpStatusCode.BadRequest, ModelState);
                }
                else
                {
                    foreach (var item in lop_ChuongTrinhDaoTaoVm)
                    {
                        var newLop_ChuongTrinhDaoTao = new Lop_ChuongTrinhDaoTao();
                        newLop_ChuongTrinhDaoTao.UpdateLop_ChuongTrinhDaoTao(item);

                        _lop_ChuongTrinhDaoTaoService.Add(newLop_ChuongTrinhDaoTao);
                        _lop_ChuongTrinhDaoTaoService.Save();
                    }


                    //var responseData = Mapper.Map<DangKy_TamThoi, DangKy_TamThoiViewModel>(newDangKy_TamThoi);
                    response = request.CreateResponse(HttpStatusCode.OK);
                }

                return response;
            });
        }
        public HttpResponseMessage Create(HttpRequestMessage request, Lop_ChuongTrinhDaoTao lop_ChuongTrinhDaoTao)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                if (ModelState.IsValid)
                {
                    request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
                }
                else
                {
                    _lop_ChuongTrinhDaoTaoService.Add(lop_ChuongTrinhDaoTao);
                    _lop_ChuongTrinhDaoTaoService.Commit();
                    response = request.CreateResponse(HttpStatusCode.Created, lop_ChuongTrinhDaoTao);
                }
                return response;
            });

        }
        [Route("getall")]
        public HttpResponseMessage Get(HttpRequestMessage request)
        {
            return CreateHttpResponse(request, () =>
            {


                var listCategory = _lop_ChuongTrinhDaoTaoService.GetAll();

                HttpResponseMessage response = request.CreateResponse(HttpStatusCode.OK, listCategory);


                return response;
            });
        }

        public HttpResponseMessage Post(HttpRequestMessage request, Lop_ChuongTrinhDaoTao lop_ChuongTrinhDaoTao)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                if (ModelState.IsValid)
                {
                    request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
                }
                else
                {
                    _lop_ChuongTrinhDaoTaoService.Add(lop_ChuongTrinhDaoTao);
                    _lop_ChuongTrinhDaoTaoService.Commit();

                    response = request.CreateResponse(HttpStatusCode.Created);

                }
                return response;
            });
        }

        public HttpResponseMessage Put(HttpRequestMessage request, Lop_ChuongTrinhDaoTao lop_ChuongTrinhDaoTao)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                if (ModelState.IsValid)
                {
                    request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
                }
                else
                {
                    _lop_ChuongTrinhDaoTaoService.Update(lop_ChuongTrinhDaoTao);
                    _lop_ChuongTrinhDaoTaoService.Commit();

                    response = request.CreateResponse(HttpStatusCode.OK);

                }
                return response;
            });
        }

        public HttpResponseMessage Delete(HttpRequestMessage request, int id)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                if (ModelState.IsValid)
                {
                    request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
                }
                else
                {
                    _lop_ChuongTrinhDaoTaoService.delete(id);
                    _lop_ChuongTrinhDaoTaoService.Commit();

                    response = request.CreateResponse(HttpStatusCode.OK);

                }
                return response;
            });
        }

    }
}