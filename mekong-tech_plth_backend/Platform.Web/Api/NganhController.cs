﻿using Platform.Model;
using Platform.Service;
using Platform.Web.infratructure.core;
using Platform.Web.infratructure.extensions;
using Platform.Web.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace Platform.Web.Api
{
    [RoutePrefix("api/nganh")]
    [Authorize]
    public class NganhController : ApiControllerBase
    {
        INganhService _nganhService;

        public NganhController(ILoiService loiService, INganhService nganhService) : base(loiService)
        {
            this._nganhService = nganhService;
        }
    
    [Route("createExcel")]
    [HttpPost]
    public HttpResponseMessage Create(HttpRequestMessage request, IEnumerable<NganhViewModel> nganhVm)
    {


        return CreateHttpResponse(request, () =>
        {
            HttpResponseMessage response = null;
            if (!ModelState.IsValid)
            {
                response = request.CreateResponse(HttpStatusCode.BadRequest, ModelState);
            }
            else
            {
                foreach (var item in nganhVm)
                {
                    var newNganh = new Nganh();
                    newNganh.UpdateNganh(item);

                    _nganhService.Add(newNganh);
                    _nganhService.Save();
                }


                //var responseData = Mapper.Map<DangKy_TamThoi, DangKy_TamThoiViewModel>(newDangKy_TamThoi);
                response = request.CreateResponse(HttpStatusCode.OK);
            }

            return response;
        });
    }
    public HttpResponseMessage Create(HttpRequestMessage request, Nganh nganh)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                if (ModelState.IsValid)
                {
                    request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
                }
                else
                {
                    _nganhService.Add(nganh);
                    _nganhService.Commit();
                    response = request.CreateResponse(HttpStatusCode.Created, nganh);
                }
                return response;
            });

        }
        [Route("getall")]
        public HttpResponseMessage Get(HttpRequestMessage request)
        {
            return CreateHttpResponse(request, () =>
            {


                var listCategory = _nganhService.GetAll();

                HttpResponseMessage response = request.CreateResponse(HttpStatusCode.OK, listCategory);


                return response;
            });
        }

        public HttpResponseMessage Post(HttpRequestMessage request, Nganh nganh)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                if (ModelState.IsValid)
                {
                    request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
                }
                else
                {
                    _nganhService.Add(nganh);
                    _nganhService.Commit();

                    response = request.CreateResponse(HttpStatusCode.Created);

                }
                return response;
            });
        }

        public HttpResponseMessage Put(HttpRequestMessage request, Nganh nganh)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                if (ModelState.IsValid)
                {
                    request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
                }
                else
                {
                    _nganhService.Update(nganh);
                    _nganhService.Commit();

                    response = request.CreateResponse(HttpStatusCode.OK);

                }
                return response;
            });
        }

        public HttpResponseMessage Delete(HttpRequestMessage request, int id)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                if (ModelState.IsValid)
                {
                    request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
                }
                else
                {
                    _nganhService.delete(id);
                    _nganhService.Commit();

                    response = request.CreateResponse(HttpStatusCode.OK);

                }
                return response;
            });
        }

    }
}