﻿using AutoMapper;
using Platform.Model;
using Platform.Service;
using Platform.Web.infratructure.core;
using Platform.Web.Models;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Platform.Web.infratructure.extensions;


namespace Platform.Web.Api
{
    [RoutePrefix("api/giangday")]
    [Authorize]
    public class GiangDayController : ApiControllerBase
    {
        IGiangDayService _giangDayService;

        public GiangDayController(ILoiService loiService, IGiangDayService giangDayService) : base(loiService)
        {
            this._giangDayService = giangDayService;
        }
        [Route("createExcel")]
        [HttpPost]
        public HttpResponseMessage Create(HttpRequestMessage request, IEnumerable<GiangDayViewModel> giangDayVm)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                if (!ModelState.IsValid)
                {
                    response = request.CreateResponse(HttpStatusCode.BadRequest, ModelState);
                }
                else
                {
                    foreach (var item in giangDayVm)
                    {
                        var newGiangDay = new GiangDay();
                        newGiangDay.UpdateGiangDay(item);

                        _giangDayService.Add(newGiangDay);
                        _giangDayService.Save();
                    }


                    //var responseData = Mapper.Map<DangKy_TamThoi, DangKy_TamThoiViewModel>(newDangKy_TamThoi);
                    response = request.CreateResponse(HttpStatusCode.OK);
                }

                return response;
            });
        }
        public HttpResponseMessage Create(HttpRequestMessage request, GiangDay giangDay)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                if (ModelState.IsValid)
                {
                    request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
                }
                else
                {
                    _giangDayService.Add(giangDay);
                    _giangDayService.Commit();
                    response = request.CreateResponse(HttpStatusCode.Created, giangDay);
                }
                return response;
            });

        }
        [Route("getall")]
        public HttpResponseMessage Get(HttpRequestMessage request)
        {
            return CreateHttpResponse(request, () =>
            {


                var listCategory = _giangDayService.GetAll();
              

                HttpResponseMessage response = request.CreateResponse(HttpStatusCode.OK, listCategory);


                return response;
            });
        }
        [Route("getDanhSachLop")]
        [HttpGet]
        public HttpResponseMessage getDanhSachLop(HttpRequestMessage request, string msvc, string hocKy)
        {
            return CreateHttpResponse(request, () =>
            {
                var model = _giangDayService.getDanhSachLop( msvc,  hocKy);

                //  var responseData = Mapper.Map<List<SinhVienViewModel>>(model);

                var response = request.CreateResponse(HttpStatusCode.OK, model);

                return response;
            });
        }


        [Route("GetLichGiangDay")]

        [HttpGet]
        public HttpResponseMessage GetLichGiangDay(HttpRequestMessage request, string msvc, string hocky, string namhoc)
        {
            return CreateHttpResponse(request, () =>
            {
                var model = _giangDayService.GetLichGiangDay( msvc,hocky,namhoc);

               

                var response = request.CreateResponse(HttpStatusCode.OK, model);

                return response;
            });
        }

        [Route("getDanhSachHocKy")]
        [HttpGet]
        public HttpResponseMessage getDanhSachHocKy(HttpRequestMessage request, string msvc)
        {
            return CreateHttpResponse(request, () =>
            {
                var model = _giangDayService.getDanhSachHocKy(msvc);

                //  var responseData = Mapper.Map<List<SinhVienViewModel>>(model);

                var response = request.CreateResponse(HttpStatusCode.OK, model);

                return response;
            });
        }
        [Route("getDanhSachGiangDay")]
        [HttpGet]
        public HttpResponseMessage getDanhSachGiangDay(HttpRequestMessage request, string maNhom)
        {
            return CreateHttpResponse(request, () =>
            {
                var model = _giangDayService.getDanhSachGiangDay(maNhom);

                //  var responseData = Mapper.Map<List<SinhVienViewModel>>(model);

                var response = request.CreateResponse(HttpStatusCode.OK, model);

                return response;
            });
        }
        [Route("getID")]
        public HttpResponseMessage GetById(HttpRequestMessage request)
        {
            return CreateHttpResponse(request, () =>
            {


                var sinhvien = _giangDayService.GetByID(1);
                

                HttpResponseMessage response = request.CreateResponse(HttpStatusCode.OK, sinhvien);


                return response;
            });
        }

       

        

    }
}