﻿using AutoMapper;
using Platform.Model;
using Platform.Service;
using Platform.Web.Infrastructure.Core;
using Platform.Web.infratructure.core;
using Platform.Web.infratructure.extensions;
using Platform.Web.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace Platform.Web.Api
{
    [RoutePrefix("api/donvienchuc")]
    public class DonVienChucController : ApiControllerBase
    {
        IDonVienChucService _donVienChucService;

        public DonVienChucController(ILoiService loiService, IDonVienChucService donVienChucService) : base(loiService)
        {
            this._donVienChucService = donVienChucService;
        }
        public HttpResponseMessage Create(HttpRequestMessage request, DonVienChuc donVienChuc)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                if (ModelState.IsValid)
                {
                    request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
                }
                else
                {
                    _donVienChucService.Add(donVienChuc);
                    _donVienChucService.Commit();
                    response = request.CreateResponse(HttpStatusCode.Created, donVienChuc);
                }
                return response;
            });

        }
        [Route("getall")]
        public HttpResponseMessage Get(HttpRequestMessage request)
        {
            return CreateHttpResponse(request, () =>
            {


                var listCategory = _donVienChucService.GetAll();

                HttpResponseMessage response = request.CreateResponse(HttpStatusCode.OK, listCategory);


                return response;
            });
        }

       



       


        [Route("GetChucnangdondanopvienchucs")]
        public HttpResponseMessage GetChucnangdondanopvienchucs(HttpRequestMessage request, int page,string msvc, int pageSize = 20)
        {
            return CreateHttpResponse(request, () =>
            {

                int totalRow = 0;

                IEnumerable<chucnangdondanopvienchuc> viecdalam = _donVienChucService.GetChucnangdondanopvienchucs(msvc);

                totalRow = viecdalam.Count();
                var query = viecdalam.OrderByDescending(x => x.MaDon).Skip(page * pageSize).Take(pageSize);

                var responseData = Mapper.Map<IEnumerable<chucnangdondanopvienchuc>, IEnumerable<chucnangdondanopvienchuc>>(query);

                var PaginationSet = new PaginationSet<chucnangdondanopvienchuc>()
                {

                    Items = responseData,
                    Page = page,
                    TotalCount = totalRow,/*tong so bai ghi*/
                    TotalPages = (int)Math.Ceiling((decimal)totalRow / pageSize),/*tong số trang*/



                };

                HttpResponseMessage response = request.CreateResponse(HttpStatusCode.OK, PaginationSet);


                return response;
            });
        }
        [Route("demdondanopvienchuc")]
        [HttpGet]
        public HttpResponseMessage demdondanopvienchuc(HttpRequestMessage request,string msvc)
        {
            return CreateHttpResponse(request, () =>
            {


                var dondanopvienchucs = _donVienChucService.demdondanopvienchuc(msvc);

                HttpResponseMessage response = request.CreateResponse(HttpStatusCode.OK, dondanopvienchucs);


                return response;
            });
        }

        public HttpResponseMessage Post(HttpRequestMessage request, DonVienChuc donVienChuc)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                if (ModelState.IsValid)
                {
                    request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
                }
                else
                {
                    _donVienChucService.Add(donVienChuc);
                    _donVienChucService.Commit();

                    response = request.CreateResponse(HttpStatusCode.Created);

                }
                return response;
            });
        }

        public HttpResponseMessage Put(HttpRequestMessage request, DonVienChuc donVienChuc)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                if (ModelState.IsValid)
                {
                    request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
                }
                else
                {
                    _donVienChucService.Update(donVienChuc);
                    _donVienChucService.Commit();

                    response = request.CreateResponse(HttpStatusCode.OK);

                }
                return response;
            });
        }
		[Route("GetChiTietDonDaNopVienChuc")]
        public HttpResponseMessage GetChiTietDonDaNopVienChuc(HttpRequestMessage request, int MaDon)
        {
            return CreateHttpResponse(request, () =>
            {


                var Getdondanopvienchucs = _donVienChucService.GetChiTietDonDaNopVienChuc(MaDon);

                HttpResponseMessage response = request.CreateResponse(HttpStatusCode.OK, Getdondanopvienchucs);


                return response;
            });
        }
        public HttpResponseMessage Delete(HttpRequestMessage request, int id)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                if (ModelState.IsValid)
                {
                    request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
                }
                else
                {
                    _donVienChucService.delete(id);
                    _donVienChucService.Commit();

                    response = request.CreateResponse(HttpStatusCode.OK);

                }
                return response;
            });
        }

    }
}
