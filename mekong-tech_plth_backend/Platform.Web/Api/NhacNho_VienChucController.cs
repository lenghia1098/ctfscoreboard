﻿using Platform.Model;
using Platform.Service;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Platform.Web.infratructure.core;
using AutoMapper;
using Platform.Web.Infrastructure.Core;

namespace Platform.Web.Api
{
    [RoutePrefix("api/nhacnho-vc")]
    public class NhacNho_VienChucController : ApiControllerBase
    {
        INhacNho_VienChucService _nhacNho_VienChucService;

        public NhacNho_VienChucController(ILoiService loiService, INhacNho_VienChucService nhacNho_VienChucService) : base(loiService)
        {
            this._nhacNho_VienChucService = nhacNho_VienChucService;
        }
        public HttpResponseMessage Create(HttpRequestMessage request, NhacNho_VienChuc nhacNho_VienChuc)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                if (ModelState.IsValid)
                {
                    request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
                }
                else
                {
                    _nhacNho_VienChucService.Add(nhacNho_VienChuc);
                    _nhacNho_VienChucService.Commit();
                    response = request.CreateResponse(HttpStatusCode.Created, nhacNho_VienChuc);
                }
                return response;
            });

        }
        [Route("getall")]
        public HttpResponseMessage Get(HttpRequestMessage request)
        {
            return CreateHttpResponse(request, () =>
            {


                var listCategory = _nhacNho_VienChucService.GetAll();

                HttpResponseMessage response = request.CreateResponse(HttpStatusCode.OK, listCategory);


                return response;
            });
        }
        [Route("getMaNN")]
        public HttpResponseMessage GetMaNN(HttpRequestMessage request, string msvc)
        {
            return CreateHttpResponse(request, () =>
            {


                IQueryable<string> maNN = _nhacNho_VienChucService.GetMaNhacNhoVC(msvc);

                HttpResponseMessage response = request.CreateResponse(HttpStatusCode.OK, maNN);


                return response;
            });
        }
        [Route("getNhacNhoChuaHoanThanh")]
        public HttpResponseMessage GetNhacNhoChuaHoanThanh(HttpRequestMessage request, int page, string msvc, int pageSize = 20)
        {
            return CreateHttpResponse(request, () =>
            {

                int totalRow = 0;

                IEnumerable<NhacNhoVienChucChuaHoanThanh> nhacnho = _nhacNho_VienChucService.GetNhacNhoChuaHoanThanh(msvc);
                totalRow = nhacnho.Count();
                var query = nhacnho.OrderByDescending(x => x.MaSoNN).Skip(page * pageSize).Take(pageSize);

                var responseData = Mapper.Map<IEnumerable<NhacNhoVienChucChuaHoanThanh>, IEnumerable<NhacNhoVienChucChuaHoanThanh>>(query);

                var PaginationSet = new PaginationSet<NhacNhoVienChucChuaHoanThanh>()
                {

                    Items = responseData,
                    Page = page,
                    TotalCount = totalRow,/*tong so bai ghi*/
                    TotalPages = (int)Math.Ceiling((decimal)totalRow / pageSize),/*tong số trang*/



                };
                HttpResponseMessage response = request.CreateResponse(HttpStatusCode.OK, PaginationSet);


                return response;
            });
        }
        [Route("Getchitietnhacnho")]
        public HttpResponseMessage Getchitietnhacnho(HttpRequestMessage request, int MaSoNN)
        {
            return CreateHttpResponse(request, () =>
            {


                var chitietnhacnho = _nhacNho_VienChucService.Getchitietnhacnho(MaSoNN);

                HttpResponseMessage response = request.CreateResponse(HttpStatusCode.OK, chitietnhacnho);


                return response;
            });
        }

        public HttpResponseMessage Post(HttpRequestMessage request, NhacNho_VienChuc nhacNho_VienChuc)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                if (ModelState.IsValid)
                {
                    request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
                }
                else
                {
                    _nhacNho_VienChucService.Add(nhacNho_VienChuc);
                    _nhacNho_VienChucService.Commit();

                    response = request.CreateResponse(HttpStatusCode.Created);

                }
                return response;
            });
        }

        public HttpResponseMessage Put(HttpRequestMessage request, NhacNho_VienChuc nhacNho_VienChuc)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                if (ModelState.IsValid)
                {
                    request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
                }
                else
                {
                    _nhacNho_VienChucService.Update(nhacNho_VienChuc);
                    _nhacNho_VienChucService.Commit();

                    response = request.CreateResponse(HttpStatusCode.OK);

                }
                return response;
            });
        }

        public HttpResponseMessage Delete(HttpRequestMessage request, int id)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                if (ModelState.IsValid)
                {
                    request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
                }
                else
                {
                    _nhacNho_VienChucService.delete(id);
                    _nhacNho_VienChucService.Commit();

                    response = request.CreateResponse(HttpStatusCode.OK);

                }
                return response;
            });
        }
    }
}
