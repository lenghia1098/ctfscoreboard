﻿using AutoMapper;
using Platform.Model;
using Platform.Service;
using Platform.Web.infratructure.core;
using Platform.Web.Models;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Platform.Web.infratructure.extensions;


namespace Platform.Web.Api
{
    [RoutePrefix("api/hockyhientai")]
    [Authorize]
    public class HocKyHienTaiController : ApiControllerBase
    {
        IHocKyHienTaiService _hocKyHienTaiService;

        public HocKyHienTaiController(ILoiService loiService, IHocKyHienTaiService hocKyHienTaiService) : base(loiService)
        {
            this._hocKyHienTaiService = hocKyHienTaiService;
        }
    
    [Route("createExcel")]
    [HttpPost]
    public HttpResponseMessage CreateExcel(HttpRequestMessage request, IEnumerable<HocKyHienTaiViewModel> hocKyHienTaiVm)
    {
        return CreateHttpResponse(request, () =>
        {
            HttpResponseMessage response = null;
            if (!ModelState.IsValid)
            {
                response = request.CreateResponse(HttpStatusCode.BadRequest, ModelState);
            }
            else
            {
                foreach (var item in hocKyHienTaiVm)
                {
                    var newHocKyHienTai = new HocKyHienTai();
                    newHocKyHienTai.UpdateHocKyHienTai(item);

                    _hocKyHienTaiService.Add(newHocKyHienTai);
                    _hocKyHienTaiService.Save();
                }


                //var responseData = Mapper.Map<DangKy_TamThoi, DangKy_TamThoiViewModel>(newDangKy_TamThoi);
                response = request.CreateResponse(HttpStatusCode.OK);
            }

            return response;
        });
    }
    [Route("create")]
        [HttpPost]
        // [AllowAnonymous]
        public HttpResponseMessage Create(HttpRequestMessage request, HocKyHienTaiViewModel hocKyHienTaiVm)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                if (!ModelState.IsValid)
                {
                    response = request.CreateResponse(HttpStatusCode.BadRequest, ModelState);
                }
                else
                {

                    var newHocKyHienTai = new HocKyHienTai();
                    newHocKyHienTai.UpdateHocKyHienTai(hocKyHienTaiVm);

                    _hocKyHienTaiService.Add(newHocKyHienTai);
                    _hocKyHienTaiService.Save();



                    //var responseData = Mapper.Map<HocKyHienTai, HocKyHienTaiViewModel>(newHocKyHienTai);
                    response = request.CreateResponse(HttpStatusCode.OK);
                }

                return response;
            });
        }

        [Route("getall")]
        public HttpResponseMessage Get(HttpRequestMessage request)
        {
            return CreateHttpResponse(request, () =>
            {


                var listCategory = _hocKyHienTaiService.GetAll();
                var listHocKyHienTaiVm = Mapper.Map<List<HocKyHienTaiViewModel>>(listCategory);

                HttpResponseMessage response = request.CreateResponse(HttpStatusCode.OK, listHocKyHienTaiVm);
                //  HttpResponseMessage response = request.CreateResponse(HttpStatusCode.OK, listCategory);

                return response;
            });
        }


        [Route("getID")]
        public HttpResponseMessage GetById(HttpRequestMessage request)
        {
            return CreateHttpResponse(request, () =>
            {


                var sinhvien = _hocKyHienTaiService.GetByID(1);
                var HocKyHienTaiVm = Mapper.Map<HocKyHienTaiViewModel>(sinhvien);

                HttpResponseMessage response = request.CreateResponse(HttpStatusCode.OK, HocKyHienTaiVm);


                return response;
            });
        }


        public HttpResponseMessage Post(HttpRequestMessage request, HocKyHienTaiViewModel hocKyHienTaiVm)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                if (ModelState.IsValid)
                {
                    request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
                }
                else
                {
                    HocKyHienTai newHocKyHienTai = new HocKyHienTai();
                    newHocKyHienTai.UpdateHocKyHienTai(hocKyHienTaiVm);
                    _hocKyHienTaiService.Add(newHocKyHienTai);
                    _hocKyHienTaiService.Commit();

                    response = request.CreateResponse(HttpStatusCode.Created);

                }
                return response;
            });
        }



        [Route("delete")]
        [HttpDelete]
        [HttpGet]
        // [AllowAnonymous]
        public HttpResponseMessage delete(HttpRequestMessage request, string ID)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                if (!ModelState.IsValid)
                {
                    response = request.CreateResponse(HttpStatusCode.BadRequest, ModelState);
                }
                else
                {
                    var oldProductCategory = _hocKyHienTaiService.delete(int.Parse(ID));
                    _hocKyHienTaiService.Save();

                    var responseData = Mapper.Map<HocKyHienTai, HocKyHienTaiViewModel>(oldProductCategory);
                    response = request.CreateResponse(HttpStatusCode.Created, responseData);
                }

                return response;
            });
        }

        [Route("update")]
        [HttpPut]
        [AllowAnonymous]
        public HttpResponseMessage Put(HttpRequestMessage request, HocKyHienTaiViewModel hocKyHienTaiVM)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                if (!ModelState.IsValid)
                {
                    request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
                }
                else
                {

                    var sinhVienDb = _hocKyHienTaiService.GetByID(hocKyHienTaiVM.ID);
                  
                    sinhVienDb.UpdateHocKyHienTai(hocKyHienTaiVM);
                    sinhVienDb.DaTinhHocPhi = true;


                    _hocKyHienTaiService.Update(sinhVienDb);
                    _hocKyHienTaiService.Commit();

                    response = request.CreateResponse(HttpStatusCode.OK);

                }
                return response;
            });
        }


    }
}