﻿using System;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using AutoMapper;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin.Security;
using Platform.Model.Models;
using Platform.Service;
using Platform.Web.Api;
using Platform.Web.App_Start;
using Platform.Web.infratructure.core;
using Platform.Web.Models;


namespace uStora.Web.Controllers
{
    [RoutePrefix("api/Manage")]
    [Authorize]
   
    public class ManageController : ApiControllerBase
      
    {
        private ApplicationSignInManager _signInManager;
        private ApplicationUserManager _userManager;
        public ManageController(ILoiService errorService,
            
            ApplicationUserManager userManager, ApplicationSignInManager signInManager
            ) : base(errorService)
        {

            _userManager = userManager;
            _signInManager = signInManager;
        }




        //

        private void AddErrors(IdentityResult result)
        {
            foreach (var error in result.Errors)
            {
                ModelState.AddModelError("", error);
            }
        }

        //
        // POST: /Manage/ChangePassword
        [HttpPost]
       
        [Route("DoiMatKhau")]
        public async Task<HttpResponseMessage> ChangePassword(HttpRequestMessage request, ChangePasswordViewModel model)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    var result = await _userManager.ChangePasswordAsync(User.Identity.GetUserId(), model.OldPassword, model.NewPassword);
                if (result.Succeeded)
                {
                    var user = await _userManager.FindByIdAsync(User.Identity.GetUserId());
                    if (user != null)
                    {
                        await _signInManager.SignInAsync(user, isPersistent: false, rememberBrowser: false);
                    }
                    return request.CreateResponse(HttpStatusCode.OK, model);
                }
                else
                {

                    return request.CreateErrorResponse(HttpStatusCode.BadRequest, string.Join(",", result.Errors));
                    //return  AddErrors(result);
                }
                }
                catch (Exception ex)
                {
                    return request.CreateErrorResponse(HttpStatusCode.BadRequest, ex.Message);
                }
            }
            else
            {
                return request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
            }
           

        }

        //
        // GET: /Manage/SetPassword
       

       
    }
}