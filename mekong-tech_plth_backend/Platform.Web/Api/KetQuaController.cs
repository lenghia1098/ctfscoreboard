﻿using AutoMapper;
using Platform.Model;
using Platform.Service;
using Platform.Web.infratructure.core;
using Platform.Web.infratructure.extensions;
using Platform.Web.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace Platform.Web.Api
{
    [RoutePrefix("api/ketqua")]
    [Authorize]
    public class KetQuaController : ApiControllerBase
    {
        IKetQuaService _ketQuaService;

        public KetQuaController(ILoiService loiService, IKetQuaService ketQuaService) : base(loiService)
        {
            this._ketQuaService = ketQuaService;
        }
        [Route("createExcel")]
        [HttpPost]
        public HttpResponseMessage Create(HttpRequestMessage request, IEnumerable<KetQuaViewModel> ketQuaVm)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                if (!ModelState.IsValid)
                {
                    response = request.CreateResponse(HttpStatusCode.BadRequest, ModelState);
                }
                else
                {
                    foreach (var item in ketQuaVm)
                    {
                        var newKetQua = new KetQua();
                        newKetQua.UpdateKetQua(item);

                        _ketQuaService.Add(newKetQua);
                        _ketQuaService.Save();
                    }


                    //var responseData = Mapper.Map<DangKy_TamThoi, DangKy_TamThoiViewModel>(newDangKy_TamThoi);
                    response = request.CreateResponse(HttpStatusCode.OK);
                }

                return response;
            });
        }
        [Route("getAllBangDiem")]
        public HttpResponseMessage getAllBangDiem(HttpRequestMessage request, string mssv)
        {
            return CreateHttpResponse(request, () =>
            {


                var listBangDiem = _ketQuaService.getAllBangDiem(mssv);


                HttpResponseMessage response = request.CreateResponse(HttpStatusCode.OK, listBangDiem);


                return response;
            });
        }
        [Route("getHocKy")]
        public HttpResponseMessage getHocKy(HttpRequestMessage request, string mssv)
        {
            return CreateHttpResponse(request, () =>
            {


                var getHocKy = _ketQuaService.getHocKy(mssv);


                HttpResponseMessage response = request.CreateResponse(HttpStatusCode.OK, getHocKy);


                return response;
            });
        }

        [Route("getBangDiemHocKy")]
        public HttpResponseMessage getBangDiemHocKy(HttpRequestMessage request, string mssv,string hocKy, string namHoc)
        {
            return CreateHttpResponse(request, () =>
            {


                var listBangDiem = _ketQuaService.getBangDiemHocKy(mssv, hocKy, namHoc);


                HttpResponseMessage response = request.CreateResponse(HttpStatusCode.OK, listBangDiem);


                return response;
            });
        }
        [Route("getBangDiemMonHoc")]
        public HttpResponseMessage getBangDiemMonHoc(HttpRequestMessage request, string maNhom, string maMH, string malop, string hocKy, string namHoc)
        {
            return CreateHttpResponse(request, () =>
            {


                var listBangDiem = _ketQuaService.getBangDiemMonHoc(maNhom, maMH, malop, hocKy, namHoc);


                HttpResponseMessage response = request.CreateResponse(HttpStatusCode.OK, listBangDiem);


                return response;
            });
        }
        [Route("getall")]
        public HttpResponseMessage Get(HttpRequestMessage request)
        {
            return CreateHttpResponse(request, () =>
            {


                var listCategory = _ketQuaService.GetAll();
                var listHocPhiVm = Mapper.Map<List<KetQuaViewModel>>(listCategory);

                HttpResponseMessage response = request.CreateResponse(HttpStatusCode.OK, listHocPhiVm);


                return response;
            });
        }

        [Route("themdiemcuoiki")]
        [HttpPut]
        //[AllowAnonymous]
        public HttpResponseMessage Put(HttpRequestMessage request, KetQuaViewModel ketQuaVm)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                if (!ModelState.IsValid)
                {
                    request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
                }
                else
                {
                    var dbkq = _ketQuaService.GetDetail(ketQuaVm.MaSoSV, ketQuaVm.MaNhom);

                    dbkq.UpdateKetQua(ketQuaVm);

                    dbkq.DaXoa = true;
                    dbkq.ThoiGianXoa = DateTime.Now;

                    _ketQuaService.Update(dbkq);
                    _ketQuaService.Commit();

                    var responseData = Mapper.Map<KetQua, KetQuaViewModel>(dbkq);


                    response = request.CreateResponse(HttpStatusCode.OK);

                }
                return response;
            });
        }


        [Route("themdiemgiuaki")]
        [HttpPost]
        public HttpResponseMessage ThemDiemGiuaKi(HttpRequestMessage request, KetQuaViewModel ketQuaVm)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                if (!ModelState.IsValid)
                {
                    response = request.CreateResponse(HttpStatusCode.BadRequest, ModelState);
                }
                else
                {
                    
                        var newKetQua = new KetQua();
                        newKetQua.UpdateKetQua(ketQuaVm);

                        _ketQuaService.Add(newKetQua);
                        _ketQuaService.Save();
                    


                    response = request.CreateResponse(HttpStatusCode.OK);
                }

                return response;
            });
        }


        [Route("getbyid")]
        [HttpGet]
        public HttpResponseMessage GetById(HttpRequestMessage request, string manhom)
        {
            return CreateHttpResponse(request, () =>
            {
                var model = _ketQuaService.getbyMaNhom(manhom);

               

                var response = request.CreateResponse(HttpStatusCode.OK, model);

                return response;
            });
        }
        [Route("getidsuadiem")]
        [HttpGet]
        public HttpResponseMessage getidsuadiem(HttpRequestMessage request,string mssv, string manhom)
        {
            return CreateHttpResponse(request, () =>
            {
                var model = _ketQuaService.getSuaDiem(mssv,manhom);
                var response = request.CreateResponse(HttpStatusCode.OK, model);
                return response;
            });
        }
    }
}

    