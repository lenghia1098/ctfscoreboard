﻿using AutoMapper;
using Platform.Model;
using Platform.Service;
using Platform.Web.infratructure.core;
using Platform.Web.infratructure.extensions;
using Platform.Web.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace Platform.Web.Api
{
    [RoutePrefix("api/VienChuc_ViecPhaiLam")]
    public class VienChuc_ViecPhaiLamController : ApiControllerBase
    {
        IVienChuc_ViecPhaiLamService _vienChuc_ViecPhaiLamService;

        public VienChuc_ViecPhaiLamController(ILoiService loiService, IVienChuc_ViecPhaiLamService vienChuc_ViecPhaiLamService) : base(loiService)
        {
            this._vienChuc_ViecPhaiLamService = vienChuc_ViecPhaiLamService;
        }
        public HttpResponseMessage Create(HttpRequestMessage request, VienChuc_ViecPhaiLam vienChuc_ViecPhaiLam)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                if (ModelState.IsValid)
                {
                    request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
                }
                else
                {
                    _vienChuc_ViecPhaiLamService.Add(vienChuc_ViecPhaiLam);
                    _vienChuc_ViecPhaiLamService.Commit();
                    response = request.CreateResponse(HttpStatusCode.Created, vienChuc_ViecPhaiLam);
                }
                return response;
            });

        }
        [Route("getall")]
        public HttpResponseMessage Get(HttpRequestMessage request)
        {
            return CreateHttpResponse(request, () =>
            {


                var listCategory = _vienChuc_ViecPhaiLamService.GetAll();

                HttpResponseMessage response = request.CreateResponse(HttpStatusCode.OK, listCategory);


                return response;
            });
        }
       

        public HttpResponseMessage Post(HttpRequestMessage request, VienChuc_ViecPhaiLam vienChuc_ViecPhaiLam)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                if (ModelState.IsValid)
                {
                    request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
                }
                else
                {
                    _vienChuc_ViecPhaiLamService.Add(vienChuc_ViecPhaiLam);
                    _vienChuc_ViecPhaiLamService.Commit();

                    response = request.CreateResponse(HttpStatusCode.Created);

                }
                return response;
            });
        }


        [Route("getID")]
        [HttpGet]
        public HttpResponseMessage getID(HttpRequestMessage request, int mscv)
        {
            return CreateHttpResponse(request, () =>
            {
                var model = _vienChuc_ViecPhaiLamService.getIDViecPhaiLam(mscv);

                var responseData = Mapper.Map<VienChuc_ViecPhaiLam, VienChuc_ViecPhaiLamViewModel>(model);

                var response = request.CreateResponse(HttpStatusCode.OK, responseData);

                return response;
            });
        }
         [Route("getIdAll")]
        [HttpGet]
        public HttpResponseMessage getIdAll(HttpRequestMessage request, string msvc)
        {
            return CreateHttpResponse(request, () =>
            {
                var model = _vienChuc_ViecPhaiLamService.getIdAll(msvc);

                var responseData = Mapper.Map<IEnumerable< VienChuc_ViecPhaiLam>,IEnumerable< VienChuc_ViecPhaiLamViewModel>>(model);

                var response = request.CreateResponse(HttpStatusCode.OK, responseData);

                return response;
            });
        }

        [Route("update")]
        [HttpPut]
        //[AllowAnonymous]
        public HttpResponseMessage Put(HttpRequestMessage request, VienChuc_ViecPhaiLamViewModel vienChuc_ViecPhaiLamVM)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
            if (!ModelState.IsValid)
            {
                request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
            }
            else
            {
                var dbvienchuc = _vienChuc_ViecPhaiLamService.getIDViecPhaiLam(vienChuc_ViecPhaiLamVM.MaSoCongViec);

                dbvienchuc.UpdateVienChuc_ViecPhaiLam(vienChuc_ViecPhaiLamVM);

                dbvienchuc.DaXoa = true;
                dbvienchuc.ThoiGianXoa = DateTime.Now;
             
                _vienChuc_ViecPhaiLamService.Update(dbvienchuc);
                _vienChuc_ViecPhaiLamService.Commit();

                var responseData = Mapper.Map<VienChuc_ViecPhaiLam, VienChuc_ViecPhaiLamViewModel>(dbvienchuc);


                response = request.CreateResponse(HttpStatusCode.OK);

                }
                return response;
            });
        }

        [Route("updateAll")]
        [HttpPut]
        //    [AllowAnonymous]
        public HttpResponseMessage UpdateAll(HttpRequestMessage request, VienChuc_ViecPhaiLamViewModel vienChuc_ViecPhaiLamVM)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                if (!ModelState.IsValid)
                {
                    request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
                }
                else
                {
                    var dbvienchuc = _vienChuc_ViecPhaiLamService.getIDViecPhaiLam(vienChuc_ViecPhaiLamVM.MaSoCongViec);

                    dbvienchuc.UpdateVienChuc_ViecPhaiLam(vienChuc_ViecPhaiLamVM);

                    dbvienchuc.DaXoa = true;
                    dbvienchuc.ThoiGianXoa = DateTime.Now;
                    dbvienchuc.NguoiXoa = vienChuc_ViecPhaiLamVM.NguoiXoa;

                    _vienChuc_ViecPhaiLamService.Update(dbvienchuc);
                    _vienChuc_ViecPhaiLamService.Commit();

                    var responseData = Mapper.Map<VienChuc_ViecPhaiLam, VienChuc_ViecPhaiLamViewModel>(dbvienchuc);

                    response = request.CreateResponse(HttpStatusCode.Created, responseData);

                }
                return response;
            });
        }





        public HttpResponseMessage Delete(HttpRequestMessage request, int id)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                if (ModelState.IsValid)
                {
                    request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
                }
                else
                {
                    _vienChuc_ViecPhaiLamService.delete(id);
                    _vienChuc_ViecPhaiLamService.Commit();

                    response = request.CreateResponse(HttpStatusCode.OK);

                }
                return response;
            });
        }
    }
}
