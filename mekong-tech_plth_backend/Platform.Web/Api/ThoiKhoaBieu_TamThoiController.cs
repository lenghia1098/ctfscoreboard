﻿
using Platform.Model;
using Platform.Service;
using Platform.Web.infratructure.core;
using Platform.Web.infratructure.extensions;
using Platform.Web.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace Platform.Web.Api
{
    [RoutePrefix("api/ThoiKhoaBieu_TamThoi")]
    [Authorize]
    public class ThoiKhoaBieu_TamThoiController : ApiControllerBase
    {



        IThoiKhoaBieu_TamThoiService _thoiKhoaBieu_TamThoiService;

        public ThoiKhoaBieu_TamThoiController(ILoiService loiService, IThoiKhoaBieu_TamThoiService thoiKhoaBieu_TamThoiService) : base(loiService)
        {
            this._thoiKhoaBieu_TamThoiService = thoiKhoaBieu_TamThoiService;
        }

        [Route("createExcel")]
        [HttpPost]
        // [AllowAnonymous]
        public HttpResponseMessage Create(HttpRequestMessage request, IEnumerable<ThoiKhoaBieu_TamThoiViewModel> thoiKhoaBieu_TamThoiVm)
        {


            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                if (!ModelState.IsValid)
                {
                    response = request.CreateResponse(HttpStatusCode.BadRequest, ModelState);
                }
                else
                {
                    foreach (var item in thoiKhoaBieu_TamThoiVm)
                    {
                        var newThoiKhoaBieu_TamThoi = new ThoiKhoaBieu_TamThoi();
                        newThoiKhoaBieu_TamThoi.UpdateThoiKhoaBieu_TamThoi(item);

                        _thoiKhoaBieu_TamThoiService.Add(newThoiKhoaBieu_TamThoi);
                        _thoiKhoaBieu_TamThoiService.Save();
                    }


                    //var responseData = Mapper.Map<DangKy_TamThoi, DangKy_TamThoiViewModel>(newDangKy_TamThoi);
                    response = request.CreateResponse(HttpStatusCode.OK);
                }

                return response;
            });
        }

        [Route("getall")]
        public HttpResponseMessage Get(HttpRequestMessage request)
        {
            return CreateHttpResponse(request, () =>
            {


                var listCategory = _thoiKhoaBieu_TamThoiService.GetAll();

                HttpResponseMessage response = request.CreateResponse(HttpStatusCode.OK, listCategory);


                return response;
            });
        }

       
    }
}
