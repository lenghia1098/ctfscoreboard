﻿using Platform.Model;
using Platform.Service;
using Platform.Web.infratructure.core;
using Platform.Web.infratructure.extensions;
using Platform.Web.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace Platform.Web.Api
{
    [RoutePrefix("api/nhacnho")]
    [Authorize]
    public class NhacNhoController : ApiControllerBase
    {
        INhacNhoService _nhacNhoService;

        public NhacNhoController (ILoiService loiService, INhacNhoService nhacNhoService) : base(loiService)
        {
            this._nhacNhoService = nhacNhoService;
        }
        [Route("createExcel")]
        [HttpPost]
        public HttpResponseMessage Create(HttpRequestMessage request, IEnumerable<NhacNhoViewModel> nhacNhoVm)
        {


            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                if (!ModelState.IsValid)
                {
                    response = request.CreateResponse(HttpStatusCode.BadRequest, ModelState);
                }
                else
                {
                    foreach (var item in nhacNhoVm)
                    {
                        var newNhacNho = new NhacNho();
                        newNhacNho.UpdateNhacNho(item);

                        _nhacNhoService.Add(newNhacNho);
                        _nhacNhoService.Save();
                    }


                    //var responseData = Mapper.Map<DangKy_TamThoi, DangKy_TamThoiViewModel>(newDangKy_TamThoi);
                    response = request.CreateResponse(HttpStatusCode.OK);
                }

                return response;
            });
        }
        public HttpResponseMessage Create(HttpRequestMessage request, NhacNho nhacNho)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                if (ModelState.IsValid)
                {
                    request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
                }
                else
                {
                    _nhacNhoService.Add(nhacNho);
                    _nhacNhoService.Commit();
                    response = request.CreateResponse(HttpStatusCode.Created, nhacNho);
                }
                return response;
            });
           
        }
        [Route("getall")]
        public HttpResponseMessage Get(HttpRequestMessage request)
        {
            return CreateHttpResponse(request, () =>
            {
               

                var listCategory = _nhacNhoService.GetAll();

                HttpResponseMessage response = request.CreateResponse(HttpStatusCode.OK, listCategory);

                
                return response;
            });
        }
        [Route("getNoiDungNN/{maNN}")]
        public HttpResponseMessage GetNoiDungNN(HttpRequestMessage request,string maNN)
        {
            return CreateHttpResponse(request, () =>
            {


                IQueryable<NhacNho> NoiDungNN = _nhacNhoService.GetNoiDungNhacNho(maNN);

                HttpResponseMessage response = request.CreateResponse(HttpStatusCode.OK, NoiDungNN);


                return response;
            });
        }
		[Route("Getchitietnhacnho")]
        public HttpResponseMessage Getchitietnhacnho(HttpRequestMessage request, int MaSoNN)
        {
            return CreateHttpResponse(request, () =>
            {


                IQueryable<chitietnhacnho> chitietnhacnho = _nhacNhoService.Getchitietnhacnho(MaSoNN);

                HttpResponseMessage response = request.CreateResponse(HttpStatusCode.OK, chitietnhacnho);


                return response;
            });
        }
        public HttpResponseMessage Post(HttpRequestMessage request, NhacNho nhacNho)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                if (ModelState.IsValid)
                {
                    request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
                }
                else
                {
                    _nhacNhoService.Add(nhacNho);
                    _nhacNhoService.Commit();

                    response = request.CreateResponse(HttpStatusCode.Created);

                }
                return response;
            });
        }

        public HttpResponseMessage Put(HttpRequestMessage request, NhacNho nhacNho)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                if (ModelState.IsValid)
                {
                    request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
                }
                else
                {
                    _nhacNhoService.Update(nhacNho);
                    _nhacNhoService.Commit();

                    response = request.CreateResponse(HttpStatusCode.OK);

                }
                return response;
            });
        }

        public HttpResponseMessage Delete(HttpRequestMessage request, int id)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                if (ModelState.IsValid)
                {
                    request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
                }
                else
                {
                    _nhacNhoService.delete(id);
                    _nhacNhoService.Commit();

                    response = request.CreateResponse(HttpStatusCode.OK);

                }
                return response;
            });
        }

    }
}