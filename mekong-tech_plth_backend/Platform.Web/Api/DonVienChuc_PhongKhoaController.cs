﻿using AutoMapper;
using Platform.Model;
using Platform.Service;
using Platform.Web.infratructure.core;
using Platform.Web.infratructure.extensions;
using Platform.Web.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace Platform.Web.Api
{
    [RoutePrefix("api/DonVienChuc_PhongKhoa")]
    [Authorize]
    public class DonVienChuc_PhongKhoaController : ApiControllerBase
    {
        IDonVienChuc_PhongKhoaService _donVienChuc_PhongKhoaService;

        public DonVienChuc_PhongKhoaController(ILoiService loiService, IDonVienChuc_PhongKhoaService donVienChuc_PhongKhoaService) : base(loiService)
        {
            this._donVienChuc_PhongKhoaService = donVienChuc_PhongKhoaService;
        }
        [Route("createExcel")]
        [HttpPost]
      
        public HttpResponseMessage Create(HttpRequestMessage request, IEnumerable<DonVienChuc_PhongKhoaViewModel> donVienChuc_PhongKhoaVm)
        {


            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                if (!ModelState.IsValid)
                {
                    response = request.CreateResponse(HttpStatusCode.BadRequest, ModelState);
                }
                else
                {
                    foreach (var item in donVienChuc_PhongKhoaVm)
                    {
                        var newDonVienChuc_PhongKhoa = new DonVienChuc_PhongKhoa();
                        newDonVienChuc_PhongKhoa.UpdateDonVienChuc_PhongKhoa(item);

                        _donVienChuc_PhongKhoaService.Add(newDonVienChuc_PhongKhoa);
                        _donVienChuc_PhongKhoaService.Save();
                    }


                    //var responseData = Mapper.Map<DangKy_TamThoi, DangKy_TamThoiViewModel>(newDangKy_TamThoi);
                    response = request.CreateResponse(HttpStatusCode.OK);
                }

                return response;
            });
        }
        public HttpResponseMessage Create(HttpRequestMessage request, DonVienChuc_PhongKhoa donVienChuc_PhongKhoa)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                if (ModelState.IsValid)
                {
                    request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
                }
                else
                {
                    _donVienChuc_PhongKhoaService.Add(donVienChuc_PhongKhoa);
                    _donVienChuc_PhongKhoaService.Commit();
                    response = request.CreateResponse(HttpStatusCode.Created, donVienChuc_PhongKhoa);
                }
                return response;
            });

        }
        [Route("getall")]
        public HttpResponseMessage Get(HttpRequestMessage request)
        {
            return CreateHttpResponse(request, () =>
            {


                var listCategory = _donVienChuc_PhongKhoaService.GetAll();

                HttpResponseMessage response = request.CreateResponse(HttpStatusCode.OK, listCategory);


                return response;
            });
        }
        
        
        
        
        
        
        public HttpResponseMessage Post(HttpRequestMessage request, DonVienChuc_PhongKhoa donVienChuc_PhongKhoa)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                if (ModelState.IsValid)
                {
                    request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
                }
                else
                {
                    _donVienChuc_PhongKhoaService.Add(donVienChuc_PhongKhoa);
                    _donVienChuc_PhongKhoaService.Commit();

                    response = request.CreateResponse(HttpStatusCode.Created);

                }
                return response;
            });
        }

        public HttpResponseMessage Put(HttpRequestMessage request, DonVienChuc_PhongKhoa donVienChuc_PhongKhoa)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                if (ModelState.IsValid)
                {
                    request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
                }
                else
                {
                    _donVienChuc_PhongKhoaService.Update(donVienChuc_PhongKhoa);
                    _donVienChuc_PhongKhoaService.Commit();

                    response = request.CreateResponse(HttpStatusCode.OK);

                }
                return response;
            });
        }

        public HttpResponseMessage Delete(HttpRequestMessage request, int id)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                if (ModelState.IsValid)
                {
                    request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
                }
                else
                {
                    _donVienChuc_PhongKhoaService.delete(id);
                    _donVienChuc_PhongKhoaService.Commit();

                    response = request.CreateResponse(HttpStatusCode.OK);

                }
                return response;
            });
        }

    }
}
