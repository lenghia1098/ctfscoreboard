﻿using Platform.Model;
using Platform.Service;
using Platform.Web.infratructure.core;
using Platform.Web.infratructure.extensions;
using Platform.Web.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace Platform.Web.Api
{
    [RoutePrefix("api/thoikhoabieu")]
    [Authorize]
    public class ThoiKhoaBieuController : ApiControllerBase
    {



        IThoiKhoaBieuService _thoiKhoaBieuService;

        public ThoiKhoaBieuController(ILoiService loiService, IThoiKhoaBieuService thoiKhoaBieuService) : base(loiService)
        {
            this._thoiKhoaBieuService = thoiKhoaBieuService;
        }

        [Route("createExcel")]
        [HttpPost]
        // [AllowAnonymous]
        public HttpResponseMessage Create(HttpRequestMessage request, IEnumerable<ThoiKhoaBieuViewModel> thoiKhoaBieuVm)
        {


            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                if (!ModelState.IsValid)
                {
                    response = request.CreateResponse(HttpStatusCode.BadRequest, ModelState);
                }
                else
                {
                    foreach (var item in thoiKhoaBieuVm)
                    {
                        var newThoiKhoaBieu = new ThoiKhoaBieu();
                        newThoiKhoaBieu.UpdateThoiKhoaBieu(item);

                        _thoiKhoaBieuService.Add(newThoiKhoaBieu);
                        _thoiKhoaBieuService.Save();
                    }


                    //var responseData = Mapper.Map<DangKy_TamThoi, DangKy_TamThoiViewModel>(newDangKy_TamThoi);
                    response = request.CreateResponse(HttpStatusCode.OK);
                }

                return response;
            });
        }

        [Route("getall")]
        public HttpResponseMessage Get(HttpRequestMessage request)
        {
            return CreateHttpResponse(request, () =>
            {


                var listCategory = _thoiKhoaBieuService.GetAll();

                HttpResponseMessage response = request.CreateResponse(HttpStatusCode.OK, listCategory);


                return response;
            });
        }

        [Route("getTKB")]
        public HttpResponseMessage getTKB(HttpRequestMessage request, string MaSoVC, string hocKy, string tuan)
        {
            return CreateHttpResponse(request, () =>
            {


                var query = _thoiKhoaBieuService.getTKB(MaSoVC, hocKy, tuan);

                HttpResponseMessage response = request.CreateResponse(HttpStatusCode.OK, query);




                return response;
            });
        }
        [Route("xemTKBmonhoc")]
        [HttpGet]
        public HttpResponseMessage xemTKBmonhoc(HttpRequestMessage request, string tenMH, string hocKy, string namhoc)
        {
            return CreateHttpResponse(request, () =>
            {


                var query = _thoiKhoaBieuService.xemTKBmonhoc(tenMH, hocKy, namhoc);

                HttpResponseMessage response = request.CreateResponse(HttpStatusCode.OK, query);




                return response;
            });
        }


        [Route("xemThongTinNhom")]
        [HttpGet]
        public HttpResponseMessage xemThongTinNhom(HttpRequestMessage request, string maNhom, string maMH,string malop)
        {
            return CreateHttpResponse(request, () =>
            {


                var query = _thoiKhoaBieuService.xemThongTinNhom(maNhom, maMH, malop);

                HttpResponseMessage response = request.CreateResponse(HttpStatusCode.OK, query);




                return response;
            });
        }
    }
}
