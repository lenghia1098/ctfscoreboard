﻿using Platform.Model;
using Platform.Service;
using Platform.Web.infratructure.core;
using Platform.Web.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Platform.Web.infratructure.extensions;
using AutoMapper;
using Platform.Web.Infrastructure.Core;

namespace Platform.Web.Api
{
    [RoutePrefix("api/SinhVien_ViecPhaiLam")]
    [Authorize]
    public class SinhVien_ViecPhaiLamController : ApiControllerBase
    {
        ISinhVien_ViecPhaiLamService _sinhVien_ViecPhaiLamService;

        public SinhVien_ViecPhaiLamController(ILoiService loiService, ISinhVien_ViecPhaiLamService sinhVien_ViecPhaiLamService) : base(loiService)
        {
            this._sinhVien_ViecPhaiLamService = sinhVien_ViecPhaiLamService;
        }
         [Route("createExcel")]
        [HttpPost]
        public HttpResponseMessage Create(HttpRequestMessage request, IEnumerable<SinhVien_ViecPhaiLamViewModel> sinhVien_ViecPhaiLamVm)
        {


            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                if (!ModelState.IsValid)
                {
                    response = request.CreateResponse(HttpStatusCode.BadRequest, ModelState);
                }
                else
                {
                    foreach (var item in sinhVien_ViecPhaiLamVm)
                    {
                        var newSinhVien_ViecPhaiLam = new SinhVien_ViecPhaiLam();
                        newSinhVien_ViecPhaiLam.UpdateSinhVien_ViecPhaiLam(item);

                        _sinhVien_ViecPhaiLamService.Add(newSinhVien_ViecPhaiLam);
                        _sinhVien_ViecPhaiLamService.Save();
                    }


                    //var responseData = Mapper.Map<DangKy_TamThoi, DangKy_TamThoiViewModel>(newDangKy_TamThoi);
                    response = request.CreateResponse(HttpStatusCode.OK);
                }

                return response;
            });
        }
        public HttpResponseMessage Create(HttpRequestMessage request, SinhVien_ViecPhaiLamViewModel sinhVien_ViecPhaiLamVm)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                if (ModelState.IsValid)
                {
                    request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
                }
                else
                {
                    SinhVien_ViecPhaiLam sinhVien_ViecPhaiLam = new SinhVien_ViecPhaiLam();
                    sinhVien_ViecPhaiLam.UpdateSinhVien_ViecPhaiLam(sinhVien_ViecPhaiLamVm);
                    
                    _sinhVien_ViecPhaiLamService.Add(sinhVien_ViecPhaiLam);

                    _sinhVien_ViecPhaiLamService.Commit();
                    response = request.CreateResponse(HttpStatusCode.Created, sinhVien_ViecPhaiLam);
                }
                return response;
            });

        }

        [Route("viecdalam")]
        [HttpGet]
        public HttpResponseMessage viecdalam(HttpRequestMessage request, int page, string mssv, int pageSize = 20)
        {
            return CreateHttpResponse(request, () =>
            {
                int totalRow = 0;

                IEnumerable<viecdalam> viecdalam = _sinhVien_ViecPhaiLamService.viecdalam(mssv);

                totalRow = viecdalam.Count();
                var query = viecdalam.OrderByDescending(x => x.MaSoCongViec).Skip(page * pageSize).Take(pageSize);

                var responseData = Mapper.Map<IEnumerable<viecdalam>, IEnumerable<viecdalam>>(query);

                var PaginationSet = new PaginationSet<viecdalam>()
                {

                    Items = responseData,
                    Page = page,
                    TotalCount = totalRow,/*tong so bai ghi*/
                    TotalPages = (int)Math.Ceiling((decimal)totalRow / pageSize),/*tong số trang*/



                };

                HttpResponseMessage response = request.CreateResponse(HttpStatusCode.OK, PaginationSet);



                return response;
            });
        }




        [Route("getall")]
        public HttpResponseMessage Get(HttpRequestMessage request)
        {
            return CreateHttpResponse(request, () =>
            {


                var listCategory = _sinhVien_ViecPhaiLamService.GetAll();

                HttpResponseMessage response = request.CreateResponse(HttpStatusCode.OK, listCategory);


                return response;
            });
        }
        [Route("soViec/{Id}")]
        public HttpResponseMessage GetSoViec(HttpRequestMessage request,string Id)
        {
            return CreateHttpResponse(request, () =>
            {


                int soViec = _sinhVien_ViecPhaiLamService.DemViecPhaiLamSv(Id);

                HttpResponseMessage response = request.CreateResponse(HttpStatusCode.OK, soViec);


                return response;
            });
        }

        public HttpResponseMessage Post(HttpRequestMessage request, SinhVien_ViecPhaiLam sinhVien_ViecPhaiLam)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                if (ModelState.IsValid)
                {
                    request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
                }
                else
                {
                    _sinhVien_ViecPhaiLamService.Add(sinhVien_ViecPhaiLam);
                    _sinhVien_ViecPhaiLamService.Commit();

                    response = request.CreateResponse(HttpStatusCode.Created);

                }
                return response;
            });
        }

        [Route("getID")]
        [HttpGet]
       
        public HttpResponseMessage getID(HttpRequestMessage request, int mscv)
        {
            return CreateHttpResponse(request, () =>
            {
                var model = _sinhVien_ViecPhaiLamService.getIDViecPhaiLam(mscv);

                var responseData = Mapper.Map<SinhVien_ViecPhaiLam, SinhVien_ViecPhaiLamViewModel>(model);

                var response = request.CreateResponse(HttpStatusCode.OK, responseData);

                return response;
            });
        }

        [Route("getIdAll")]
        [HttpGet]

        public HttpResponseMessage getIdAll(HttpRequestMessage request, string mssv)
        {
            return CreateHttpResponse(request, () =>
            {
                var model = _sinhVien_ViecPhaiLamService.getIdAll(mssv);

                var responseData = Mapper.Map<IEnumerable< SinhVien_ViecPhaiLam>,IEnumerable<SinhVien_ViecPhaiLamViewModel>>(model);

                var response = request.CreateResponse(HttpStatusCode.OK, responseData);

                return response;
            });
        }

        [Route("update")]
        [HttpPut]
        [AllowAnonymous]
        public HttpResponseMessage Update(HttpRequestMessage request, SinhVien_ViecPhaiLamViewModel sinhVien_ViecPhaiLamVm )
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                if (!ModelState.IsValid)
                {
                    request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
                }
                else
                {

                    var dbsinhvien = _sinhVien_ViecPhaiLamService.getIDViecPhaiLam(sinhVien_ViecPhaiLamVm.MaSoCongViec);

                    dbsinhvien.UpdateSinhVien_ViecPhaiLam(sinhVien_ViecPhaiLamVm);

                    dbsinhvien.DaXoa = true;
                    dbsinhvien.DaHoanThanh = false;
                _sinhVien_ViecPhaiLamService.Update(dbsinhvien);
                    _sinhVien_ViecPhaiLamService.Commit();

                    var responseData = Mapper.Map<SinhVien_ViecPhaiLam, SinhVien_ViecPhaiLamViewModel>(dbsinhvien);

                    response = request.CreateResponse(HttpStatusCode.Created, responseData);

            }
                return response;
            });
        }



        [Route("updateAll")]
        [HttpPut]
        //    [AllowAnonymous]
        public HttpResponseMessage UpdateAll(HttpRequestMessage request, SinhVien_ViecPhaiLamViewModel sinhVien_ViecPhaiLamVm)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
            if (!ModelState.IsValid)
            {
                request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
            }
            else
            {

                var dbsinhvien = _sinhVien_ViecPhaiLamService.getIDViecPhaiLam(sinhVien_ViecPhaiLamVm.MaSoCongViec);

                dbsinhvien.UpdateSinhVien_ViecPhaiLam(sinhVien_ViecPhaiLamVm);

                dbsinhvien.DaXoa = true;
                dbsinhvien.ThoiGianXoa = DateTime.Now;
                    dbsinhvien.NguoiXoa = sinhVien_ViecPhaiLamVm.NguoiXoa;
                _sinhVien_ViecPhaiLamService.Update(dbsinhvien);
                _sinhVien_ViecPhaiLamService.Commit();

                var responseData = Mapper.Map<SinhVien_ViecPhaiLam, SinhVien_ViecPhaiLamViewModel>(dbsinhvien);

                response = request.CreateResponse(HttpStatusCode.Created, responseData);

                }
                return response;
            });
        }




        public HttpResponseMessage Delete(HttpRequestMessage request, int id)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                if (ModelState.IsValid)
                {
                    request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
                }
                else
                {
                    var oldProductCategory = _sinhVien_ViecPhaiLamService.delete(id);
                    _sinhVien_ViecPhaiLamService.Commit();

                    var responseData = Mapper.Map<SinhVien_ViecPhaiLam, SinhVien_ViecPhaiLamViewModel>(oldProductCategory);
                    response = request.CreateResponse(HttpStatusCode.Created, responseData);

                   

                }
                return response;
            });
        }
        [Route("getbyid")]
        [HttpGet]
        public HttpResponseMessage GetById(HttpRequestMessage request, int id)
        {
            return CreateHttpResponse(request, () =>
            {
                var model = _sinhVien_ViecPhaiLamService.GetByID(Convert.ToInt32(id));

              

                var response = request.CreateResponse(HttpStatusCode.OK, model);

                return response;
            });
        }

       
    }
}
//var dbviecphailamsinhvien = _viecPhaiLamSinhVienService.viecdalam(sinhVien_ViecPhaiLamVm.MaSoCongViec.ToString());
//dbviecphailamsinhvien..

//var dbviecphailamsinhvien = _sinhVien_ViecPhaiLamService.viecdalam(sinhVien_ViecPhaiLamVm.MaSoCongViec.ToString());
//dbviecphailamsinhvien.UpdateSinhVien_ViecPhaiLam(sinhVien_ViecPhaiLamVm);
//                    _sinhVien_ViecPhaiLamService.Update(dbviecphailamsinhvien);
//                    _sinhVien_ViecPhaiLamService.Commit();

//                    var responseData = Mapper.Map<SinhVien_ViecPhaiLam, SinhVien_ViecPhaiLamViewModel>(dbviecphailamsinhvien);
//response = request.CreateResponse(HttpStatusCode.Created, responseData);																													