﻿using AutoMapper;
using Platform.Model;
using Platform.Service;
using Platform.Web.infratructure.core;
using Platform.Web.Models;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Platform.Web.infratructure.extensions;


namespace Platform.Web.Api
{
    [RoutePrefix("api/ngaylethaydoi")]
    [Authorize]
    public class NgayLeThayDoiController : ApiControllerBase
    {
        INgayLeThayDoiService _ngayLeThayDoiService;

        public NgayLeThayDoiController(ILoiService loiService, INgayLeThayDoiService ngayLeThayDoiService) : base(loiService)
        {
            this._ngayLeThayDoiService = ngayLeThayDoiService;
        }
        [Route("createExcel")]
        [HttpPost]
        public HttpResponseMessage CreateExcel(HttpRequestMessage request, IEnumerable<NgayLeThayDoiViewModel> ngayLeThayDoiVm)
        {


            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                if (!ModelState.IsValid)
                {
                    response = request.CreateResponse(HttpStatusCode.BadRequest, ModelState);
                }
                else
                {
                    foreach (var item in ngayLeThayDoiVm)
                    {
                        var newNgayLeThayDoi = new NgayLeThayDoi();
                        newNgayLeThayDoi.UpdateNgayLeThayDoi(item);

                        _ngayLeThayDoiService.Add(newNgayLeThayDoi);
                        _ngayLeThayDoiService.Save();
                    }


                    //var responseData = Mapper.Map<DangKy_TamThoi, DangKy_TamThoiViewModel>(newDangKy_TamThoi);
                    response = request.CreateResponse(HttpStatusCode.OK);
                }

                return response;
            });
        }
        [Route("create")]
        [HttpPost]
        // [AllowAnonymous]
        public HttpResponseMessage Create(HttpRequestMessage request, NgayLeThayDoiViewModel ngayLeThayDoiVm)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                if (!ModelState.IsValid)
                {
                    response = request.CreateResponse(HttpStatusCode.BadRequest, ModelState);
                }
                else
                {

                    var newNgayLeThayDoi = new NgayLeThayDoi();
                    newNgayLeThayDoi.UpdateNgayLeThayDoi(ngayLeThayDoiVm);

                    _ngayLeThayDoiService.Add(newNgayLeThayDoi);
                    _ngayLeThayDoiService.Save();



                    //var responseData = Mapper.Map<NgayLeThayDoi, NgayLeThayDoiViewModel>(newNgayLeThayDoi);
                    response = request.CreateResponse(HttpStatusCode.OK);
                }

                return response;
            });
        }

        [Route("getall")]
        public HttpResponseMessage Get(HttpRequestMessage request)
        {
            return CreateHttpResponse(request, () =>
            {


                var listCategory = _ngayLeThayDoiService.GetAll();
                var listNgayLeThayDoiVm = Mapper.Map<List<NgayLeThayDoiViewModel>>(listCategory);

                HttpResponseMessage response = request.CreateResponse(HttpStatusCode.OK, listNgayLeThayDoiVm);
                //  HttpResponseMessage response = request.CreateResponse(HttpStatusCode.OK, listCategory);

                return response;
            });
        }


        [Route("getID")]
        public HttpResponseMessage GetById(HttpRequestMessage request,int ID)
        {
            return CreateHttpResponse(request, () =>
            {


                var sinhvien = _ngayLeThayDoiService.GetByID(ID);
                var NgayLeThayDoiVm = Mapper.Map<NgayLeThayDoiViewModel>(sinhvien);

                HttpResponseMessage response = request.CreateResponse(HttpStatusCode.OK, NgayLeThayDoiVm);


                return response;
            });
        }
        [Route("GetByNamHoc")]
        public HttpResponseMessage GetByNamHoc(HttpRequestMessage request, string namhoc)
        {
            return CreateHttpResponse(request, () =>
            {


                var query = _ngayLeThayDoiService.GetByNamHoc(namhoc);
                var NgayLeThayDoiVm = Mapper.Map<IEnumerable<NgayLeThayDoi>, IEnumerable<NgayLeThayDoiViewModel>>(query);


                HttpResponseMessage response = request.CreateResponse(HttpStatusCode.OK, NgayLeThayDoiVm);


                return response;
            });
        }


        public HttpResponseMessage Post(HttpRequestMessage request, NgayLeThayDoiViewModel ngayLeThayDoiVm)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                if (ModelState.IsValid)
                {
                    request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
                }
                else
                {
                    NgayLeThayDoi newNgayLeThayDoi = new NgayLeThayDoi();
                    newNgayLeThayDoi.UpdateNgayLeThayDoi(ngayLeThayDoiVm);
                    _ngayLeThayDoiService.Add(newNgayLeThayDoi);
                    _ngayLeThayDoiService.Commit();

                    response = request.CreateResponse(HttpStatusCode.Created);

                }
                return response;
            });
        }

        [Route("delete")]
        [HttpDelete]
        [HttpGet]
        // [AllowAnonymous]
        public HttpResponseMessage delete(HttpRequestMessage request, string ID)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                if (!ModelState.IsValid)
                {
                    response = request.CreateResponse(HttpStatusCode.BadRequest, ModelState);
                }
                else
                {
                    var oldProductCategory = _ngayLeThayDoiService.delete(int.Parse(ID));
                    _ngayLeThayDoiService.Save();

                    var responseData = Mapper.Map<NgayLeThayDoi, NgayLeThayDoiViewModel>(oldProductCategory);
                    response = request.CreateResponse(HttpStatusCode.Created, responseData);
                }

                return response;
            });
        }





    }
}