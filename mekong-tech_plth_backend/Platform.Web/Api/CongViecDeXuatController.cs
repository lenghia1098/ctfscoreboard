﻿using AutoMapper;
using Platform.Model;
using Platform.Service;
using Platform.Web.infratructure.core;
using Platform.Web.infratructure.extensions;
using Platform.Web.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace Platform.Web.Api
{
    [RoutePrefix("api/congviecdexuat")]
    [Authorize]
    public class CongViecDeXuatController : ApiControllerBase
    {
        ICongViecDeXuatService _congViecDeXuatService;

        public CongViecDeXuatController(ILoiService loiService, ICongViecDeXuatService congViecDeXuatService) : base(loiService)
        {
            this._congViecDeXuatService = congViecDeXuatService;
        }
        [Route("createExcel")]
        [HttpPost]
        // [AllowAnonymous]
        public HttpResponseMessage Create(HttpRequestMessage request, IEnumerable<CongViecDeXuatViewModel> congViecDeXuatVM)
        {


            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                if (!ModelState.IsValid)
                {
                    response = request.CreateResponse(HttpStatusCode.BadRequest, ModelState);
                }
                else
                {
                    foreach (var item in congViecDeXuatVM)
                    {
                        var newCongViecDeXuat = new CongViecDeXuat();
                        newCongViecDeXuat.UpdateCongViecDeXuat(item);

                        _congViecDeXuatService.Add(newCongViecDeXuat);
                        _congViecDeXuatService.Save();
                    }


                    //var responseData = Mapper.Map<DangKy_TamThoi, DangKy_TamThoiViewModel>(newDangKy_TamThoi);
                    response = request.CreateResponse(HttpStatusCode.OK);
                }

                return response;
            });
        }
        public HttpResponseMessage Create(HttpRequestMessage request, CongViecDeXuat congViecDeXuat)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                if (ModelState.IsValid)
                {
                    request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
                }
                else
                {
                    _congViecDeXuatService.Add(congViecDeXuat);
                    _congViecDeXuatService.Commit();
                    response = request.CreateResponse(HttpStatusCode.Created, congViecDeXuat);
                }
                return response;
            });

        }
        [Route("getall")]
        public HttpResponseMessage Get(HttpRequestMessage request)
        {
            return CreateHttpResponse(request, () =>
            {


                var listCategory = _congViecDeXuatService.GetAll();

                HttpResponseMessage response = request.CreateResponse(HttpStatusCode.OK, listCategory);


                return response;
            });
        }

        [Route("getbyid")]
        [HttpGet]
        public HttpResponseMessage GetById(HttpRequestMessage request, string Id)
        {
            return CreateHttpResponse(request, () =>
            {
                var model = _congViecDeXuatService.GetAllById(Id);


                var response = request.CreateResponse(HttpStatusCode.OK, model);

                return response;
            });
        }
       
        [Route("getID")]
        public HttpResponseMessage GetById(HttpRequestMessage request)
        {
            return CreateHttpResponse(request, () =>
            {


                var sinhvien = _congViecDeXuatService.GetByID(1);
              

                HttpResponseMessage response = request.CreateResponse(HttpStatusCode.OK, sinhvien);


                return response;
            });
        }

      

    }
}
