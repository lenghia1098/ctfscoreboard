﻿using AutoMapper;
using Platform.Model;
using Platform.Service;
using Platform.Web.infratructure.core;
using Platform.Web.infratructure.extensions;
using Platform.Web.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace Platform.Web.Api
{
    [RoutePrefix("api/don-vienchuc")]
    public class Don_VienChucController : ApiControllerBase
    {
        IDon_VienChucService _don_VienChucService;

        public Don_VienChucController(ILoiService loiService, IDon_VienChucService don_VienChucService) : base(loiService)
        {
            this._don_VienChucService = don_VienChucService;
        }
        public HttpResponseMessage Create(HttpRequestMessage request, Don_VienChuc don_VienChuc)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                if (ModelState.IsValid)
                {
                    request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
                }
                else
                {
                    _don_VienChucService.Add(don_VienChuc);
                    _don_VienChucService.Commit();
                    response = request.CreateResponse(HttpStatusCode.Created, don_VienChuc);
                }
                return response;
            });

        }

       

        [Route("updateAll")]
        [HttpPut]
        //    [AllowAnonymous]
        public HttpResponseMessage UpdateAll(HttpRequestMessage request, Don_VienChucViewModel don_VienChucVM)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                if (!ModelState.IsValid)
                {
                    request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
                }
                else
                {

                    var dbvienchuc = _don_VienChucService.getIdDonDaNop(Convert.ToInt32(don_VienChucVM.MaDon));

                    dbvienchuc.UpdateDon_VienChuc(don_VienChucVM);

                    
                    dbvienchuc.DaXoa = true;
                    dbvienchuc.ThoiGianXoa = DateTime.Now;
                    dbvienchuc.NguoiXoa = don_VienChucVM.NguoiXoa;

                    _don_VienChucService.Update(dbvienchuc);
                    _don_VienChucService.Commit();

                    var responseData = Mapper.Map<Don_VienChuc, Don_VienChuc>(dbvienchuc);

                    response = request.CreateResponse(HttpStatusCode.Created, responseData);

                }
                return response;
            });
        }


        [Route("getall")]
        public HttpResponseMessage Get(HttpRequestMessage request)
        {
            return CreateHttpResponse(request, () =>
            {


                var listCategory = _don_VienChucService.GetAll();

                HttpResponseMessage response = request.CreateResponse(HttpStatusCode.OK, listCategory);


                return response;
            });
        }

        public HttpResponseMessage Post(HttpRequestMessage request, Don_VienChuc don_VienChuc)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                if (ModelState.IsValid)
                {
                    request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
                }
                else
                {
                    _don_VienChucService.Add(don_VienChuc);
                    _don_VienChucService.Commit();

                    response = request.CreateResponse(HttpStatusCode.Created);

                }
                return response;
            });
        }
        [Route("getID")]
        [HttpGet]

        public HttpResponseMessage getIdDonDaNop(HttpRequestMessage request, int maDon)
        {
            return CreateHttpResponse(request, () =>
            {
                var model = _don_VienChucService.getIdDonDaNop(maDon);

                var responseData = Mapper.Map<Don_VienChuc, Don_VienChucViewModel>(model);

                var response = request.CreateResponse(HttpStatusCode.OK, responseData);

                return response;
            });
        }

       
        public HttpResponseMessage Delete(HttpRequestMessage request, int id)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                if (ModelState.IsValid)
                {
                    request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
                }
                else
                {
                    _don_VienChucService.delete(id);
                    _don_VienChucService.Commit();

                    response = request.CreateResponse(HttpStatusCode.OK);

                }
                return response;
            });
        }

    }
}
