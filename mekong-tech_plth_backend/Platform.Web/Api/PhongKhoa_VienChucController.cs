﻿using Platform.Model;
using Platform.Service;
using Platform.Web.infratructure.core;
using Platform.Web.infratructure.extensions;
using Platform.Web.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace Platform.Web.Api
{
    [RoutePrefix("api/phongkhoa-vienchuc")]
    [Authorize]
    public class PhongKhoa_VienChucController : ApiControllerBase
    {
        IPhongKhoa_VienChucService _phongKhoa_VienChucService;

        public PhongKhoa_VienChucController(ILoiService loiService, IPhongKhoa_VienChucService phongKhoa_VienChucService) : base(loiService)
        {
            this._phongKhoa_VienChucService = phongKhoa_VienChucService;
        }
        [Route("createExcel")]
        [HttpPost]
        public HttpResponseMessage Create(HttpRequestMessage request, IEnumerable<PhongKhoa_VienChucViewModel> phongHocVm)
        {


            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                if (!ModelState.IsValid)
                {
                    response = request.CreateResponse(HttpStatusCode.BadRequest, ModelState);
                }
                else
                {
                    foreach (var item in phongHocVm)
                    {
                        var newPhongKhoa_VienChuc = new PhongKhoa_VienChuc();
                        newPhongKhoa_VienChuc.UpdatePhongKhoa_VienChuc(item);

                        _phongKhoa_VienChucService.Add(newPhongKhoa_VienChuc);
                        _phongKhoa_VienChucService.Save();
                    }


                    //var responseData = Mapper.Map<DangKy_TamThoi, DangKy_TamThoiViewModel>(newDangKy_TamThoi);
                    response = request.CreateResponse(HttpStatusCode.OK);
                }

                return response;
            });
        }
        public HttpResponseMessage Create(HttpRequestMessage request, PhongKhoa_VienChuc phongKhoa_VienChuc)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                if (ModelState.IsValid)
                {
                    request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
                }
                else
                {
                    _phongKhoa_VienChucService.Add(phongKhoa_VienChuc);
                    _phongKhoa_VienChucService.Commit();
                    response = request.CreateResponse(HttpStatusCode.Created, phongKhoa_VienChuc);
                }
                return response;
            });

        }
        [Route("getall")]
        public HttpResponseMessage Get(HttpRequestMessage request)
        {
            return CreateHttpResponse(request, () =>
            {


                var listCategory = _phongKhoa_VienChucService.GetAll();

                HttpResponseMessage response = request.CreateResponse(HttpStatusCode.OK, listCategory);


                return response;
            });
        }

        public HttpResponseMessage Post(HttpRequestMessage request, PhongKhoa_VienChuc phongKhoa_VienChuc)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                if (ModelState.IsValid)
                {
                    request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
                }
                else
                {
                    _phongKhoa_VienChucService.Add(phongKhoa_VienChuc);
                    _phongKhoa_VienChucService.Commit();

                    response = request.CreateResponse(HttpStatusCode.Created);

                }
                return response;
            });
        }

        public HttpResponseMessage Put(HttpRequestMessage request, PhongKhoa_VienChuc phongKhoa_VienChuc)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                if (ModelState.IsValid)
                {
                    request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
                }
                else
                {
                    _phongKhoa_VienChucService.Update(phongKhoa_VienChuc);
                    _phongKhoa_VienChucService.Commit();

                    response = request.CreateResponse(HttpStatusCode.OK);

                }
                return response;
            });
        }

        public HttpResponseMessage Delete(HttpRequestMessage request, int id)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                if (ModelState.IsValid)
                {
                    request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
                }
                else
                {
                    _phongKhoa_VienChucService.delete(id);
                    _phongKhoa_VienChucService.Commit();

                    response = request.CreateResponse(HttpStatusCode.OK);

                }
                return response;
            });
        }

    }
}