﻿using AutoMapper;
using Platform.Model;
using Platform.Service;
using Platform.Web.infratructure.core;
using Platform.Web.infratructure.extensions;
using Platform.Web.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Platform.Web.Infrastructure.Core;			   

namespace Platform.Web.Api
{
    [RoutePrefix("api/ViecPhaiLamSinhVien")]
    [Authorize]
    public class ViecPhaiLamSinhVienController : ApiControllerBase
    {
        IViecPhaiLamSinhVienService _viecPhaiLamSinhVienService;

        public ViecPhaiLamSinhVienController(ILoiService loiService, IViecPhaiLamSinhVienService viecPhaiLamSinhVienService) : base(loiService)
        {
            this._viecPhaiLamSinhVienService = viecPhaiLamSinhVienService;
        }
        [Route("createExcel")]
        [HttpPost]
        // [AllowAnonymous]
        public HttpResponseMessage Create(HttpRequestMessage request, IEnumerable<ViecPhaiLamSinhVienViewModel> viecPhaiLamSinhVienVm)
        {


            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                if (!ModelState.IsValid)
                {
                    response = request.CreateResponse(HttpStatusCode.BadRequest, ModelState);
                }
                else
                {
                    foreach (var item in viecPhaiLamSinhVienVm)
                    {
                        var newViecPhaiLamSinhVien = new ViecPhaiLamSinhVien();
                        newViecPhaiLamSinhVien.UpdateViecPhaiLamSinhVien(item);

                        _viecPhaiLamSinhVienService.Add(newViecPhaiLamSinhVien);
                        _viecPhaiLamSinhVienService.Save();
                    }


                    //var responseData = Mapper.Map<DangKy_TamThoi, DangKy_TamThoiViewModel>(newDangKy_TamThoi);
                    response = request.CreateResponse(HttpStatusCode.OK);
                }

                return response;
            });
        }
        public HttpResponseMessage Create(HttpRequestMessage request, ViecPhaiLamSinhVien viecPhaiLamSinhVien)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                if (ModelState.IsValid)
                {
                    request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
                }
                else
                {
                    _viecPhaiLamSinhVienService.Add(viecPhaiLamSinhVien);
                    _viecPhaiLamSinhVienService.Commit();
                    response = request.CreateResponse(HttpStatusCode.Created, viecPhaiLamSinhVien);
                }
                return response;
            });

        }
        [Route("getall")]
        public HttpResponseMessage Get(HttpRequestMessage request)
        {
            return CreateHttpResponse(request, () =>
            {


                var listCategory = _viecPhaiLamSinhVienService.GetAll();

                HttpResponseMessage response = request.CreateResponse(HttpStatusCode.OK, listCategory);


                return response;
            });
        }

        [Route("getviecphailam")]
        public HttpResponseMessage getviecphailam(HttpRequestMessage request, int page, string mssv, int pageSize = 20)
        {
            return CreateHttpResponse(request, () =>
            {


                int totalRow = 0;

                IEnumerable<chucnangvieclamsinhvien> viecdalam = _viecPhaiLamSinhVienService.getviecphailam(mssv);

                totalRow = viecdalam.Count();
                var query = viecdalam.OrderByDescending(x => x.MaSoCongViec).Skip(page * pageSize).Take(pageSize);

                var responseData = Mapper.Map<IEnumerable<chucnangvieclamsinhvien>, IEnumerable<chucnangvieclamsinhvien>>(query);

                var PaginationSet = new PaginationSet<chucnangvieclamsinhvien>()
                {

                    Items = responseData,
                    Page = page,
                    TotalCount = totalRow,/*tong so bai ghi*/
                    TotalPages = (int)Math.Ceiling((decimal)totalRow / pageSize),/*tong số trang*/



                };

                HttpResponseMessage response = request.CreateResponse(HttpStatusCode.OK, PaginationSet);


                return response;
            });
        }
        [Route("getviecdalam")]
        public HttpResponseMessage getviecdalam(HttpRequestMessage request, string mssv)
        {
            return CreateHttpResponse(request, () =>
            {


                var viecdalam = _viecPhaiLamSinhVienService.getviecdalam(mssv);

                HttpResponseMessage response = request.CreateResponse(HttpStatusCode.OK, viecdalam);


                return response;
            });
        }
        [Route("getchitietviecdalam")]
        public HttpResponseMessage getchitietviecdalam(HttpRequestMessage request, int MaSoCongViec)
        {
            return CreateHttpResponse(request, () =>
            {


                var chitietviecdalam = _viecPhaiLamSinhVienService.getchitietviecdalam(MaSoCongViec);

                HttpResponseMessage response = request.CreateResponse(HttpStatusCode.OK, chitietviecdalam);


                return response;
            });
        }

        [Route("demviecdalam")]
        [HttpGet]
        public HttpResponseMessage demviecdalam(HttpRequestMessage request, string mssv)
        {
            return CreateHttpResponse(request, () =>
            {


                var Dem = _viecPhaiLamSinhVienService.DemViecPhaiLamSinhVien(mssv);

                HttpResponseMessage response = request.CreateResponse(HttpStatusCode.OK, Dem);


                return response;
            });
        }

        public HttpResponseMessage Post(HttpRequestMessage request, ViecPhaiLamSinhVien viecPhaiLamSinhVien)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                if (ModelState.IsValid)
                {
                    request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
                }
                else
                {
                    _viecPhaiLamSinhVienService.Add(viecPhaiLamSinhVien);
                    _viecPhaiLamSinhVienService.Commit();

                    response = request.CreateResponse(HttpStatusCode.Created);

                }
                return response;
            });
        }

        public HttpResponseMessage Put(HttpRequestMessage request, ViecPhaiLamSinhVien viecPhaiLamSinhVien)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                if (ModelState.IsValid)
                {
                    request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
                }
                else
                {
                    _viecPhaiLamSinhVienService.Update(viecPhaiLamSinhVien);
                    _viecPhaiLamSinhVienService.Commit();

                    response = request.CreateResponse(HttpStatusCode.OK);

                }
                return response;
            });
        }

        public HttpResponseMessage Delete(HttpRequestMessage request, int id)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                if (ModelState.IsValid)
                {
                    request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
                }
                else
                {
                    _viecPhaiLamSinhVienService.delete(id);
                    _viecPhaiLamSinhVienService.Commit();

                    response = request.CreateResponse(HttpStatusCode.OK);

                }
                return response;
            });
        }
    }
}
