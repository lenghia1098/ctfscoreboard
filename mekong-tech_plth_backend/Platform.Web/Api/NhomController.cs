﻿using AutoMapper;
using Platform.Model;
using Platform.Service;
using Platform.Web.infratructure.core;
using Platform.Web.infratructure.extensions;
using Platform.Web.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;

namespace Platform.Web.Api
{
    [RoutePrefix("api/nhom")]
    [Authorize]
    public class NhomController : ApiControllerBase
    {
        INhomService _nhomService;
        IDangKyService _dangKyService;
        public NhomController(ILoiService loiService, INhomService nhomService, IDangKyService dangKyService) : base(loiService)
        {
            this._nhomService = nhomService;
            this._dangKyService = dangKyService;
        }
        [Route("createExcel")]
        [HttpPost]
        public HttpResponseMessage Create(HttpRequestMessage request, IEnumerable<NhomViewModel>nhomVm)
        {


            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                if (!ModelState.IsValid)
                {
                    response = request.CreateResponse(HttpStatusCode.BadRequest, ModelState);
                }
                else
                {
                    foreach (var item in nhomVm)
                    {
                        var newNhom = new Nhom();
                        newNhom.UpdatNhom(item);

                        _nhomService.Add(newNhom);
                        _nhomService.Save();
                    }


                    //var responseData = Mapper.Map<DangKy_TamThoi, DangKy_TamThoiViewModel>(newDangKy_TamThoi);
                    response = request.CreateResponse(HttpStatusCode.OK);
                }

                return response;
            });
        }
        [Route("getNhomFromDK")]
        public HttpResponseMessage GetNhomFromDK(HttpRequestMessage request,string mssv, int hocKy)
        {
            return CreateHttpResponse(request, () =>
            {


                var listNhom = _dangKyService.GetMaNhomByMSSV(mssv, hocKy);
 
                 



                HttpResponseMessage response = request.CreateResponse(HttpStatusCode.OK, listNhom);


                return response;
            });
        }
        [Route("getNhom")]
        public HttpResponseMessage GetNhom(HttpRequestMessage request, string mssv, string hocKy,string tuan)
        {
            return CreateHttpResponse(request, () =>
            {


                var query = _nhomService.getNhom(mssv, hocKy,tuan);

                HttpResponseMessage response = request.CreateResponse(HttpStatusCode.OK, query);
    

                

                return response;
            });
        }
        [Route("getHocKy")]
        public HttpResponseMessage getHocKy(HttpRequestMessage request, string mssv)
        {
            return CreateHttpResponse(request, () =>
            {


                var query = _nhomService.getHocKy(mssv);

                HttpResponseMessage response = request.CreateResponse(HttpStatusCode.OK, query);




                return response;
            });
        }

        [Route("getHocKyTH")]
       
        public HttpResponseMessage getHocKyTH(HttpRequestMessage request, string mssv)
        {
            return CreateHttpResponse(request, () =>
            {


                var query = _nhomService.getHocKyTH(mssv);

                HttpResponseMessage response = request.CreateResponse(HttpStatusCode.OK, query);




                return response;
            });
        }
        [Route("getNgayBatDauVaKetThuc")]

        public HttpResponseMessage getNgayBatDauVaKetThuc(HttpRequestMessage request, string mssv)
        {
            return CreateHttpResponse(request, () =>
            {


                var query = _nhomService.getNgayBatDauVaKetThuc(mssv);

                HttpResponseMessage response = request.CreateResponse(HttpStatusCode.OK, query);




                return response;
            });
        }
        [Route("getall")]
        public HttpResponseMessage Get(HttpRequestMessage request)
        {
            return CreateHttpResponse(request, () =>
            {


                var listCategory = _nhomService.GetAll();
                var listHocPhiVm = Mapper.Map<List<NhomViewModel>>(listCategory);

                HttpResponseMessage response = request.CreateResponse(HttpStatusCode.OK, listHocPhiVm);


                return response;
            });
        }
    }


}