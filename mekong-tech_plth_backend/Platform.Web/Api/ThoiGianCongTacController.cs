﻿using AutoMapper;
using Platform.Model;
using Platform.Service;
using Platform.Web.infratructure.core;
using Platform.Web.Models;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Platform.Web.infratructure.extensions;
using System.Data.SqlClient;
using System;

using System.Configuration;
using System.Web.Configuration;

namespace Platform.Web.Api
{
    [RoutePrefix("api/ThoiGianCongTac")]
    [Authorize]
    // [RoutePrefix("{Type:regex(api/sinhviendauvao|api/ThoiGianCongTac)}")]

    public class ThoiGianCongTacController : ApiControllerBase
    {
        IThoiGianCongTacService _thoiGianCongTacService;

        public ThoiGianCongTacController(ILoiService loiService, IThoiGianCongTacService thoiGianCongTacService) : base(loiService)
        {
            this._thoiGianCongTacService = thoiGianCongTacService;
        }

        [Route("createExcel")]
        [HttpPost]
        // [AllowAnonymous]
        public HttpResponseMessage Create(HttpRequestMessage request, IEnumerable<ThoiGianCongTacViewModel> thoiGianCongTacVm)
        {
           

            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                if (!ModelState.IsValid)
                {
                    response = request.CreateResponse(HttpStatusCode.BadRequest, ModelState);
                }
                else
                {
                    foreach (var item in thoiGianCongTacVm)
                    {
                        var newThoiGianCongTac = new ThoiGianCongTac();
                        newThoiGianCongTac.UpdateThoiGianCongTac(item);

                        _thoiGianCongTacService.Add(newThoiGianCongTac);
                        _thoiGianCongTacService.Save();
                    }


                    //var responseData = Mapper.Map<DangKy_TamThoi, DangKy_TamThoiViewModel>(newDangKy_TamThoi);
                    response = request.CreateResponse(HttpStatusCode.OK);
                }

                return response;
            });
        }



        [Route("getThoiGianCongTac")]
        [HttpGet]

        public HttpResponseMessage getThoiGianCongTac(HttpRequestMessage request, string msvc)
        {
            return CreateHttpResponse(request, () =>
            {
                var model = _thoiGianCongTacService.getThoiGianCongTac(msvc);
                var responseData = Mapper.Map<IEnumerable< ThoiGianCongTac>, IEnumerable< ThoiGianCongTac>>(model);
                var response = request.CreateResponse(HttpStatusCode.OK, responseData);

                return response;
            });
        }

        [Route("getbyid")]
        [HttpGet]
        public HttpResponseMessage GetById(HttpRequestMessage request, int ID)
        {
            return CreateHttpResponse(request, () =>
            {
                var model = _thoiGianCongTacService.getid(ID);

                var responseData = Mapper.Map<ThoiGianCongTac, ThoiGianCongTacViewModel>(model);

                var response = request.CreateResponse(HttpStatusCode.OK, responseData);

                return response;
            });
        }


        [Route("update")]
        [HttpPut]
        [AllowAnonymous]
        public HttpResponseMessage Put(HttpRequestMessage request, ThoiGianCongTacViewModel thoiGianCongTacVM)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                if (!ModelState.IsValid)
                {
                    request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
                }
                else
                {

                    var vienchucDb = _thoiGianCongTacService.getid(Convert.ToInt32( thoiGianCongTacVM.ID));
                   
                    vienchucDb.UpdateThoiGianCongTac(thoiGianCongTacVM);



                    _thoiGianCongTacService.Update(vienchucDb);
                    _thoiGianCongTacService.Commit();

                    response = request.CreateResponse(HttpStatusCode.OK);

                }
                return response;
            });
        }




        [Route("create")]
        [HttpPost]
        // [AllowAnonymous]
        public HttpResponseMessage Creates(HttpRequestMessage request, IEnumerable<ThoiGianCongTacViewModel> thoiGianCongTacVM)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                if (!ModelState.IsValid)
                {
                    response = request.CreateResponse(HttpStatusCode.BadRequest, ModelState);
                }
                else
                {
                    var b = "";
                    foreach (var item in thoiGianCongTacVM)
                    {
                        if (item.MaSoVC != null)
                        {
                            b = item.MaSoVC;
                        }
                        var newThoiGianCongTac = new ThoiGianCongTac();
                        newThoiGianCongTac.UpdateThoiGianCongTac(item);
                        newThoiGianCongTac.MaSoVC = b;

                        _thoiGianCongTacService.Add(newThoiGianCongTac);
                        _thoiGianCongTacService.Save();
                    }



                    response = request.CreateResponse(HttpStatusCode.OK);
                }

                return response;
            });
        }

        [Route("getall")]
        [HttpGet]
        public HttpResponseMessage Get(HttpRequestMessage request)
        {
            return CreateHttpResponse(request, () =>
            {


                var listCategory = _thoiGianCongTacService.GetAll();
                var listThoiGianCongTacVm = Mapper.Map<List<ThoiGianCongTacViewModel>>(listCategory);

                HttpResponseMessage response = request.CreateResponse(HttpStatusCode.OK, listThoiGianCongTacVm);
               
                
                Console.WriteLine(1);

                return response;
            });
        }


        [Route("getID")]
        public HttpResponseMessage GetByIds(HttpRequestMessage request,int ID)
        {
            return CreateHttpResponse(request, () =>
            {


                var sinhvien = _thoiGianCongTacService.GetByID(ID);
                var ThoiGianCongTacVm = Mapper.Map<ThoiGianCongTacViewModel>(sinhvien);

                HttpResponseMessage response = request.CreateResponse(HttpStatusCode.OK, ThoiGianCongTacVm);


                return response;
            });
        }
       

    public HttpResponseMessage Post(HttpRequestMessage request, ThoiGianCongTacViewModel thoiGianCongTacVm)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                if (ModelState.IsValid)
                {
                    request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
                }
                else
                {
                    ThoiGianCongTac newThoiGianCongTac = new ThoiGianCongTac();
                    newThoiGianCongTac.UpdateThoiGianCongTac(thoiGianCongTacVm);
                    _thoiGianCongTacService.Add(newThoiGianCongTac);
                    _thoiGianCongTacService.Commit();

                    response = request.CreateResponse(HttpStatusCode.Created);

                }
                return response;
            });
        }

        

       



       

















    }
}