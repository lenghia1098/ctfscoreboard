﻿using AutoMapper;
using Platform.Model;
using Platform.Service;
using Platform.Web.infratructure.core;
using Platform.Web.Models;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Platform.Web.infratructure.extensions;
using System.Data.Entity.Core.Metadata.Edm;
using System.Linq;

namespace Platform.Web.Api
{
    [RoutePrefix("api/dangky_tamthoi")]
    [Authorize]
    public class DangKy_TamThoiController : ApiControllerBase
    {
        IDangKy_TamThoiService _dangKy_TamThoiService;

        public DangKy_TamThoiController(ILoiService loiService, IDangKy_TamThoiService dangKy_TamThoiService) : base(loiService)
        {
            this._dangKy_TamThoiService = dangKy_TamThoiService;
        }
        [Route("createExcel")]
        [HttpPost]
        // [AllowAnonymous]
        public HttpResponseMessage CreateExcel(HttpRequestMessage request, IEnumerable<DangKy_TamThoiViewModel> dangKy_TamThoiVm)
        {


            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                if (!ModelState.IsValid)
                {
                    response = request.CreateResponse(HttpStatusCode.BadRequest, ModelState);
                }
                else
                {
                    foreach (var item in dangKy_TamThoiVm)
                    {
                        var newDangKy_TamThoi = new DangKy_TamThoi();
                        newDangKy_TamThoi.UpdateDangKy_TamThoi(item);

                        _dangKy_TamThoiService.Add(newDangKy_TamThoi);
                        _dangKy_TamThoiService.Save();
                    }


                    //var responseData = Mapper.Map<DangKy_TamThoi, DangKy_TamThoiViewModel>(newDangKy_TamThoi);
                    response = request.CreateResponse(HttpStatusCode.OK);
                }

                return response;
            });
        }
        [Route("create")]
        [HttpPost]
       // [AllowAnonymous]
        public HttpResponseMessage Create(HttpRequestMessage request, IEnumerable<DangKy_TamThoiViewModel> dangKy_TamThoiVm)
            {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                if (!ModelState.IsValid)
                {
                    response = request.CreateResponse(HttpStatusCode.BadRequest, ModelState);
                }
                else
                {
                    foreach(var item in dangKy_TamThoiVm)
                    {
                        var newDangKy_TamThoi = new DangKy_TamThoi();
                        newDangKy_TamThoi.UpdateDangKy_TamThoi(item);

                        _dangKy_TamThoiService.Add(newDangKy_TamThoi);
                        _dangKy_TamThoiService.Save();
                    }
                   

                    //var responseData = Mapper.Map<DangKy_TamThoi, DangKy_TamThoiViewModel>(newDangKy_TamThoi);
                    response = request.CreateResponse(HttpStatusCode.OK);
                }

                return response;
            });
        }
       
        [Route("getall")]
        public HttpResponseMessage Get(HttpRequestMessage request)
        {
            return CreateHttpResponse(request, () =>
            {


                var listCategory = _dangKy_TamThoiService.GetAll();
                  var listDangKy_TamThoiVm = Mapper.Map<List<DangKy_TamThoiViewModel>>(listCategory);

                 HttpResponseMessage response = request.CreateResponse(HttpStatusCode.OK, listDangKy_TamThoiVm);
              //  HttpResponseMessage response = request.CreateResponse(HttpStatusCode.OK, listCategory);

                return response;
            });
        }

        [Route("getDangKyTanThoi_TinhHocPhi")]
        public HttpResponseMessage getDangKyTanThoi_TinhHocPhi(HttpRequestMessage request)
        {
            return CreateHttpResponse(request, () =>
            {

               

                 var listCategory = _dangKy_TamThoiService.getDangKyTanThoi_TinhHocPhi();

               
                  var listDangKy_TamThoiVm = Mapper.Map<List<chucnangtinhhocphi>>(listCategory);
              

                 HttpResponseMessage response = request.CreateResponse(HttpStatusCode.OK, listDangKy_TamThoiVm);
             

                return response;
            });
        }
        [Route("getDangKyTanThoi_TinhHocPhiGruop")]
        public HttpResponseMessage getDangKyTanThoi_TinhHocPhiGruop(HttpRequestMessage request)
        {
            return CreateHttpResponse(request, () =>
            {


               
                var listCategory = _dangKy_TamThoiService.getDangKyTanThoi_TinhHocPhi();

               
                
                var group = listCategory.GroupBy(g => g.MaSoSV);
                //var listDangKy_TamThoiVm = Mapper.Map<List<DanhSachMonHocTheoMaLopNghanhKhoa>>(group);
                HttpResponseMessage response = request.CreateResponse(HttpStatusCode.OK, group);
             

                return response;
            });
        }

     
        [Route("getID")]
        public HttpResponseMessage GetById(HttpRequestMessage request)
        {
            return CreateHttpResponse(request, () =>
            {


                var sinhvien = _dangKy_TamThoiService.GetByID(1);
                var DangKy_TamThoiVm = Mapper.Map<DangKy_TamThoiViewModel>(sinhvien);

                HttpResponseMessage response = request.CreateResponse(HttpStatusCode.OK, DangKy_TamThoiVm);


                return response;
            });
        }
        [Route("getDangKy_TamThoi")]
        public HttpResponseMessage getDangKy_TamThoi(HttpRequestMessage request,string mssv)
        {
            return CreateHttpResponse(request, () =>
            {


                var sinhvien = _dangKy_TamThoiService.getDangKy_TamThoi(mssv);
               

                HttpResponseMessage response = request.CreateResponse(HttpStatusCode.OK, sinhvien);


                return response;
            });
        }
        [Route("getDKMHTH")]
        public HttpResponseMessage getDKMHTH(HttpRequestMessage request,string mssv,string hocky, string namHoc)
        {
            return CreateHttpResponse(request, () =>
            {


                var sinhvien = _dangKy_TamThoiService.getDKMHTH(mssv,hocky,namHoc);
               

                HttpResponseMessage response = request.CreateResponse(HttpStatusCode.OK, sinhvien);


                return response;
            });
        }
        
        public HttpResponseMessage Post(HttpRequestMessage request, DangKy_TamThoiViewModel dangKy_TamThoiVm)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                if (ModelState.IsValid)
                {
                    request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
                }
                else
                {
                    DangKy_TamThoi newDangKy_TamThoi = new DangKy_TamThoi();
                    newDangKy_TamThoi.UpdateDangKy_TamThoi(dangKy_TamThoiVm);
                    _dangKy_TamThoiService.Add(newDangKy_TamThoi);
                    _dangKy_TamThoiService.Commit();

                    response = request.CreateResponse(HttpStatusCode.Created);

                }
                return response;
            });
        }

        public HttpResponseMessage Put(HttpRequestMessage request, DangKy_TamThoiViewModel dangKy_TamThoiVm)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                if (ModelState.IsValid)
                {
                    request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
                }
                else
                {
                    var dangKy_TamThoiDb = _dangKy_TamThoiService.GetByID(int.Parse(dangKy_TamThoiVm.MaSoSV));
                    dangKy_TamThoiDb.UpdateDangKy_TamThoi(dangKy_TamThoiVm);
                    _dangKy_TamThoiService.Update(dangKy_TamThoiDb);
                    _dangKy_TamThoiService.Commit();

                    response = request.CreateResponse(HttpStatusCode.OK);

                }
                return response;
            });
        }

        //public HttpResponseMessage Delete(HttpRequestMessage request, int id)
        //{
        //    return CreateHttpResponse(request, () =>
        //    {
        //        HttpResponseMessage response = null;
        //        if (ModelState.IsValid)
        //        {
        //            request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
        //        }
        //        else
        //        {
        //            _dangKy_TamThoiService.delete(id);
        //            _dangKy_TamThoiService.Commit();

        //            response = request.CreateResponse(HttpStatusCode.OK);

        //        }
        //        return response;
        //    });
        //}

        [Route("delete")]
        [HttpDelete]
        [HttpGet]
        // [AllowAnonymous]
        public HttpResponseMessage Delete(HttpRequestMessage request, string ID)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                if (!ModelState.IsValid)
                {
                    response = request.CreateResponse(HttpStatusCode.BadRequest, ModelState);
                }
                else
                {
                    var oldProductCategory = _dangKy_TamThoiService.Delete(int.Parse(ID));
                    _dangKy_TamThoiService.Save();

                    var responseData = Mapper.Map<DangKy_TamThoi, DangKy_TamThoiViewModel>(oldProductCategory);
                    response = request.CreateResponse(HttpStatusCode.Created, responseData);
                }

                return response;
            });
        }

    }
}