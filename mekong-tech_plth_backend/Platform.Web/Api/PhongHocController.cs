﻿using AutoMapper;
using Platform.Model;
using Platform.Service;
using Platform.Web.infratructure.core;
using Platform.Web.infratructure.extensions;
using Platform.Web.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;

namespace Platform.Web.Api
{
    [RoutePrefix("api/PhongHoc")]
    [Authorize]
    public class PhongHocController : ApiControllerBase
    {
        IPhongHocService _phongHocService;
        IDangKyService _dangKyService;
        public PhongHocController(ILoiService loiService, IPhongHocService phongHocService, IDangKyService dangKyService) : base(loiService)
        {
            this._phongHocService = phongHocService;
            this._dangKyService = dangKyService;
        }
        [Route("createExcel")]
        [HttpPost]
        public HttpResponseMessage Create(HttpRequestMessage request, IEnumerable<PhongHocViewModel>phongHocVm)
        {


            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                if (!ModelState.IsValid)
                {
                    response = request.CreateResponse(HttpStatusCode.BadRequest, ModelState);
                }
                else
                {
                    foreach (var item in phongHocVm)
                    {
                        var newPhongHoc = new PhongHoc();
                        newPhongHoc.UpdatePhongHoc(item);

                        _phongHocService.Add(newPhongHoc);
                        _phongHocService.Save();
                    }


                    //var responseData = Mapper.Map<DangKy_TamThoi, DangKy_TamThoiViewModel>(newDangKy_TamThoi);
                    response = request.CreateResponse(HttpStatusCode.OK);
                }

                return response;
            });
        }
       
        [Route("getall")]
        public HttpResponseMessage Get(HttpRequestMessage request)
        {
            return CreateHttpResponse(request, () =>
            {


                var listCategory = _phongHocService.GetAll();
                var listHocPhiVm = Mapper.Map<List<PhongHocViewModel>>(listCategory);

                HttpResponseMessage response = request.CreateResponse(HttpStatusCode.OK, listHocPhiVm);


                return response;
            });
        }


        [Route("create")]
        [HttpPost]
        // [AllowAnonymous]
        public HttpResponseMessage Create(HttpRequestMessage request, PhongHocViewModel phongHocVM)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                if (!ModelState.IsValid)
                {
                    response = request.CreateResponse(HttpStatusCode.BadRequest, ModelState);
                }
                else
                {

                    var newPhongHoc = new PhongHoc();
                    newPhongHoc.UpdatePhongHoc(phongHocVM);

                    _phongHocService.Add(newPhongHoc);
                    _phongHocService.Save();



                  
                    response = request.CreateResponse(HttpStatusCode.OK);
                }

                return response;
            });
        }

        [Route("delete")]
        [HttpDelete]
        [HttpGet]
        // [AllowAnonymous]
        public HttpResponseMessage delete(HttpRequestMessage request, string ID)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                if (!ModelState.IsValid)
                {
                    response = request.CreateResponse(HttpStatusCode.BadRequest, ModelState);
                }
                else
                {
                    var deletephonghoc = _phongHocService.delete(ID);
                    _phongHocService.Save();

                    var responseData = Mapper.Map<PhongHoc, PhongHocViewModel>(deletephonghoc);
                    response = request.CreateResponse(HttpStatusCode.Created, responseData);
                }

                return response;
            });
        }
    }


}