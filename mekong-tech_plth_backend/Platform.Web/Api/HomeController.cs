﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Platform.Service;

using Platform.Web.infratructure.core;

namespace Platform.Web.Api
{
    [RoutePrefix("api/home")]
    [Authorize]
    public class HomeController : ApiControllerBase
    {
        ILoiService _loiService;
        public HomeController(ILoiService loiService) : base(loiService)
        {
            this._loiService = loiService;
        }

        [HttpGet]
        [Route("TestMethod")]
        public string TestMethod()
        {
            return "Hello, Mekong-Tech Member. ";
        }
    }
}
