﻿using AutoMapper;
using Platform.Model;
using Platform.Service;
using Platform.Web.infratructure.core;
using Platform.Web.Models;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Platform.Web.infratructure.extensions;
using System.Data.SqlClient;
using System;

using System.Configuration;
using System.Web.Configuration;

namespace Platform.Web.Api
{
    [RoutePrefix("api/sinhviendauvao")]
    [Authorize]
    // [RoutePrefix("{Type:regex(api/sinhviendauvao|api/SinhVienDauVao)}")]

    public class SinhVienDauVaoController : ApiControllerBase
    {
        ISinhVienDauVaoService _sinhVienDauVaoService;

        public SinhVienDauVaoController(ILoiService loiService, ISinhVienDauVaoService sinhVienDauVaoService) : base(loiService)
        {
            this._sinhVienDauVaoService = sinhVienDauVaoService;
        }

        [Route("createExcel")]
        [HttpPost]
        // [AllowAnonymous]
        public HttpResponseMessage Create(HttpRequestMessage request, IEnumerable<SinhVienDauVaoViewModel> sinhVienDauVaoVm)
        {
           

            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                if (!ModelState.IsValid)
                {
                    response = request.CreateResponse(HttpStatusCode.BadRequest, ModelState);
                }
                else
                {
                    foreach (var item in sinhVienDauVaoVm)
                    {
                        var newSinhVienDauVao = new SinhVienDauVao();
                        newSinhVienDauVao.UpdateSinhVienDauVao(item);

                        _sinhVienDauVaoService.Add(newSinhVienDauVao);
                        _sinhVienDauVaoService.Save();
                    }


                    //var responseData = Mapper.Map<DangKy_TamThoi, DangKy_TamThoiViewModel>(newDangKy_TamThoi);
                    response = request.CreateResponse(HttpStatusCode.OK);
                }

                return response;
            });
        }




       




        [Route("create")]
        [HttpPost]
        // [AllowAnonymous]
        public HttpResponseMessage CreateExcel(HttpRequestMessage request, SinhVienDauVaoViewModel sinhVienDauVaoVm)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                if (!ModelState.IsValid)
                {
                    response = request.CreateResponse(HttpStatusCode.BadRequest, ModelState);
                }
                else
                {
                  
                        var newSinhVienDauVao = new SinhVienDauVao();
                        newSinhVienDauVao.UpdateSinhVienDauVao(sinhVienDauVaoVm);

                        _sinhVienDauVaoService.Add(newSinhVienDauVao);
                        _sinhVienDauVaoService.Save();
                  


                    //var responseData = Mapper.Map<DangKy_TamThoi, DangKy_TamThoiViewModel>(newDangKy_TamThoi);
                    response = request.CreateResponse(HttpStatusCode.OK);
                }

                return response;
            });
        }


        [Route("getall")]
        [HttpGet]
        public HttpResponseMessage Get(HttpRequestMessage request)
        {
            return CreateHttpResponse(request, () =>
            {


                var listCategory = _sinhVienDauVaoService.GetAll();
                var listSinhVienDauVaoVm = Mapper.Map<List<SinhVienDauVaoViewModel>>(listCategory);

                HttpResponseMessage response = request.CreateResponse(HttpStatusCode.OK, listSinhVienDauVaoVm);
               
             
                return response;
            });
        }


        [Route("getID")]
        public HttpResponseMessage GetById(HttpRequestMessage request,int ID)
        {
            return CreateHttpResponse(request, () =>
            {


                var sinhvien = _sinhVienDauVaoService.GetByID(ID);
                var SinhVienDauVaoVm = Mapper.Map<SinhVienDauVaoViewModel>(sinhvien);

                HttpResponseMessage response = request.CreateResponse(HttpStatusCode.OK, SinhVienDauVaoVm);


                return response;
            });
        }
       

    public HttpResponseMessage Post(HttpRequestMessage request, SinhVienDauVaoViewModel sinhVienDauVaoVm)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                if (ModelState.IsValid)
                {
                    request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
                }
                else
                {
                    SinhVienDauVao newSinhVienDauVao = new SinhVienDauVao();
                    newSinhVienDauVao.UpdateSinhVienDauVao(sinhVienDauVaoVm);
                    _sinhVienDauVaoService.Add(newSinhVienDauVao);
                    _sinhVienDauVaoService.Commit();

                    response = request.CreateResponse(HttpStatusCode.Created);

                }
                return response;
            });
        }

        [Route("GetByNamTrungTuyen")]
        public HttpResponseMessage GetByNamHoc(HttpRequestMessage request, string namtrungtuyen)
        {
            return CreateHttpResponse(request, () =>
            {


                var query = _sinhVienDauVaoService.GetByNamTrungTuyen(namtrungtuyen);
                var SinhVienDauVaoVm = Mapper.Map<IEnumerable<SinhVienDauVao>, IEnumerable<SinhVienDauVaoViewModel>>(query);


                HttpResponseMessage response = request.CreateResponse(HttpStatusCode.OK, SinhVienDauVaoVm);

               

                return response;
            });
        }

        [Route("GetByNamTrungTuyenVaSoBaoDanh")]
        public HttpResponseMessage GetByNamTrungTuyenVaSoBaoDanh(HttpRequestMessage request, string namtrungtuyen, string sobaodanh)
        {
            return CreateHttpResponse(request, () =>
            {


                var query = _sinhVienDauVaoService.GetByNamTrungTuyenVaSoBaoDanh(namtrungtuyen, sobaodanh);
                var SinhVienDauVaoVm = Mapper.Map<IEnumerable<SinhVienDauVao>, IEnumerable<SinhVienDauVaoViewModel>>(query);


                HttpResponseMessage response = request.CreateResponse(HttpStatusCode.OK, SinhVienDauVaoVm);


                return response;
            });
        }

        [Route("GetListDatabase")]
        public HttpResponseMessage GetListDatabase(HttpRequestMessage request)
        {
            return CreateHttpResponse(request, () =>
            {


                var listCategory = _sinhVienDauVaoService.GetListDatabase();
               // var listSinhVienDauVaoVm = Mapper.Map<List<SinhVienDauVaoViewModel>>(listCategory);

                HttpResponseMessage response = request.CreateResponse(HttpStatusCode.OK, listCategory);

                //_sinhVienDauVaoService.GetListDatabase();
                Console.WriteLine(1);

                return response;
            });
        }

        [Route("GetAllTable")]
        public HttpResponseMessage GetAllTable(HttpRequestMessage request)
        {
            return CreateHttpResponse(request, () =>
            {


                var listCategory = _sinhVienDauVaoService.GetAllTable();
                // var listSinhVienDauVaoVm = Mapper.Map<List<SinhVienDauVaoViewModel>>(listCategory);

                HttpResponseMessage response = request.CreateResponse(HttpStatusCode.OK, listCategory);

                //_sinhVienDauVaoService.GetListDatabase();
                Console.WriteLine(1);

                return response;
            });
        }




        [Route("createExcelMultiDB")]
        [HttpPost]
        // [AllowAnonymous]
        public HttpResponseMessage CreateMultiDB(HttpRequestMessage request, IEnumerable<SinhVienDauVaoViewModel> sinhVienDauVaoVm)
        {

            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                if (!ModelState.IsValid)
                {
                    response = request.CreateResponse(HttpStatusCode.BadRequest, ModelState);
                }
                else
                {
                    foreach (var item in sinhVienDauVaoVm)
                    {
                        var newSinhVienDauVao = new SinhVienDauVao();
                        newSinhVienDauVao.UpdateSinhVienDauVao(item);

                        _sinhVienDauVaoService.Add(newSinhVienDauVao);
                        _sinhVienDauVaoService.Save();
                    }
                    response = request.CreateResponse(HttpStatusCode.OK);
                }

                return response;
            });
        }






        [Route("getallsvdv")]
        [HttpGet]
        public HttpResponseMessage getallsvdv(HttpRequestMessage request)
        {
            return CreateHttpResponse(request, () =>
            {


                var listCategory = _sinhVienDauVaoService.getallsv();
                var listSinhVienDauVaoVm = Mapper.Map<List<SinhVienDauVao_MSSVViewModel>>(listCategory);

                HttpResponseMessage response = request.CreateResponse(HttpStatusCode.OK, listSinhVienDauVaoVm);

              
                return response;
            });
        }















    }
}