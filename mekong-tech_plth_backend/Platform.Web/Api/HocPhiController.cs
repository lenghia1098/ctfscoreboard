﻿using AutoMapper;
using Platform.Model;
using Platform.Service;
using Platform.Web.infratructure.core;
using Platform.Web.infratructure.extensions;
using Platform.Web.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace Platform.Web.Api
{
    [RoutePrefix("api/hocphi")]
    [Authorize]
    public class HocPhiController : ApiControllerBase
    {
        IHocPhiService _hocPhiService;
        public HocPhiController(ILoiService loiService, IHocPhiService hocPhiService) : base(loiService)
        {
            this._hocPhiService = hocPhiService;
        }
        [Route("createExcel")]
        [HttpPost]
        public HttpResponseMessage Create(HttpRequestMessage request, IEnumerable<HocPhiViewModel> hocPhiVm)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                if (!ModelState.IsValid)
                {
                    response = request.CreateResponse(HttpStatusCode.BadRequest, ModelState);
                }
                else
                {
                    foreach (var item in hocPhiVm)
                    {
                        var newHocPhi = new HocPhi();
                        newHocPhi.UpdateHocPhi(item);

                        _hocPhiService.Add(newHocPhi);
                        _hocPhiService.Save();
                    }


                    //var responseData = Mapper.Map<DangKy_TamThoi, DangKy_TamThoiViewModel>(newDangKy_TamThoi);
                    response = request.CreateResponse(HttpStatusCode.OK);
                }

                return response;
            });
        }
        [Route("getbyid")]
        [HttpGet]
        public HttpResponseMessage GetAllById(HttpRequestMessage request, string mssv, string hocKy, string namHoc)
        {
            return CreateHttpResponse(request, () =>
            {
                var model = _hocPhiService.GetAllById(mssv, hocKy,namHoc);

             

                var response = request.CreateResponse(HttpStatusCode.OK, model);

                return response;
            });
        }
        [Route("GetHocKy")]
        [HttpGet]
        public HttpResponseMessage GetHocKy(HttpRequestMessage request, string mssv)
        {
            return CreateHttpResponse(request, () =>
            {
                var model = _hocPhiService.GetHocKy(mssv);

              

                var response = request.CreateResponse(HttpStatusCode.OK, model);

                return response;
            });
        }


        [Route("getall")]
        public HttpResponseMessage Get(HttpRequestMessage request)
        {
            return CreateHttpResponse(request, () =>
            {


                var listCategory = _hocPhiService.GetAll();
                var listHocPhiVm = Mapper.Map<List<HocPhiViewModel>>(listCategory);

                HttpResponseMessage response = request.CreateResponse(HttpStatusCode.OK, listHocPhiVm);


                return response;
            });
        }

        [Route("create")]
        [HttpPost]
        // [AllowAnonymous]
        public HttpResponseMessage Creates(HttpRequestMessage request, IEnumerable<HocPhiViewModel> hocPhiVM)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                if (!ModelState.IsValid)
                {
                    response = request.CreateResponse(HttpStatusCode.BadRequest, ModelState);
                }
                else
                {
                   
                    foreach (var item in hocPhiVM)
                    {
                       
                        var newHocphi= new HocPhi();
                       
                        newHocphi.UpdateHocPhi(item);
                      

                        _hocPhiService.Add(newHocphi);
                        _hocPhiService.Save();
                    }
                   



                    response = request.CreateResponse(HttpStatusCode.OK);
                }

                return response;
            });
        }
    }
}
