﻿using AutoMapper;
using Platform.Model;
using Platform.Service;
using Platform.Web.infratructure.core;
using Platform.Web.infratructure.extensions;
using Platform.Web.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;

namespace Platform.Web.Api
{
    [RoutePrefix("api/Nhom_TamThoi")]
    [Authorize]
    public class Nhom_TamThoiController : ApiControllerBase
    {
        INhom_TamThoiService _nhom_TamThoiService;
        IDangKyService _dangKyService;
        public Nhom_TamThoiController(ILoiService loiService, INhom_TamThoiService nhom_TamThoiService, IDangKyService dangKyService) : base(loiService)
        {
            this._nhom_TamThoiService = nhom_TamThoiService;
            this._dangKyService = dangKyService;
        }
        [Route("createExcel")]
        [HttpPost]
        public HttpResponseMessage Create(HttpRequestMessage request, IEnumerable<Nhom_TamThoiViewModel>nhom_TamThoiVm)
        {


            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                if (!ModelState.IsValid)
                {
                    response = request.CreateResponse(HttpStatusCode.BadRequest, ModelState);
                }
                else
                {
                    foreach (var item in nhom_TamThoiVm)
                    {
                        var newNhom_TamThoi = new Nhom_TamThoi();
                        newNhom_TamThoi.UpdateNhom_TamThoi(item);

                        _nhom_TamThoiService.Add(newNhom_TamThoi);
                        _nhom_TamThoiService.Save();
                    }


                    //var responseData = Mapper.Map<DangKy_TamThoi, DangKy_TamThoiViewModel>(newDangKy_TamThoi);
                    response = request.CreateResponse(HttpStatusCode.OK);
                }

                return response;
            });
        }
       
        [Route("getall")]
        public HttpResponseMessage Get(HttpRequestMessage request)
        {
            return CreateHttpResponse(request, () =>
            {


                var listCategory = _nhom_TamThoiService.GetAll();
                var listHocPhiVm = Mapper.Map<List<Nhom_TamThoiViewModel>>(listCategory);

                HttpResponseMessage response = request.CreateResponse(HttpStatusCode.OK, listHocPhiVm);


                return response;
            });
        }
    }


}