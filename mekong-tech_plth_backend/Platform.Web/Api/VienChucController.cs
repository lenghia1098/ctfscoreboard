﻿using AutoMapper;
using Platform.Model;
using Platform.Service;
using Platform.Web.infratructure.core;
using Platform.Web.infratructure.extensions;
using Platform.Web.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Runtime.CompilerServices;
using System.Web.Http;

namespace Platform.Web.Api
{
    [RoutePrefix("api/vienchuc")]
    [Authorize]
    public class VienChucController : ApiControllerBase
    {
        IVienChucService _vienChucService;
        public VienChucController(ILoiService loiService, IVienChucService vienChucService) : base(loiService)
        {
            this._vienChucService = vienChucService;
        }
        [Route("createExcel")]
        [HttpPost]
        // [AllowAnonymous]
        public HttpResponseMessage Create(HttpRequestMessage request, IEnumerable<VienChucViewModel> VienChucVm)
        {


            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                if (!ModelState.IsValid)
                {
                    response = request.CreateResponse(HttpStatusCode.BadRequest, ModelState);
                }
                else
                {
                    foreach (var item in VienChucVm)
                    {
                        var newVienChuc = new VienChuc();
                        newVienChuc.UpdateVienChuc(item);

                        _vienChucService.Add(newVienChuc);
                        _vienChucService.Save();
                    }


                    //var responseData = Mapper.Map<DangKy_TamThoi, DangKy_TamThoiViewModel>(newDangKy_TamThoi);
                    response = request.CreateResponse(HttpStatusCode.OK);
                }

                return response;
            });
        }
        [Route("getall")]
        public HttpResponseMessage Get(HttpRequestMessage request)
        {
            return CreateHttpResponse(request, () =>
            {


                var listCategory = _vienChucService.GetAll();
                var listHocPhiVm = Mapper.Map<List<VienChucViewModel>>(listCategory);

                HttpResponseMessage response = request.CreateResponse(HttpStatusCode.OK, listHocPhiVm);


                return response;
            });
        }
        [Route("GetLyLichVienChuc")]
        [HttpGet]

        public HttpResponseMessage GetLyLichVienChuc(HttpRequestMessage request, string msvc)
        {
            return CreateHttpResponse(request, () =>
            {
                var model = _vienChucService.GetLyLichVienChuc(msvc);

                var responseData = Mapper.Map<VienChuc, VienChucViewModel>(model);
                var response = request.CreateResponse(HttpStatusCode.OK, responseData);

                return response;
            });
        }

        [Route("update")]
        [HttpPut]
        [AllowAnonymous]
        public HttpResponseMessage Put(HttpRequestMessage request, VienChucViewModel vienChucVM)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                if (!ModelState.IsValid)
                {
                     request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
                    //var errors = ModelState.Select(x => x.Value.Errors)
                    //        .Where(y => y.Count > 0)
                    //        .ToList();
                }
                else
                {

                    var sinhVienDb = _vienChucService.GetLyLichVienChuc(vienChucVM.MaSoVC);
                    //SinhVien sinhVienDb = new SinhVien();
                    sinhVienDb.UpdateVienChuc(vienChucVM);
                   


                    _vienChucService.Update(sinhVienDb);
                    _vienChucService.Commit();

                    response = request.CreateResponse(HttpStatusCode.OK);

                }
                return response;
            });
        }


       


        [Route("create")]
        [HttpPost]
        // [AllowAnonymous]
        public HttpResponseMessage CreateVienChuc(HttpRequestMessage request, VienChucViewModel phongHocVM)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                if (!ModelState.IsValid)
                {
                    response = request.CreateResponse(HttpStatusCode.BadRequest, ModelState);
                }
                else
                {

                    var newPhongHoc = new VienChuc();
                    newPhongHoc.UpdateVienChuc(phongHocVM);
                   
                    _vienChucService.Add(newPhongHoc);
                    _vienChucService.Save();




                    response = request.CreateResponse(HttpStatusCode.OK);
                }

                return response;
            });
        }



        [Route("GetTTCN")]
        [HttpGet]
      
        public HttpResponseMessage GetTTCN(HttpRequestMessage request, string Id)
        {
            return CreateHttpResponse(request, () =>
            {
                var model = _vienChucService.GetTTCN(Id);

              

                var response = request.CreateResponse(HttpStatusCode.OK, model);

                return response;
            });
        }
    }
}
