﻿using AutoMapper;
using Platform.Model;
using Platform.Service;
using Platform.Web.infratructure.core;
using Platform.Web.infratructure.extensions;
using Platform.Web.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Platform.Web.Infrastructure.Core;

namespace Platform.Web.Api
{
    [RoutePrefix("api/TrinhDoNgoaiNgu")]
    [Authorize]
    public class TrinhDoNgoaiNguController : ApiControllerBase
    {
        ITrinhDoNgoaiNguService _trinhDoNgoaiNguService;

        public TrinhDoNgoaiNguController(ILoiService loiService, ITrinhDoNgoaiNguService trinhDoNgoaiNguService) : base(loiService)
        {
            this._trinhDoNgoaiNguService = trinhDoNgoaiNguService;
        }
        [Route("createExcel")]
        [HttpPost]
        // [AllowAnonymous]
        public HttpResponseMessage Create(HttpRequestMessage request, IEnumerable<TrinhDoNgoaiNguViewModel> trinhDoNgoaiNguVm)
        {


            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                if (!ModelState.IsValid)
                {
                    response = request.CreateResponse(HttpStatusCode.BadRequest, ModelState);
                }
                else
                {
                    foreach (var item in trinhDoNgoaiNguVm)
                    {
                        var newTrinhDoNgoaiNgu = new TrinhDoNgoaiNgu();
                        newTrinhDoNgoaiNgu.UpdateTrinhDoNgoaiNgu(item);

                        _trinhDoNgoaiNguService.Add(newTrinhDoNgoaiNgu);
                        _trinhDoNgoaiNguService.Save();
                    }


                    //var responseData = Mapper.Map<DangKy_TamThoi, DangKy_TamThoiViewModel>(newDangKy_TamThoi);
                    response = request.CreateResponse(HttpStatusCode.OK);
                }

                return response;
            });
        }
        public HttpResponseMessage Create(HttpRequestMessage request, TrinhDoNgoaiNgu trinhDoNgoaiNgu)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                if (ModelState.IsValid)
                {
                    request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
                }
                else
                {
                    _trinhDoNgoaiNguService.Add(trinhDoNgoaiNgu);
                    _trinhDoNgoaiNguService.Commit();
                    response = request.CreateResponse(HttpStatusCode.Created, trinhDoNgoaiNgu);
                }
                return response;
            });

        }
        [Route("getall")]
        public HttpResponseMessage Get(HttpRequestMessage request)
        {
            return CreateHttpResponse(request, () =>
            {


                var listCategory = _trinhDoNgoaiNguService.GetAll();

                HttpResponseMessage response = request.CreateResponse(HttpStatusCode.OK, listCategory);


                return response;
            });
        }




        [Route("GetTrinhDoNgoaiNgu")]
        [HttpGet]

        public HttpResponseMessage GetTrinhDoNgoaiNgu(HttpRequestMessage request, string msvc)
        {
            return CreateHttpResponse(request, () =>
            {
                var model = _trinhDoNgoaiNguService.GetTrinhDoNgoaiNgu(msvc);
                var responseData = Mapper.Map<IEnumerable<TrinhDoNgoaiNgu>, IEnumerable<TrinhDoNgoaiNgu>>(model);
                var response = request.CreateResponse(HttpStatusCode.OK, responseData);

                return response;
            });
        }


        public HttpResponseMessage Post(HttpRequestMessage request, TrinhDoNgoaiNgu trinhDoNgoaiNgu)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                if (ModelState.IsValid)
                {
                    request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
                }
                else
                {
                    _trinhDoNgoaiNguService.Add(trinhDoNgoaiNgu);
                    _trinhDoNgoaiNguService.Commit();

                    response = request.CreateResponse(HttpStatusCode.Created);

                }
                return response;
            });
        }

        public HttpResponseMessage Put(HttpRequestMessage request, TrinhDoNgoaiNgu trinhDoNgoaiNgu)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                if (ModelState.IsValid)
                {
                    request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
                }
                else
                {
                    _trinhDoNgoaiNguService.Update(trinhDoNgoaiNgu);
                    _trinhDoNgoaiNguService.Commit();

                    response = request.CreateResponse(HttpStatusCode.OK);

                }
                return response;
            });
        }

        public HttpResponseMessage Delete(HttpRequestMessage request, int id)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                if (ModelState.IsValid)
                {
                    request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
                }
                else
                {
                    _trinhDoNgoaiNguService.delete(id);
                    _trinhDoNgoaiNguService.Commit();

                    response = request.CreateResponse(HttpStatusCode.OK);

                }
                return response;
            });
        }


        [Route("getbyid")]
        [HttpGet]
        public HttpResponseMessage GetById(HttpRequestMessage request, int ID)
        {
            return CreateHttpResponse(request, () =>
            {
                var model = _trinhDoNgoaiNguService.GetId(ID);

                var responseData = Mapper.Map<TrinhDoNgoaiNgu, TrinhDoNgoaiNguViewModel>(model);

                var response = request.CreateResponse(HttpStatusCode.OK, responseData);

                return response;
            });
        }


        [Route("update")]
        [HttpPut]
        [AllowAnonymous]
        public HttpResponseMessage Put(HttpRequestMessage request, TrinhDoNgoaiNguViewModel trinhDoNgoaiNguVM)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                if (!ModelState.IsValid)
                {
                    request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
                }
                else
                {

                    var vienchucDb = _trinhDoNgoaiNguService.GetId(Convert.ToInt32( trinhDoNgoaiNguVM.ID));
                   
                    vienchucDb.UpdateTrinhDoNgoaiNgu(trinhDoNgoaiNguVM);
                    


                    _trinhDoNgoaiNguService.Update(vienchucDb);
                    _trinhDoNgoaiNguService.Commit();

                    response = request.CreateResponse(HttpStatusCode.OK);

                }
                return response;
            });
        }




        [Route("create")]
        [HttpPost]
        // [AllowAnonymous]
        public HttpResponseMessage Creates(HttpRequestMessage request, IEnumerable<TrinhDoNgoaiNguViewModel> trinhDoNgoaiNguVM)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                if (!ModelState.IsValid)
                {
                    response = request.CreateResponse(HttpStatusCode.BadRequest, ModelState);
                }
                else
                {
                    var b = "";
                    foreach (var item in trinhDoNgoaiNguVM)
                    {
                        if (item.MaSoVC != null)
                        {
                            b = item.MaSoVC;
                        }
                            
                        var newKhungGio = new TrinhDoNgoaiNgu();
                        newKhungGio.UpdateTrinhDoNgoaiNgu(item);
                        newKhungGio.MaSoVC = b;

                        _trinhDoNgoaiNguService.Add(newKhungGio);
                        _trinhDoNgoaiNguService.Save();
                    }



                    response = request.CreateResponse(HttpStatusCode.OK);
                }

                return response;
            });
        }

    }
}
