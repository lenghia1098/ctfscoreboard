﻿using AutoMapper;
using Platform.Model;
using Platform.Service;
using Platform.Web.infratructure.core;
using Platform.Web.Models;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Platform.Web.infratructure.extensions;


namespace Platform.Web.Api
{
    [RoutePrefix("api/SinhVien_Lop")]
    [Authorize]
    public class SinhVien_LopController : ApiControllerBase
    {
        ISinhVien_LopService _sinhVien_LopService;

        public SinhVien_LopController (ILoiService loiService, ISinhVien_LopService sinhVien_LopService) : base(loiService)
        {
            this._sinhVien_LopService = sinhVien_LopService;
        }
        [Route("createExcel")]
        [HttpPost]
        public HttpResponseMessage Create(HttpRequestMessage request, IEnumerable<SinhVien_LopViewModel> sinhVien_LopVm)
        {


            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                if (!ModelState.IsValid)
                {
                    response = request.CreateResponse(HttpStatusCode.BadRequest, ModelState);
                }
                else
                {
                    foreach (var item in sinhVien_LopVm)
                    {
                        var newSinhVien_Lop = new SinhVien_Lop();
                        newSinhVien_Lop.UpdateSinhVien_Lop(item);

                        _sinhVien_LopService.Add(newSinhVien_Lop);
                        _sinhVien_LopService.Save();
                    }


                    //var responseData = Mapper.Map<DangKy_TamThoi, DangKy_TamThoiViewModel>(newDangKy_TamThoi);
                    response = request.CreateResponse(HttpStatusCode.OK);
                }

                return response;
            });
        }
        public HttpResponseMessage Create(HttpRequestMessage request, SinhVien_Lop sinhVien_Lop)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                if (ModelState.IsValid)
                {
                    request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
                }
                else
                {
                    _sinhVien_LopService.Add(sinhVien_Lop);
                    _sinhVien_LopService.Commit();
                    response = request.CreateResponse(HttpStatusCode.Created, sinhVien_Lop);
                }
                return response;
            });
           
        }
        [Route("getall")]
        public HttpResponseMessage Get(HttpRequestMessage request)
        {
            return CreateHttpResponse(request, () =>
            {


                var listCategory = _sinhVien_LopService.GetAll();
                var listHocPhiVm = Mapper.Map<List<SinhVien_LopViewModel>>(listCategory);

                HttpResponseMessage response = request.CreateResponse(HttpStatusCode.OK, listHocPhiVm);


                return response;
            });
        }

      


       

        public HttpResponseMessage Post(HttpRequestMessage request, SinhVien_LopViewModel sinhVien_LopVm)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                if (ModelState.IsValid)
                {
                    request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
                }
                else
                {
                    SinhVien_Lop newSinhVien_Lop = new SinhVien_Lop();
                    newSinhVien_Lop.UpdateSinhVien_Lop(sinhVien_LopVm);
                    _sinhVien_LopService.Add(newSinhVien_Lop);
                    _sinhVien_LopService.Commit();

                    response = request.CreateResponse(HttpStatusCode.Created);

                }
                return response;
            });
        }
       

        public HttpResponseMessage Delete(HttpRequestMessage request, int id)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                if (ModelState.IsValid)
                {
                    request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
                }
                else
                {
                    _sinhVien_LopService.delete(id);
                    _sinhVien_LopService.Commit();

                    response = request.CreateResponse(HttpStatusCode.OK);

                }
                return response;
            });
        }

    }
}