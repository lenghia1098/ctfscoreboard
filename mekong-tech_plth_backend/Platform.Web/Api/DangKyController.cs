﻿using AutoMapper;
using Platform.Model;
using Platform.Service;
using Platform.Web.infratructure.core;
using Platform.Web.Models;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Platform.Web.infratructure.extensions;


namespace Platform.Web.Api
{
    [RoutePrefix("api/dangky")]
    [Authorize]
    public class DangKyController : ApiControllerBase
    {
        IDangKyService _dangKyService;

        public DangKyController(ILoiService loiService, IDangKyService dangKyService) : base(loiService)
        {
            this._dangKyService = dangKyService;
        }

        [Route("createExcel")]
        [HttpPost]
        // [AllowAnonymous]
        public HttpResponseMessage Create(HttpRequestMessage request, IEnumerable<DangKyViewModel> dangKyVm)
        {


            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                if (!ModelState.IsValid)
                {
                    response = request.CreateResponse(HttpStatusCode.BadRequest, ModelState);
                }
                else
                {
                    foreach (var item in dangKyVm)
                    {
                        var newDangKy = new DangKy();
                        newDangKy.UpdateDangKy(item);

                        _dangKyService.Add(newDangKy);
                        _dangKyService.Save();
                    }


                    //var responseData = Mapper.Map<DangKy_TamThoi, DangKy_TamThoiViewModel>(newDangKy_TamThoi);
                    response = request.CreateResponse(HttpStatusCode.OK);
                }

                return response;
            });
        }
        [Route("getall")]
        public HttpResponseMessage Get(HttpRequestMessage request)
        {
            return CreateHttpResponse(request, () =>
            {


                var listCategory = _dangKyService.GetAll();
                var listDangKyVm = Mapper.Map<List<DangKyViewModel>>(listCategory);

                HttpResponseMessage response = request.CreateResponse(HttpStatusCode.OK, listDangKyVm);
                //  HttpResponseMessage response = request.CreateResponse(HttpStatusCode.OK, listCategory);

                return response;
            });
        }


        [Route("getID")]
        public HttpResponseMessage GetById(HttpRequestMessage request)
        {
            return CreateHttpResponse(request, () =>
            {


                var sinhvien = _dangKyService.GetByID(1);
                var DangKyVm = Mapper.Map<DangKyViewModel>(sinhvien);

                HttpResponseMessage response = request.CreateResponse(HttpStatusCode.OK, DangKyVm);


                return response;
            });
        }
        [Route("getDKMH")]
        [HttpGet]
        public HttpResponseMessage getDKMH(HttpRequestMessage request, string mssv, string hocKy, string namHoc)
        {
            return CreateHttpResponse(request, () =>
            {


                var sinhvien = _dangKyService.getDKMH(mssv, hocKy, namHoc);


                HttpResponseMessage response = request.CreateResponse(HttpStatusCode.OK, sinhvien);


                return response;
            });
        }
        [Route("getMH")]
        [HttpGet]
        public HttpResponseMessage xemThongTinMH(HttpRequestMessage request, string MaMH, string MaNhom,string MaLop)
        {
            return CreateHttpResponse(request, () =>
            {


                var sinhvien = _dangKyService.xemThongTinMH(MaMH, MaNhom, MaLop);


                HttpResponseMessage response = request.CreateResponse(HttpStatusCode.OK, sinhvien);


                return response;
            });
        }

        public HttpResponseMessage Post(HttpRequestMessage request, DangKyViewModel dangKyVm)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                if (ModelState.IsValid)
                {
                    request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
                }
                else
                {
                    DangKy newDangKy = new DangKy();
                    newDangKy.UpdateDangKy(dangKyVm);
                    _dangKyService.Add(newDangKy);
                    _dangKyService.Commit();

                    response = request.CreateResponse(HttpStatusCode.Created);

                }
                return response;
            });
        }

        public HttpResponseMessage Put(HttpRequestMessage request, DangKyViewModel dangKyVm)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                if (ModelState.IsValid)
                {
                    request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
                }
                else
                {
                    var dangKyDb = _dangKyService.GetByID(int.Parse(dangKyVm.MaSoSV));
                    dangKyDb.UpdateDangKy(dangKyVm);
                    _dangKyService.Update(dangKyDb);
                    _dangKyService.Commit();

                    response = request.CreateResponse(HttpStatusCode.OK);

                }
                return response;
            });
        }

        public HttpResponseMessage Delete(HttpRequestMessage request, int id)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                if (ModelState.IsValid)
                {
                    request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
                }
                else
                {
                    _dangKyService.delete(id);
                    _dangKyService.Commit();

                    response = request.CreateResponse(HttpStatusCode.OK);

                }
                return response;
            });
        }

        //[Route("delete")]
        //[HttpDelete]
        //[AllowAnonymous]
        //public HttpResponseMessage Delete(HttpRequestMessage request, int id)
        //{
        //    return CreateHttpResponse(request, () =>
        //    {
        //        HttpResponseMessage response = null;
        //        if (!ModelState.IsValid)
        //        {
        //            response = request.CreateResponse(HttpStatusCode.BadRequest, ModelState);
        //        }
        //        else
        //        {
        //            var oldProductCategory = _productCategoryService.Delete(id);
        //            _productCategoryService.Save();

        //            var responseData = Mapper.Map<ProductCategory, ProductCategoryViewModel>(oldProductCategory);
        //            response = request.CreateResponse(HttpStatusCode.Created, responseData);
        //        }

        //        return response;
        //    });
        //}

    }
}