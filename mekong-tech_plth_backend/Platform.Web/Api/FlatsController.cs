﻿using AutoMapper;
using Platform.Model;
using Platform.Service;
using Platform.Web.infratructure.core;
using Platform.Web.infratructure.extensions;
using Platform.Web.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;

namespace Platform.Web.Api
{
    [RoutePrefix("api/Flags")]
    //[Authorize]
    public class FlatsController : ApiControllerBase
    {
        IFlatsService _flatsService;
        IDangKyService _dangKyService;
        IServiceCTFService _serviceCTFService;

        public FlatsController(ILoiService loiService, IFlatsService flatsService, IDangKyService dangKyService, IServiceCTFService serviceCTFService) : base(loiService)
        {
            this._flatsService = flatsService;
            this._dangKyService = dangKyService;
            this._serviceCTFService = serviceCTFService;
        }
  
       
        [Route("getall")]
        public HttpResponseMessage Get(HttpRequestMessage request)
        {
            return CreateHttpResponse(request, () =>
            {


                var listCategory = _flatsService.GetAll();
                var responseData = Mapper.Map<IEnumerable<Flats>,IEnumerable<FlatsViewModel>>(listCategory);

                HttpResponseMessage response = request.CreateResponse(HttpStatusCode.OK, responseData);


                return response;
            });
        }

        [Route("update")]
        [HttpPut]
       
        public HttpResponseMessage Put(HttpRequestMessage request, FlatsViewModel flat)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                if (!ModelState.IsValid)
                {
                    request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
                }
                else
                {
                    

                    var flats = new Flats();
                    flats.FlatID = flat.FlatID;
                    flats.Flat = flat.Flat;
                    flats.TeamID = flat.TeamID;
                    flats.IsAttack = false;
                    //flats.UpdateSinhVien(sinhVienVm);
                    _flatsService.Update(flats);
                    _flatsService.Commit();
                    response = request.CreateResponse(HttpStatusCode.OK);
                }
                return response;
            });
        }
        [Route("tancong")]
        [HttpPut]

        public HttpResponseMessage tancong(HttpRequestMessage request,string Flat, string TeamID)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                if (!ModelState.IsValid)
                {
                    request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
                }
                else
                {
                    var flatid="";
                    if (TeamID=="1")
                    {
                        flatid = "02";
                    }
                    else if (TeamID == "2")
                    {
                        flatid = "01";
                    }

                    var getflat = _flatsService.GetByID(flatid);
                    //var responflat = Mapper.Map<Flats, FlatsViewModel>(getflat);
                    if (getflat.FlatID == flatid && getflat.Flat==Flat)
                    {
                        var getservice = _serviceCTFService.GetAll();
                        var newService = getservice.Last();

                            getflat.IsAttack = true;
                            _flatsService.Update(getflat);
                            _flatsService.Commit();

                            //ServiceCTF ctf = new ServiceCTF();
                            //newService.ServiceName = "Service1";
                            newService.TeamID = TeamID;
                            newService.AttackScore = "1000";
                            _serviceCTFService.Add(newService);
                            _serviceCTFService.Commit();

                            response = request.CreateResponse(HttpStatusCode.OK, "Tấn công thành công");

                        
                    }
                    else
                    {
                        response = request.CreateResponse(HttpStatusCode.BadRequest, "Tấn công không thành công");

                    }


                }
                return response;
            });
        }


    }


}