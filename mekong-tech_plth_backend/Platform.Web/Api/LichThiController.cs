﻿using AutoMapper;
using Platform.Model;
using Platform.Service;
using Platform.Web.infratructure.core;
using Platform.Web.infratructure.extensions;
using Platform.Web.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace Platform.Web.Api
{
    [RoutePrefix("api/lichthi")]
    [Authorize]
    public class LichThiController : ApiControllerBase
    {
        ILichThiService _lichThiService;

        public LichThiController(ILoiService loiService, ILichThiService lichThiService) : base(loiService)
        {
            this._lichThiService = lichThiService;
        }
        [Route("createExcel")]
        [HttpPost]

        public HttpResponseMessage Create(HttpRequestMessage request, IEnumerable<LichThiViewModel> lichThiVm)
        {


            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                if (!ModelState.IsValid)
                {
                    response = request.CreateResponse(HttpStatusCode.BadRequest, ModelState);
                }
                else
                {
                    foreach (var item in lichThiVm)
                    {
                        var newLichThi = new LichThi();
                        newLichThi.UpdateLichThi(item);

                        _lichThiService.Add(newLichThi);
                        _lichThiService.Save();
                    }


                    //var responseData = Mapper.Map<DangKy_TamThoi, DangKy_TamThoiViewModel>(newDangKy_TamThoi);
                    response = request.CreateResponse(HttpStatusCode.OK);
                }

                return response;
            });
        }
        [Route("getLichThi")]
        public HttpResponseMessage GetLichThi(HttpRequestMessage request,string mssv, string hocKy,string namhoc)
        {
            return CreateHttpResponse(request, () =>
            {


                var listLichThi = _lichThiService.GetLichThi(mssv, hocKy,namhoc);


                HttpResponseMessage response = request.CreateResponse(HttpStatusCode.OK, listLichThi);


                return response;
            });
        }
        [Route("getLichThiGV")]
        public HttpResponseMessage GetLichThiGV(HttpRequestMessage request, string msvc)
        {
            return CreateHttpResponse(request, () =>
            {


                var listLichThi = _lichThiService.GetLichThiGV(msvc);


                HttpResponseMessage response = request.CreateResponse(HttpStatusCode.OK, listLichThi);


                return response;
            });
        }
        [Route("getAllMonHoc")]
       [HttpGet]
        public HttpResponseMessage getAllMonHoc(HttpRequestMessage request, string NamHoc, string hocKy)
        {
            return CreateHttpResponse(request, () =>
            {


                var listMonHoc = _lichThiService.getAllMonHoc(NamHoc, hocKy);


                HttpResponseMessage response = request.CreateResponse(HttpStatusCode.OK, listMonHoc);


                return response;
            });
        }
         [Route("getHocKyHienTai")]
       [HttpGet]
        public HttpResponseMessage getHocKyHienTai(HttpRequestMessage request)
        {
            return CreateHttpResponse(request, () =>
            {


                var getHocKyHienTai = _lichThiService.getHocKyHienTai();
               //  var responseData = Mapper.Map<List<SinhVienViewModel>>(model);


                HttpResponseMessage response = request.CreateResponse(HttpStatusCode.OK, getHocKyHienTai);


                return response;
            });
        }




         [Route("getLichThiMH")]
          [HttpGet]
        public HttpResponseMessage getLichThiMH(HttpRequestMessage request, string tenMH, string hocKy, string namhoc)
        {
            return CreateHttpResponse(request, () =>
            {


                var LichThiMH = _lichThiService.getLichThiMH(tenMH,hocKy,namhoc);


                HttpResponseMessage response = request.CreateResponse(HttpStatusCode.OK, LichThiMH);


                return response;
            });
        }
         [Route("getTTMH")]
          [HttpGet]
        public HttpResponseMessage getTTMH(HttpRequestMessage request, string maMH, string maNhom, string malop)
        {
            return CreateHttpResponse(request, () =>
            {


                var LichThiMH = _lichThiService.getTTMH(maMH, maNhom,  malop);


                HttpResponseMessage response = request.CreateResponse(HttpStatusCode.OK, LichThiMH);


                return response;
            });
        }


        [Route("getall")]
        public HttpResponseMessage Get(HttpRequestMessage request)
        {
            return CreateHttpResponse(request, () =>
            {


                var listCategory = _lichThiService.GetAll();
                var listHocPhiVm = Mapper.Map<List<LichThiViewModel>>(listCategory);

                HttpResponseMessage response = request.CreateResponse(HttpStatusCode.OK, listHocPhiVm);


                return response;
            });
        }
    }
}
