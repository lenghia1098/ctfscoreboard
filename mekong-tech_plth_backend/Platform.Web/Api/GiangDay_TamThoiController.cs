﻿using AutoMapper;
using Platform.Model;
using Platform.Service;
using Platform.Web.infratructure.core;
using Platform.Web.Models;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Platform.Web.infratructure.extensions;


namespace Platform.Web.Api
{
    [RoutePrefix("api/GiangDay_TamThoi")]
    [Authorize]
    public class GiangDay_TamThoiController : ApiControllerBase
    {
        IGiangDay_TamThoiService _giangDay_TamThoiService;

        public GiangDay_TamThoiController(ILoiService loiService, IGiangDay_TamThoiService giangDay_TamThoiService) : base(loiService)
        {
            this._giangDay_TamThoiService = giangDay_TamThoiService;
        }
        [Route("createExcel")]
        [HttpPost]
        public HttpResponseMessage Create(HttpRequestMessage request, IEnumerable<GiangDay_TamThoiViewModel> giangDay_TamThoiVm)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                if (!ModelState.IsValid)
                {
                    response = request.CreateResponse(HttpStatusCode.BadRequest, ModelState);
                }
                else
                {
                    foreach (var item in giangDay_TamThoiVm)
                    {
                        var newGiangDay_TamThoi = new GiangDay_TamThoi();
                        newGiangDay_TamThoi.UpdateGiangDay_TamThoi(item);

                        _giangDay_TamThoiService.Add(newGiangDay_TamThoi);
                        _giangDay_TamThoiService.Save();
                    }


                    //var responseData = Mapper.Map<DangKy_TamThoi, DangKy_TamThoiViewModel>(newDangKy_TamThoi);
                    response = request.CreateResponse(HttpStatusCode.OK);
                }

                return response;
            });
        }
        public HttpResponseMessage Create(HttpRequestMessage request, GiangDay_TamThoi giangDay_TamThoi)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                if (ModelState.IsValid)
                {
                    request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
                }
                else
                {
                    _giangDay_TamThoiService.Add(giangDay_TamThoi);
                    _giangDay_TamThoiService.Commit();
                    response = request.CreateResponse(HttpStatusCode.Created, giangDay_TamThoi);
                }
                return response;
            });

        }
        [Route("getall")]
        public HttpResponseMessage Get(HttpRequestMessage request)
        {
            return CreateHttpResponse(request, () =>
            {


                var listCategory = _giangDay_TamThoiService.GetAll();
              

                HttpResponseMessage response = request.CreateResponse(HttpStatusCode.OK, listCategory);


                return response;
            });
        }
       
        [Route("getID")]
        public HttpResponseMessage GetById(HttpRequestMessage request)
        {
            return CreateHttpResponse(request, () =>
            {


                var sinhvien = _giangDay_TamThoiService.GetByID(1);
                

                HttpResponseMessage response = request.CreateResponse(HttpStatusCode.OK, sinhvien);


                return response;
            });
        }

       

        

    }
}