﻿using AutoMapper;
using Platform.Model;
using Platform.Service;
using Platform.Web.infratructure.core;
using Platform.Web.infratructure.extensions;
using Platform.Web.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace Platform.Web.Api
{
    [RoutePrefix("api/DonSinhVien_PhongKhoa")]
    [Authorize]
    public class DonSinhVien_PhongKhoaController : ApiControllerBase
    {
        IDonSinhVien_PhongKhoaService _donSinhVien_PhongKhoaService;

        public DonSinhVien_PhongKhoaController(ILoiService loiService, IDonSinhVien_PhongKhoaService donSinhVien_PhongKhoaService) : base(loiService)
        {
            this._donSinhVien_PhongKhoaService = donSinhVien_PhongKhoaService;
        }
        [Route("createExcel")]
        [HttpPost]
      
        public HttpResponseMessage Create(HttpRequestMessage request, IEnumerable<DonSinhVien_PhongKhoaViewModel> donSinhVien_PhongKhoaVm)
        {


            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                if (!ModelState.IsValid)
                {
                    response = request.CreateResponse(HttpStatusCode.BadRequest, ModelState);
                }
                else
                {
                    foreach (var item in donSinhVien_PhongKhoaVm)
                    {
                        var newDonSinhVien_PhongKhoa = new DonSinhVien_PhongKhoa();
                        newDonSinhVien_PhongKhoa.UpdateDonSinhVien_PhongKhoa(item);

                        _donSinhVien_PhongKhoaService.Add(newDonSinhVien_PhongKhoa);
                        _donSinhVien_PhongKhoaService.Save();
                    }


                    //var responseData = Mapper.Map<DangKy_TamThoi, DangKy_TamThoiViewModel>(newDangKy_TamThoi);
                    response = request.CreateResponse(HttpStatusCode.OK);
                }

                return response;
            });
        }
        public HttpResponseMessage Create(HttpRequestMessage request, DonSinhVien_PhongKhoa donSinhVien_PhongKhoa)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                if (ModelState.IsValid)
                {
                    request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
                }
                else
                {
                    _donSinhVien_PhongKhoaService.Add(donSinhVien_PhongKhoa);
                    _donSinhVien_PhongKhoaService.Commit();
                    response = request.CreateResponse(HttpStatusCode.Created, donSinhVien_PhongKhoa);
                }
                return response;
            });

        }
        [Route("getall")]
        public HttpResponseMessage Get(HttpRequestMessage request)
        {
            return CreateHttpResponse(request, () =>
            {


                var listCategory = _donSinhVien_PhongKhoaService.GetAll();

                HttpResponseMessage response = request.CreateResponse(HttpStatusCode.OK, listCategory);


                return response;
            });
        }
        
        
        
        
        
        
        public HttpResponseMessage Post(HttpRequestMessage request, DonSinhVien_PhongKhoa donSinhVien_PhongKhoa)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                if (ModelState.IsValid)
                {
                    request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
                }
                else
                {
                    _donSinhVien_PhongKhoaService.Add(donSinhVien_PhongKhoa);
                    _donSinhVien_PhongKhoaService.Commit();

                    response = request.CreateResponse(HttpStatusCode.Created);

                }
                return response;
            });
        }

        public HttpResponseMessage Put(HttpRequestMessage request, DonSinhVien_PhongKhoa donSinhVien_PhongKhoa)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                if (ModelState.IsValid)
                {
                    request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
                }
                else
                {
                    _donSinhVien_PhongKhoaService.Update(donSinhVien_PhongKhoa);
                    _donSinhVien_PhongKhoaService.Commit();

                    response = request.CreateResponse(HttpStatusCode.OK);

                }
                return response;
            });
        }

        public HttpResponseMessage Delete(HttpRequestMessage request, int id)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                if (ModelState.IsValid)
                {
                    request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
                }
                else
                {
                    _donSinhVien_PhongKhoaService.delete(id);
                    _donSinhVien_PhongKhoaService.Commit();

                    response = request.CreateResponse(HttpStatusCode.OK);

                }
                return response;
            });
        }

    }
}
