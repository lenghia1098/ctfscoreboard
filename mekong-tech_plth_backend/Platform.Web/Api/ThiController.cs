﻿using AutoMapper;
using Platform.Model;
using Platform.Service;
using Platform.Web.infratructure.core;
using Platform.Web.Models;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Platform.Web.infratructure.extensions;
using System.Data.SqlClient;
using System;

using System.Configuration;
using System.Web.Configuration;

namespace Platform.Web.Api
{
    [RoutePrefix("api/Thi")]
    [Authorize]
    // [RoutePrefix("{Type:regex(api/sinhviendauvao|api/Thi)}")]

    public class ThiController : ApiControllerBase
    {
        IThiService _thiService;

        public ThiController(ILoiService loiService, IThiService thiService) : base(loiService)
        {
            this._thiService = thiService;
        }

        [Route("createExcel")]
        [HttpPost]
        // [AllowAnonymous]
        public HttpResponseMessage Create(HttpRequestMessage request, IEnumerable<ThiViewModel> thiVm)
        {
           

            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                if (!ModelState.IsValid)
                {
                    response = request.CreateResponse(HttpStatusCode.BadRequest, ModelState);
                }
                else
                {
                    foreach (var item in thiVm)
                    {
                        var newThi = new Thi();
                        newThi.UpdateThi(item);

                        _thiService.Add(newThi);
                        _thiService.Save();
                    }


                    //var responseData = Mapper.Map<DangKy_TamThoi, DangKy_TamThoiViewModel>(newDangKy_TamThoi);
                    response = request.CreateResponse(HttpStatusCode.OK);
                }

                return response;
            });
        }




       




        [Route("create")]
        [HttpPost]
        // [AllowAnonymous]
        public HttpResponseMessage CreateExcel(HttpRequestMessage request, ThiViewModel thiVm)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                if (!ModelState.IsValid)
                {
                    response = request.CreateResponse(HttpStatusCode.BadRequest, ModelState);
                }
                else
                {
                  
                        var newThi = new Thi();
                        newThi.UpdateThi(thiVm);

                        _thiService.Add(newThi);
                        _thiService.Save();
                  


                    //var responseData = Mapper.Map<DangKy_TamThoi, DangKy_TamThoiViewModel>(newDangKy_TamThoi);
                    response = request.CreateResponse(HttpStatusCode.OK);
                }

                return response;
            });
        }


        [Route("getall")]
        [HttpGet]
        public HttpResponseMessage Get(HttpRequestMessage request)
        {
            return CreateHttpResponse(request, () =>
            {


                var listCategory = _thiService.GetAll();
                var listThiVm = Mapper.Map<List<ThiViewModel>>(listCategory);

                HttpResponseMessage response = request.CreateResponse(HttpStatusCode.OK, listThiVm);
               
                
                Console.WriteLine(1);

                return response;
            });
        }


        [Route("getID")]
        public HttpResponseMessage GetById(HttpRequestMessage request,int ID)
        {
            return CreateHttpResponse(request, () =>
            {


                var sinhvien = _thiService.GetByID(ID);
                var ThiVm = Mapper.Map<ThiViewModel>(sinhvien);

                HttpResponseMessage response = request.CreateResponse(HttpStatusCode.OK, ThiVm);


                return response;
            });
        }
       

    public HttpResponseMessage Post(HttpRequestMessage request, ThiViewModel thiVm)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                if (ModelState.IsValid)
                {
                    request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
                }
                else
                {
                    Thi newThi = new Thi();
                    newThi.UpdateThi(thiVm);
                    _thiService.Add(newThi);
                    _thiService.Commit();

                    response = request.CreateResponse(HttpStatusCode.Created);

                }
                return response;
            });
        }

        

       



       

















    }
}