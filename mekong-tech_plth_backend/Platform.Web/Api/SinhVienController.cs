﻿using AutoMapper;
using Platform.Model;
using Platform.Service;
using Platform.Web.infratructure.core;
using Platform.Web.Models;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Platform.Web.infratructure.extensions;


namespace Platform.Web.Api
{
    [RoutePrefix("api/sinhvien")]
    [Authorize]
    public class SinhVienController : ApiControllerBase
    {
        ISinhVienService _sinhVienService;

        public SinhVienController (ILoiService loiService, ISinhVienService sinhVienService) : base(loiService)
        {
            this._sinhVienService = sinhVienService;
        }
        [Route("createExcel")]
        [HttpPost]
        public HttpResponseMessage Create(HttpRequestMessage request, IEnumerable<SinhVienViewModel> sinhVienVm)
        {


            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                if (!ModelState.IsValid)
                {
                    response = request.CreateResponse(HttpStatusCode.BadRequest, ModelState);
                }
                else
                {
                    foreach (var item in sinhVienVm)
                    {
                        var newSinhVien = new SinhVien();
                        newSinhVien.UpdateSinhVien(item);

                        _sinhVienService.Add(newSinhVien);
                        _sinhVienService.Save();
                    }


                    //var responseData = Mapper.Map<DangKy_TamThoi, DangKy_TamThoiViewModel>(newDangKy_TamThoi);
                    response = request.CreateResponse(HttpStatusCode.OK);
                }

                return response;
            });
        }
        public HttpResponseMessage Create(HttpRequestMessage request, SinhVien sinhVien)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                if (ModelState.IsValid)
                {
                    request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
                }
                else
                {
                    _sinhVienService.Add(sinhVien);
                    _sinhVienService.Commit();
                    response = request.CreateResponse(HttpStatusCode.Created, sinhVien);
                }
                return response;
            });
           
        }
        [Route("getall")]
        public HttpResponseMessage Get(HttpRequestMessage request)
        {
            return CreateHttpResponse(request, () =>
            {


                var listCategory = _sinhVienService.GetAll();
                var listHocPhiVm = Mapper.Map<List<SinhVienViewModel>>(listCategory);

                HttpResponseMessage response = request.CreateResponse(HttpStatusCode.OK, listHocPhiVm);


                return response;
            });
        }

        [Route("getbyid")]
        [HttpGet]
        public HttpResponseMessage GetById(HttpRequestMessage request,string Id) 
        {
            return CreateHttpResponse(request, () =>
            {
                var model = _sinhVienService.GetByStringId(Id);

                var responseData = Mapper.Map<SinhVien, SinhVienViewModel>(model);

                var response = request.CreateResponse(HttpStatusCode.OK, responseData);

                return response;
            });
        }
        [Route("getTTCN")]
        [HttpGet]
        public HttpResponseMessage getTTCN(HttpRequestMessage request, string Id)
        {
            return CreateHttpResponse(request, () =>
            {
                var model = _sinhVienService.GetTTCN(Id);

              //  var responseData = Mapper.Map<List<SinhVienViewModel>>(model);

                var response = request.CreateResponse(HttpStatusCode.OK, model);

                return response;
            });
        }
        [Route("getthongtinsinhvien_dauvao")]
        [HttpGet]
        public HttpResponseMessage getthongtinsinhvien_dauvao(HttpRequestMessage request, string mssv)
        {
            return CreateHttpResponse(request, () =>
            {
                var model = _sinhVienService.getthongtinsinhvien_dauvao(mssv);
                var responseData = Mapper.Map<List<chucnangxemthongtinsinhvien_dauvao>>(model);

                var response = request.CreateResponse(HttpStatusCode.OK, responseData);

                return response;
            });
        }



        [Route("getID")]
        public HttpResponseMessage GetByStringId(HttpRequestMessage request, string id)
        {
            return CreateHttpResponse(request, () =>
            {


                var sinhvien = _sinhVienService.GetByStringId(id);
                var SinhVienVm = Mapper.Map<SinhVienViewModel>(sinhvien);

                HttpResponseMessage response = request.CreateResponse(HttpStatusCode.OK, SinhVienVm);


                return response;
            });
        }

        [Route("getThongTinSinhVien")]
        [HttpGet]
        public HttpResponseMessage ThongTinSinhVien(HttpRequestMessage request ,string Id)
        {
            return CreateHttpResponse(request, () =>
            {


                var sinhvien = _sinhVienService.ThongTinSinhVien(Id);
               

                HttpResponseMessage response = request.CreateResponse(HttpStatusCode.OK, sinhvien);


                return response;
            });
        }

        public HttpResponseMessage Post(HttpRequestMessage request, SinhVienViewModel sinhVienVm)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                if (ModelState.IsValid)
                {
                    request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
                }
                else
                {
                    SinhVien newSinhVien = new SinhVien();
                    newSinhVien.UpdateSinhVien(sinhVienVm);
                    _sinhVienService.Add(newSinhVien);
                    _sinhVienService.Commit();

                    response = request.CreateResponse(HttpStatusCode.Created);

                }
                return response;
            });
        }
        [Route("update")]
        [HttpPut]
        [AllowAnonymous]
        public HttpResponseMessage Put(HttpRequestMessage request, SinhVienViewModel sinhVienVm )
        {
            return CreateHttpResponse(request, () =>
            {
            HttpResponseMessage response = null;
            if (!ModelState.IsValid)
            {
                request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
            }
            else
            {

                    var sinhVienDb = _sinhVienService.GetByStringId(sinhVienVm.MaSoSV);
                    //SinhVien sinhVienDb = new SinhVien();
                    sinhVienDb.UpdateSinhVien(sinhVienVm);
                    sinhVienDb.DaXoa = false;
                   
                  
                    _sinhVienService.Update(sinhVienDb);
                    _sinhVienService.Commit();

                    response = request.CreateResponse(HttpStatusCode.OK);

                }
                return response;
            });
        }

        public HttpResponseMessage Delete(HttpRequestMessage request, int id)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                if (ModelState.IsValid)
                {
                    request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
                }
                else
                {
                    _sinhVienService.delete(id);
                    _sinhVienService.Commit();

                    response = request.CreateResponse(HttpStatusCode.OK);

                }
                return response;
            });
        }

    }
}