﻿using AutoMapper;
using Platform.Model;
using Platform.Service;
using Platform.Web.Infrastructure.Core;
using Platform.Web.infratructure.core;
using Platform.Web.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace Platform.Web.Api
{
    [Authorize]
    [RoutePrefix("api/ViecPhaiLamVienChuc")]
    public class ViecPhaiLamVienChucController : ApiControllerBase
    {
        IViecPhaiLamVienChucService _viecPhaiLamVienChucService;

        public ViecPhaiLamVienChucController(ILoiService loiService, IViecPhaiLamVienChucService viecPhaiLamVienChucService) : base(loiService)
        {
            this._viecPhaiLamVienChucService = viecPhaiLamVienChucService;
        }
        public HttpResponseMessage Create(HttpRequestMessage request, ViecPhaiLamVienChuc viecPhaiLamVienChuc)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                if (ModelState.IsValid)
                {
                    request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
                }
                else
                {
                    _viecPhaiLamVienChucService.Add(viecPhaiLamVienChuc);
                    _viecPhaiLamVienChucService.Commit();
                    response = request.CreateResponse(HttpStatusCode.Created, viecPhaiLamVienChuc);
                }
                return response;
            });

        }
        [Route("getall")]
        public HttpResponseMessage Get(HttpRequestMessage request)
        {
            return CreateHttpResponse(request, () =>
            {


                var listCategory = _viecPhaiLamVienChucService.GetAll();

                HttpResponseMessage response = request.CreateResponse(HttpStatusCode.OK, listCategory);


                return response;
            });
        }
        [Route("GetViecPhaiLamVienChucs")]
        public HttpResponseMessage GetViecPhaiLamVienChucs(HttpRequestMessage request,string msvc)
        {
            return CreateHttpResponse(request, () =>
            {


                var ViecPhaiLamVienChucs = _viecPhaiLamVienChucService.GetViecPhaiLamVienChucs(msvc);

                HttpResponseMessage response = request.CreateResponse(HttpStatusCode.OK, ViecPhaiLamVienChucs);


                return response;
            });
        }
		[Route("ViecDaLamVienChuc")]
        [HttpGet]
        public HttpResponseMessage ViecDaLamVienChuc(HttpRequestMessage request, int page, string msvc, int pageSize = 20)
        {
            return CreateHttpResponse(request, () =>
            {


                int totalRow = 0;

                IEnumerable<ViecDaLamVienChuc> viecdalam = _viecPhaiLamVienChucService.ViecDaLamVienChuc(msvc);

                totalRow = viecdalam.Count();
                var query = viecdalam.OrderByDescending(x => x.MaSoCongViec).Skip(page * pageSize).Take(pageSize);

                var responseData = Mapper.Map<IEnumerable<ViecDaLamVienChuc>, IEnumerable<ViecDaLamVienChuc>>(query);

                var PaginationSet = new PaginationSet<ViecDaLamVienChuc>()
                {

                    Items = responseData,
                    Page = page,
                    TotalCount = totalRow,/*tong so bai ghi*/
                    TotalPages = (int)Math.Ceiling((decimal)totalRow / pageSize),/*tong số trang*/



                };

                HttpResponseMessage response = request.CreateResponse(HttpStatusCode.OK, PaginationSet);


                return response;
            });
        }

        [Route("getChiTietViecDaLamVienChuc")]
        public HttpResponseMessage getChiTietViecDaLamVienChuc(HttpRequestMessage request, int MaSoCongViec)
        {
            return CreateHttpResponse(request, () =>
            {


                var ChiTietViecDaLamVienChuc = _viecPhaiLamVienChucService.getChiTietViecDaLamVienChuc(MaSoCongViec);

                HttpResponseMessage response = request.CreateResponse(HttpStatusCode.OK, ChiTietViecDaLamVienChuc);


                return response;
            });
        }

        [Route("DemViecPhaiLamVienChuc")]
        [HttpGet]
        public HttpResponseMessage DemViecPhaiLamVienChuc(HttpRequestMessage request, string MaSoVienChuc)
        {
            return CreateHttpResponse(request, () =>
            {


                var DemViecLamVienChuc = _viecPhaiLamVienChucService.DemViecPhaiLamVienChuc(MaSoVienChuc);

                HttpResponseMessage response = request.CreateResponse(HttpStatusCode.OK, DemViecLamVienChuc);


                return response;
            });
        }

        public HttpResponseMessage Post(HttpRequestMessage request, ViecPhaiLamVienChuc viecPhaiLamVienChuc)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                if (ModelState.IsValid)
                {
                    request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
                }
                else
                {
                    _viecPhaiLamVienChucService.Add(viecPhaiLamVienChuc);
                    _viecPhaiLamVienChucService.Commit();

                    response = request.CreateResponse(HttpStatusCode.Created);

                }
                return response;
            });
        }



        
        [Route("update")]
        [HttpPut]
        [AllowAnonymous]
        public HttpResponseMessage Put(HttpRequestMessage request, ViecPhaiLamVienChuc viecPhaiLamVienChuc)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                if (ModelState.IsValid)
                {
                    request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
                }
                else
                {
                    _viecPhaiLamVienChucService.Update(viecPhaiLamVienChuc);
                    _viecPhaiLamVienChucService.Commit();

                    response = request.CreateResponse(HttpStatusCode.OK);

                }
                return response;
            });
        }

        public HttpResponseMessage Delete(HttpRequestMessage request, int id)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                if (ModelState.IsValid)
                {
                    request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
                }
                else
                {
                    _viecPhaiLamVienChucService.delete(id);
                    _viecPhaiLamVienChucService.Commit();

                    response = request.CreateResponse(HttpStatusCode.OK);

                }
                return response;
            });
        }
    }
}
