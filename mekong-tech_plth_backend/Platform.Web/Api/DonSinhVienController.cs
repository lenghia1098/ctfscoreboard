﻿using AutoMapper;
using Platform.Model;
using Platform.Service;
using Platform.Web.Infrastructure.Core;
using Platform.Web.infratructure.core;
using Platform.Web.infratructure.extensions;
using Platform.Web.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace Platform.Web.Api
{
    [RoutePrefix("api/donsinhvien")]
    [Authorize]
    public class DonSinhVienController : ApiControllerBase
    {
        IDonSinhVienService _donSinhVienService;

        public DonSinhVienController(ILoiService loiService, IDonSinhVienService donSinhVienService) : base(loiService)
        {
            this._donSinhVienService = donSinhVienService;
        }
        [Route("createExcel")]
        [HttpPost]
      
        public HttpResponseMessage Create(HttpRequestMessage request, IEnumerable<DonSinhVienViewModel> donSinhVienVm)
        {


            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                if (!ModelState.IsValid)
                {
                    response = request.CreateResponse(HttpStatusCode.BadRequest, ModelState);
                }
                else
                {
                    foreach (var item in donSinhVienVm)
                    {
                        var newDonSinhVien = new DonSinhVien();
                        newDonSinhVien.UpdateDonSinhVien(item);

                        _donSinhVienService.Add(newDonSinhVien);
                        _donSinhVienService.Save();
                    }


                    //var responseData = Mapper.Map<DangKy_TamThoi, DangKy_TamThoiViewModel>(newDangKy_TamThoi);
                    response = request.CreateResponse(HttpStatusCode.OK);
                }

                return response;
            });
        }
        public HttpResponseMessage Create(HttpRequestMessage request, DonSinhVien donSinhVien)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                if (ModelState.IsValid)
                {
                    request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
                }
                else
                {
                    _donSinhVienService.Add(donSinhVien);
                    _donSinhVienService.Commit();
                    response = request.CreateResponse(HttpStatusCode.Created, donSinhVien);
                }
                return response;
            });

        }
        [Route("getall")]
        public HttpResponseMessage Get(HttpRequestMessage request)
        {
            return CreateHttpResponse(request, () =>
            {


                var listCategory = _donSinhVienService.GetAll();

                HttpResponseMessage response = request.CreateResponse(HttpStatusCode.OK, listCategory);


                return response;
            });
        }
        [Route("GetChucnangdondanops")]
        public HttpResponseMessage GetChucnangdondanops(HttpRequestMessage request, int page,string mssv,int  pageSize=20)
        {
            return CreateHttpResponse(request, () =>
            {



                int totalRow = 0;

                IEnumerable<chucnangdondanop> viecdalam = _donSinhVienService.GetChucnangdondanops(mssv);

                totalRow = viecdalam.Count();
                var query = viecdalam.OrderByDescending(x => x.MaDon).Skip(page * pageSize).Take(pageSize);

                var responseData = Mapper.Map<IEnumerable<chucnangdondanop>, IEnumerable<chucnangdondanop>>(query);

                var PaginationSet = new PaginationSet<chucnangdondanop>()
                {

                    Items = responseData,
                    Page = page,
                    TotalCount = totalRow,/*tong so bai ghi*/
                    TotalPages = (int)Math.Ceiling((decimal)totalRow / pageSize),/*tong số trang*/



                };

                HttpResponseMessage response = request.CreateResponse(HttpStatusCode.OK, PaginationSet);


                return response;
            });
        }
        [Route("demdonsinhvien")]
        [HttpGet]
        public HttpResponseMessage demdonsinhvien(HttpRequestMessage request,string mssv)
        {
            return CreateHttpResponse(request, () =>
            {


                var dem = _donSinhVienService.demdonsinhvien(mssv);

                HttpResponseMessage response = request.CreateResponse(HttpStatusCode.OK, dem);


                return response;
            });
        }
        
        
        [Route("chitietdon")]
        [HttpGet]
        public HttpResponseMessage ChiTietDonDaNop(HttpRequestMessage request, int MaDon)
        {
            return CreateHttpResponse(request, () =>
            {


                var chitietdon = _donSinhVienService.ChiTietDonDaNop(MaDon);

                HttpResponseMessage response = request.CreateResponse(HttpStatusCode.OK, chitietdon);


                return response;
            });
        }
        
        
        public HttpResponseMessage Post(HttpRequestMessage request, DonSinhVien donSinhVien)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                if (ModelState.IsValid)
                {
                    request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
                }
                else
                {
                    _donSinhVienService.Add(donSinhVien);
                    _donSinhVienService.Commit();

                    response = request.CreateResponse(HttpStatusCode.Created);

                }
                return response;
            });
        }

        public HttpResponseMessage Put(HttpRequestMessage request, DonSinhVien donSinhVien)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                if (ModelState.IsValid)
                {
                    request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
                }
                else
                {
                    _donSinhVienService.Update(donSinhVien);
                    _donSinhVienService.Commit();

                    response = request.CreateResponse(HttpStatusCode.OK);

                }
                return response;
            });
        }

        public HttpResponseMessage Delete(HttpRequestMessage request, int id)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                if (ModelState.IsValid)
                {
                    request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
                }
                else
                {
                    _donSinhVienService.delete(id);
                    _donSinhVienService.Commit();

                    response = request.CreateResponse(HttpStatusCode.OK);

                }
                return response;
            });
        }

    }
}
