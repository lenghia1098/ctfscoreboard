﻿using AutoMapper;
using Platform.Model;
using Platform.Service;
using Platform.Web.infratructure.core;
using Platform.Web.infratructure.extensions;
using Platform.Web.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Platform.Web.Infrastructure.Core;

namespace Platform.Web.Api
{
    [RoutePrefix("api/TinTuc")]
    [Authorize]
    public class TinTucController : ApiControllerBase
    {
        ITinTucService _tinTucService;

        public TinTucController(ILoiService loiService, ITinTucService tinTucService) : base(loiService)
        {
            this._tinTucService = tinTucService;
        }
        [Route("createExcel")]
        [HttpPost]
        // [AllowAnonymous]
        public HttpResponseMessage Create(HttpRequestMessage request, IEnumerable<TinTucViewModel> tinTucVm)
        {


            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                if (!ModelState.IsValid)
                {
                    response = request.CreateResponse(HttpStatusCode.BadRequest, ModelState);
                }
                else
                {
                    foreach (var item in tinTucVm)
                    {
                        var newTinTuc = new TinTuc();
                        newTinTuc.TinTuc(item);

                        _tinTucService.Add(newTinTuc);
                        _tinTucService.Save();
                    }


                    //var responseData = Mapper.Map<DangKy_TamThoi, DangKy_TamThoiViewModel>(newDangKy_TamThoi);
                    response = request.CreateResponse(HttpStatusCode.OK);
                }

                return response;
            });
        }
        public HttpResponseMessage Create(HttpRequestMessage request, TinTuc tinTuc)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                if (ModelState.IsValid)
                {
                    request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
                }
                else
                {
                    _tinTucService.Add(tinTuc);
                    _tinTucService.Commit();
                    response = request.CreateResponse(HttpStatusCode.Created, tinTuc);
                }
                return response;
            });

        }
        [Route("getall")]
        public HttpResponseMessage Get(HttpRequestMessage request)
        {
            return CreateHttpResponse(request, () =>
            {


                var listCategory = _tinTucService.GetAll();

                HttpResponseMessage response = request.CreateResponse(HttpStatusCode.OK, listCategory);


                return response;
            });
        }
        [Route("ChiTietTT")]
        [HttpGet]
        public HttpResponseMessage ChiTietTT(HttpRequestMessage request,int MaTinTuc)
        {
            return CreateHttpResponse(request, () =>
            {


                var ChiTietTT = _tinTucService.ChiTietTT(MaTinTuc);

                HttpResponseMessage response = request.CreateResponse(HttpStatusCode.OK, ChiTietTT);


                return response;
            });
        }

        [Route("getTT")]
        public HttpResponseMessage GetTT(HttpRequestMessage request, int page, string mssv, int pageSize = 20)
        {
            return CreateHttpResponse(request, () =>
            {
                int totalRow = 0;

                IEnumerable<TinTuc> tintuc = _tinTucService.GetTinTuc(mssv);
                totalRow = tintuc.Count();
                var query = tintuc.OrderByDescending(x => x.MaTinTuc).Skip(page * pageSize).Take(pageSize);

                var responseData = Mapper.Map<IEnumerable<TinTuc>, IEnumerable<TinTucViewModel>>(query);

                var PaginationSet = new PaginationSet<TinTucViewModel>()
                {

                    Items = responseData,
                    Page = page,
                    TotalCount = totalRow,/*tong so bai ghi*/
                    TotalPages = (int)Math.Ceiling((decimal)totalRow / pageSize),/*tong số trang*/



                };

                HttpResponseMessage response = request.CreateResponse(HttpStatusCode.OK, PaginationSet);


                return response;
            });
        }
        public HttpResponseMessage Post(HttpRequestMessage request, TinTuc tinTuc)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                if (ModelState.IsValid)
                {
                    request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
                }
                else
                {
                    _tinTucService.Add(tinTuc);
                    _tinTucService.Commit();

                    response = request.CreateResponse(HttpStatusCode.Created);

                }
                return response;
            });
        }

        public HttpResponseMessage Put(HttpRequestMessage request, TinTuc tinTuc)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                if (ModelState.IsValid)
                {
                    request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
                }
                else
                {
                    _tinTucService.Update(tinTuc);
                    _tinTucService.Commit();

                    response = request.CreateResponse(HttpStatusCode.OK);

                }
                return response;
            });
        }

        public HttpResponseMessage Delete(HttpRequestMessage request, int id)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                if (ModelState.IsValid)
                {
                    request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
                }
                else
                {
                    _tinTucService.delete(id);
                    _tinTucService.Commit();

                    response = request.CreateResponse(HttpStatusCode.OK);

                }
                return response;
            });
        }
    }
}
