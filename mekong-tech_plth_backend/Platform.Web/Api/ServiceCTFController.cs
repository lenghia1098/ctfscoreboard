﻿using AutoMapper;
using Platform.Model;
using Platform.Service;
using Platform.Web.infratructure.core;
using Platform.Web.infratructure.extensions;
using Platform.Web.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;

namespace Platform.Web.Api
{
    [RoutePrefix("api/ServiceCTF")]
    //[Authorize]
    public class ServiceCTFController : ApiControllerBase
    {
        IServiceCTFService _serviceCTFService;
        IDangKyService _dangKyService;
        public ServiceCTFController(ILoiService loiService, IServiceCTFService serviceCTFService, IDangKyService dangKyService) : base(loiService)
        {
            this._serviceCTFService = serviceCTFService;
            this._dangKyService = dangKyService;
        }
        [Route("create")]
        [HttpPost]
        public HttpResponseMessage Create(HttpRequestMessage request, ServiceCTFViewModel serviceCTFVm)
        {


            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                if (!ModelState.IsValid)
                {
                    response = request.CreateResponse(HttpStatusCode.BadRequest, ModelState);
                }
                else
                {
                   
                        var newServiceCTF = new ServiceCTF();
                        newServiceCTF.UpdateServiceCTF(serviceCTFVm);
                        _serviceCTFService.Add(newServiceCTF);
                        _serviceCTFService.Save();

                       



                    //var responseData = Mapper.Map<DangKy_TamThoi, DangKy_TamThoiViewModel>(newDangKy_TamThoi);
                    response = request.CreateResponse(HttpStatusCode.OK);
                }

                return response;
            });
        }
        [Route("createhoa")]
        [HttpPost]
        public HttpResponseMessage CreateHoa(HttpRequestMessage request, ServiceCTF serviceCTFVm)
        {


            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                if (!ModelState.IsValid)
                {
                    response = request.CreateResponse(HttpStatusCode.BadRequest, ModelState);
                }
                else
                {
                    var getservice = _serviceCTFService.GetAll().Last();
                    serviceCTFVm.ServiceName = getservice.ServiceName;
                    //var newServiceCTF = new ServiceCTF();
                    //newServiceCTF.UpdateServiceCTF(serviceCTFVm);
                    _serviceCTFService.Add(serviceCTFVm);
                    _serviceCTFService.Save();





                    //var responseData = Mapper.Map<DangKy_TamThoi, DangKy_TamThoiViewModel>(newDangKy_TamThoi);
                    response = request.CreateResponse(HttpStatusCode.OK);
                }

                return response;
            });
        }

        [Route("getall")]
        public HttpResponseMessage Get(HttpRequestMessage request)
        {
            return CreateHttpResponse(request, () =>
            {


                var listCategory = _serviceCTFService.GetAll();
                var listHocPhiVm = Mapper.Map<List<ServiceCTFViewModel>>(listCategory);

                HttpResponseMessage response = request.CreateResponse(HttpStatusCode.OK, listHocPhiVm);


                return response;
            });
        }

        [Route("update")]
        [HttpPut]

        public HttpResponseMessage Put(HttpRequestMessage request, ServiceCTFViewModel flat)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                if (!ModelState.IsValid)
                {
                    request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
                }
                else
                {
                    var getservice = _serviceCTFService.GetAll().Last();
                    getservice.DefenseScore = flat.DefenseScore;
                    _serviceCTFService.Update(getservice);
                    _serviceCTFService.Commit();
                    response = request.CreateResponse(HttpStatusCode.OK);
                }
                return response;
            });
        }

        [Route("deleteall")]
        [HttpDelete]
        public HttpResponseMessage Delete(HttpRequestMessage request)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                if (!ModelState.IsValid)
                {
                    request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
                }
                else
                {
                   
                    _serviceCTFService.deleteall();
                    

                    response = request.CreateResponse(HttpStatusCode.OK);

                }
                return response;
            });
        }

    }


}