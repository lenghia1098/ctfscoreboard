﻿using AutoMapper;
using Platform.Model;
using Platform.Service;
using Platform.Web.infratructure.core;
using Platform.Web.infratructure.extensions;
using Platform.Web.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace Platform.Web.Api
{
    [RoutePrefix("api/monhoc")]
    [Authorize]
    public class MonHocController : ApiControllerBase
    {
        IMonHocService _monHocService;

        public MonHocController(ILoiService loiService, IMonHocService monHocService) : base(loiService)
        {
            this._monHocService = monHocService;
        }
        [Route("createExcel")]
        [HttpPost]
        public HttpResponseMessage Create(HttpRequestMessage request, IEnumerable<MonHocViewModel> monHocVm)
        {


            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                if (!ModelState.IsValid)
                {
                    response = request.CreateResponse(HttpStatusCode.BadRequest, ModelState);
                }
                else
                {
                    foreach (var item in monHocVm)
                    {
                        var newMonHoc = new MonHoc();
                        newMonHoc.UpdateMonHoc(item);

                        _monHocService.Add(newMonHoc);
                        _monHocService.Save();
                    }


                    //var responseData = Mapper.Map<DangKy_TamThoi, DangKy_TamThoiViewModel>(newDangKy_TamThoi);
                    response = request.CreateResponse(HttpStatusCode.OK);
                }

                return response;
            });
        }
        public HttpResponseMessage Create(HttpRequestMessage request, MonHoc monHoc)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                if (ModelState.IsValid)
                {
                    request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
                }
                else
                {
                    _monHocService.Add(monHoc);
                    _monHocService.Commit();
                    response = request.CreateResponse(HttpStatusCode.Created, monHoc);
                }
                return response;
            });

        }
        [Route("getall")]
        public HttpResponseMessage Get(HttpRequestMessage request)
        {
            return CreateHttpResponse(request, () =>
            {


                var listCategory = _monHocService.GetAll();

                HttpResponseMessage response = request.CreateResponse(HttpStatusCode.OK, listCategory);


                return response;
            });
        }
        [Route("GetHocPhi")]
        [HttpGet]
        public HttpResponseMessage GetHocPhi(HttpRequestMessage request, string mssv, string hocKy)
        {
            return CreateHttpResponse(request, () =>
            {


                var list = _monHocService.GetHocPhi(mssv, hocKy);

                HttpResponseMessage response = request.CreateResponse(HttpStatusCode.OK, list);


                return response;
            });
        }
        [Route("getThongTinMonHoc")]
        public HttpResponseMessage getThongTinMonHoc(HttpRequestMessage request, string tenMH, string hocKy, string namhoc, string mssv, DateTime ngayDangKy)
        {
            return CreateHttpResponse(request, () =>
            {


                var listCategory = _monHocService.getThongTinMonHoc(tenMH,  hocKy,  namhoc,  mssv,  ngayDangKy);

                HttpResponseMessage response = request.CreateResponse(HttpStatusCode.OK, listCategory);


                return response;
            });
        }
        [Route("getDSMHTheoLop")]
        public HttpResponseMessage getDSMHTheoLop(HttpRequestMessage request, string tenLop, string hocKy, string namhoc, string mssv, DateTime ngayDangKy)
        {
            return CreateHttpResponse(request, () =>
            {


                var listCategory = _monHocService.getDSMHTheoLop(tenLop, hocKy, namhoc,  mssv,ngayDangKy);

                HttpResponseMessage response = request.CreateResponse(HttpStatusCode.OK, listCategory);


                return response;
            });
        }
        [Route("getDSMHTheoNganh")]
        public HttpResponseMessage getDSMHTheoNganh(HttpRequestMessage request, string tenNganh, string hocKy, string namhoc, string mssv, DateTime ngayDangKy)
        {
            return CreateHttpResponse(request, () =>
            {


                var listCategory = _monHocService.getDSMHTheoNganh(tenNganh, hocKy, namhoc, mssv, ngayDangKy);

                HttpResponseMessage response = request.CreateResponse(HttpStatusCode.OK, listCategory);


                return response;
            });
        }
        [Route("getDSMHTheoKhoa")]
        public HttpResponseMessage getDSMHTheoKhoa(HttpRequestMessage request, string tenKhoa, string hocKy, string namhoc, string mssv, DateTime ngayDangKy)
        {
            return CreateHttpResponse(request, () =>
            {


                var listCategory = _monHocService.getDSMHTheoKhoa(tenKhoa, hocKy, namhoc, mssv, ngayDangKy);

                HttpResponseMessage response = request.CreateResponse(HttpStatusCode.OK, listCategory);


                return response;
            });
        }

        [Route("getAllMonHoc")]
        public HttpResponseMessage getAllMonHoc(HttpRequestMessage request,  string hocKy, string namhoc)
        {
            return CreateHttpResponse(request, () =>
            {


                var listCategory = _monHocService.getAllMonHoc(hocKy, namhoc);

                HttpResponseMessage response = request.CreateResponse(HttpStatusCode.OK, listCategory);


                return response;
            });
        }
        [Route("getMonHocTheoMSSV")]
        public HttpResponseMessage getMonHocTheoMSSV(HttpRequestMessage request,string mssv, string hocKy, string namhoc)
        {
            return CreateHttpResponse(request, () =>
            {


                var listCategory = _monHocService.getMonHocTheoMSSV(mssv,hocKy, namhoc);

                HttpResponseMessage response = request.CreateResponse(HttpStatusCode.OK, listCategory);


                return response;
            });
        }


        public HttpResponseMessage Post(HttpRequestMessage request, MonHoc monHoc)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                if (ModelState.IsValid)
                {
                    request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
                }
                else
                {
                    _monHocService.Add(monHoc);
                    _monHocService.Commit();

                    response = request.CreateResponse(HttpStatusCode.Created);

                }
                return response;
            });
        }

        public HttpResponseMessage Put(HttpRequestMessage request, MonHoc monHoc)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                if (ModelState.IsValid)
                {
                    request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
                }
                else
                {
                    _monHocService.Update(monHoc);
                    _monHocService.Commit();

                    response = request.CreateResponse(HttpStatusCode.OK);

                }
                return response;
            });
        }

        public HttpResponseMessage Delete(HttpRequestMessage request, int id)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                if (ModelState.IsValid)
                {
                    request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
                }
                else
                {
                    _monHocService.delete(id);
                    _monHocService.Commit();

                    response = request.CreateResponse(HttpStatusCode.OK);

                }
                return response;
            });
        }
        [Route("getbyid")]
        [HttpGet]
        public HttpResponseMessage GetById(HttpRequestMessage request, string Id)
        {
            return CreateHttpResponse(request, () =>
            {
                var model = _monHocService.GetSingleById(Id);

                var responseData = Mapper.Map<MonHoc, MonHocViewModel>(model);

                var response = request.CreateResponse(HttpStatusCode.OK, responseData);

                return response;
            });
        }
        

    }
}