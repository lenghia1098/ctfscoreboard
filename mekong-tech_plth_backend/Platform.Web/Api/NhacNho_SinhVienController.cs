﻿using Platform.Model;
using Platform.Service;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Platform.Web.infratructure.core;
using AutoMapper;
using Platform.Web.Models;
using Platform.Web.Infrastructure.Core;
using Platform.Web.infratructure.extensions;

namespace Platform.Web.Api
{
    [RoutePrefix("api/nhacnho-sv")]
    [Authorize]
    public class NhacNho_SinhVienController : ApiControllerBase
    {
        INhacNho_SinhVienService _nhacNho_SinhVienService;

        public NhacNho_SinhVienController(ILoiService loiService, INhacNho_SinhVienService nhacNho_SinhVienService) : base(loiService)
        {
            this._nhacNho_SinhVienService = nhacNho_SinhVienService;
        }
    
    [Route("createExcel")]
    [HttpPost]
    public HttpResponseMessage Create(HttpRequestMessage request, IEnumerable<NhacNho_SinhVienViewModel> nhacNho_SinhVienVm)
    {


        return CreateHttpResponse(request, () =>
        {
            HttpResponseMessage response = null;
            if (!ModelState.IsValid)
            {
                response = request.CreateResponse(HttpStatusCode.BadRequest, ModelState);
            }
            else
            {
                foreach (var item in nhacNho_SinhVienVm)
                {
                    var newNhacNho_SinhVien = new NhacNho_SinhVien();
                    newNhacNho_SinhVien.UpdateNhacNho_SinhVien(item);

                    _nhacNho_SinhVienService.Add(newNhacNho_SinhVien);
                    _nhacNho_SinhVienService.Save();
                }


                //var responseData = Mapper.Map<DangKy_TamThoi, DangKy_TamThoiViewModel>(newDangKy_TamThoi);
                response = request.CreateResponse(HttpStatusCode.OK);
            }

            return response;
        });
    }
    public HttpResponseMessage Create(HttpRequestMessage request, NhacNho_SinhVien nhacNho_SinhVien)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                if (ModelState.IsValid)
                {
                    request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
                }
                else
                {
                    _nhacNho_SinhVienService.Add(nhacNho_SinhVien);
                    _nhacNho_SinhVienService.Commit();
                    response = request.CreateResponse(HttpStatusCode.Created, nhacNho_SinhVien);
                }
                return response;
            });

        }
        [Route("getall")]
        public HttpResponseMessage Get(HttpRequestMessage request)
        {
            return CreateHttpResponse(request, () =>
            {


                var listCategory = _nhacNho_SinhVienService.GetAll();

                HttpResponseMessage response = request.CreateResponse(HttpStatusCode.OK, listCategory);


                return response;
            });
        }
        [Route("getMaNN")]
        public HttpResponseMessage GetMaNN(HttpRequestMessage request,string mssv)
        {
            return CreateHttpResponse(request, () =>
            {


                IQueryable<string> maNN = _nhacNho_SinhVienService.GetMaNhacNhoSV(mssv);

                HttpResponseMessage response = request.CreateResponse(HttpStatusCode.OK, maNN);


                return response;
            });
        }
        [Route("getNhacNhoChuaHoanThanh")]
        public HttpResponseMessage GetNhacNhoChuaHoanThanh(HttpRequestMessage request, int page, string mssv, int pageSize = 20)
        {
            return CreateHttpResponse(request, () =>
            {
                int totalRow = 0;

                IEnumerable<NhacNhoSinhVienChuaHoanThanh> tintuc = _nhacNho_SinhVienService.GetNhacNhoChuaHoanThanh(mssv);
                totalRow = tintuc.Count();
                var query = tintuc.OrderByDescending(x => x.MaSoNN).Skip(page * pageSize).Take(pageSize);

                var responseData = Mapper.Map<IEnumerable<NhacNhoSinhVienChuaHoanThanh>, IEnumerable<NhacNhoSinhVienChuaHoanThanh>>(query);

                var PaginationSet = new PaginationSet<NhacNhoSinhVienChuaHoanThanh>()
                {

                    Items = responseData,
                    Page = page,
                    TotalCount = totalRow,/*tong so bai ghi*/
                    TotalPages = (int)Math.Ceiling((decimal)totalRow / pageSize),/*tong số trang*/



                };

                HttpResponseMessage response = request.CreateResponse(HttpStatusCode.OK, PaginationSet);


                return response;
            });
        }

        public HttpResponseMessage Post(HttpRequestMessage request, NhacNho_SinhVien nhacNho_SinhVien)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                if (ModelState.IsValid)
                {
                    request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
                }
                else
                {
                    _nhacNho_SinhVienService.Add(nhacNho_SinhVien);
                    _nhacNho_SinhVienService.Commit();

                    response = request.CreateResponse(HttpStatusCode.Created);

                }
                return response;
            });
        }

        public HttpResponseMessage Put(HttpRequestMessage request, NhacNho_SinhVien nhacNho_SinhVien)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                if (ModelState.IsValid)
                {
                    request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
                }
                else
                {
                    _nhacNho_SinhVienService.Update(nhacNho_SinhVien);
                    _nhacNho_SinhVienService.Commit();

                    response = request.CreateResponse(HttpStatusCode.OK);

                }
                return response;
            });
        }

        public HttpResponseMessage Delete(HttpRequestMessage request, int id)
        {
            return CreateHttpResponse(request, () =>
            {
                HttpResponseMessage response = null;
                if (ModelState.IsValid)
                {
                    request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
                }
                else
                {
                    _nhacNho_SinhVienService.delete(id);
                    _nhacNho_SinhVienService.Commit();

                    response = request.CreateResponse(HttpStatusCode.OK);

                }
                return response;
            });
        }
    }
}
