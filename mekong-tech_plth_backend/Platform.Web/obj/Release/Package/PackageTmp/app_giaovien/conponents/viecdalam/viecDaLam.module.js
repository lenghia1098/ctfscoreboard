﻿/// <reference path="../../../assets/giaovien/libs/angular/angular.js" />

(function () {
    angular.module('platformTH_GV.viecdalam', ['platformTH_GV.common']).config(config);
    window.appVersion = angular.element(document.getElementsByTagName('html')).attr('data-ver');

    config.$inject = ['$stateProvider', '$urlRouterProvider'];

    function config($stateProvider, $urlRouterProvider) {
        $stateProvider.state('viecdalam', {
            url: "/viecdalam",
            parent: 'base',
            templateUrl: "app_giaovien/conponents/viecdalam/viecDaLamView.html?v=" + window.appVersion,
            controller: "viecDaLamController"
        });

    }
})();