﻿
(function (app) {
    app.controller('giangDayController', giangDayController);

    function giangDayController($scope, loginService, $injector, notificationService) {

        if ($scope.kiemtrarole('SinhVien') === 'SinhVien' && $scope.kiemtrarole('Admin') != 'Admin') {
            loginService.logOut();
            var stateService = $injector.get('$state');
            stateService.go('dangnhap');
            notificationService.displayError("Bạn không có quyền truy cập trang này");
        }
        else {
            //#region
           
            //#endregion

        }

    }
})(angular.module('platformTH_GV.giangday'));
