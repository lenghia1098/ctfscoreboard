﻿(function (app) {
    app.controller('lyLichCaNhanController', lyLichCaNhanController);


    lyLichCaNhanController.$inject = ['$scope', 'apiService', '$stateParams', '$window', 'notificationService', '$ngBootbox', '$filter'];

    function lyLichCaNhanController($scope, apiService, $stateParams, $window, notificationService, $ngBootbox, $filter) {
        $scope.msvc = $scope.authentication.userName;
        $scope.AllVienChucChitiet = [];
        $scope.AllTrinhDoNgoaiNgu = [];
        $scope.AllThoiGianCongTac = [];
        $scope.AllQuaTrinhDaoTao = [];
        

        $scope.GetLyLichVienChuc = GetLyLichVienChuc;
        $scope.GetTrinhDoNgoaiNgu = GetTrinhDoNgoaiNgu;
        $scope.getThoiGianCongTac = getThoiGianCongTac;
        $scope.getQuaTrinhDaoTao = getQuaTrinhDaoTao;

        $scope.putupdateimage = putupdateimage;

        $scope.putupdate = putupdate;
        $scope.putupdateTrinhDoNgoaiNgu = putupdateTrinhDoNgoaiNgu;
        $scope.putupdateThoiGianCongTac = putupdateThoiGianCongTac;
        $scope.putupdateQuaTrinhDaoTao = putupdateQuaTrinhDaoTao;
        $scope.kiemtraloi = kiemtraloi;
        $scope.hide = true;
      
        $scope.mobang = function () {
            $scope.Edit = true
            $scope.hide = false;
            $scope.save = true;
            $scope.closehuy = true;
            
        }
        $scope.huy = function () {
            $scope.Edit = false
            $scope.hide = true
            $scope.save = false;
            $scope.closehuy = false;
        }

      
        function GetLyLichVienChuc() {
            var config = {
                params: {
                    msvc: $scope.msvc
                }
            }
            apiService.get('/api/vienchuc/GetLyLichVienChuc', config, function (result) {

                $scope.AllVienChucChitietModel = result.data;
                $scope.AllVienChucChitiet = result.data;
                var date1 = new Date($scope.AllVienChucChitiet.NgayCap);
                $scope.AllVienChucChitiet.NgayCap = $filter('date')(date1, "yyyy-MM-dd");
                var date2 = new Date($scope.AllVienChucChitiet.NamBatDauCongTac);
                $scope.AllVienChucChitiet.NamBatDauCongTac = $filter('date')(date2, "yyyy-MM-dd");
                var date3 = new Date($scope.AllVienChucChitiet.NamNghiHuu);
                $scope.AllVienChucChitiet.NamNghiHuu = $filter('date')(date3, "yyyy-MM-dd");
                var date4 = new Date($scope.AllVienChucChitiet.NgaySinh);
                $scope.AllVienChucChitiet.NgaySinh = $filter('date')(date4, "yyyy-MM-dd");

            }, function () {
                console.log('Load Thông tin  failed.');
            });
        }

        function GetTrinhDoNgoaiNgu() {
            var config = {
                params: {
                    msvc: $scope.msvc
                }
            }
            apiService.get('/api/TrinhDoNgoaiNgu/GetTrinhDoNgoaiNgu', config, function (result) {

                $scope.AllTrinhDoNgoaiNgu = result.data;

            }, function () {
                console.log('Load Thông tin  failed.');
            });
        }

        function getThoiGianCongTac() {
            var config = {
                params: {
                    msvc: $scope.msvc
                }
            }
            apiService.get('/api/ThoiGianCongTac/getThoiGianCongTac', config, function (result) {

                $scope.AllThoiGianCongTac = result.data;
                angular.forEach($scope.AllThoiGianCongTac, function (item) {
                    var date1 = new Date(item.ThoiGian);
                    item.ThoiGian = $filter('date')(date1, "yyyy-MM-dd");
                });
            }, function () {
                console.log('Load Thông tin  failed.');
            });
        }

        function getQuaTrinhDaoTao() {
            var config = {
                params: {
                    msvc: $scope.msvc
                }
            }
            apiService.get('/api/QuaTrinhDaoTao/getQuaTrinhDaoTao', config, function (result) {

                $scope.AllQuaTrinhDaoTao = result.data;
                angular.forEach($scope.AllQuaTrinhDaoTao, function (item) {
                    var date1 = new Date(item.NamTN);
                    item.NamTN = $filter('date')(date1, "yyyy-MM-dd");
                    var date2 = new Date(item.NamBD);
                    item.NamBD = $filter('date')(date2, "yyyy-MM-dd");
                });
            }, function () {
                console.log('Load Thông tin  failed.');
            });
        }



        function putupdate() {

           
            $ngBootbox.confirm('Bạn chắc muốn Sửa  không').then(function () {
                    apiService.put('/api/vienchuc/update', $scope.AllVienChucChitiet, function (result) {
                        notificationService.displaySuccess("Sửa thành công");
                        $scope.GetLyLichVienChuc();

                        $scope.hide = !$scope.hide;
                        $scope.Edit = !$scope.Edit
                        console.log('Sửa thành công.');
                    }, function () {
                        console.log('sửa thông tin không thành công.');
                    });
                
               
            });
           
        }

        function putupdateTrinhDoNgoaiNgu() {
            $ngBootbox.confirm('Bạn chắc muốn Sửa  không').then(function () {
                angular.forEach($scope.AllTrinhDoNgoaiNgu, function (item) {
                    apiService.put('/api/TrinhDoNgoaiNgu/update',item, function (result) {
                        notificationService.displaySuccess("Sửa thành công");
                        $scope.GetTrinhDoNgoaiNgu();
                        $scope.hide = !$scope.hide;
                        $scope.Edit = !$scope.Edit
                        
                    }, function () {
                        console.log('sửa thông tin không thành công.');
                    });
                });
            });
           

        };

        function putupdateThoiGianCongTac() {
            $ngBootbox.confirm('Bạn chắc muốn Sửa  không').then(function () {
                angular.forEach($scope.AllThoiGianCongTac, function (item) {
                    apiService.put('/api/ThoiGianCongTac/update', item, function (result) {
                        notificationService.displaySuccess("Sửa thành công");
                        $scope.getThoiGianCongTac();
                        console.log(item);
                        $scope.hide = !$scope.hide;
                        $scope.Edit = !$scope.Edit
                        console.log('Sửa thành công.');
                    }, function () {
                        console.log('sửa thông tin không thành công.');
                    });
                });
            });
        };
        function putupdateQuaTrinhDaoTao() {
            $ngBootbox.confirm('Bạn chắc muốn Sửa  không').then(function () {
                angular.forEach($scope.AllQuaTrinhDaoTao, function (item) {

                    apiService.put('/api/QuaTrinhDaoTao/update', item, function (result) {
                        notificationService.displaySuccess("Sửa thành công");
                        $scope.getQuaTrinhDaoTao();

                        $scope.hide = !$scope.hide;
                        $scope.Edit = !$scope.Edit
                        console.log('Sửa thành công.');
                    }, function () {
                        console.log('sửa thông tin không thành công.');
                    });
                });
            });
        };


     
        function kiemtraloi() {
            var c = 0
            angular.forEach($scope.AllQuaTrinhDaoTao, (function (item) {
                var b = new Date(item.NamTN).getTime();
                var a = new Date(item.NamBD).getTime();


                if (a > b||a==b) {


                    c = 1;
                    alert(" Quá trình đào tạo năm bắt đầu phải nhỏ hơn năm tốt nghiệp");
                    console.log(a + "/" + b);


                }
                

            }));
         
            if (c !=1) {
               
                $scope.updateall();
               
            }
            

        }


     
            $scope.updateall = function () {

                var a = 0;


                if ($scope.AllVienChucChitiet.NamBatDauCongTac >= $scope.AllVienChucChitiet.NamNghiHuu) {
                        a = 1;
                        alert("Lý lịch cá nhân năm bắt đầu công tác phải lớn hơn năm nghĩ hưu");
                    }


                if (a != 1) {
                    $ngBootbox.confirm('Bạn chắc muốn sửa  không').then(function () {
                        apiService.put('/api/vienchuc/update', $scope.AllVienChucChitiet, function (result) {
                            notificationService.displaySuccess("Sửa thành công");
                            $scope.GetLyLichVienChuc();
                            $scope.hide = true
                            $scope.Edit = false


                        }, function () {
                            notificationService.displayWarning("Sửa thông tin viên chức không thành công");
                        });
                        angular.forEach($scope.AllTrinhDoNgoaiNgu, function (item) {
                            apiService.put('/api/TrinhDoNgoaiNgu/update', item, function (result) {

                                $scope.GetTrinhDoNgoaiNgu();
                                $scope.hide = true
                                $scope.Edit = false
                                $scope.save = false
                            }, function () {
                                notificationService.displayWarning("Sửa trình độ ngoại ngữ không thành công");
                            });
                        });
                        angular.forEach($scope.AllThoiGianCongTac, function (item) {
                            apiService.put('/api/ThoiGianCongTac/update', item, function (result) {

                                $scope.getThoiGianCongTac();
                                console.log(item);
                                $scope.hide = true
                                $scope.Edit = false
                                $scope.save = false
                            }, function () {
                                notificationService.displayWarning("Sửa thời gian công tác không thành công");
                            });
                        });

                        angular.forEach($scope.AllQuaTrinhDaoTao, function (item) {

                            apiService.put('/api/QuaTrinhDaoTao/update', item, function (result) {
                                $scope.getQuaTrinhDaoTao();
                                $scope.hide = true
                                $scope.Edit = false
                                $scope.save = false
                            }, function () {
                                notificationService.displayWarning("Sửa quá trình đào tạo không thành công");
                            });

                        });
                    });

                }
            };

        



        function putupdateimage() {
            $ngBootbox.confirm('Bạn chắc muốn Sửa  không').then(function () {
                apiService.put('/api/vienchuc/update', $scope.AllVienChucChitiet, function (result) {
                    notificationService.displaySuccess("Sửa ảnh thành công");
                    $scope.GetLyLichVienChuc();
                   
                   
                    console.log('Sửa thành công.');
                }, function () {
                    console.log('sửa thông tin không thành công.');
                });
            });

        }





















        $scope.GetLyLichVienChuc();


      
        $scope.GetTrinhDoNgoaiNgu();
        $scope.getThoiGianCongTac();
        $scope.getQuaTrinhDaoTao();










        $scope.ChooseImage = function () {
            var finder = new CKFinder();
            finder.selectActionFunction = function (fileUrl) {
                $scope.AllVienChucChitiet.Hinh = fileUrl;
            }
            finder.popup();
            $scope.putupdateimage();

            
        }




    }
})(angular.module('platformTH_GV.thongtincanhan'));