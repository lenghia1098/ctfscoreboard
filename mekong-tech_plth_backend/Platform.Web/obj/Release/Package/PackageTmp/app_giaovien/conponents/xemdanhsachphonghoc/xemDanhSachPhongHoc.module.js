﻿/// <reference path="../../../assets/giaovien/libs/angular/angular.js" />

(function () {
    angular.module('platformTH_GV.xemdanhsachphonghoc', ['platformTH_GV.common']).config(config);
    window.appVersion = angular.element(document.getElementsByTagName('html')).attr('data-ver');

    config.$inject = ['$stateProvider', '$urlRouterProvider'];

    function config($stateProvider, $urlRouterProvider) {
        $stateProvider.state('xemdanhsachphonghoc', {
            url: "/xemdanhsachphonghoc/",
            parent: 'base', 
            templateUrl: "app_giaovien/conponents/xemdanhsachphonghoc/xemDanhSachPhongHocView.html?v=" + window.appVersion,
            controller: "xemDanhSachPhongHocController"
        });

    }
})();