﻿/// <reference path="../../../assets/giaovien/libs/angular/angular.js" />

(function () {
    angular.module('platformTH_GV.datngaynghi', ['platformTH_GV.common']).config(config);
    window.appVersion = angular.element(document.getElementsByTagName('html')).attr('data-ver');

    config.$inject = ['$stateProvider', '$urlRouterProvider'];

    function config($stateProvider, $urlRouterProvider) {
        $stateProvider.state('datngaynghi', {
            url: "/datngaynghi/",
            parent: 'base',
            templateUrl: "app_giaovien/conponents/datngaynghi/datNgayNghiView.html?v=" + window.appVersion,
            controller: "datNgayNghiController"
        });

    }
})();