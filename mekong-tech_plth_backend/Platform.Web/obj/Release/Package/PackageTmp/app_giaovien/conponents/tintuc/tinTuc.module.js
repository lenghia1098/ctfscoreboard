﻿/// <reference path="../../../assets/giaovien/libs/angular/angular.js" />

(function () {
    angular.module('platformTH_GV.tintuc', ['platformTH_GV.common']).config(config);

    config.$inject = ['$stateProvider', '$urlRouterProvider'];
    window.appVersion = angular.element(document.getElementsByTagName('html')).attr('data-ver');

    function config($stateProvider, $urlRouterProvider) {
        $stateProvider.state('tintuc', {
            url: "/tintuc/:MaTinTuc",
            parent: 'base',
            templateUrl: "app_giaovien/conponents/tintuc/tinTucView.html?v=" + window.appVersion,
            controller: "tinTucController"
        });

    }
})();