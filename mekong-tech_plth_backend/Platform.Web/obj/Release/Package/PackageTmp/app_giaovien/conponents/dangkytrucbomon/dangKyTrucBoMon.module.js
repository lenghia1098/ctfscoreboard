﻿/// <reference path="../../../assets/giaovien/libs/angular/angular.js" />

(function () {
    angular.module('platformTH_GV.dangkytrucbomon', ['platformTH_GV.common']).config(config);

    config.$inject = ['$stateProvider', '$urlRouterProvider'];
    window.appVersion = angular.element(document.getElementsByTagName('html')).attr('data-ver');

    function config($stateProvider, $urlRouterProvider) {
       
            $stateProvider.state('dangkytrucbomon', {
                url: "/dangkytrucbomon",
                parent: 'base',
                templateUrl: "app_giaovien/conponents/dangkytrucbomon/dangKyTrucBoMonView.html?v=" + window.appVersion,
                controller: "dangKyTrucBoMonController"
            });

    }
})();