﻿(function (app) {
    app.controller('xemThongTinSinhVienTrungTuyenController', xemThongTinSinhVienTrungTuyenController);

    xemThongTinSinhVienTrungTuyenController.$inject = ['$scope', 'apiService', '$stateParams', 'loginService', '$injector', 'notificationService'];
    function xemThongTinSinhVienTrungTuyenController($scope, apiService, $stateParams, loginService, $injector, notificationService) {
        if ($scope.kiemtrarole('SinhVien') === 'SinhVien' && $scope.kiemtrarole('Admin') != 'Admin') {
            loginService.logOut();
            var stateService = $injector.get('$state');
            stateService.go('dangnhap');
            notificationService.displayError("Bạn không có quyền truy cập trang này");
        }
        else {
            //#region
            $scope.bangDiem = [];

            $scope.danhSachTheoNamTrungTuyen = [];
            $scope.danhSachTheoNamTrungTuyenVaSoBaoDanh = [];
            $scope.GetByNamTrungTuyen = GetByNamTrungTuyen;
            $scope.GetByNamTrungTuyenVaSoBaoDanh = GetByNamTrungTuyenVaSoBaoDanh;
            $scope.namtrungtuyen = "";
            $scope.sobaodanh = "";
            function GetByNamTrungTuyen() {
                var config = {
                    params: {
                        namtrungtuyen: $scope.namtrungtuyen,

                    }
                }
                apiService.get('/api/sinhviendauvao/GetByNamTrungTuyen', config, function (result) {

                    $scope.danhSachTheoNamTrungTuyen = result.data;

                }, function () {
                    console.log('Load product failed.');
                });
            }

            function GetByNamTrungTuyenVaSoBaoDanh() {
                var config = {
                    params: {
                        namtrungtuyen: $scope.namtrungtuyen,
                        sobaodanh: $scope.sobaodanh

                    }
                }
                apiService.get('/api/sinhviendauvao/GetByNamTrungTuyenVaSoBaoDanh/', config, function (result) {
                    $scope.danhSachTheoNamTrungTuyenVaSoBaoDanh = result.data;
                }, function () {
                    console.log('Load thông tin failed.');
                });

            }


            $scope.khung1 = false;
            $scope.khung2 = false;
            $scope.bang1 = false;
            $scope.bang2 = false
            $scope.showkhung1 = function () {
                $scope.khung1 = true;
                $scope.khung2 = false;
            };
            $scope.showkhung2 = function () {
                $scope.khung1 = false;
                $scope.khung2 = true;
            };
            $scope.showbang1 = function () {
                $scope.bang1 = true;
                $scope.bang2 = false
            }
            $scope.showbang2 = function () {
                $scope.bang1 = false;
                $scope.bang2 = true
            }

            //#endregion

        }
        



        
      



    }
       
})(angular.module('platformTH_GV.xemthongtinsinhvientrungtuyen'));