﻿/// <reference path="../../../assets/giaovien/libs/angular/angular.js" />


(function () {
    angular.module('platformTH_GV.nhapdiemtructuyen', ['platformTH_GV.common']).config(config);
    window.appVersion = angular.element(document.getElementsByTagName('html')).attr('data-ver');

    config.$inject = ['$stateProvider', '$urlRouterProvider'];

    function config($stateProvider, $urlRouterProvider) {
        $stateProvider.state('nhapdiemtructuyen', {
            url: "/nhapdiemtructuyen/:msvc",
            parent: 'base',
            templateUrl: "app_giaovien/conponents/nhapdiemtructuyen/nhapDiemView.html?v=" + window.appVersion,
            controller: "nhapDiemTrucTuyenController"
        })
            .state('nhapdiemtructuyengiuaki', {
            url: "/nhapdiemtructuyengiuaki/:MaNhom,:MaMH,:MaLop",
            parent: 'base',
                templateUrl: "app_giaovien/conponents/nhapdiemtructuyen/nhapDiemGiuaKiView.html?v=" + window.appVersion,
                controller: "nhapDiemTrucTuyenGiuaKiController"
            }).state('nhapdiemtructuyencuoiki', {
                url: "/nhapdiemtructuyencuoiki/:MaNhom,:MaMH,:MaLop",
            parent: 'base',
                templateUrl: "app_giaovien/conponents/nhapdiemtructuyen/nhapDiemCuoiKiView.html?v=" + window.appVersion,
                controller: "nhapDiemTrucTuyenCuoiKiController"
        })

    }
})();