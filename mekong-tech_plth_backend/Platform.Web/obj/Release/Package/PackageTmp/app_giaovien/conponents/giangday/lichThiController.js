﻿
(function (app) {
    app.controller('lichThiController', lichThiController);
    lichThiController.$inject = ['$scope', 'apiService', '$stateParams', 'loginService', '$injector', 'notificationService'];
    function lichThiController($scope, apiService, $stateParams,loginService, $injector, notificationService) {
        if ($scope.kiemtrarole('SinhVien') === 'SinhVien' && $scope.kiemtrarole('Admin') != 'Admin') {
            loginService.logOut();
            var stateService = $injector.get('$state');
            stateService.go('dangnhap');
            notificationService.displayError("Bạn không có quyền truy cập trang này");
        }
        else {
            //#region
            $scope.thongTincaNhan = [];
            $scope.lichThi = [];
            $scope.myvalue = false;
            $scope.myvaluemonhoc = false;
            $scope.mssv = "";




            $scope.tenMH = "";
            $scope.allMonHoc = [];
            $scope.LichThiMH = [];
            $scope.TTMH = [];
            $scope.getThongTinCaNhan = getThongTinCaNhan;
            $scope.getLichThi = getLichThi;
            $scope.getMonHoc = getMonHoc;
            $scope.getLichThiMH = getLichThiMH;
            $scope.getTTMH = getTTMH;
            $scope.hocKyHienTai = [];

            function getThongTinCaNhan() {
                var config = {
                    params: {
                        Id: $scope.mssv,

                    }
                }
                apiService.get('/api/sinhvien/getTTCN/', config, function (result) {
                    $scope.thongTincaNhan = result.data;
                }, function () {
                    console.log('Load thông tin failed.');
                });

            }


            $scope.gethockyhientai = gethockyhientai;


            $scope.hocKy = [];
            function gethockyhientai() {

                apiService.get('/api/lichthi/getHocKyHienTai/', null, function (result) {
                    $scope.hocKyHienTai = result.data[result.data.length - 1];
                    $scope.hocKy = result.data[result.data.length - 1].HocKy;
                    $scope.namhoc = result.data[result.data.length - 1].NamHoc;
                }, function () {
                    console.log('Load thông tin failed.');
                });

            }




            function getLichThi() {
                var config = {
                    params: {
                        mssv: $scope.mssv,
                        hocKy: $scope.hocKy,
                        namhoc: $scope.namhoc
                    }
                }
                apiService.get('/api/lichthi/getLichThi/', config, function (result) {
                    $scope.lichThi = result.data;
                }, function () {
                    console.log('Load thông tin failed.');
                });

            }



            function getMonHoc() {
                var config = {
                    params: {
                        NamHoc: $scope.namhoc,
                        hocKy: $scope.hocKy

                    }
                }
                apiService.get('/api/lichthi/getAllMonHoc', config, function (result) {

                    $scope.allMonHoc = result.data;

                }, function () {
                    console.log('Load thông tin failed.');
                });

            }





            function getLichThiMH(tenMH) {

                var config = {
                    params: {
                        tenMH: tenMH,
                        NamHoc: $scope.namhoc,
                        hocKy: $scope.hocKy

                    }
                }
                apiService.get('/api/lichthi/getLichThiMH/', config, function (result) {
                    $scope.lichThi = "";
                    $scope.LichThiMH = result.data;

                }, function () {
                    console.log('Load thông tin failed.');
                });

            }
            function getTTMH(MaMH, MaNhom, malop) {
                $scope.showallTTMH = true;
                var config = {
                    params: {
                        maMH: MaMH,
                        maNhom: MaNhom,
                        malop: malop

                    }
                }
                apiService.get('/api/lichthi/getTTMH/', config, function (result) {
                    $scope.TTMH = result.data;

                }, function () {
                    console.log('Load thông tin failed.');
                });

            }





            $scope.showAlert = function () {
                if ($scope.myvalue = true) {
                    $scope.myvaluemonhoc = false;
                }
                else {
                    $scope.myvalue = true;
                }
                if ($scope.showall = true) {
                    $scope.showall = false
                }
                $scope.showallTTMH = false;
                $scope.showtheomonhoc = false;
                $scope.LichThiMH = "";
            };

            $scope.showmonhoc = function () {
                if ($scope.myvaluemonhoc = true) {
                    $scope.myvalue = false;
                }


                else {
                    $scope.myvaluemonhoc = true;
                }
                if ($scope.showall = true) {
                    $scope.showall = false
                }
                $scope.showallTTMH = false;

            };

            $scope.clickshowall = function () {
                $scope.showall = true;
            };


            $scope.clickshowalltheomon = function () {
                //$scope.showall = true;
                $scope.showallTTMH = false;
                $scope.showtheomonhoc = true;
            };


            $scope.clickshowallTTMH = function () {

                if ($scope.showall = true) {
                    //$scope.showall = false
                    $scope.showallTTMH = true;
                }


            };




            //$scope.getLichThi();
            $scope.gethockyhientai();
         //$scope.getMonHoc();

            //#endregion
        }
        
    }
})(angular.module('platformTH_GV.giangday'));
