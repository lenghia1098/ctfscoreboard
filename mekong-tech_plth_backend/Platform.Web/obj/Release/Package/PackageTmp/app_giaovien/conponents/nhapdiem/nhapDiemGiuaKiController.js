﻿(function (app) {
    app.controller('nhapDiemGiuaKiController', nhapDiemGiuaKiController);

    nhapDiemGiuaKiController.$inject = ['$scope', 'apiService', '$stateParams', '$ngBootbox', 'notificationService', '$injector','loginService'];
    function nhapDiemGiuaKiController($scope, apiService, $stateParams, $ngBootbox, notificationService, $injector, loginService) {
        if ($scope.kiemtrarole('SinhVien') === 'SinhVien' && $scope.kiemtrarole('Admin') != 'Admin') {
            loginService.logOut();
            var stateService = $injector.get('$state');
            stateService.go('dangnhap');
            notificationService.displayError("Bạn không có quyền truy cập trang này");
        }
        else {
            //#region
            $scope.MaNhom = $stateParams.MaNhom;
            $scope.MaMH = $stateParams.MaMH;
            $scope.MaLop = $stateParams.MaLop;


            $scope.TTMonHoc = {};
            $scope.getTTMonHoc = getTTMonHoc;
            function getTTMonHoc() {
                var config = {
                    params: {
                        Id: $scope.MaMH
                    }
                }
                apiService.get('/api/monhoc/getbyid/', config, function (result) {

                    $scope.TTMonHoc = result.data;

                }, function () {
                    console.log('Load thông tin failed.');
                });

            }
            $scope.getTTMonHoc();





            $scope.thogTinNhom = [];
            $scope.getThongTinNhom = getThongTinNhom;
            function getThongTinNhom() {
                var config = {
                    params: {
                        maNhom: $scope.MaNhom,
                        maMH: $scope.MaMH,
                        malop: $scope.MaLop,

                    }
                }
                apiService.get('/api/thoikhoabieu/xemThongTinNhom', config, function (result) {

                    $scope.thogTinNhom = result.data;


                }, function () {
                    console.log('Load thông tin failed.');
                });
            } $scope.getThongTinNhom()



            $scope.themDiem = themDiem;
            $scope.Diem = [];
            $scope.bangdiem = true;

            function themDiem() {

                $ngBootbox.confirm('Xác nhận thêm điểm').then(function () {
                    $scope.bangdiem = false;


                    angular.forEach($scope.thogTinNhom, function (item) {
                        item.GiangVienNhapGiuaKi = $scope.authentication.userName;
                        var d = new Date()
                        item.NgayNhapGiuaKi = d.toLocaleTimeString();
                        apiService.post('api/ketqua/themdiemgiuaki', item, function (result) {


                            notificationService.displaySuccess("Thêm điểm giữa kì thành công cho sinh viên " + item.MaSoSV);


                        }, function () {
                            notificationService.displayWarning('Thêm không thành công');
                        });
                    });


                });
            }
            //#endregion

        }
  
      
    }
  
})(angular.module('platformTH_GV.nhapdiem'));