﻿/// <reference path="../../../assets/giaovien/libs/angular/angular.js" />


(function () {
    angular.module('platformTH_GV.thaydoikhunggiohoc', ['platformTH_GV.common']).config(config);

    config.$inject = ['$stateProvider', '$urlRouterProvider'];
    window.appVersion = angular.element(document.getElementsByTagName('html')).attr('data-ver');

    function config($stateProvider, $urlRouterProvider) {
        $stateProvider.state('thaydoikhunggiohoc', {
            url: "/thaydoikhunggiohoc",

            parent: 'base',
            templateUrl: "app_giaovien/conponents/thaydoikhunggiohoc/thayDoiKhungGioHocView.html?v=" + window.appVersion,
            controller: "thayDoiKhungGioHocController"
        });

    }
})();