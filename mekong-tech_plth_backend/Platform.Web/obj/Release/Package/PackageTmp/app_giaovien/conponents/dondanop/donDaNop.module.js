﻿/// <reference path="../../../assets/giaovien/libs/angular/angular.js" />

(function () {
    angular.module('platformTH_GV.dondanop', ['platformTH_GV.common']).config(config);
    window.appVersion = angular.element(document.getElementsByTagName('html')).attr('data-ver');

    config.$inject = ['$stateProvider', '$urlRouterProvider'];

    function config($stateProvider, $urlRouterProvider) {
        $stateProvider.state('dondanop', {
            url: "/dondanop",
            parent: 'base',
            templateUrl: "app_giaovien/conponents/dondanop/donDaNopView.html?v=" + window.appVersion,
            controller: "donDaNopController"
        });

    }
})();