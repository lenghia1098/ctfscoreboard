﻿/// <reference path="../../../assets/giaovien/libs/angular/angular.js" />

(function () {
    angular.module('platformTH_GV.themthongtinvienchuc', ['platformTH_GV.common']).config(config);

    config.$inject = ['$stateProvider', '$urlRouterProvider'];
    window.appVersion = angular.element(document.getElementsByTagName('html')).attr('data-ver');

    function config($stateProvider, $urlRouterProvider) {
       
            $stateProvider.state('themthongtinvienchuc', {
                url: "/themthongtinvienchuc",
                parent: 'base',
                templateUrl: "app_giaovien/conponents/themthongtinvienchuc/themThongTinVienChuc.html?v=" + window.appVersion,
                controller: "themThongTinVienChucController"
            });

    }
})();