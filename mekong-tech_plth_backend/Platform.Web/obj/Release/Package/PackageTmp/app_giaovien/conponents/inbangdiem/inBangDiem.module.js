﻿/// <reference path="../../../assets/giaovien/libs/angular/angular.js" />


(function () {
    angular.module('platformTH_GV.inbangdiem', ['platformTH_GV.common']).config(config);

    config.$inject = ['$stateProvider', '$urlRouterProvider'];
    window.appVersion = angular.element(document.getElementsByTagName('html')).attr('data-ver');

    function config($stateProvider, $urlRouterProvider) {
        $stateProvider.state('inbangdiem', {
            url: "/inbangdiem",
            parent: 'base',
           // templateUrl: "app_giaovien/conponents/inbangdiem/inBangDiemA3View.html",
            templateUrl: "app_giaovien/conponents/inbangdiem/inBangDiemView.html?v=" + window.appVersion,
            controller: "inBangDiemController"
        });

    }
})();