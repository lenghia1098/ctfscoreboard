﻿(function (app) {
    app.controller('nhapSinhVienTrungTuyenController', nhapSinhVienTrungTuyenController);

    nhapSinhVienTrungTuyenController.$inject = ['$scope', 'apiService', '$stateParams','notificationService'];
    function nhapSinhVienTrungTuyenController($scope, apiService, $stateParams, notificationService) {
        $scope.SinhVienDauVao = {
           
        };

        $scope.kiemtra = function () {
            $scope.getAllsinhdauvaoAll();
            setTimeout($scope.addSinhVien, 100);
        }

        $scope.addSinhVien = function () {
            var a = 0;
            angular.forEach($scope.sinhdauvaoAll, function (item) {
                if (item.SoBaoDanh == $scope.SinhVienDauVao.SoBaoDanh) {
                    alert("Mã số báo danh đã tồn tại ")
                    a = 1;

                } 
              
            });
            if (a != 1) {
                apiService.post('api/sinhviendauvao/create', $scope.SinhVienDauVao,
                    function (result) {
                        notificationService.displaySuccess("Thêm thành công")
                        $scope.SinhVienDauVao = null;
                    }, function () {
                        notificationService.displayWarning("Thêm không thành công")
                    });

            }
           
            
              
        }


      



        $scope.Huy = function () {
            $scope.SinhVienDauVao = null;
        }


        $scope.sinhdauvaoAll = [];
       


        $scope.getAllsinhdauvaoAll = getAllsinhdauvaoAll;





        function getAllsinhdauvaoAll() {
            apiService.get('/api/sinhviendauvao/getall', null, function (result) {

                $scope.sinhdauvaoAll = result.data;
                
            }, function () {
                console.log('Load thông tin failed.');

            });
        }








        $scope.SinhVienDauVaoExcel = [];
        $scope.READ = function () {

            $scope.getAllsinhdauvaoAll();
            $scope.SinhVienDauVaoExcel = [];
            /*Checks whether the file is a valid excel file*/
            var regex = /^([a-zA-Z0-9\s_\\.\-:])+(.xlsx|.xls)$/;
            var xlsxflag = false; /*Flag for checking whether excel is .xls format or .xlsx format*/
            if ($("#ngexcelfile").val().toLowerCase().indexOf(".xlsx") > 0) {
                xlsxflag = true;
            }
            var reader = new FileReader();
            reader.onload = function (e) {
                var data = e.target.result;
                if (xlsxflag) {
                    var workbook = XLSX.read(data, { type: 'binary' });
                }
                else {
                    var workbook = XLS.read(data, { type: 'binary' });
                }

                var sheet_name_list = workbook.SheetNames;
                var cnt = 0;
                sheet_name_list.forEach(function (y) { /*Iterate through all sheets*/

                    if (xlsxflag) {
                        var exceljson = XLSX.utils.sheet_to_json(workbook.Sheets[y]);
                    }
                    else {
                        var exceljson = XLS.utils.sheet_to_row_object_array(workbook.Sheets[y]);
                    }
                    if (exceljson.length > 0) {
                        for (var i = 0; i < exceljson.length; i++) {
                            $scope.SinhVienDauVaoExcel.push(exceljson[i]);
                            $scope.$apply();
                        }
                    }
                });
            }
            if (xlsxflag) {
                reader.readAsArrayBuffer($("#ngexcelfile")[0].files[0]);
            }
            else {
                reader.readAsBinaryString($("#ngexcelfile")[0].files[0]);
            }
        };



        $scope.AddExcel = function () {
            $scope.getAllsinhdauvaoAll();
            $scope.bang1 = true;
            $scope.bang2 = false;
            $scope.btnhuy = false;
            var a = 0;
            angular.forEach($scope.sinhdauvaoAll, function (item) {
                angular.forEach($scope.SinhVienDauVaoExcel, function (item1) {
                    if (item.SoBaoDanh == item1.SoBaoDanh) {
                        a = 1;
                       
                        
                    }
                });
               
               
            });
            if (a == 1) {
                alert("Mã số báo danh đã tồn tại ")
                $scope.btntailen = false;
                angular.element("input[type='file']").val(null);
            }
            else if (a != 1) {
                apiService.post('api/sinhviendauvao/createExcel', $scope.SinhVienDauVaoExcel,

                    function (result) {
                        $scope.getAllsinhdauvaoAll();
                        notificationService.displaySuccess("Thêm thành công")
                        $scope.SinhVienDauVaoExcel = "";
                        $scope.btntailen = false;
                        angular.element("input[type='file']").val(null);
                    }, function () {
                        notificationService.displayWarning("Thêm không thành công")
                    })
                    ;
            }
           
        }
        

        $scope.bang1 = true;
        $scope.bang2 = false;
        $scope.btnhuy = false;
        $scope.btntailen = false;
        $scope.showbang2 = function () {
            $scope.bang1 = false;
            $scope.bang2 = true;
            $scope.btnhuy = true;
            $scope.btntailen = true;
        }
        $scope.HuyExcel = function () {
            $scope.bang1 = true;
            $scope.bang2 = false;
            $scope.btnhuy = false;
            $scope.btntailen = false;
           
        }

        $scope.getAllsinhdauvaoAll();
    }
   
    
       
})(angular.module('platformTH_GV.nhapsinhvientrungtuyen'));