﻿(function (app) {
    app.controller('chiTietDonDaNopController', chiTietDonDaNopController);

    function chiTietDonDaNopController($scope, apiService, $stateParams,loginService, $injector, notificationService) {
        if ($scope.kiemtrarole('SinhVien') === 'SinhVien' && $scope.kiemtrarole('Admin') != 'Admin') {
            loginService.logOut();
            var stateService = $injector.get('$state');
            stateService.go('dangnhap');
            notificationService.displayError("Bạn không có quyền truy cập trang này");
        }
        else {
            //#region
            $scope.chiTietDonDaNop = [];
            $scope.MaDon = $stateParams.MaDon;


            $scope.getDonDaNop = getDonDaNop;


            function getDonDaNop() {
                var config = {
                    params: {
                        MaDon: $scope.MaDon
                    }
                }
                apiService.get('/api/donvienchuc/GetChiTietDonDaNopVienChuc/', config, function (result) {
                    $scope.chiTietDonDaNop = result.data;
                }, function () {
                    console.log('Load thông tin failed.');
                });

            }
            $scope.getDonDaNop();
            //#endregion

        }
        

    }
})(angular.module('platformTH_GV.chitietdondanop'));