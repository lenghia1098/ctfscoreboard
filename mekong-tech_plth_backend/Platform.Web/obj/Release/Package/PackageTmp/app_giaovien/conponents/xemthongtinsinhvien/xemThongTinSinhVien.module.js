﻿/// <reference path="../../../assets/giaovien/libs/angular/angular.js" />


(function () {
    angular.module('platformTH_GV.xemthongtinsinhvien', ['platformTH_GV.common']).config(config);
    window.appVersion = angular.element(document.getElementsByTagName('html')).attr('data-ver');

    config.$inject = ['$stateProvider', '$urlRouterProvider'];

    function config($stateProvider, $urlRouterProvider) {
        $stateProvider.state('xemthongtinsinhvien', {
            url: "/xemthongtinsinhvien",
            parent: 'base',
            templateUrl: "app_giaovien/conponents/xemthongtinsinhvien/xemthongtinsinhvienView.html?v=" + window.appVersion,
            controller: "xemthongtinsinhvienController"
        });

    }
})();