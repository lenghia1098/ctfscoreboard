﻿(function (app) {
    app.controller('viecDaLamController', viecDaLamController);

    function viecDaLamController($scope, apiService, $stateParams, $ngBootbox, notificationService, loginService, $injector) {
        if ($scope.kiemtrarole('SinhVien') === 'SinhVien' && $scope.kiemtrarole('Admin') != 'Admin') {
            loginService.logOut();
            var stateService = $injector.get('$state');
            stateService.go('dangnhap');
            notificationService.displayError("Bạn không có quyền truy cập trang này");
        }
        else {
            //#region
            $scope.viecDaLam = [];
            $scope.page = 0;
            $scope.pagesCount = 0;
            $scope.msvc = $scope.authentication.userName;
            $scope.getViecDaLam = getViecDaLam;
            $scope.luu = {}
            $scope.selected = [];

            function getViecDaLam(page) {
                page = page || 0;
                var config = {
                    params: {
                        page: page,
                        msvc: $scope.msvc,
                        pageSize: 5
                    }
                }
                apiService.get('/api/ViecPhaiLamVienChuc/ViecDaLamVienChuc/', config, function (result) {
                    $scope.viecDaLam = result.data.Items;
                    $scope.page = result.data.Page;
                    $scope.pagesCount = result.data.TotalPages;
                    $scope.TotalCount = result.data.TotalCount;
                }, function () {
                    console.log('Load thông tin failed.');
                });

            }



            $scope.exist = function (item) {
                return $scope.selected.indexOf(item) > -1;

            }

            $scope.toge = function (item) {
                var idx = $scope.selected.indexOf(item);
                if (idx > -1) {
                    $scope.selected.splice(idx, 1);

                }
                else {
                    $scope.selected.push(item);
                    console.log($scope.selected);
                }
            }


            $scope.checkAll = function () {
                if ($scope.selectedAll) {
                    angular.forEach($scope.viecDaLam, function (item) {
                        idx = $scope.selected.indexOf(item);
                        if (idx >= 0) {
                            return true;
                        } else {
                            $scope.selected.push(item);
                        }
                    })
                }
                else {
                    $scope.selected = [];
                }
            }



            $scope.loadId = loadId;

            function loadId(id) {

                var config = {
                    params: {
                        mscv: id,

                    }
                }

                apiService.get('api/VienChuc_ViecPhaiLam/getID/', config, function (result) {

                    $scope.luu = result.data;
                    console.log($scope.luu.MaSoCongViec);
                    $scope.DeleteViecDaLam()

                }, function (error) {
                    console.log('Load product failed.');
                });
            }

            $scope.allID = [];
            $scope.getAllId = getAllID;
            function getAllID() {
                var config = {
                    params: {
                        msvc: $scope.msvc,

                    }
                }

                apiService.get('api/VienChuc_ViecPhaiLam/getIdAll/', config, function (result) {

                    $scope.allID = result.data;



                }, function (error) {
                    console.log('Load product failed.');
                });
            }


            $scope.DeleteViecDaLam = DeleteViecDaLam;


            function DeleteViecDaLam() {

                $ngBootbox.confirm('Bạn chắc muốn xóa không').then(function () {

                    apiService.put('api/VienChuc_ViecPhaiLam/update', $scope.luu, function (result) {

                        notificationService.displaySuccess("Xóa thành công");
                        $scope.getViecDaLam();


                    }, function () {
                        notificationService.displayWarning('Xóa không thành công');
                    })
                });
            }



            $scope.DeleteAllViecDaLam = DeleteAllViecDaLam;

            function DeleteAllViecDaLam() {
                if ($scope.selected == "") {
                    alert("Bạn chưa chọn");
                } else {
                    $ngBootbox.confirm('Bạn chắc muốn xóa không').then(function () {
                        angular.forEach($scope.selected, function (item) {
                            item.NguoiXoa = $scope.authentication.userName;
                            apiService.put('api/VienChuc_ViecPhaiLam/updateAll', item, function (result) {

                                //notificationService.displaySuccess("Xóa thành công");
                                $scope.getViecDaLam();


                            }, function () {
                                notificationService.displaySuccess('Xóa không thành công');
                            });
                        });
                    });

                }
            };

            $scope.getViecDaLam();
            $scope.getAllId();
            //#endregion

        }
       
    }
})(angular.module('platformTH_GV.viecdalam'));