﻿/// <reference path="../../../assets/giaovien/libs/angular/angular.js" />


(function () {
    angular.module('platformTH_GV.xemlichgacthicanhan', ['platformTH_GV.common']).config(config);
    window.appVersion = angular.element(document.getElementsByTagName('html')).attr('data-ver');

    config.$inject = ['$stateProvider', '$urlRouterProvider'];

    function config($stateProvider, $urlRouterProvider) {
        $stateProvider.state('xemlichgacthicanhan', {
            url: "/xemlichgacthicanhan/:Id",
            parent: 'base',
            templateUrl: "app_giaovien/conponents/xemlichgacthicanhan/xemLichGacThiCaNhanView.html?v=" + window.appVersion,
            controller: "XemLichGacThiCaNhanController"
        });

    }
})();