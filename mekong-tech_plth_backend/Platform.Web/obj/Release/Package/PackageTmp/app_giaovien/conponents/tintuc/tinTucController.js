﻿(function (app) {
    app.controller('tinTucController', tinTucController);

    function tinTucController($scope, apiService, $stateParams, loginService, $injector, notificationService) {
        if ($scope.kiemtrarole('SinhVien') === 'SinhVien' && $scope.kiemtrarole('Admin') != 'Admin') {
            loginService.logOut();
            var stateService = $injector.get('$state');
            stateService.go('dangnhap');
            notificationService.displayError("Bạn không có quyền truy cập trang này");
        }
        else {
            //#region
            $scope.tintuc = [];
            $scope.MaTinTuc = $stateParams.MaTinTuc;


            $scope.getchitiettintuc = getchitiettintuc;


            function getchitiettintuc() {
                var config = {
                    params: {
                        MaTinTuc: $scope.MaTinTuc
                    }
                }
                apiService.get('/api/tintuc/chitiettt/', config, function (result) {
                    $scope.tintuc = result.data;
                }, function () {
                    console.log('load thông tin failed.');
                });

            }
            $scope.getchitiettintuc();
            //#endregion

        }
       

    }
})(angular.module('platformTH_GV.tintuc'));