﻿/// <reference path="../../../assets/giaovien/libs/angular/angular.js" />
(function () {
    angular.module('platformTH_GV.thongkevangmat', ['platformTH_GV.common']).config(config);
    window.appVersion = angular.element(document.getElementsByTagName('html')).attr('data-ver');

    config.$inject = ['$stateProvider', '$urlRouterProvider'];

    function config($stateProvider, $urlRouterProvider) {
        $stateProvider.state('thongkevangmat', {
            url: "/thongkevangmat",
            parent: 'base',
            templateUrl: "app_giaovien/conponents/thongkevangmat/thongKeVangMatView.html?v=" + window.appVersion,
            controller: "thongKeVangMatController"
        });

    }
})();