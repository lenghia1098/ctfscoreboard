﻿(function (app) {
    app.controller('nhapDiemCuoiKiController', nhapDiemCuoiKiController);

    nhapDiemCuoiKiController.$inject = ['$scope', 'apiService', '$stateParams', '$ngBootbox', 'notificationService', '$injector', '$q', '$filter', 'loginService'];
    function nhapDiemCuoiKiController($scope, apiService, $stateParams, $ngBootbox, notificationService, $injector, $q, $filter, loginService) {
        if ($scope.kiemtrarole('SinhVien') === 'SinhVien' && $scope.kiemtrarole('Admin') != 'Admin') {
            loginService.logOut();
            var stateService = $injector.get('$state');
            stateService.go('dangnhap');
            notificationService.displayError("Bạn không có quyền truy cập trang này");
        }
        else {
            //#region
            $scope.MaNhom = $stateParams.MaNhom;
            $scope.MaMH = $stateParams.MaMH;
            $scope.MaLop = $stateParams.MaLop;



            $scope.getHocKyHienTai = getHocKyHienTai;
            $scope.hocKy;
            $scope.namhoc;
            $scope.TyLeKiemTra;

            $scope.TTMonHoc = {};
            $scope.getTTMonHoc = getTTMonHoc;
            function getTTMonHoc() {
                var config = {
                    params: {
                        Id: $scope.MaMH
                    }
                }
                apiService.get('/api/monhoc/getbyid/', config, function (result) {

                    $scope.TTMonHoc = result.data;
                    $scope.TyLeKiemTra = result.data.TyLeKiemTra;


                }, function () {
                    console.log('Load thông tin failed.');
                });

            }
            $scope.getTTMonHoc();

            function getHocKyHienTai() {
                apiService.get('/api/hockyhientai/getall/', null, function (result) {

                    $scope.hocKyHienTai = result.data[result.data.length - 1];
                    $scope.hocKy = result.data[result.data.length - 1].HocKy;
                    $scope.namhoc = result.data[result.data.length - 1].NamHoc;

                    $scope.getThongTinNhom();

                }, function () {
                    console.log('Load thông tin failed.');
                });

            }
            $scope.getHocKyHienTai();




            $scope.thogTinNhom = [];
            $scope.getThongTinNhom = getThongTinNhom;
            function getThongTinNhom() {
                if ($scope.TyLeKiemTra > 0) {
                    var config = {
                        params: {
                            maNhom: $scope.MaNhom,
                            maMH: $scope.MaMH,
                            malop: $scope.MaLop,
                            hocKy: $scope.hocKy,
                            namHoc: $scope.namhoc
                        }
                    }
                    apiService.get('/api/ketqua/getBangDiemMonHoc', config, function (result) {

                        $scope.thogTinNhom = result.data;


                    }, function () {
                        console.log('Load product failed.');
                    });

                }
                else {
                    var config = {
                        params: {
                            maNhom: $scope.MaNhom,
                            maMH: $scope.MaMH,
                            malop: $scope.MaLop,

                        }
                    }
                    apiService.get('/api/thoikhoabieu/xemThongTinNhom', config, function (result) {

                        $scope.thogTinNhom = result.data;


                    }, function () {
                        console.log('Load thông tin failed.');
                    });
                }


            }



            $scope.themDiem = themDiem;

            $scope.bangdiem = true;
            function themDiem() {
                if ($scope.TyLeKiemTra > 0) {
                    $ngBootbox.confirm('Xác nhận thêm điểm').then(function () {
                        $scope.bangdiem = false;
                        angular.forEach($scope.thogTinNhom, function (item) {
                            item.DiemTongKet = ((item.DiemChuyenCan * $scope.TTMonHoc.TyLeChuyenCan) + (item.DiemKiemTra * $scope.TTMonHoc.TyLeKiemTra) + (item.DiemThucHanh * $scope.TTMonHoc.TyLeThucHanh) + (item.DiemSeminar * $scope.TTMonHoc.TyLeSeminar) + (item.DiemThi * $scope.TTMonHoc.TyLeThi)) / 100
                            if ((((item.DiemChuyenCan * $scope.TTMonHoc.TyLeChuyenCan) + (item.DiemKiemTra * $scope.TTMonHoc.TyLeKiemTra) + (item.DiemThucHanh * $scope.TTMonHoc.TyLeThucHanh) + (item.DiemSeminar * $scope.TTMonHoc.TyLeSeminar) + (item.DiemThi * $scope.TTMonHoc.TyLeThi)) / 100) >= $scope.TTMonHoc.DiemDau) {
                                item.Dau = true;
                            }
                            if ((((item.DiemChuyenCan * $scope.TTMonHoc.TyLeChuyenCan) + (item.DiemKiemTra * $scope.TTMonHoc.TyLeKiemTra) + (item.DiemThucHanh * $scope.TTMonHoc.TyLeThucHanh) + (item.DiemSeminar * $scope.TTMonHoc.TyLeSeminar) + (item.DiemThi * $scope.TTMonHoc.TyLeThi)) / 100) < $scope.TTMonHoc.DiemDau) {
                                item.Dau = false;
                            }
                            item.GiangVienNhapCuoiKi = $scope.authentication.userName;
                            var d = new Date()
                            item.NgayNhapCuoiKi = d.toLocaleTimeString();
                            apiService.put('api/ketqua/themdiemcuoiki', item, function (result) {


                                notificationService.displaySuccess("Thêm điểm cuối kì thành công cho sinh viên " + item.MaSoSV);



                            }, function () {
                                notificationService.displayWarning('Thêm không thành công');
                            });

                        });


                    });
                }
                else {
                    $ngBootbox.confirm('Xác nhận thêm điểm').then(function () {
                        $scope.bangdiem = false;


                        angular.forEach($scope.thogTinNhom, function (item) {
                            item.DiemTongKet = ((item.DiemChuyenCan * $scope.TTMonHoc.TyLeChuyenCan) + (item.DiemKiemTra * $scope.TTMonHoc.TyLeKiemTra) + (item.DiemThucHanh * $scope.TTMonHoc.TyLeThucHanh) + (item.DiemSeminar * $scope.TTMonHoc.TyLeSeminar) + (item.DiemThi * $scope.TTMonHoc.TyLeThi)) / 100
                            if ((((item.DiemChuyenCan * $scope.TTMonHoc.TyLeChuyenCan) + (item.DiemKiemTra * $scope.TTMonHoc.TyLeKiemTra) + (item.DiemThucHanh * $scope.TTMonHoc.TyLeThucHanh) + (item.DiemSeminar * $scope.TTMonHoc.TyLeSeminar) + (item.DiemThi * $scope.TTMonHoc.TyLeThi)) / 100) >= $scope.TTMonHoc.DiemDau) {
                                item.Dau = true;
                            }
                            if ((((item.DiemChuyenCan * $scope.TTMonHoc.TyLeChuyenCan) + (item.DiemKiemTra * $scope.TTMonHoc.TyLeKiemTra) + (item.DiemThucHanh * $scope.TTMonHoc.TyLeThucHanh) + (item.DiemSeminar * $scope.TTMonHoc.TyLeSeminar) + (item.DiemThi * $scope.TTMonHoc.TyLeThi)) / 100) < $scope.TTMonHoc.DiemDau) {
                                item.Dau = false;
                            }
                            item.GiangVienNhapCuoiKi = $scope.authentication.userName;
                            var d = new Date()
                            item.NgayNhapCuoiKi = d.toLocaleTimeString();
                            apiService.post('api/ketqua/themdiemgiuaki', item, function (result) {


                                notificationService.displaySuccess("Thêm điểm cuối kì thành công cho sinh viên " + item.MaSoSV);


                            }, function () {
                                notificationService.displayWarning('Thêm không thành công');
                            });
                        });


                    });
                }

            }
            $scope.huy = function () {
                var stateService = $injector.get('$state');

                stateService.go('nhapdiem(msvc: $scope.authentication.userName)');
            }


            $scope.loctongsinhvienchuanhap = function (sochuanhap) {
                var somoi;
                if (sochuanhap == null) {
                    return 0
                }
                else {
                    var loc
                    loc = sochuanhap / 4
                    var n = loc.toFixed(2)
                    var substr = n.split('.');
                    var sotruoc = substr[0];
                    somoi = parseInt(sotruoc);

                    if (substr[1] > 0) {
                        somoi += 1
                    }
                }
                return somoi;
            }
            $scope.loctongsinhviendanhap = function (sochuanhap, dodai) {
                var somoi;
                var sodanhap;
                if (sochuanhap == null) {
                    return dodai
                }
                else {
                    var loc
                    loc = sochuanhap / 4
                    var n = loc.toFixed(2)
                    var substr = n.split('.');
                    var sotruoc = substr[0];
                    somoi = parseInt(sotruoc);

                    if (substr[1] > 0) {
                        somoi += 1
                    }
                    sodanhap = dodai - somoi

                }
                return sodanhap;
            }


            $scope.countsvDau = function () {
                var cnt = 0;

                angular.forEach($scope.thogTinNhom, function (item) {
                    if ((((item.DiemChuyenCan * $scope.TTMonHoc.TyLeChuyenCan) + (item.DiemKiemTra * $scope.TTMonHoc.TyLeKiemTra) + (item.DiemThucHanh * $scope.TTMonHoc.TyLeThucHanh) + (item.DiemSeminar * $scope.TTMonHoc.TyLeSeminar) + (item.DiemThi * $scope.TTMonHoc.TyLeThi)) / 100) >= $scope.TTMonHoc.DiemDau) {
                        cnt += 1
                    }
                });


                return cnt;
            }
            $scope.countsvRot = function () {
                var cnt = 0;

                angular.forEach($scope.thogTinNhom, function (item) {
                    if (item.DiemThi != null) {
                        if ((((item.DiemChuyenCan * $scope.TTMonHoc.TyLeChuyenCan) + (item.DiemKiemTra * $scope.TTMonHoc.TyLeKiemTra) + (item.DiemThucHanh * $scope.TTMonHoc.TyLeThucHanh) + (item.DiemSeminar * $scope.TTMonHoc.TyLeSeminar) + (item.DiemThi * $scope.TTMonHoc.TyLeThi)) / 100) < $scope.TTMonHoc.DiemDau) {
                            cnt += 1
                        }
                    }


                });


                return cnt;
            }


            $scope.max = function () {
                var diemmax

                angular.forEach($scope.thogTinNhom, function (item) {
                    if (item.DiemThi != null) {
                        item.DiemTongKet = ((item.DiemChuyenCan * $scope.TTMonHoc.TyLeChuyenCan) + (item.DiemKiemTra * $scope.TTMonHoc.TyLeKiemTra) + (item.DiemThucHanh * $scope.TTMonHoc.TyLeThucHanh) + (item.DiemSeminar * $scope.TTMonHoc.TyLeSeminar) + (item.DiemThi * $scope.TTMonHoc.TyLeThi)) / 100
                    }
                    else {
                        diemmax = 0
                    }

                });
                diemmax = Math.max.apply(Math, $scope.thogTinNhom.map(function (item) { return item.DiemTongKet; }));
                return diemmax;
            }
            $scope.sinhvienmax = function () {
                var diemmax
                var holot;
                var ten;

                angular.forEach($scope.thogTinNhom, function (item) {
                    if (item.DiemThi != null) {
                        item.DiemTongKet = ((item.DiemChuyenCan * $scope.TTMonHoc.TyLeChuyenCan) + (item.DiemKiemTra * $scope.TTMonHoc.TyLeKiemTra) + (item.DiemThucHanh * $scope.TTMonHoc.TyLeThucHanh) + (item.DiemSeminar * $scope.TTMonHoc.TyLeSeminar) + (item.DiemThi * $scope.TTMonHoc.TyLeThi)) / 100
                    }
                    else {
                        diemmax = 0
                    }
                });
                diemmax = Math.max.apply(Math, $scope.thogTinNhom.map(function (item) { return item.DiemTongKet; }));
                if (diemmax != 0) {
                    angular.forEach($scope.thogTinNhom, function (item) {
                        if (item.DiemTongKet == diemmax) {
                            holot = item.HoLot;
                            ten = item.Ten;
                        }
                    });
                }
                else {
                    holot = "";
                    ten = "";
                }
                var item = holot + " " + ten
                return item;

            }

            $scope.min = function () {
                var diemmin
                var diemtongket = []
                angular.forEach($scope.thogTinNhom, function (item) {
                    if (item.DiemThi != null) {
                        item.DiemTongKet = ((item.DiemChuyenCan * $scope.TTMonHoc.TyLeChuyenCan) + (item.DiemKiemTra * $scope.TTMonHoc.TyLeKiemTra) + (item.DiemThucHanh * $scope.TTMonHoc.TyLeThucHanh) + (item.DiemSeminar * $scope.TTMonHoc.TyLeSeminar) + (item.DiemThi * $scope.TTMonHoc.TyLeThi)) / 100
                        diemtongket.push(item.DiemTongKet);
                    }
                    else {
                        diemmin = 0
                    }

                });

                diemmin = Math.min(...diemtongket);
                if (diemmin == Infinity) {
                    diemmin = 0
                }




                return diemmin;
            }
            $scope.sinhvienmin = function () {
                var holot;
                var ten;

                var diemmin
                var diemtongket = []
                angular.forEach($scope.thogTinNhom, function (item) {
                    if (item.DiemThi != null) {
                        item.DiemTongKet = ((item.DiemChuyenCan * $scope.TTMonHoc.TyLeChuyenCan) + (item.DiemKiemTra * $scope.TTMonHoc.TyLeKiemTra) + (item.DiemThucHanh * $scope.TTMonHoc.TyLeThucHanh) + (item.DiemSeminar * $scope.TTMonHoc.TyLeSeminar) + (item.DiemThi * $scope.TTMonHoc.TyLeThi)) / 100
                        diemtongket.push(item.DiemTongKet);
                    }
                    else {
                        diemmin = 0
                    }

                });

                diemmin = Math.min(...diemtongket);
                if (diemmin == Infinity) {
                    diemmin = 0
                }
                if (diemmin != 0) {
                    angular.forEach($scope.thogTinNhom, function (item) {
                        if (item.DiemTongKet == diemmin) {
                            holot = item.HoLot;
                            ten = item.Ten;
                        }
                    });
                }
                else {
                    holot = "";
                    ten = "";
                }
                var item = holot + " " + ten
                return item;

            }

            $scope.dulieu = [];
            $scope.xemthongket = function () {
                $scope.dulieu = [];
                angular.forEach($scope.thogTinNhom, function (item) {
                    if (item.DiemTongKet != null) {
                        item.DiemTongKet = ((item.DiemChuyenCan * $scope.TTMonHoc.TyLeChuyenCan) + (item.DiemKiemTra * $scope.TTMonHoc.TyLeKiemTra) + (item.DiemThucHanh * $scope.TTMonHoc.TyLeThucHanh) + (item.DiemSeminar * $scope.TTMonHoc.TyLeSeminar) + (item.DiemThi * $scope.TTMonHoc.TyLeThi)) / 100

                        $scope.dulieu.push({ DiemTongKet: (Math.round(item.DiemTongKet * 4) / 4) });
                    }



                });
                $scope.dulieu = $filter('orderBy')($scope.dulieu, 'DiemTongKet', false);


                $scope.count = function (param) {
                    var count = 0
                    //if (param == 0) {
                    //    count = 1
                    //}
                    angular.forEach($scope.dulieu, function (item) {
                        if (item.DiemTongKet == param) {
                            count++;
                        }
                    });

                    return count
                }
                $scope.labels = ['0', '0.25', '0.5', '0.75', '1', '1.25', '1.5', '1.75', '2', '2.25', '2.5', '2.75', '3', '3.25', '3.5', '3.75', '4', '4.25', '4.5', '4.75', '5',
                    '5.25', '5.5', '5.75', '6', '6.25', '6.5', '6.75', '7', '7.25', '7.5', '7.75', '8', '8.25', '8.5', '8.75', '9', '9.25', '9.5', '9.75', '10'];
                $scope.series = ['Phần trăm sinh viên'];
                $scope.options = {
                    title: {
                        display: true,
                        text: 'Biểu đồ thống kế kết quả sinh viên',
                        position: 'top'
                    },
                    legend: {
                        display: true
                    },
                    scales: {
                        yAxes: [{
                            scaleLabel: {
                                display: true,
                                labelString: 'Tổng phần trăm số sinh viên',

                            },
                            ticks: {

                                // Include a dollar sign in the ticks
                                callback: function (value, index, values) {
                                    return value + ' %';
                                },
                                //beginAtZero: true,
                                //steps: 10,
                                //stepValue: 5,
                                //max: 100
                            }
                        }],
                        xAxes: [{
                            scaleLabel: {
                                display: true,
                                labelString: 'Điểm',

                            },

                        }]
                    },
                    tooltips: {
                        //mode: "label",
                        callbacks: {
                            title: function (tooltipItem, data) {
                                return 'Điểm: ' + tooltipItem[0].label;

                            },

                            label: function (label) {
                                return 'Phần trăm sinh viên: ' + label.value + '%';
                            }
                        }
                    },
                    responsive: true,
                    maintainAspectRatio: false
                }
                $scope.data = [
                    [($scope.count(0) * 100 / $scope.thogTinNhom.length).toFixed(2), ($scope.count(0.25) * 100 / $scope.thogTinNhom.length).toFixed(2), ($scope.count(0.5) * 100 / $scope.thogTinNhom.length).toFixed(2), ($scope.count(0.75) * 100 / $scope.thogTinNhom.length).toFixed(2), ($scope.count(1) * 100 / $scope.thogTinNhom.length).toFixed(2), ($scope.count(1.25) * 100 / $scope.thogTinNhom.length).toFixed(2), ($scope.count(1.5) * 100 / $scope.thogTinNhom.length).toFixed(2), ($scope.count(1.75) * 100 / $scope.thogTinNhom.length).toFixed(2), ($scope.count(2) * 100 / $scope.thogTinNhom.length).toFixed(2), ($scope.count(2.25) * 100 / $scope.thogTinNhom.length).toFixed(2), ($scope.count(2.5) * 100 / $scope.thogTinNhom.length).toFixed(2), ($scope.count(2.75) * 100 / $scope.thogTinNhom.length).toFixed(2), ($scope.count(3) * 100 / $scope.thogTinNhom.length).toFixed(2), ($scope.count(3.25) * 100 / $scope.thogTinNhom.length).toFixed(2), ($scope.count(3.5) * 100 / $scope.thogTinNhom.length).toFixed(2), ($scope.count(3.75) * 100 / $scope.thogTinNhom.length).toFixed(2), ($scope.count(4) * 100 / $scope.thogTinNhom.length).toFixed(2), ($scope.count(4.25) * 100 / $scope.thogTinNhom.length).toFixed(2), ($scope.count(4.5) * 100 / $scope.thogTinNhom.length).toFixed(2), ($scope.count(4.75) * 100 / $scope.thogTinNhom.length).toFixed(2), ($scope.count(5) * 100 / $scope.thogTinNhom.length).toFixed(2),
                    ($scope.count(5.25) * 100 / $scope.thogTinNhom.length).toFixed(2), ($scope.count(5.5) * 100 / $scope.thogTinNhom.length).toFixed(2), ($scope.count(5.75) * 100 / $scope.thogTinNhom.length).toFixed(2), ($scope.count(6) * 100 / $scope.thogTinNhom.length).toFixed(2), ($scope.count(6.25) * 100 / $scope.thogTinNhom.length).toFixed(2), ($scope.count(6.5) * 100 / $scope.thogTinNhom.length).toFixed(2), ($scope.count(6.75) * 100 / $scope.thogTinNhom.length).toFixed(2), ($scope.count(7) * 100 / $scope.thogTinNhom.length).toFixed(2), ($scope.count(7.25) * 100 / $scope.thogTinNhom.length).toFixed(2), ($scope.count(7.5) * 100 / $scope.thogTinNhom.length).toFixed(2), ($scope.count(7.75) * 100 / $scope.thogTinNhom.length).toFixed(2), ($scope.count(8) * 100 / $scope.thogTinNhom.length).toFixed(2), ($scope.count(8.25) * 100 / $scope.thogTinNhom.length).toFixed(2), ($scope.count(8.5) * 100 / $scope.thogTinNhom.length).toFixed(2), ($scope.count(8.75) * 100 / $scope.thogTinNhom.length).toFixed(2), ($scope.count(9) * 100 / $scope.thogTinNhom.length).toFixed(2), ($scope.count(9.25) * 100 / $scope.thogTinNhom.length).toFixed(2), ($scope.count(9.5) * 100 / $scope.thogTinNhom.length).toFixed(2), ($scope.count(9.75) * 100 / $scope.thogTinNhom.length).toFixed(2), ($scope.count(10) * 100 / $scope.thogTinNhom.length).toFixed(2)]

                ];
            }

            //#endregion

        }
        
    }

})(angular.module('platformTH_GV.nhapdiem'));