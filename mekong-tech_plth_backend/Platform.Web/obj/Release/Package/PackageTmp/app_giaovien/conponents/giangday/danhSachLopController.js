﻿(function (app) {
    app.controller('danhSachLopController', danhSachLopController);
    danhSachLopController.$inject = ['$scope', 'apiService','$window'];
    function danhSachLopController($scope, apiService,$window) {
       
        $scope.danhSachLop = [];
        $scope.getDanhSachlop = getDanhSachlop;

        $scope.danhSachHocKy = [];
        $scope.getDanhSachHocKy = getDanhSachHocKy;

        $scope.msvc = $scope.authentication.userName;
        $scope.hocKy = "";
       //$scope.maNhom = "IT1";

        $scope.danhSachGiangDayTheoNhom = [];
        $scope.getDanhSachGiangDayTheoNhom = getDanhSachGiangDayTheoNhom;
       // get hoc ky
       
        function getDanhSachHocKy() {
            var config = {
                params: {
                    msvc: $scope.msvc,
                    

                }
            }
            apiService.get('/api/giangday/getDanhSachHocKy', config, function (result) {

                $scope.danhSachHocKy = result.data;

            }, function () {
                console.log('Load product failed.');
            });
        }
        $scope.getDanhSachHocKy();


        //get danh sach lop
        function getDanhSachlop() {
            var config = {
                params: {
                    msvc: $scope.msvc,
                    hocKy: $scope.hocKy,
                   
                }
            }
            apiService.get('/api/giangday/getDanhSachLop', config, function (result) {

                $scope.danhSachLop = result.data;

            }, function () {
                console.log('Load product failed.');
            });
        }
        $scope.getDanhSachlop();


        //get danh sach giang day theo nhom: 
        function getDanhSachGiangDayTheoNhom(MaNhom) {
         
            var config = {
                params: {
                    maNhom: MaNhom,
                   

                }
            }
            apiService.get('/api/giangday/getdanhsachgiangday', config, function (result) {

                $scope.danhSachGiangDayTheoNhom = result.data;

            }, function () {
                console.log('load product failed.');
            });
        }
        $scope.getDanhSachGiangDayTheoNhom();
    }
})(angular.module('platformTH_GV.giangday'));