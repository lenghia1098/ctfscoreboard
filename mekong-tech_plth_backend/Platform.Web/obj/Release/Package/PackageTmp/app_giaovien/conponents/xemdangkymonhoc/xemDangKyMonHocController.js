﻿(function (app) {
    app.controller('xemDangKyMonHocController', xemDangKyMonHocController);
    xemDangKyMonHocController.$inject = ['$scope', 'apiService', '$stateParams', '$ngBootbox', 'notificationService', 'loginService', '$injector'];
    function xemDangKyMonHocController($scope, apiService, $stateParams, $ngBootbox, notificationService, loginService, $injector) {
        if ($scope.kiemtrarole('SinhVien') === 'SinhVien' && $scope.kiemtrarole('Admin') != 'Admin') {
            loginService.logOut();
            var stateService = $injector.get('$state');
            stateService.go('dangnhap');
            notificationService.displayError("Bạn không có quyền truy cập trang này");
        }
        else {
            //#region
            $scope.mssv = "";
            $scope.dangKy = [];
            $scope.dangKyTH = [];
            $scope.getDangKy = getDangKy;
            $scope.getDangKyTH = getDangKyTH;


            function getDangKy(HocKy, NamHoc) {
                $scope.total2 = "";
                $scope.bang2 = false;
                $scope.total1 = true;
                var config = {
                    params: {
                        mssv: $scope.mssv,
                        hocKy: HocKy,
                        namHoc: NamHoc
                    }
                }
                apiService.get('/api/dangky/getDKMH', config, function (result) {
                    $scope.dangKyTH = "";
                    $scope.dangKy = result.data;


                }, function () {
                    console.log('Load product failed.');
                });
            }
            function getDangKyTH(HocKy, NamHoc) {
                $scope.total1 = "";
                $scope.dangKy = "";
                $scope.bang2 = false;
                $scope.total2 = true;
                var config = {
                    params: {
                        mssv: $scope.mssv,
                        hocKy: HocKy,
                        namHoc: NamHoc
                    }
                }
                apiService.get('/api/dangky_tamthoi/getDKMHTH', config, function (result) {

                    $scope.dangKyTH = result.data;


                }, function () {
                    console.log('Load product failed.');
                });
            }


            $scope.dsHocKy = [];
            $scope.getDSHocKy = getDSHocKy;
            function getDSHocKy() {
                var config = {
                    params: {
                        mssv: $scope.mssv,

                    }
                }
                apiService.get('/api/nhom/getHocKy', config, function (result) {

                    $scope.dsHocKy = result.data;


                }, function () {
                    console.log('Load product failed.');
                });
            }
            $scope.getDSHocKy();
            /*----------------------------*/
            $scope.dsHocKyTH = [];
            $scope.getDSHocKyTH = getDSHocKyTH;
            function getDSHocKyTH() {
                var config = {
                    params: {
                        mssv: $scope.mssv,

                    }
                }
                apiService.get('/api/nhom/getHocKyTH', config, function (result) {

                    $scope.dsHocKyTH = result.data;


                }, function () {
                    console.log('Load product failed.');
                });
            }
            $scope.getDSHocKyTH();



            $scope.monHoc = [];
            $scope.getMonHoc = getMonHoc;
            function getMonHoc(MaMH, MaNhom, MaLop) {
                $scope.btnxemsinhvien = true;
                var config = {
                    params: {
                        MaMH: MaMH,
                        MaNhom: MaNhom,
                        MaLop: MaLop

                    }
                }
                apiService.get('/api/dangky/getMH', config, function (result) {

                    $scope.monHoc = result.data;


                }, function () {
                    console.log('Load product failed.');
                });
            }




            $scope.dangKy_TamThoi = [];
            $scope.getdangKy_TamThoi = getdangKy_TamThoi;
            function getdangKy_TamThoi() {
                var config = {
                    params: {

                        mssv: $scope.mssv,

                    }
                }
                apiService.get('/api/dangky_tamthoi/getDangKy_TamThoi/', config, function (result) {
                    $scope.dangKy_TamThoi = result.data;
                }, function () {
                    console.log('Load thông tin failed.');
                });

            };

            $scope.showbang3 = function () {

                $scope.bang3 = true



            }

            $scope.bang1 = false;
            $scope.bang2 = false;
            $scope.showbang1 = function () {
                $scope.bang1 = true

            }
            $scope.showbang2 = function () {
                $scope.bang2 = true

            }
            $scope.bangsinhvien = false;
            $scope.btnxemsinhvien = true;
            $scope.showsinhvien = function () {
                $scope.bangsinhvien = true;
                $scope.btnxemsinhvien = false;
            }


            $scope.showDKMH = function () {
                if ($scope.search2 = true) {
                    $scope.search2 = false
                }
                if ($scope.bang3 = true) {
                    $scope.bang3 = false
                }
                $scope.dsHocKy = "";
                $scope.mssv = "";

                $scope.search1 = true



            }
            $scope.showDKMHTH = function () {
                if ($scope.search1 = true) {
                    $scope.search1 = false
                }
                if ($scope.bang1 = true) {
                    $scope.bang1 = false
                }
                if ($scope.bang2 = true) {
                    $scope.bang2 = false
                }
                $scope.mssv = "";
                $scope.search2 = true

            }




            $scope.thongTincaNhan = [];
            $scope.getThongTinCaNhan = getThongTinCaNhan;


            function getThongTinCaNhan() {
                var config = {
                    params: {
                        Id: $scope.mssv,

                    }
                }
                apiService.get('/api/sinhvien/getTTCN/', config, function (result) {
                    $scope.thongTincaNhan = result.data;
                }, function () {
                    console.log('Load thông tin failed.');
                });

            }

            $scope.getThongTinCaNhan();

            $scope.deleteDangKy = deleteDangKy;

            function deleteDangKy(ID) {
                $ngBootbox.confirm('Bạn có chắc muốn xóa?').then(function () {
                    var config = {
                        params: {
                            ID: ID
                        }
                    }
                    apiService.del('api/dangky_tamthoi/delete', config, function () {
                        notificationService.displaySuccess("Đã xóa thành công")
                        $scope.getdangKy_TamThoi();
                    }, function () {
                        console.log('Xóa không thành công');
                    })
                });
            }
            //#endregion

        }
       

    }
})(angular.module('platformTH_GV.xemdangkymonhoc'));