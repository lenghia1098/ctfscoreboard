﻿(function (app) {
    app.controller('nhapNoiDungDatabaseController', nhapNoiDungDatabaseController);

    nhapNoiDungDatabaseController.$inject = ['$scope', 'apiService', '$stateParams', 'notificationService', 'loginService', '$injector'];
    function nhapNoiDungDatabaseController($scope, apiService, $stateParams, notificationService, loginService, $injector) {
        if ($scope.kiemtrarole('SinhVien') === 'SinhVien' && $scope.kiemtrarole('Admin') != 'Admin') {
            loginService.logOut();
            var stateService = $injector.get('$state');
            stateService.go('dangnhap');
            notificationService.displayError("Bạn không có quyền truy cập trang này");
        }
        else {
            //#region
            $scope.listTable = [];
            $scope.getListTable = getListTable;
            function getListTable() {
                $scope.chonbang = true;

                apiService.get('/api/sinhviendauvao/GetAllTable/', null, function (result) {
                    $scope.listTable = result.data;
                }, function () {
                    console.log('Load thông tin failed.');
                });

            }
            $scope.getListTable();


            $scope.chonbang = false;
            $scope.upExcel = false;
            $scope.moUpExcel = function () {
                $scope.upExcel = true;
            }

            $scope.SinhVienDauVaoExcel = [];
            $scope.READ = function () {


                if ($scope.name == $scope.tablename) {

                    $scope.SinhVienDauVaoExcel = [];
                    /*Checks whether the file is a valid excel file*/
                    var regex = /^([a-zA-Z0-9\s_\\.\-:])+(.xlsx|.xls)$/;
                    var xlsxflag = false; /*Flag for checking whether excel is .xls format or .xlsx format*/
                    if ($("#ngexcelfile").val().toLowerCase().indexOf(".xlsx") > 0) {
                        xlsxflag = true;
                    }
                    var reader = new FileReader();
                    reader.onload = function (e) {
                        var data = e.target.result;
                        if (xlsxflag) {
                            var workbook = XLSX.read(data, { type: 'binary' });
                        }
                        else {
                            var workbook = XLS.read(data, { type: 'binary' });
                        }

                        var sheet_name_list = workbook.SheetNames;
                        var cnt = 0;
                        sheet_name_list.forEach(function (y) { /*Iterate through all sheets*/

                            if (xlsxflag) {
                                var exceljson = XLSX.utils.sheet_to_json(workbook.Sheets[y]);
                            }
                            else {
                                var exceljson = XLS.utils.sheet_to_row_object_array(workbook.Sheets[y]);
                            }
                            if (exceljson.length > 0) {
                                for (var i = 0; i < exceljson.length; i++) {
                                    $scope.SinhVienDauVaoExcel.push(exceljson[i]);
                                    $scope.$apply();
                                }
                            }
                        });
                    }
                    if (xlsxflag) {
                        reader.readAsArrayBuffer($("#ngexcelfile")[0].files[0]);
                    }
                    else {
                        reader.readAsBinaryString($("#ngexcelfile")[0].files[0]);
                    }
                }
                else {
                    notificationService.displayWarning("Vui lòng chọn file đúng với tên bảng trong database");

                }

            };


            $scope.uploadFile = function () {
                var filename = event.target.files[0].name;
                $scope.name = filename.split(".")[0];

            };



            $scope.bang1 = false;
            $scope.showbang1 = function () {
                $scope.bang1 = true;
                $scope.noidungdatabase = false;

            }





            $scope.AddExcel = function (tablenamedachuyen) {
                $scope.noidungdatabase = false;

                apiService.post('api/' + tablenamedachuyen + '/createExcel', $scope.SinhVienDauVaoExcel,
                    function (result) {
                        notificationService.displaySuccess("Thêm thành công ")

                        angular.element("input[type='file']").val(null);
                    }, function () {
                        notificationService.displayWarning("Thêm không thành công")
                    })
                    ;
            }
            $scope.tablenamedachuyen = "";
            $scope.chuyentablename = function (tablename) {

                switch (tablename) {
                    case 'SinhVienDauVao':
                        $scope.tablenamedachuyen = "sinhviendauvao"
                        break;
                    case 'CauHoiThuongGap':
                        $scope.tablenamedachuyen = "cauhoi"
                        break;
                    case 'ChuongTrinhDaoTao':
                        $scope.tablenamedachuyen = "ctdt"
                        break;
                    case 'ChuongTrinhDaoTao_MonHoc':
                        $scope.tablenamedachuyen = "ctdt-monhoc"
                        break;
                    case 'CongViecDeXuat':
                        $scope.tablenamedachuyen = "congviecdexuat"
                        break;
                    case 'DangKy':
                        $scope.tablenamedachuyen = "dangky"
                        break;
                    case 'DangKy_TamThoi':
                        $scope.tablenamedachuyen = "dangky_tamthoi"
                        break;
                    case 'Don_SinhVien':
                        $scope.tablenamedachuyen = "don-sinhvien"
                        break;
                    case 'Don_VienChuc':
                        $scope.tablenamedachuyen = "don-vienchuc"
                        break;
                    case 'DonSinhVien':
                        $scope.tablenamedachuyen = "donsinhvien"
                        break;
                    case 'DonVienChuc':
                        $scope.tablenamedachuyen = "donvienchuc"
                        break;
                    case 'GiangDay':
                        $scope.tablenamedachuyen = "giangday"
                        break;
                    case 'HocKyHienTai':
                        $scope.tablenamedachuyen = "hockyhientai"
                        break;
                    case 'HocPhi':
                        $scope.tablenamedachuyen = "hocphi"
                        break;
                    case 'KetQua':
                        $scope.tablenamedachuyen = "ketqua"
                        break;
                    case 'KhungGio':
                        $scope.tablenamedachuyen = "khunggio"
                        break;
                    case 'LichThi':
                        $scope.tablenamedachuyen = "lichthi"
                        break;
                    case 'Lop_ChuongTrinhDaoTao':
                        $scope.tablenamedachuyen = "lop_ctdt"
                        break;
                    case 'Lop':
                        $scope.tablenamedachuyen = "lop"
                        break;
                    case 'MonHoc':
                        $scope.tablenamedachuyen = "monhoc"
                        break;
                    case 'Nganh':
                        $scope.tablenamedachuyen = "nganh"
                        break;
                    case 'NgayLe':
                        $scope.tablenamedachuyen = "ngayle"
                        break;
                    case 'NgayLeThayDoi':
                        $scope.tablenamedachuyen = "ngaylethaydoi"
                        break;
                    case 'NhacNho_SinhVien':
                        $scope.tablenamedachuyen = "nhacnho-sv"
                        break;
                    case 'NhacNho_VienChuc':
                        $scope.tablenamedachuyen = "nhacnho-vc"
                        break;
                    case 'NhacNho':
                        $scope.tablenamedachuyen = "nhacnho"
                        break;
                    case 'Nhom':
                        $scope.tablenamedachuyen = "nhom"
                        break;
                    case 'PhongKhoa_VienChuc':
                        $scope.tablenamedachuyen = "phongkhoa-vienchuc"
                        break;
                    case 'PhongKhoa':
                        $scope.tablenamedachuyen = "phongkhoa"
                        break;
                    case 'SinhVien':
                        $scope.tablenamedachuyen = "sinhvien"
                        break;
                    case 'SinhVien_ViecPhaiLam':
                        $scope.tablenamedachuyen = "Sinhvien_ViecPhaiLam"
                        break;
                    case 'ThoiHanDangKyMonHoc':
                        $scope.tablenamedachuyen = "thoihandangkymonhoc"
                        break;
                    case 'ThoiKhoaBieu':
                        $scope.tablenamedachuyen = "thoikhoabieu"
                        break;
                    case 'VienChuc':
                        $scope.tablenamedachuyen = "vienchuc"
                        break;


                    default: $scope.tablenamedachuyen = tablename

                }

            }


            $scope.noidungdatabase = false;
            $scope.noiDungBang = [];
            $scope.getNoiDungBang = function (tablename) {
                $scope.bang1 = false;
                $scope.noidungdatabase = true;
                apiService.get('/api/' + tablename + '/getall', null, function (result) {
                    $scope.noiDungBang = result.data;
                }, function () {
                    console.log('Load thông tin failed.');
                });
            };
            //#endregion

        }

        


    
    }
       
})(angular.module('platformTH_GV.nhapnoidungdatabase'));