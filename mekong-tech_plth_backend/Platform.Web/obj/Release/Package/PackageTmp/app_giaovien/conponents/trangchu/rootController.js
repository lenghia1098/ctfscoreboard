﻿(function (app) {
    app.controller('rootController', rootController);

    rootController.$inject = ['$state', 'authData', 'loginService', '$scope', 'authenticationService'];

    function rootController($state, authData, loginService, $scope, authenticationService) {
        $scope.appVersion = angular.element(document.getElementsByTagName('html')).attr('data-ver');
        
        $scope.logOut = function () {
            loginService.logOut();
            $state.go('dangnhap');
           
        }
        $scope.authentication = authData.authenticationData;
        
        $scope.kiemtrarole = function (name) {
            var a;
            angular.forEach(authData.authenticationData.nameRoles, function (item) {
                if (item.Name == name) {
                    a=item.Name
                }
            });
            return a
        }
    }
    
})(angular.module('platformTH_GV'));