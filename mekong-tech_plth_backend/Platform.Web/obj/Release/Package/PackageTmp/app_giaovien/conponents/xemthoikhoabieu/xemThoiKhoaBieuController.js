﻿(function (app) {
    app.controller('xemThoiKhoaBieuController', xemThoiKhoaBieuController);
    xemThoiKhoaBieuController.$inject = ['$scope', 'apiService', '$stateParams', 'loginService', '$injector', 'notificationService'];
    function xemThoiKhoaBieuController($scope, apiService, $stateParams, loginService, $injector, notificationService) {
        if ($scope.kiemtrarole('SinhVien') === 'SinhVien') {
            if ($scope.kiemtrarole('Admin') === 'Admin') {
                var stateService = $injector.get('$state');

                //#region
                $scope.bangDiem = [];

                $scope.thoikhoabieu = [];
                $scope.thongTincaNhan = [];

                $scope.getThoikhoabieu = getThoikhoabieu;
                $scope.getThongTinCaNhan = getThongTinCaNhan;

                $scope.mssv = "";
                //$scope.hocKy = "1";
                $scope.tuan = "1";
                $scope.tuanhientai = "1";
                $scope.hocKyMH = "1";
                $scope.namHocMH = "2020-2021";
                function getThoikhoabieu() {
                    var nam = new Date().getFullYear();
                    var split1 = $scope.weekDays[1].split("/");
                    var tachchuoi1 = split1[1] + "/" + split1[0]
                    var thu2 = new Date(tachchuoi1 + "/" + nam).getTime();
                    var f = new Date().getFullYear();

                    var split = $scope.weekDays[6].split("/");
                    var a = split[1] + "/" + split[0]
                    var d = new Date(a + "/" + f).getTime();
                    $scope.ngaybatdauu = false;

                    var daymoi = new Date($scope.ngayketthuchocky).getTime();

                    var ngaybatdau = new Date($scope.ngaybatdauhocky).getTime();
                    if (thu2 < ngaybatdau || d < ngaybatdau || thu2 > daymoi || d > daymoi) {
                        $scope.ngaybatdauu = true
                    }
                    if ($scope.ngaybatdauu == true) {
                        //alert("Không có thời khóa biểu");
                    }
                    else {
                        var config = {
                            params: {
                                mssv: $scope.mssv,
                                hocKy: $scope.hocKy,
                                tuan: $scope.tuan
                            }
                        }
                        apiService.get('/api/nhom/getNhom', config, function (result) {

                            $scope.thoikhoabieu = result.data;

                        }, function () {
                            console.log('Load product failed.');
                        });
                    }
                }

                function getThongTinCaNhan() {
                    var config = {
                        params: {
                            Id: $scope.mssv,

                        }
                    }
                    apiService.get('/api/sinhvien/getTTCN/', config, function (result) {
                        $scope.thongTincaNhan = result.data;
                    }, function () {
                        console.log('Load thông tin failed.');
                    });

                }

                $scope.getThongTinCaNhan();
                //$scope.getThoikhoabieu();




                //lay ngay trong tuan
                var currentDate = moment();

                var fnWeekDays = function (dt) {

                    var currentDate = dt;
                    var weekStart = currentDate.clone().startOf('week');
                    var weekEnd = currentDate.clone().endOf('week');

                    var days = [];
                    for (i = 0; i <= 6; i++) {

                        days.push(moment(weekStart).add(i, 'days').format("DD/MM"));

                    };
                    return days;
                }
                $scope.weekDays = fnWeekDays(currentDate);
                //if ($scope.weekDays[1] == "11/05") {
                //    alert($scope.ngayLe);
                //}
                var fnWeekDays2 = function (dt) {

                    var currentDate = dt;
                    var weekStart = currentDate.clone().startOf('week');
                    var weekEnd = currentDate.clone().endOf('week');

                    var days = [];
                    for (i = 0; i <= 6; i++) {

                        days.push(moment(weekStart).add(i, 'days').format("MM/DD"));

                    };
                    return days;
                }
                $scope.weekDays2 = fnWeekDays2(currentDate);

                $scope.nextWeek = function (dt) {
                    $scope.weekDays = fnWeekDays(moment(dt, "DD/MM").add(1, 'days'));

                    $scope.tuan++;



                    var nam = new Date().getFullYear();
                    var split1 = $scope.weekDays[1].split("/");
                    var tachchuoi1 = split1[1] + "/" + split1[0]
                    var thu2 = new Date(tachchuoi1 + "/" + nam).getTime();
                    var f = new Date().getFullYear();

                    var split = $scope.weekDays[6].split("/");
                    var a = split[1] + "/" + split[0]
                    var d = new Date(a + "/" + f).getTime();
                    $scope.ngaybatdauu = false;
                    var daymoi = new Date($scope.ngayketthuchocky).getTime();

                    var ngaybatdau = new Date($scope.ngaybatdauhocky).getTime();
                    if (thu2 > daymoi || d > daymoi || thu2 < ngaybatdau || d < ngaybatdau) {
                        $scope.ngaybatdauu = true
                    }


                    if ($scope.ngaybatdauu == true) {
                        //alert("Không có thời khóa biểu");
                        $scope.thoikhoabieu = [];


                    }
                    else {

                        $scope.getThoikhoabieu = getThoikhoabieu;
                        function getThoikhoabieu() {
                            var config = {
                                params: {
                                    mssv: $scope.mssv,
                                    hocKy: $scope.hocKy,
                                    tuan: $scope.tuan
                                }
                            }
                            apiService.get('/api/nhom/getNhom', config, function (result) {

                                $scope.thoikhoabieu = result.data;
                                console.log($scope.tuan)

                            }, function () {
                                console.log('Load product failed.');
                            });


                        }
                        $scope.getThoikhoabieu();
                    }

                }

                $scope.previousWeek = function (dt) {
                    $scope.weekDays = fnWeekDays(moment(dt, " DD/MM").subtract(1, 'days'));

                    $scope.tuan--;

                    var nam = new Date().getFullYear();
                    var split1 = $scope.weekDays[1].split("/");
                    var tachchuoi1 = split1[1] + "/" + split1[0]
                    var thu2 = new Date(tachchuoi1 + "/" + nam).getTime();
                    var f = new Date().getFullYear();

                    var split = $scope.weekDays[6].split("/");
                    var a = split[1] + "/" + split[0]
                    var d = new Date(a + "/" + f).getTime();
                    $scope.ngaybatdauu = false;
                    var daymoi = new Date($scope.ngayketthuchocky).getTime();

                    var ngaybatdau = new Date($scope.ngaybatdauhocky).getTime();
                    if (thu2 < ngaybatdau || d < ngaybatdau || thu2 > daymoi || d > daymoi) {
                        $scope.ngaybatdauu = true
                    }
                    if ($scope.ngaybatdauu == true) {
                        //alert("Không có thời khóa biểu");
                        $scope.thoikhoabieu = [];


                    }
                    else {

                        $scope.getThoikhoabieu = getThoikhoabieu;
                        function getThoikhoabieu() {
                            var config = {
                                params: {
                                    mssv: $scope.mssv,
                                    hocKy: $scope.hocKy,
                                    tuan: $scope.tuan
                                }
                            }
                            apiService.get('/api/nhom/getNhom', config, function (result) {
                                $scope.thoikhoabieu = result.data;


                            }, function () {
                                console.log('Load product failed.');
                            });



                        }
                        $scope.getThoikhoabieu();

                    }


                };
                $scope.reload = function () {
                    $scope.tuan = $scope.tuanhientai;
                    $scope.weekDays = fnWeekDays(currentDate);
                    var nam = new Date().getFullYear();
                    var split1 = $scope.weekDays[1].split("/");
                    var tachchuoi1 = split1[1] + "/" + split1[0]
                    var thu2 = new Date(tachchuoi1 + "/" + nam).getTime();
                    var f = new Date().getFullYear();

                    var split = $scope.weekDays[6].split("/");
                    var a = split[1] + "/" + split[0]
                    var d = new Date(a + "/" + f).getTime();
                    $scope.ngaybatdauu = false;
                    var daymoi = new Date($scope.ngayketthuchocky).getTime();

                    var ngaybatdau = new Date($scope.ngaybatdauhocky).getTime();
                    if (thu2 < ngaybatdau || d < ngaybatdau || thu2 > daymoi || d > daymoi) {
                        $scope.ngaybatdauu = true

                    }
                    if ($scope.ngaybatdauu == true) {
                        $scope.thoikhoabieu = [];
                        // alert("Không có thời khóa biểu");


                    }
                    else {
                        $scope.getThoikhoabieu = getThoikhoabieu;
                        function getThoikhoabieu() {
                            var config = {
                                params: {
                                    mssv: $scope.mssv,
                                    hocKy: $scope.hocKy,
                                    tuan: $scope.tuanhientai
                                }
                            }
                            apiService.get('/api/nhom/getNhom', config, function (result) {
                                $scope.thoikhoabieu = result.data;


                            }, function () {
                                console.log('Load product failed.');
                            });



                        }
                        $scope.getThoikhoabieu();
                    }
                }

                //////
                $scope.ngayLe = [];
                $scope.getNgayLe = getNgayLe;




                $scope.ngayTrung = [];

                // $scope.thutrung = [{thu:''}];
                function getNgayLe() {

                    apiService.get('/api/ngayle/getall', null, function (result) {

                        $scope.ngayLe = result.data;


                        for (var i = 0; i <= $scope.ngayLe.length - 1; i++) {


                            for (var j = 1; j <= 6; j++) {

                                if ($scope.ngayLe[i].Ngay == $scope.weekDays[j]) {
                                    $scope.ngayTrung.push({ ngay: $scope.ngayLe[i].Ngay, tenngay: $scope.ngayLe[i].TenNgayLe });

                                }
                            }
                        }







                    }, function () {
                        console.log('Load product failed.');
                    });


                }
                $scope.getNgayLe();
                $scope.ngay = $scope.ngayLe;
                //console.log($scope.ngay);

                ////in TKB






                ///////get khung giờ

                $scope.khungGio = [];
                $scope.getKhungGio = getKhungGio;
                function getKhungGio() {

                    apiService.get('/api/khunggio/getall', null, function (result) {

                        $scope.khungGio = result.data;

                    }, function () {
                        console.log('Load product failed.');
                    });


                }
                $scope.getKhungGio();


                //test
                $scope.getngaytrung = function (id) {
                    var result = [];
                    $scope.ngayTrung.filter(function (item) {
                        if (item.ngay === id) {
                            result.push(id);
                        }

                    })
                    return result;
                }
                $scope.gettenngaytrung = function (id) {
                    var result = [];
                    $scope.ngayTrung.filter(function (item) {
                        if (item.ngay === id) {
                            result.push(item.tenngay);
                        }

                    })
                    return result;
                }
                //merge TKB trung
                $scope.tiet = function (thu, tiet, sotiet) {
                    var result = [];
                    $scope.thoikhoabieu.filter(function (item) {
                        if (item.Thu == thu && item.Tiet == tiet && item.SoTiet <= sotiet && item.SoTiet > 1) {
                            result.push(1);
                        }
                        else if (item.Thu == thu && item.Tiet == tiet && item.SoTiet <= tiet && item.SoTiet == 1) {
                            result.push(2);
                        }
                        else result.push(3);
                    })
                    return result;
                }
                $scope.mergetiet = function (thu, tiet, sotiet) {
                    var result = 0;
                    $scope.thoikhoabieu.filter(function (item) {
                        if (item.Thu == thu && item.Tiet == tiet && item.SoTiet <= sotiet && item.SoTiet > 1) {
                            result = item.SoTiet;
                        }

                    })
                    return result;
                }
                $scope.tietdon = function (thu, tiet, sotiet) {
                    var result = [];
                    $scope.thoikhoabieu.filter(function (item) {
                        if (item.Thu == thu && item.Tiet == tiet && item.SoTiet <= sotiet) {
                            result.push(1);
                        }
                        else if (item.Thu == thu && item.Tiet == tiet && item.SoTiet <= tiet && item.SoTiet == 1) {
                            result.push(2);
                        }
                        else result.push(3);
                    })
                    return result;
                }
                $scope.tietkiemtra = function (thu, tiet, sotiet, sotietquydinh) {
                    var result = [];
                    $scope.thoikhoabieu.filter(function (item) {
                        if (item.Thu == thu && item.Tiet == tiet && item.SoTiet <= sotiet && item.SoTiet == sotietquydinh) {
                            result.push(1);
                        }
                        else if (item.Thu == thu && item.Tiet == tiet && item.SoTiet <= tiet && item.SoTiet == 1) {
                            result.push(2);
                        }
                        else result.push(3);
                    })
                    return result;
                }

                $scope.tatCaMonHoc = [];
                $scope.getTatCaMonHoc = getTatCaMonHoc;
                function getTatCaMonHoc() {
                    var config = {
                        params: {

                            hocKy: $scope.hocKy,
                            namhoc: $scope.namhoc
                        }
                    }
                    apiService.get('/api/monhoc/getAllMonHoc', config, function (result) {

                        $scope.tatCaMonHoc = result.data;

                    }, function () {
                        console.log('Load product failed.');
                    });
                }
                //$scope.getTatCaMonHoc();


                $scope.showAlert = function () {
                    $scope.showbang = false;
                    $scope.Nhom = false;
                    $scope.bang3 = false;
                    $scope.tkbmssvmonhoc = false;
                    $scope.bangsinhvien = false;
                    if ($scope.myvalue = true) {
                        $scope.myvaluemonhoc = false;
                    }
                    else {
                        $scope.myvalue = true;
                    }
                };

                $scope.showmonhoc = function () {
                    $scope.TKB = false;
                    $scope.tkbmssvmonhoc = false;
                    $scope.bang3 = false;

                    if ($scope.myvaluemonhoc = true) {
                        $scope.myvalue = false;
                    }
                    else {
                        $scope.myvaluemonhoc = true;
                    }

                };
                $scope.showbang = false;
                $scope.toggle = function () {
                    $scope.showbang = true;
                    $scope.TKB = false;

                    $scope.Nhom = false;
                    $scope.bangsinhvien = false;
                    $scope.btnxemsinhvien = true;
                    $scope.tkbmssvmonhoc = false;
                    $scope.bang3 = false;

                };
                $scope.TKB = false;
                $scope.khunggio = false;
                $scope.showTKB = function () {

                    $scope.TKB = true;
                    $scope.showbang = false;
                    $scope.khunggio = true;
                    $scope.tkbmssvmonhoc = false;
                    $scope.bang3 = false;
                };
                $scope.tkbmssvmonhoc = false;
                $scope.showtkbmssvmonhoc = function () {
                    $scope.TKB = false;
                    $scope.Nhom = false;
                    $scope.bangsinhvien = false;
                    $scope.btnxemsinhvien = false;
                    $scope.myvalue = false;
                    $scope.myvaluemonhoc = false;
                    $scope.showbang = false;
                    $scope.tkbmssvmonhoc = true;
                }
                $scope.bang3 = false;
                $scope.showbang3 = function () {
                    $scope.bang3 = true;
                }



                $scope.tenMH = "";
                $scope.monHoc = [];
                $scope.getMonHoc = getMonHoc;


                function getMonHoc() {
                    var config = {
                        params: {
                            tenMH: $scope.tenMH,
                            hocKy: $scope.hocKy,
                            namhoc: $scope.namhoc
                        }
                    }
                    apiService.get('/api/thoikhoabieu/xemTKBmonhoc', config, function (result) {

                        $scope.monHoc = result.data;


                    }, function () {
                        console.log('Load product failed.');
                    });
                }


                //$scope.getMonHoc();


                $scope.thogTinNhom = [];
                $scope.getThongTinNhom = getThongTinNhom;
                function getThongTinNhom(manhom, mamh, malop) {
                    var config = {
                        params: {
                            maNhom: manhom,
                            maMH: mamh,
                            malop: malop
                        }
                    }
                    apiService.get('/api/thoikhoabieu/xemThongTinNhom', config, function (result) {

                        $scope.thogTinNhom = result.data;


                    }, function () {
                        console.log('Load product failed.');
                    });
                }
                $scope.Nhom = false;
                $scope.openNhom = function () {

                    $scope.Nhom = true;
                    $scope.showbang = false;
                }
                $scope.bangsinhvien = false;
                $scope.btnxemsinhvien = true;
                $scope.showsinhvien = function () {
                    $scope.bangsinhvien = true;
                    $scope.btnxemsinhvien = false;
                }



                $scope.monHocTheoMSSV = [];
                $scope.getMonHocTheoMSSV = getMonHocTheoMSSV;
                function getMonHocTheoMSSV() {
                    var config = {
                        params: {
                            mssv: $scope.mssv,
                            hocKy: $scope.hocKy,
                            namhoc: $scope.namhoc
                        }
                    }
                    apiService.get('/api/monhoc/getMonHocTheoMSSV', config, function (result) {

                        $scope.monHocTheoMSSV = result.data;

                    }, function () {
                        console.log('Load product failed.');
                    });
                }


                $scope.thongTincaNhan = [];
                $scope.getThongTinCaNhan = getThongTinCaNhan;
                function getThongTinCaNhan() {
                    var config = {
                        params: {
                            Id: $scope.mssv,

                        }
                    }
                    apiService.get('/api/sinhvien/getTTCN/', config, function (result) {
                        $scope.thongTincaNhan = result.data;
                    }, function () {
                        console.log('Load thông tin failed.');
                    });

                }

                $scope.hocKyHienTai = [];
                $scope.getHocKyHienTai = getHocKyHienTai;
                function getHocKyHienTai() {
                    apiService.get('/api/hockyhientai/getall/', null, function (result) {

                        $scope.hocKyHienTai = result.data[result.data.length - 1];
                        $scope.hocKy = result.data[result.data.length - 1].HocKy;
                        $scope.namhoc = result.data[result.data.length - 1].NamHoc;
                        $scope.ngaybatdauhocky = result.data[result.data.length - 1].NgayBatDauHocKy;
                        $scope.ngayketthuchocky = result.data[result.data.length - 1].NgayKetThucHocKy;
                        $scope.getNgayLeThayDoi();
                        // $scope.getThoikhoabieu();
                        $scope.getTatCaMonHoc();
                        $scope.getMonHoc();
                    }, function () {
                        console.log('Load thông tin failed.');
                    });

                }

                $scope.getHocKyHienTai();


                $scope.NgayLeThayDoi = [];
                $scope.getNgayLeThayDoi = getNgayLeThayDoi;
                function getNgayLeThayDoi() {
                    var config = {
                        params: {
                            namhoc: $scope.namhoc

                        }
                    }

                    apiService.get('/api/ngaylethaydoi/GetByNamHoc/', config, function (result) {

                        $scope.NgayLeThayDoi = result.data;


                    }, function () {
                        console.log('Load thông tin failed.');
                    });

                }

                $scope.getTenNgayTrungThayDoi = function (day) {
                    var f = new Date().getFullYear();
                    var split = day.split("/");
                    var a = split[1] + "/" + split[0]
                    var d = new Date(a + "/" + f).getTime();
                    var ngaybatdauu = [];
                    var daymoi;
                    var daymoi2;
                    $scope.NgayLeThayDoi.filter(function (item) {
                        daymoi = new Date(item.NgayBatDauNghi).getTime();
                        daymoi2 = new Date(item.NgayKetThucNghi).getTime();
                        if (daymoi <= d && d <= daymoi2) {
                            ngaybatdauu.push(item.TenNgayNghi)
                        }



                    })
                    return ngaybatdauu;
                }
                $scope.getNgayTrungOGiua = function (day) {
                    var f = new Date().getFullYear();
                    var split = day.split("/");
                    var a = split[1] + "/" + split[0]
                    var d = new Date(a + "/" + f).getTime();
                    var ngaybatdauu = [];
                    var daymoi;
                    var daymoi2;
                    $scope.NgayLeThayDoi.filter(function (item) {
                        daymoi = new Date(item.NgayBatDauNghi).getTime();
                        daymoi2 = new Date(item.NgayKetThucNghi).getTime();
                        if (daymoi <= d && d <= daymoi2) {
                            ngaybatdauu.push(day)
                        }
                    })
                    return ngaybatdauu;


                }
                $scope.ngayBatDauVaKetThuc = [];
                $scope.getNgayBatDauVaKetThuc = getNgayBatDauVaKetThuc;
                function getNgayBatDauVaKetThuc() {
                    var config = {
                        params: {
                            mssv: $scope.mssv

                        }
                    }

                    apiService.get('/api/nhom/getNgayBatDauVaKetThuc/', config, function (result) {

                        $scope.ngayBatDauVaKetThuc = result.data;


                    }, function () {
                        console.log('Load thông tin failed.');
                    });

                }

        //#endregion
            }
            else {
                notificationService.displayError("Bạn không có quyền truy cập trang này");
                loginService.logOut();
                var stateService = $injector.get('$state');
                stateService.go('dangnhap');
                
            }

        }
        else {
            //#region
            $scope.bangDiem = [];

            $scope.thoikhoabieu = [];
            $scope.thongTincaNhan = [];

            $scope.getThoikhoabieu = getThoikhoabieu;
            $scope.getThongTinCaNhan = getThongTinCaNhan;

            $scope.mssv = "";
            //$scope.hocKy = "1";
            $scope.tuan = "1";
            $scope.tuanhientai = "1";
            $scope.hocKyMH = "1";
            $scope.namHocMH = "2020-2021";
            function getThoikhoabieu() {
                var nam = new Date().getFullYear();
                var split1 = $scope.weekDays[1].split("/");
                var tachchuoi1 = split1[1] + "/" + split1[0]
                var thu2 = new Date(tachchuoi1 + "/" + nam).getTime();
                var f = new Date().getFullYear();

                var split = $scope.weekDays[6].split("/");
                var a = split[1] + "/" + split[0]
                var d = new Date(a + "/" + f).getTime();
                $scope.ngaybatdauu = false;

                var daymoi = new Date($scope.ngayketthuchocky).getTime();

                var ngaybatdau = new Date($scope.ngaybatdauhocky).getTime();
                if (thu2 < ngaybatdau || d < ngaybatdau || thu2 > daymoi || d > daymoi) {
                    $scope.ngaybatdauu = true
                }
                if ($scope.ngaybatdauu == true) {
                    //alert("Không có thời khóa biểu");
                }
                else {
                    var config = {
                        params: {
                            mssv: $scope.mssv,
                            hocKy: $scope.hocKy,
                            tuan: $scope.tuan
                        }
                    }
                    apiService.get('/api/nhom/getNhom', config, function (result) {

                        $scope.thoikhoabieu = result.data;

                    }, function () {
                        console.log('Load product failed.');
                    });
                }
            }

            function getThongTinCaNhan() {
                var config = {
                    params: {
                        Id: $scope.mssv,

                    }
                }
                apiService.get('/api/sinhvien/getTTCN/', config, function (result) {
                    $scope.thongTincaNhan = result.data;
                }, function () {
                    console.log('Load thông tin failed.');
                });

            }

            $scope.getThongTinCaNhan();
            //$scope.getThoikhoabieu();




            //lay ngay trong tuan
            var currentDate = moment();

            var fnWeekDays = function (dt) {

                var currentDate = dt;
                var weekStart = currentDate.clone().startOf('week');
                var weekEnd = currentDate.clone().endOf('week');

                var days = [];
                for (i = 0; i <= 6; i++) {

                    days.push(moment(weekStart).add(i, 'days').format("DD/MM"));

                };
                return days;
            }
            $scope.weekDays = fnWeekDays(currentDate);
            //if ($scope.weekDays[1] == "11/05") {
            //    alert($scope.ngayLe);
            //}
            var fnWeekDays2 = function (dt) {

                var currentDate = dt;
                var weekStart = currentDate.clone().startOf('week');
                var weekEnd = currentDate.clone().endOf('week');

                var days = [];
                for (i = 0; i <= 6; i++) {

                    days.push(moment(weekStart).add(i, 'days').format("MM/DD"));

                };
                return days;
            }
            $scope.weekDays2 = fnWeekDays2(currentDate);

            $scope.nextWeek = function (dt) {
                $scope.weekDays = fnWeekDays(moment(dt, "DD/MM").add(1, 'days'));

                $scope.tuan++;



                var nam = new Date().getFullYear();
                var split1 = $scope.weekDays[1].split("/");
                var tachchuoi1 = split1[1] + "/" + split1[0]
                var thu2 = new Date(tachchuoi1 + "/" + nam).getTime();
                var f = new Date().getFullYear();

                var split = $scope.weekDays[6].split("/");
                var a = split[1] + "/" + split[0]
                var d = new Date(a + "/" + f).getTime();
                $scope.ngaybatdauu = false;
                var daymoi = new Date($scope.ngayketthuchocky).getTime();

                var ngaybatdau = new Date($scope.ngaybatdauhocky).getTime();
                if (thu2 > daymoi || d > daymoi || thu2 < ngaybatdau || d < ngaybatdau) {
                    $scope.ngaybatdauu = true
                }


                if ($scope.ngaybatdauu == true) {
                    //alert("Không có thời khóa biểu");
                    $scope.thoikhoabieu = [];


                }
                else {

                    $scope.getThoikhoabieu = getThoikhoabieu;
                    function getThoikhoabieu() {
                        var config = {
                            params: {
                                mssv: $scope.mssv,
                                hocKy: $scope.hocKy,
                                tuan: $scope.tuan
                            }
                        }
                        apiService.get('/api/nhom/getNhom', config, function (result) {

                            $scope.thoikhoabieu = result.data;
                            console.log($scope.tuan)

                        }, function () {
                            console.log('Load product failed.');
                        });


                    }
                    $scope.getThoikhoabieu();
                }

            }

            $scope.previousWeek = function (dt) {
                $scope.weekDays = fnWeekDays(moment(dt, " DD/MM").subtract(1, 'days'));

                $scope.tuan--;

                var nam = new Date().getFullYear();
                var split1 = $scope.weekDays[1].split("/");
                var tachchuoi1 = split1[1] + "/" + split1[0]
                var thu2 = new Date(tachchuoi1 + "/" + nam).getTime();
                var f = new Date().getFullYear();

                var split = $scope.weekDays[6].split("/");
                var a = split[1] + "/" + split[0]
                var d = new Date(a + "/" + f).getTime();
                $scope.ngaybatdauu = false;
                var daymoi = new Date($scope.ngayketthuchocky).getTime();

                var ngaybatdau = new Date($scope.ngaybatdauhocky).getTime();
                if (thu2 < ngaybatdau || d < ngaybatdau || thu2 > daymoi || d > daymoi) {
                    $scope.ngaybatdauu = true
                }
                if ($scope.ngaybatdauu == true) {
                    //alert("Không có thời khóa biểu");
                    $scope.thoikhoabieu = [];


                }
                else {

                    $scope.getThoikhoabieu = getThoikhoabieu;
                    function getThoikhoabieu() {
                        var config = {
                            params: {
                                mssv: $scope.mssv,
                                hocKy: $scope.hocKy,
                                tuan: $scope.tuan
                            }
                        }
                        apiService.get('/api/nhom/getNhom', config, function (result) {
                            $scope.thoikhoabieu = result.data;


                        }, function () {
                            console.log('Load product failed.');
                        });



                    }
                    $scope.getThoikhoabieu();

                }


            };
            $scope.reload = function () {
                $scope.tuan = $scope.tuanhientai;
                $scope.weekDays = fnWeekDays(currentDate);
                var nam = new Date().getFullYear();
                var split1 = $scope.weekDays[1].split("/");
                var tachchuoi1 = split1[1] + "/" + split1[0]
                var thu2 = new Date(tachchuoi1 + "/" + nam).getTime();
                var f = new Date().getFullYear();

                var split = $scope.weekDays[6].split("/");
                var a = split[1] + "/" + split[0]
                var d = new Date(a + "/" + f).getTime();
                $scope.ngaybatdauu = false;
                var daymoi = new Date($scope.ngayketthuchocky).getTime();

                var ngaybatdau = new Date($scope.ngaybatdauhocky).getTime();
                if (thu2 < ngaybatdau || d < ngaybatdau || thu2 > daymoi || d > daymoi) {
                    $scope.ngaybatdauu = true

                }
                if ($scope.ngaybatdauu == true) {
                    $scope.thoikhoabieu = [];
                    // alert("Không có thời khóa biểu");


                }
                else {
                    $scope.getThoikhoabieu = getThoikhoabieu;
                    function getThoikhoabieu() {
                        var config = {
                            params: {
                                mssv: $scope.mssv,
                                hocKy: $scope.hocKy,
                                tuan: $scope.tuanhientai
                            }
                        }
                        apiService.get('/api/nhom/getNhom', config, function (result) {
                            $scope.thoikhoabieu = result.data;


                        }, function () {
                            console.log('Load product failed.');
                        });



                    }
                    $scope.getThoikhoabieu();
                }
            }

            //////
            $scope.ngayLe = [];
            $scope.getNgayLe = getNgayLe;




            $scope.ngayTrung = [];

            // $scope.thutrung = [{thu:''}];
            function getNgayLe() {

                apiService.get('/api/ngayle/getall', null, function (result) {

                    $scope.ngayLe = result.data;


                    for (var i = 0; i <= $scope.ngayLe.length - 1; i++) {


                        for (var j = 1; j <= 6; j++) {

                            if ($scope.ngayLe[i].Ngay == $scope.weekDays[j]) {
                                $scope.ngayTrung.push({ ngay: $scope.ngayLe[i].Ngay, tenngay: $scope.ngayLe[i].TenNgayLe });

                            }
                        }
                    }







                }, function () {
                    console.log('Load product failed.');
                });


            }
            $scope.getNgayLe();
            $scope.ngay = $scope.ngayLe;
            //console.log($scope.ngay);

            ////in TKB






            ///////get khung giờ

            $scope.khungGio = [];
            $scope.getKhungGio = getKhungGio;
            function getKhungGio() {

                apiService.get('/api/khunggio/getall', null, function (result) {

                    $scope.khungGio = result.data;

                }, function () {
                    console.log('Load product failed.');
                });


            }
            $scope.getKhungGio();


            //test
            $scope.getngaytrung = function (id) {
                var result = [];
                $scope.ngayTrung.filter(function (item) {
                    if (item.ngay === id) {
                        result.push(id);
                    }

                })
                return result;
            }
            $scope.gettenngaytrung = function (id) {
                var result = [];
                $scope.ngayTrung.filter(function (item) {
                    if (item.ngay === id) {
                        result.push(item.tenngay);
                    }

                })
                return result;
            }
            //merge TKB trung
            $scope.tiet = function (thu, tiet, sotiet) {
                var result = [];
                $scope.thoikhoabieu.filter(function (item) {
                    if (item.Thu == thu && item.Tiet == tiet && item.SoTiet <= sotiet && item.SoTiet > 1) {
                        result.push(1);
                    }
                    else if (item.Thu == thu && item.Tiet == tiet && item.SoTiet <= tiet && item.SoTiet == 1) {
                        result.push(2);
                    }
                    else result.push(3);
                })
                return result;
            }
            $scope.mergetiet = function (thu, tiet, sotiet) {
                var result = 0;
                $scope.thoikhoabieu.filter(function (item) {
                    if (item.Thu == thu && item.Tiet == tiet && item.SoTiet <= sotiet && item.SoTiet > 1) {
                        result = item.SoTiet;
                    }

                })
                return result;
            }
            $scope.tietdon = function (thu, tiet, sotiet) {
                var result = [];
                $scope.thoikhoabieu.filter(function (item) {
                    if (item.Thu == thu && item.Tiet == tiet && item.SoTiet <= sotiet) {
                        result.push(1);
                    }
                    else if (item.Thu == thu && item.Tiet == tiet && item.SoTiet <= tiet && item.SoTiet == 1) {
                        result.push(2);
                    }
                    else result.push(3);
                })
                return result;
            }
            $scope.tietkiemtra = function (thu, tiet, sotiet, sotietquydinh) {
                var result = [];
                $scope.thoikhoabieu.filter(function (item) {
                    if (item.Thu == thu && item.Tiet == tiet && item.SoTiet <= sotiet && item.SoTiet == sotietquydinh) {
                        result.push(1);
                    }
                    else if (item.Thu == thu && item.Tiet == tiet && item.SoTiet <= tiet && item.SoTiet == 1) {
                        result.push(2);
                    }
                    else result.push(3);
                })
                return result;
            }

            $scope.tatCaMonHoc = [];
            $scope.getTatCaMonHoc = getTatCaMonHoc;
            function getTatCaMonHoc() {
                var config = {
                    params: {

                        hocKy: $scope.hocKy,
                        namhoc: $scope.namhoc
                    }
                }
                apiService.get('/api/monhoc/getAllMonHoc', config, function (result) {

                    $scope.tatCaMonHoc = result.data;

                }, function () {
                    console.log('Load product failed.');
                });
            }
            //$scope.getTatCaMonHoc();


            $scope.showAlert = function () {
                $scope.showbang = false;
                $scope.Nhom = false;
                $scope.bang3 = false;
                $scope.tkbmssvmonhoc = false;
                $scope.bangsinhvien = false;
                if ($scope.myvalue = true) {
                    $scope.myvaluemonhoc = false;
                }
                else {
                    $scope.myvalue = true;
                }
            };

            $scope.showmonhoc = function () {
                $scope.TKB = false;
                $scope.tkbmssvmonhoc = false;
                $scope.bang3 = false;

                if ($scope.myvaluemonhoc = true) {
                    $scope.myvalue = false;
                }
                else {
                    $scope.myvaluemonhoc = true;
                }

            };
            $scope.showbang = false;
            $scope.toggle = function () {
                $scope.showbang = true;
                $scope.TKB = false;

                $scope.Nhom = false;
                $scope.bangsinhvien = false;
                $scope.btnxemsinhvien = true;
                $scope.tkbmssvmonhoc = false;
                $scope.bang3 = false;

            };
            $scope.TKB = false;
            $scope.khunggio = false;
            $scope.showTKB = function () {

                $scope.TKB = true;
                $scope.showbang = false;
                $scope.khunggio = true;
                $scope.tkbmssvmonhoc = false;
                $scope.bang3 = false;
            };
            $scope.tkbmssvmonhoc = false;
            $scope.showtkbmssvmonhoc = function () {
                $scope.TKB = false;
                $scope.Nhom = false;
                $scope.bangsinhvien = false;
                $scope.btnxemsinhvien = false;
                $scope.myvalue = false;
                $scope.myvaluemonhoc = false;
                $scope.showbang = false;
                $scope.tkbmssvmonhoc = true;
            }
            $scope.bang3 = false;
            $scope.showbang3 = function () {
                $scope.bang3 = true;
            }



            $scope.tenMH = "";
            $scope.monHoc = [];
            $scope.getMonHoc = getMonHoc;


            function getMonHoc() {
                var config = {
                    params: {
                        tenMH: $scope.tenMH,
                        hocKy: $scope.hocKy,
                        namhoc: $scope.namhoc
                    }
                }
                apiService.get('/api/thoikhoabieu/xemTKBmonhoc', config, function (result) {

                    $scope.monHoc = result.data;


                }, function () {
                    console.log('Load product failed.');
                });
            }


            //$scope.getMonHoc();


            $scope.thogTinNhom = [];
            $scope.getThongTinNhom = getThongTinNhom;
            function getThongTinNhom(manhom, mamh, malop) {
                var config = {
                    params: {
                        maNhom: manhom,
                        maMH: mamh,
                        malop: malop
                    }
                }
                apiService.get('/api/thoikhoabieu/xemThongTinNhom', config, function (result) {

                    $scope.thogTinNhom = result.data;


                }, function () {
                    console.log('Load product failed.');
                });
            }
            $scope.Nhom = false;
            $scope.openNhom = function () {

                $scope.Nhom = true;
                $scope.showbang = false;
            }
            $scope.bangsinhvien = false;
            $scope.btnxemsinhvien = true;
            $scope.showsinhvien = function () {
                $scope.bangsinhvien = true;
                $scope.btnxemsinhvien = false;
            }



            $scope.monHocTheoMSSV = [];
            $scope.getMonHocTheoMSSV = getMonHocTheoMSSV;
            function getMonHocTheoMSSV() {
                var config = {
                    params: {
                        mssv: $scope.mssv,
                        hocKy: $scope.hocKy,
                        namhoc: $scope.namhoc
                    }
                }
                apiService.get('/api/monhoc/getMonHocTheoMSSV', config, function (result) {

                    $scope.monHocTheoMSSV = result.data;

                }, function () {
                    console.log('Load product failed.');
                });
            }


            $scope.thongTincaNhan = [];
            $scope.getThongTinCaNhan = getThongTinCaNhan;
            function getThongTinCaNhan() {
                var config = {
                    params: {
                        Id: $scope.mssv,

                    }
                }
                apiService.get('/api/sinhvien/getTTCN/', config, function (result) {
                    $scope.thongTincaNhan = result.data;
                }, function () {
                    console.log('Load thông tin failed.');
                });

            }

            $scope.hocKyHienTai = [];
            $scope.getHocKyHienTai = getHocKyHienTai;
            function getHocKyHienTai() {
                apiService.get('/api/hockyhientai/getall/', null, function (result) {

                    $scope.hocKyHienTai = result.data[result.data.length - 1];
                    $scope.hocKy = result.data[result.data.length - 1].HocKy;
                    $scope.namhoc = result.data[result.data.length - 1].NamHoc;
                    $scope.ngaybatdauhocky = result.data[result.data.length - 1].NgayBatDauHocKy;
                    $scope.ngayketthuchocky = result.data[result.data.length - 1].NgayKetThucHocKy;
                    $scope.getNgayLeThayDoi();
                    // $scope.getThoikhoabieu();
                    $scope.getTatCaMonHoc();
                    $scope.getMonHoc();
                }, function () {
                    console.log('Load thông tin failed.');
                });

            }

            $scope.getHocKyHienTai();


            $scope.NgayLeThayDoi = [];
            $scope.getNgayLeThayDoi = getNgayLeThayDoi;
            function getNgayLeThayDoi() {
                var config = {
                    params: {
                        namhoc: $scope.namhoc

                    }
                }

                apiService.get('/api/ngaylethaydoi/GetByNamHoc/', config, function (result) {

                    $scope.NgayLeThayDoi = result.data;


                }, function () {
                    console.log('Load thông tin failed.');
                });

            }

            $scope.getTenNgayTrungThayDoi = function (day) {
                var f = new Date().getFullYear();
                var split = day.split("/");
                var a = split[1] + "/" + split[0]
                var d = new Date(a + "/" + f).getTime();
                var ngaybatdauu = [];
                var daymoi;
                var daymoi2;
                $scope.NgayLeThayDoi.filter(function (item) {
                    daymoi = new Date(item.NgayBatDauNghi).getTime();
                    daymoi2 = new Date(item.NgayKetThucNghi).getTime();
                    if (daymoi <= d && d <= daymoi2) {
                        ngaybatdauu.push(item.TenNgayNghi)
                    }



                })
                return ngaybatdauu;
            }
            $scope.getNgayTrungOGiua = function (day) {
                var f = new Date().getFullYear();
                var split = day.split("/");
                var a = split[1] + "/" + split[0]
                var d = new Date(a + "/" + f).getTime();
                var ngaybatdauu = [];
                var daymoi;
                var daymoi2;
                $scope.NgayLeThayDoi.filter(function (item) {
                    daymoi = new Date(item.NgayBatDauNghi).getTime();
                    daymoi2 = new Date(item.NgayKetThucNghi).getTime();
                    if (daymoi <= d && d <= daymoi2) {
                        ngaybatdauu.push(day)
                    }
                })
                return ngaybatdauu;


            }
            $scope.ngayBatDauVaKetThuc = [];
            $scope.getNgayBatDauVaKetThuc = getNgayBatDauVaKetThuc;
            function getNgayBatDauVaKetThuc() {
                var config = {
                    params: {
                        mssv: $scope.mssv

                    }
                }

                apiService.get('/api/nhom/getNgayBatDauVaKetThuc/', config, function (result) {

                    $scope.ngayBatDauVaKetThuc = result.data;


                }, function () {
                    console.log('Load thông tin failed.');
                });

            }

        //#endregion
        }
       

    }
})(angular.module('platformTH_GV.xemthoikhoabieu'));