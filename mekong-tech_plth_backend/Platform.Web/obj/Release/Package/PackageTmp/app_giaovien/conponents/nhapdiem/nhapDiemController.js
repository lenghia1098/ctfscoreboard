﻿(function (app) {
    app.controller('nhapDiemController', nhapDiemController);

    nhapDiemController.$inject = ['$scope', 'apiService', '$injector', 'loginService',  'notificationService', '$stateParams'];
    function nhapDiemController($scope, apiService, $injector, loginService, notificationService, $stateParams) {
        if ($scope.kiemtrarole('SinhVien') === 'SinhVien' && $scope.kiemtrarole('Admin') != 'Admin') {
            loginService.logOut();
            var stateService = $injector.get('$state');
            stateService.go('dangnhap');
            notificationService.displayError("Bạn không có quyền truy cập trang này");
        }
        else {
            //#region
            $scope.TTCN = [];
            $scope.XemLichGiangDay = [];

            $scope.getHocKyHienTai = getHocKyHienTai;
            $scope.hocKy;
            $scope.namhoc;
            function getHocKyHienTai() {
                apiService.get('/api/hockyhientai/getall/', null, function (result) {

                    $scope.hocKyHienTai = result.data[result.data.length - 1];
                    $scope.hocKy = result.data[result.data.length - 1].HocKy;
                    $scope.namhoc = result.data[result.data.length - 1].NamHoc;

                    $scope.getXemLichGiangDay();
                }, function () {
                    console.log('Load thông tin failed.');
                });

            }
            $scope.getHocKyHienTai();

            $scope.getXemLichGiangDay = getXemLichGiangDay;
            function getXemLichGiangDay() {
                var config = {
                    params: {
                        hocky: $scope.hocKy,
                        namhoc: $scope.namhoc


                    }
                }
                apiService.get('/api/monhoc/getAllMonHoc/', config, function (result) {
                    $scope.LichGiangDay = result.data;
                }, function () {
                    console.log('Load thông tin failed.');
                });

            }





            $scope.kiemTraGiuaKi = [];
            $scope.getKiemTraGiuaKi = getKiemTraGiuaKi;
            function getKiemTraGiuaKi(MaNhom, MaMH, MaLop) {
                var config = {
                    params: {
                        manhom: MaNhom


                    }
                }
                apiService.get('/api/ketqua/getbyid/', config, function (result) {
                    $scope.kiemTraGiuaKi = result.data;

                    if ($scope.kiemTraGiuaKi.length > 0) {
                        alert("Bạn đã nhập điểm giữa kì");
                    }
                    else {
                        var stateService = $injector.get('$state');

                        stateService.go('nhapdiemgiuaki', {
                            MaNhom: MaNhom,
                            MaMH: MaMH,
                            MaLop: MaLop
                        });

                    }
                }, function () {
                    console.log('Load thông tin failed.');
                });
            }
            $scope.kiemTraCuoiKi = [];
            $scope.getKiemTraCuoiKi = getKiemTraCuoiKi;
            function getKiemTraCuoiKi(MaNhom, MaMH, MaLop) {
                $scope.getTTMonHoc(MaMH);
                var config = {
                    params: {
                        manhom: MaNhom


                    }
                }
                apiService.get('/api/ketqua/getbyid/', config, function (result) {
                    $scope.kiemTraCuoiKi = result.data;
                    var kiemtra = [];
                    for (var i = 0; i < $scope.kiemTraCuoiKi.length; i++) {
                        if ($scope.kiemTraCuoiKi[i].NgayNhapCuoiKi != null) {
                            kiemtra.push({ NgayNhapCuoiKi: '1' })
                        }
                    }

                    if (kiemtra.length > 0) {
                        alert("Bạn đã nhập điểm cuối kì");
                    }
                    else if ($scope.kiemTraCuoiKi.length == 0 && $scope.TTMonHoc > 0) {
                        alert("Bạn chưa nhập điểm giữa kì");
                    }
                    else {
                        var stateService = $injector.get('$state');

                        stateService.go('nhapdiemcuoiki', {
                            MaNhom: MaNhom,
                            MaMH: MaMH,
                            MaLop: MaLop
                        });

                    }
                }, function () {
                    console.log('Load thông tin failed.');
                });
            }

            $scope.TTMonHoc = {};
            $scope.getTTMonHoc = getTTMonHoc;
            function getTTMonHoc(MaMH) {
                var config = {
                    params: {
                        Id: MaMH
                    }
                }
                apiService.get('/api/monhoc/getbyid/', config, function (result) {

                    $scope.TTMonHoc = result.data.TyLeKiemTra;

                }, function () {
                    console.log('Load thông tin failed.');
                });

            }
            //#endregion
        }    
    }
  
})(angular.module('platformTH_GV.nhapdiem'));