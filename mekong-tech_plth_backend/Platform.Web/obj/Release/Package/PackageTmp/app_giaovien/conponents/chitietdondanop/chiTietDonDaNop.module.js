﻿/// <reference path="../../../assets/giaovien/libs/angular/angular.js" />


(function () {
    angular.module('platformTH_GV.chitietdondanop', ['platformTH_GV.common']).config(config);
    window.appVersion = angular.element(document.getElementsByTagName('html')).attr('data-ver');

    config.$inject = ['$stateProvider', '$urlRouterProvider'];

    function config($stateProvider, $urlRouterProvider) {
        $stateProvider.state('chitietdondanop', {
            url: "/chitietdondanop/:MaDon",
            parent: 'base',
            templateUrl: "app_giaovien/conponents/chitietdondanop/chiTietDonDaNopView.html?v=" + window.appVersion,
            controller: "chiTietDonDaNopController"
        });

    }
})();