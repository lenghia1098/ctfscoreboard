﻿(function (app) {
    app.controller('thayDoiKhungGioHocController', thayDoiKhungGioHocController);
    thayDoiKhungGioHocController.$inject = ['$scope', 'apiService', '$stateParams', 'filterFilter', '$ngBootbox', 'notificationService', 'loginService', '$injector'];
    function thayDoiKhungGioHocController($scope, apiService, $stateParams, filterFilter, $ngBootbox, notificationService, loginService, $injector) {
        if ($scope.kiemtrarole('SinhVien') === 'SinhVien' && $scope.kiemtrarole('Admin') != 'Admin') {
            loginService.logOut();
            var stateService = $injector.get('$state');
            stateService.go('dangnhap');
            notificationService.displayError("Bạn không có quyền truy cập trang này");
        }
        else {
            //#region
            $scope.khungGio = [];
            $scope.save = {};
            $scope.ten = "luan";
            $scope.sotiet = "";
            $scope.sotietdachon = [];
            $scope.tam = 0;
            $scope.getKhungGio = getKhungGio;
            function getKhungGio() {

                apiService.get('/api/khunggio/getall', null, function (result) {

                    $scope.khungGio = result.data;
                    $scope.save = result.data;
                }, function () {
                    console.log('Load product failed.');
                });
            }

            $scope.getKhungGio();



            $scope.Update = Update;
            function Update() {

                $scope.showchon = false;
                var a = $scope.authentication.userName;
                angular.forEach($scope.khungGiocopy, function (item) {
                    item.NguoiThem = a;
                });
                apiService.post('/api/khunggio/create', $scope.khungGiocopy, function (result) {

                    notificationService.displaySuccess("Sửa thành công");
                    $scope.getKhungGio();
                    //$scope.showchon = false;
                    //$scope.show = false;



                }, function () {
                    notificationService.displaySuccess('Sửa không thành công');
                });
            }




            $scope.toggle = function () {
                $scope.myVar = !$scope.myVar;
                $scope.btnsua = false;
                $scope.btnhuy = true;
                $scope.show = true;



            };

            $scope.sotiet = "";
            $scope.filter = function () {
                $scope.filtershow = true
            };

            $scope.clickchon = function () {

                $scope.showchon = true
            };
            $scope.log = function () {
                console.log('Thêm mới không thành công.' + $scope.Addkhunggio);
            };







            $scope.btnsua = true;
            $scope.btnhuy = false;
            $scope.btnxacnhan = false;

            $scope.huy = function () {

                $scope.showchon = false;
                $scope.show = false;
                $scope.btnsua = true;
                $scope.btnhuy = false;
                $scope.user.roles = null;
                $scope.dangKyTamThoi = null;
                $scope.myVar = false;


            }

            $scope.vonglaptiet = function (sotiet) {
                $scope.solanlap = [];
                $scope.khungGiocopy = { NguoiThem: $scope.nguoithem }

                for (var i = 1; i <= sotiet; i++) {
                    $scope.solanlap.push(i);
                }

                $scope.khungGiocopy = $scope.khungGio.slice(0, sotiet);

                return $scope.solanlap;


            }
            function dateCompare(oldtime1, oldtime2) {
                var t1 = new Date();
                time1 = oldtime1 + ":00";
                time2 = oldtime2 + ":00";
                var parts = time1.split(":");
                t1.setHours(parts[0], parts[1], parts[2], 0);
                var t2 = new Date();
                parts = time2.split(":");
                t2.setHours(parts[0], parts[1], parts[2], 0);

                // returns 1 if greater, -1 if less and 0 if the same
                if (t1.getTime() > t2.getTime()) return 1;
                if (t1.getTime() < t2.getTime()) return -1;
                return 0;
            }

            $scope.a = [];

            $scope.compare = function (time1, time2) {
                var compare = (dateCompare(time1, time2))
                return compare
            }
            $scope.checkkhungio = function () {
                var chophep = [];
                var chophep2 = [];

                for (var i = 0; i < $scope.khungGiocopy.length; i++) {
                    if (i > 0) {
                        var compare = (dateCompare($scope.khungGiocopy[i - 1].GioKetThuc, $scope.khungGiocopy[i].GioBatDau));
                        if (compare == 1 || compare == 0) {
                            chophep.push(1)

                        }
                        else {
                            //$scope.Update();
                            chophep.push(2)

                        }

                    }
                    var compare2 = (dateCompare($scope.khungGiocopy[i].GioBatDau, $scope.khungGiocopy[i].GioKetThuc));
                    if (compare2 == 1 || compare == 0) {
                        chophep2.push(1)

                    }
                    else {
                        //$scope.Update();
                        chophep2.push(2)

                    }
                }

                if ((chophep.indexOf(1) > -1) || (chophep2.indexOf(1) > -1)) {
                    notificationService.displayWarning('Vui lòng kiểm tra lại!');
                    console.log(chophep + "   " + chophep2)

                }
                if ((chophep.indexOf(1) == -1) && (chophep2.indexOf(1) == -1)) {
                    console.log(chophep + "   " + chophep2)
                    $scope.Update();
                }
            }
            //#endregion

        }
        
        
    }
})(angular.module('platformTH_GV.thaydoikhunggiohoc'));