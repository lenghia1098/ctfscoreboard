﻿(function (app) {
    app.controller('themThongTinVienChucController', themThongTinVienChucController);
    themThongTinVienChucController.$inject = ['$scope', 'apiService', '$stateParams', '$window', 'notificationService', '$ngBootbox'];
    function themThongTinVienChucController($scope, apiService, $stateParams, $window, notificationService, $ngBootbox) {
        $scope.msvc = "";
        $scope.AllVienChucChitiet = {}
        $scope.AllTrinhDoNgoaiNgu = [];
        $scope.AllThoiGianCongTac = []
        $scope.AllQuaTrinhDaoTao = []
      
        $scope.AddVienChucChitiet = AddVienChucChitiet;
        $scope.AddTrinhDoNgoaiNgu = AddTrinhDoNgoaiNgu;
        $scope.AddThoigiancongtac = AddThoigiancongtac;
        $scope.AddQuaTrinhDaoTao = AddQuaTrinhDaoTao;   
        $scope.AddAllThongtinvienchuc = AddAllThongtinvienchuc;   
        
        function AddAllThongtinvienchuc() {

            if ($scope.AllVienChucChitiet.MaSoVC == null) {
                alert("Mã số viên chức không dược bỏ trống");
                return false;
            } else if($scope.AllVienChucChitiet.NamBatDauCongTac >= $scope.AllVienChucChitiet.NamNghiHuu) {

                alert("Năm bắt đầu công tác phải lớn hơn năm nghĩ hưu");
                return false;
            } else if ($scope.AllQuaTrinhDaoTao.NamBD >= $scope.AllQuaTrinhDaoTao.NamTN) {

                alert("Quá trình đạo tạo: Năm bắt đầu phải nhỏ hơn năm tốt nghiệp");
                return false;
            }
            else  {
               
            apiService.post('/api/vienchuc/create', $scope.AllVienChucChitiet, function (result) {
                notificationService.displaySuccess("Thêm thành công");

                apiService.post('/api/TrinhDoNgoaiNgu/create', $scope.AllTrinhDoNgoaiNgu, function (result) {
                    
                }, function () {
                        notificationService.displayWarning('Thêm trình độ ngoại ngữ thất bại');
                });
                apiService.post('/api/ThoiGianCongTac/create', $scope.AllThoiGianCongTac, function (result) {
                   
                }, function () {
                        notificationService.displayWarning('Thêm thời gian công tác thất bại');
                });

                if ($scope.AllQuaTrinhDaoTao.NamBD >= $scope.AllQuaTrinhDaoTao.NamTN) {

                    alert("Ngày kết thúc phải lớn hơn ngày bắt đầu")
                } else {
                apiService.post('/api/QuaTrinhDaoTao/create', $scope.AllQuaTrinhDaoTao, function (result) {
                   
                }, function () {
                        notificationService.displayWarning('Thêm quá trình đào tạo thất bại');
                });
                }

            }, function () {
                    notificationService.displayWarning('thêm thất bại');
            });
           
            }
            
        }
       

        $scope.getSeoTitle = getSeoTitle;

        function getSeoTitle(item) {
           
            $scope.AllTrinhDoNgoaiNgu = [{ MaSoVC: item }];
            $scope.AllThoiGianCongTac = [{ MaSoVC: item }];
            $scope.AllQuaTrinhDaoTao = [{ MaSoVC: item }];


        }


       

        $scope.solanlap = [1];
        $scope.solanlap1 = [1];
        $scope.solanlap2 = [1];
        $scope.click=0;
        $scope.myFunction = function () {
            $scope.click++;
            $scope.solanlap = [];

            for (var i = 1; i <= $scope.click; i++) {
                $scope.solanlap.push(i);
              
            }
            return $scope.solanlap;
        }


            $scope.clicks = 0;
            $scope.myFunction1 = function () {

                $scope.clicks++;
                $scope.solanlap1 = [];
                for (var i = 1; i <= $scope.clicks; i++) {
                    $scope.solanlap1.push(i);
                }
                return $scope.solanlap1;
        }

            $scope.clickss = 0;
            $scope.myFunction2 = function () {

                $scope.clickss++;
                $scope.solanlap2 = [];
                for (var i = 1; i <= $scope.clickss; i++) {
                    $scope.solanlap2.push(i);
                }
                return $scope.solanlap2;
            }

            

            //var table = document.getElementById("myTable");
            //var row = table.insertRow(length - 1);
            
            //var cell1 = row.insertCell(0);
            //var cell2 = row.insertCell(1);
            //var cell3 = row.insertCell(2);
            //var cell4 = row.insertCell(3);
            //var cell5 = row.insertCell(4);
            //var cell6 = row.insertCell(5);
            //var cell7 = row.insertCell(6);
            //var a = 2;
            
            //cell1.innerHTML = a++;
            //cell2.innerHTML = "<input ng-model='AllTrinhDoNgoaiNgu[$index].MaSoVC' type='text' />";
            //cell3.innerHTML = "<input ng-model='AllTrinhDoNgoaiNgu[$index].Nghe' type='text'  />";
            //cell4.innerHTML = "<input ng-model='AllTrinhDoNgoaiNgu[$index].Noi' type='text'  />";
            //cell5.innerHTML = "<input ng-model='AllTrinhDoNgoaiNgu[$index].Viet' type='text'  />";
            //cell6.innerHTML = "<input ng-model='AllTrinhDoNgoaiNgu[$index].Doc' type='text'  />";
            //cell7.innerHTML = "<input ng-model='AllTrinhDoNgoaiNgu[$index].GhiChu' type='text' />";
            
            //var row = document.getElementById("rowToClone"); // find row to copy
            //var table = document.getElementById("tableToModify"); // find table to append to
            //var clone = row.cloneNode(true); // copy children too
            //clone.id = "newID"; // change id or other attributes/contents
            //table.appendChild(clone); // add new row to end of table
       


        function AddVienChucChitiet() {

            apiService.post('/api/vienchuc/create', $scope.AllVienChucChitiet, function (result) {
                notificationService.displaySuccess("Thêm thành công");
            }, function () {
                notificationService.displaySuccess('Mã số viên chức đã tồn tại');
            });



        }

        $scope.ChooseImage = function () {
            var finder = new CKFinder();
            finder.selectActionFunction = function (fileUrl) {
                $scope.AllVienChucChitiet.Hinh = fileUrl;
            }
            finder.popup();
            $scope.putupdateimage();


        }

        function AddTrinhDoNgoaiNgu() {

            apiService.post('/api/TrinhDoNgoaiNgu/create', $scope.AllTrinhDoNgoaiNgu, function (result) {
                notificationService.displaySuccess("Thêm thành công");
            }, function () {
                notificationService.displaySuccess('Mã số viên chức đã tồn tại');
            });
        }

       


        function AddTrinhDoNgoaiNgu() {

            apiService.post('/api/TrinhDoNgoaiNgu/create', $scope.AllTrinhDoNgoaiNgu, function (result) {
                notificationService.displaySuccess("Thêm thành công");
            }, function () {
                notificationService.displaySuccess('Mã số viên chức đã tồn tại');
            });
        }

        function AddThoigiancongtac() {

            apiService.post('/api/ThoiGianCongTac/create', $scope.AllThoiGianCongTac, function (result) {
                notificationService.displaySuccess("Thêm thành công");
            }, function () {
                notificationService.displaySuccess('Thêm thất bại');
            });
        }
        function AddQuaTrinhDaoTao() {

            apiService.post('/api/QuaTrinhDaoTao/create', $scope.AllQuaTrinhDaoTao, function (result) {
                notificationService.displaySuccess("Thêm thành công");
            }, function () {
                notificationService.displaySuccess('Thêm thất bại');
            });
        }
        function loadGroups() {
            apiService.get('/api/applicationGroup/getlistall',
                null,
                function (response) {
                    $scope.groups = response.data;
                }, function (response) {
                    notificationService.displayError('Không tải được danh sách nhóm.');
                });

        }
        function loadRoles() {
            apiService.get('/api/applicationRole/getlistall',
                null,
                function (response) {
                    $scope.roles = response.data;
                }, function (response) {
                    notificationService.displayError('Không tải được danh sách quyền.');
                });

        }

        loadGroups();
        loadRoles();

    }
})(angular.module('platformTH_GV.themthongtinvienchuc'));