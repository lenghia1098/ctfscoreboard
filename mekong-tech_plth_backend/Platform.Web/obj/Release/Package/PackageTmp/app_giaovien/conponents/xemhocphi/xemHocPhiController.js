﻿(function (app) {
    app.controller('xemHocPhiController', xemHocPhiController);

    xemHocPhiController.$inject = ['$scope', 'apiService', '$stateParams', 'loginService', '$injector', 'notificationService'];
    function xemHocPhiController($scope, apiService, $stateParams, loginService, $injector, notificationService) {
        if ($scope.kiemtrarole('SinhVien') === 'SinhVien' && $scope.kiemtrarole('Admin') != 'Admin') {
            loginService.logOut();
            var stateService = $injector.get('$state');
            stateService.go('dangnhap');
            notificationService.displayError("Bạn không có quyền truy cập trang này");
        }
        else {
            //#region
            $scope.thongTincaNhan = [];
            $scope.hocPhi = [];

            $scope.mssv = "";
            $scope.hocKy = "";
            $scope.hocKyHP = "";


            $scope.getThongTinCaNhan = getThongTinCaNhan;
            $scope.getHocPhi = getHocPhi;
            $scope.getHocKy = getHocKy;

            function getThongTinCaNhan() {
                var config = {
                    params: {
                        Id: $scope.mssv,


                    }
                }
                apiService.get('/api/sinhvien/getTTCN/', config, function (result) {
                    $scope.thongTincaNhan = result.data;
                }, function () {
                    console.log('Load thông tin failed.');
                });

            }


            function getHocPhi(hocKyHP) {
                var config = {
                    params: {
                        mssv: $scope.mssv,
                        hocKy: $scope.hocKyHP.HocKy,
                        namHoc: $scope.hocKyHP.NamHoc


                    }
                }
                apiService.get('/api/hocphi/getbyid/', config, function (result) {
                    $scope.hocPhi = result.data;
                }, function () {
                    console.log('Load thông tin failed.');
                });

            }


            function getHocKy() {
                var config = {
                    params: {
                        mssv: $scope.mssv


                    }
                }
                apiService.get('/api/hocphi/GetHocKy', config, function (result) {
                    $scope.hocKy = result.data;
                }, function () {
                    console.log('Load thông tin failed.');
                });

            }
            //#endregion

        }
        


        
       
        }
       
})(angular.module('platformTH_GV.xemhocphi'));