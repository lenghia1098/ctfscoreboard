﻿(function (app) {
    app.controller('tinhHocPhiController', tinhHocPhiController);
    tinhHocPhiController.$inject = ['$scope', 'apiService', '$stateParams','notificationService'];
   

    
    function tinhHocPhiController($scope, apiService, $stateParams, notificationService ) {
        $scope.password = 123;
        $scope.DanhSachSinhVien = [];
        $scope.saveTinhHocPhi = [];
        $scope.hocKy = [];
        $scope.getDanhSachSinhVien = getDanhSachSinhVien;
      
        $scope.AddTinhHocPhi = AddTinhHocPhi;
        $scope.gethockyhientai = gethockyhientai;
        $scope.Update = Update;

       
        function gethockyhientai() {

            apiService.get('/api/lichthi/getHocKyHienTai/', null, function (result) {
                $scope.hocKyHienTai = result.data[result.data.length - 1];
                $scope.hocKy = result.data[result.data.length - 1].HocKy;
                $scope.namhoc = result.data[result.data.length - 1].NamHoc;
                $scope.DaTinhHocPhi = result.data[result.data.length - 1].DaTinhHocPhi;
                if ($scope.DaTinhHocPhi === false || $scope.DaTinhHocPhi === null) {
                    $scope.getDanhSachSinhVien();
                }
                else {
                    alert("Da Tinh Hoc Phi")
                }

                console.log($scope.DaTinhHocPhi);
            }, function () {
                console.log('Load thông tin failed.');
            });

        }

      

       

       
        function getDanhSachSinhVien() {
            apiService.get('/api/dangky_tamthoi/getDangKyTanThoi_TinhHocPhi', null, function (result) {
               
                $scope.DanhSachSinhVien = result.data;
                var newArr = [];
                angular.forEach($scope.DanhSachSinhVien, function (value) {
                    var exists = false;
                    angular.forEach(newArr, function (val2) {
                        if (angular.equals(value.MaSoSV, val2.MaSoSV)) {
                            exists = true
                            val2.SoTinChi += value.SoTinChi
                            val2.SoTinChiHocPhi += value.SoTinChiHocPhi
                         
                        };
                    });
                    if (exists == false && value.MaSoSV != "") { newArr.push(value); }
                });
                $scope.newArr = newArr;
                angular.forEach(newArr, function (item) {
                    item.TongSoTinChi = item.SoTinChi;
                    item.TongSoHocPhi = item.SoTinChiHocPhi * 27000;
                    item.TongSoTinChiHocPhi = item.SoTinChiHocPhi;
                    
                  
                });

            }, function () {
                console.log('Load thông tin failed.');

            });
        }

        

        function AddTinhHocPhi() {


            apiService.post('/api/hocphi/create', $scope.newArr, function (result) {
                notificationService.displaySuccess("Thêm thành công");
                $scope.Update();
            }, function () {
                notificationService.displaySuccess('Thêm không thành công');
            });
        }



        function Update() {
            apiService.put('/api/hockyhientai/update', $scope.hocKyHienTai, function (result) {
               
            }, function () {
                notificationService.displaySuccess('Thêm không thành công');
            });
        }


      


        $scope.clickmodangnhap = function () {
            $scope.modangnhap = true;

        }


        

        $scope.clickmobangtinhhocphi = function (item) {
            if (item == $scope.password) {
                $scope.loadbangtinhhocphi = true;

            }
            else {
                alert("Sai mật khẩu");
            }

        }


      
        $scope.gethockyhientai()

    }
})(angular.module('platformTH_GV.tinhhocphi'));