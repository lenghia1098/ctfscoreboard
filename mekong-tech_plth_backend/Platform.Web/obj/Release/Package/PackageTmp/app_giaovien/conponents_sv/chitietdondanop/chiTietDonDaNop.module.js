﻿/// <reference path="../../../assets/sinhvien/libs/angular/angular.js" />


(function () {
    angular.module('platformTH_GV.chitietdondanop_sv', ['platformTH_GV.common']).config(config);
    window.appVersion = angular.element(document.getElementsByTagName('html')).attr('data-ver');

    config.$inject = ['$stateProvider', '$urlRouterProvider'];

    function config($stateProvider, $urlRouterProvider) {
        $stateProvider.state('chitietdondanop_sv', {
            url: "/chitietdondanop_sv/:MaDon",
            parent: 'base',
            templateUrl: "app_giaovien/conponents_sv/chitietdondanop/chiTietDonDaNop_svView.html?v=" + window.appVersion,
            controller: "chiTietDonDaNop_svController"
        });

    }
})();