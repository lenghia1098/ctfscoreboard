﻿/// <reference path="../../../assets/sinhvien/libs/angular/angular.js" />


(function () {
    angular.module('platformTH_GV.hocphi', ['platformTH_GV.common']).config(config);

    config.$inject = ['$stateProvider', '$urlRouterProvider'];
    window.appVersion = angular.element(document.getElementsByTagName('html')).attr('data-ver');

    function config($stateProvider, $urlRouterProvider) {
        $stateProvider.state('hocphi', {
            url: "/hocphi",
            parent: 'base',
            templateUrl: "app_giaovien/conponents_sv/hocphi/hocPhiView.html?v=" + window.appVersion,
            controller: "hocPhiController"
        });
    }
})();