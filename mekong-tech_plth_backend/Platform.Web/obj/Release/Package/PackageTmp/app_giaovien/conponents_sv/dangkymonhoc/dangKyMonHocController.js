﻿(function (app) {
    app.controller('dangKyMonHocController', dangKyMonHocController);

   


    dangKyMonHocController.$inject = ['$scope', 'apiService', '$stateParams', 'filterFilter', '$window', '$state', '$ngBootbox', 'notificationService', 'loginService', '$injector'];
   
    //show table dangkymonhoc:
   


    function dangKyMonHocController($scope, apiService, $stateParams, filterFilter, $window, $state, $ngBootbox, notificationService, loginService, $injector) {
        if ($scope.kiemtrarole('SinhVien') === 'SinhVien') {

            //#region
            $scope.hocKyHienTai = [];
            $scope.getHocKyHienTai = getHocKyHienTai;
            function getHocKyHienTai() {
                apiService.get('/api/hockyhientai/getall/', null, function (result) {

                    $scope.hocKyHienTai = result.data[result.data.length - 1];
                    $scope.hocKy = result.data[result.data.length - 1].HocKy;
                    $scope.namhoc = result.data[result.data.length - 1].NamHoc;
                }, function () {
                    console.log('Load thông tin failed.');
                });

            }
            $scope.getHocKyHienTai();
            $scope.luachon = ["Lớp", "Ngành", "Khoa"];
            $scope.MaSoSV = $scope.authentication.userName;;
            $scope.ngayDangKy = new Date();


            $scope.monHocTheoTen;
            $scope.getMonHocTheoTen = getMonHocTheoTen;

            $scope.Lop;
            $scope.getLop = getLop;

            $scope.Nganh;
            $scope.getNganh = getNganh;

            $scope.Khoa;
            $scope.getKhoa = getKhoa;

            $scope.tenLop = "";
            $scope.tenNganh = "";
            $scope.tenPhongKhoa = "";


            $scope.temMH = "";




            //  $scope.user.roles = [];
            $scope.check = function () {
                $scope.dangKyTamThoi = angular.copy($scope.user.roles);
            };
            $scope.dangKyTamThoi = {

            }


            $scope.AddDangKy_TamThoi = AddDangKy_TamThoi;


            function AddDangKy_TamThoi() {
                $ngBootbox.confirm('Xác nhận nộp ĐKMH?').then(function () {
                    apiService.post('api/dangky_tamthoi/create', $scope.user.roles,
                        function (result) {
                            notificationService.displaySuccess("Đăng ký môn học thành công")
                            $scope.getdangKy_TamThoi();
                            $scope.dangKyTamThoi = null;
                            $scope.user.roles = null;
                        }, function () {
                            notificationService.displaySuccess('Thêm mới không thành công.');
                        })
                });
            }








            $scope.myVar = false;
            $scope.toggle = function () {
                $scope.myVar = !$scope.myVar;
                $scope.btnsua = false;
                $scope.btnhuy = true;


            };

            $scope.btnsua = true;
            $scope.btnhuy = false;
            $scope.btnxacnhan = false;

            $scope.huy = function () {
                $scope.btnsua = true;
                $scope.btnhuy = false;
                $scope.user.roles = null;
                $scope.dangKyTamThoi = null;
                $scope.myVar = false;
            }


            $scope.danhSachMonHocLop = [];
            $scope.getDanhSachMonHocLop = getDanhSachMonHocLop;
            function getDanhSachMonHocLop() {
                $scope.monHocTheoTen = null;
                var config = {
                    params: {
                        tenLop: $scope.tenLop,

                        hocKy: $scope.hocKy,
                        namhoc: $scope.namhoc,
                        mssv: $scope.MaSoSV,
                        ngayDangKy: $scope.ngayDangKy

                    }
                }
                apiService.get('/api/monhoc/getDSMHTheoLop/', config, function (result) {
                    $scope.danhSachMonHocLop = result.data;
                }, function () {
                    console.log('Load thông tin failed.');
                });

            };
            // $scope.getDanhSachMonHocLop();

            $scope.danhSachMonHocNganh = [];
            $scope.getDanhSachMonHocNganh = getDanhSachMonHocNganh;
            function getDanhSachMonHocNganh() {

                var config = {
                    params: {
                        tenNganh: $scope.tenNganh,

                        hocKy: $scope.hocKy,
                        namhoc: $scope.namhoc,
                        mssv: $scope.MaSoSV,
                        ngayDangKy: $scope.ngayDangKy

                    }
                }
                apiService.get('/api/monhoc/getDSMHTheoNganh/', config, function (result) {
                    $scope.danhSachMonHocNganh = result.data;
                }, function () {
                    console.log('Load thông tin failed.');
                });

            };
            //$scope.getDanhSachMonHocNganh();


            $scope.danhSachMonHocKhoa = [];
            $scope.getDanhSachMonHocKhoa = getDanhSachMonHocKhoa;
            function getDanhSachMonHocKhoa() {
                var config = {
                    params: {
                        tenKhoa: $scope.tenPhongKhoa,

                        hocKy: $scope.hocKy,
                        namhoc: $scope.namhoc,
                        mssv: $scope.MaSoSV,
                        ngayDangKy: $scope.ngayDangKy

                    }
                }
                apiService.get('/api/monhoc/getDSMHTheoKhoa/', config, function (result) {
                    $scope.danhSachMonHocKhoa = result.data;
                }, function () {
                    console.log('Load thông tin failed.');
                });

            };
            // $scope.getDanhSachMonHocKhoa();





            function getMonHocTheoTen() {
                $scope.danhSachMonHocLop = null;
                $scope.danhSachMonHocNganh = null;
                $scope.danhSachMonHocKhoa = null;
                var config = {
                    params: {

                        tenMH: $scope.temMH,
                        hocKy: $scope.hocKy,
                        namhoc: $scope.namhoc,
                        mssv: $scope.MaSoSV,
                        ngayDangKy: $scope.ngayDangKy

                    }
                }
                apiService.get('/api/monhoc/getThongTinMonHoc/', config, function (result) {
                    $scope.monHocTheoTen = result.data;
                }, function () {
                    console.log('Load thông tin failed.');
                });

            };
            // $scope.getMonHocTheoTen();


            function getLop() {

                apiService.get('/api/lop/getall/', null, function (result) {
                    $scope.Lop = result.data;
                }, function () {
                    console.log('Load thông tin failed.');
                });

            };
            $scope.getLop();

            function getNganh() {

                apiService.get('/api/nganh/getall/', null, function (result) {
                    $scope.Nganh = result.data;
                }, function () {
                    console.log('Load thông tin failed.');
                });

            };
            $scope.getNganh();


            function getKhoa() {

                apiService.get('/api/phongkhoa/getall/', null, function (result) {
                    $scope.Khoa = result.data;
                }, function () {
                    console.log('Load thông tin failed.');
                });

            };
            $scope.getKhoa();

            $scope.dangKy_TamThoi = [];
            $scope.getdangKy_TamThoi = getdangKy_TamThoi;
            function getdangKy_TamThoi() {
                var config = {
                    params: {

                        mssv: $scope.MaSoSV,

                    }
                }
                apiService.get('/api/dangky_tamthoi/getDangKy_TamThoi/', config, function (result) {
                    $scope.dangKy_TamThoi = result.data;
                }, function () {
                    console.log('Load thông tin failed.');
                });

            };
            $scope.getdangKy_TamThoi();



            $scope.user = {

            };
            $scope.checkAll = function () {
                $scope.user.roles = angular.copy($scope.roles);
            };
            $scope.uncheckAll = function () {
                $scope.user.roles = [];
            };
            $scope.checkFirst = function () {
                $scope.user.roles.splice(0, $scope.user.roles.length);
                $scope.user.roles.push($scope.roles[0]);
            };




            $scope.deleteDangKy = deleteDangKy;

            function deleteDangKy(ID) {
                $ngBootbox.confirm('Bạn có chắc muốn xóa?').then(function () {
                    var config = {
                        params: {
                            ID: ID
                        }
                    }
                    apiService.del('api/dangky_tamthoi/delete', config, function () {
                        console.log('Xóa thành công');
                        notificationService.displaySuccess("Xóa thành công")
                        $scope.getdangKy_TamThoi();
                        //$window.location.reload();
                    }, function () {
                        notificationService.displaySuccess('Xóa không thành công');
                    })
                });
            }

            $scope.checkdacomonhoc = function (MaMH, Thu, Tiet, SoTiet) {
                var result = [];
                $scope.dangKy_TamThoi.filter(function (item) {
                    if (item.MaMH === MaMH || (item.Thu == Thu && item.Tiet == Tiet && item.Sotiet == SoTiet)) {
                        result.push(1);

                    }
                    else result.push(2);

                })
                return result;
            }
            $scope.checkdacomonhoc();
            $scope.ThoiHanDangKyMonHoc = [];
            $scope.getThoiHanDangKyMonHoc = getThoiHanDangKyMonHoc;
            function getThoiHanDangKyMonHoc() {

                apiService.get('/api/thoihandangkymonhoc/getall/', null, function (result) {
                    $scope.ThoiHanDangKyMonHoc = result.data;
                    $scope.ngaydau = new Date($scope.ThoiHanDangKyMonHoc[0].NgayDau).getTime();
                    $scope.ngaycuoi = new Date($scope.ThoiHanDangKyMonHoc[0].NgayCuoi).getTime();


                }, function () {
                    console.log('Load thông tin failed.');
                });

            };
            $scope.getThoiHanDangKyMonHoc();

            $scope.ngayHienTai = new Date().getTime();

            $scope.xoa = function ($index) {


                $scope.dangKyTamThoi.splice($index, 1);
                $scope.user.roles.splice($index, 1);
                console.log($index)
            }

            $scope.showlop = false;
            $scope.vuotsotinchi = function (MaMH, Thu, Tiet, SoTiet) {
                var tinchitamthoi = 0;
                var tinchithemvao = 0;
                var sumtc = 0;
                $scope.dangKy_TamThoi.filter(function (item) {
                    tinchitamthoi += item.SoTinChi;

                })
                $scope.user.roles.filter(function (item) {
                    tinchithemvao += item.SoTinChi;

                })
                sumtc = tinchitamthoi + tinchithemvao;
                if (sumtc > 20) {

                    $scope.dangKyTamThoi.splice(-1, 1);
                    $scope.user.roles.splice(-1, 1);
                    alert("Tổng số tín chỉ không được lớn hơn 20");

                }


                $scope.countmonhoctrung = "1"
                $scope.dangKyTamThoi.filter(function (item) {

                    if (item.MaMH == MaMH) {

                        $scope.countmonhoctrung++;

                    }
                })
                if ($scope.countmonhoctrung > 2) {
                    alert("Môn học đã trùng với môn đã chọn");
                    $scope.dangKyTamThoi.splice(-1, 1);
                    $scope.user.roles.splice(-1, 1);
                }

                $scope.countthoigianhoctrung = "1"
                $scope.dangKyTamThoi.filter(function (item) {

                    if (item.Thu == Thu && item.Tiet == Tiet && item.Sotiet == SoTiet) {

                        $scope.countthoigianhoctrung++;

                    }
                })
                if ($scope.countthoigianhoctrung > 2) {
                    alert("Thời gian học đã trùng");
                    $scope.dangKyTamThoi.splice(-1, 1);
                    $scope.user.roles.splice(-1, 1);
                }
                return
            }
            //#endregion
        }
        else {
            loginService.logOut();
            var stateService = $injector.get('$state');
            stateService.go('dangnhap');
            notificationService.displayError("Bạn không có quyền truy cập trang này");

        }
        
    }
})(angular.module('platformTH_GV.dangkymonhoc'));