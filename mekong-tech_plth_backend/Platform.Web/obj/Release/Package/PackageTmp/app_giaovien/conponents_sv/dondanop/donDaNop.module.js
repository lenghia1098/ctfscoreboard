﻿/// <reference path="../../../assets/sinhvien/libs/angular/angular.js" />


(function () {
    angular.module('platformTH_GV.dondanop_sv', ['platformTH_GV.common']).config(config);

    config.$inject = ['$stateProvider', '$urlRouterProvider'];
    window.appVersion = angular.element(document.getElementsByTagName('html')).attr('data-ver');

    function config($stateProvider, $urlRouterProvider) {
        $stateProvider.state('dondanop_sv', {
            url: "/dondanop_sv",
            parent: 'base',
            templateUrl: "app_giaovien/conponents_sv/dondanop/donDaNop_svView.html?v=" + window.appVersion,
            controller: "donDaNop_svController"
        });

    }
})();