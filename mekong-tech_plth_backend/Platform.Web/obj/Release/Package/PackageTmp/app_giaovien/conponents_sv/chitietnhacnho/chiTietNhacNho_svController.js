﻿(function (app) {
    app.controller('chiTietNhacNho_svController', chiTietNhacNho_svController);

    function chiTietNhacNho_svController($scope, apiService, $stateParams, loginService, $injector, notificationService) {
        if ($scope.kiemtrarole('SinhVien') === 'SinhVien') {

            //#region
            $scope.chitietnhacnho = [];
            $scope.MaSoNN = $stateParams.MaSoNN;


            $scope.getChiTietNhacNho = getChiTietNhacNho;


            function getChiTietNhacNho() {
                var config = {
                    params: {
                        MaSoNN: $scope.MaSoNN
                    }
                }
                apiService.get('/api/nhacnho/Getchitietnhacnho/', config, function (result) {
                    $scope.chitietnhacnho = result.data;
                }, function () {
                    console.log('Load thông tin failed.');
                });

            }
            $scope.getChiTietNhacNho();
            //#endregion
        }
        else {
            loginService.logOut();
            var stateService = $injector.get('$state');
            stateService.go('dangnhap');
            notificationService.displayError("Bạn không có quyền truy cập trang này");

        }
       

    }
})(angular.module('platformTH_GV.chitietnhacnho_sv'));