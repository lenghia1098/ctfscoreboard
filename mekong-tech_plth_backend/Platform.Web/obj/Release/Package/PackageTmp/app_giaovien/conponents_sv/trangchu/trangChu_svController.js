﻿
(function (app) {
    app.controller('trangChu_svController', trangChu_svController);
    trangChu_svController.$inject = ['$scope', 'apiService', '$stateParams', 'loginService', '$injector', 'notificationService'];
    function trangChu_svController($scope, apiService, $stateParams, loginService, $injector, notificationService) {
        if ($scope.kiemtrarole('SinhVien') === 'SinhVien') {

            //#region
            $scope.demvieclamsinhvien = "0";
            $scope.demdonsinhvien = "0";
            $scope.mssv = $scope.authentication.userName;
            $scope.nhacNho = [];
            $scope.tintuc = [];
            $scope.thongBao = [];
            $scope.getNhacNho = getNhacNho;
            $scope.getThongBao = getThongBao;
            $scope.getdondanop = getdondanop;
            $scope.getTinTuc = getTinTuc;
            $scope.getViecPhaiLamSinhVien = getViecPhaiLamSinhVien;


            function getViecPhaiLamSinhVien() {
                var config = {
                    params: {
                        mssv: $scope.mssv
                    }
                }
                apiService.get('/api/ViecPhaiLamSinhVien/demviecdalam/', config, function (result) {
                    $scope.demvieclamsinhvien = result.data;
                }, function () {
                    console.log('Load thông tin failed.');
                });

            }
            function getdondanop() {
                var config = {
                    params: {
                        mssv: $scope.mssv
                    }
                }
                apiService.get('/api/donsinhvien/demdonsinhvien/', config, function (result) {
                    $scope.demdonsinhvien = result.data;
                }, function () {
                    console.log('Load thông tin failed.');
                });

            }

            function getNhacNho(page) {
                page = page || 0;
                var config = {
                    params: {
                        page: page,
                        mssv: $scope.mssv,
                        pageSize: 5,
                    }
                }
                apiService.get('/api/nhacnho-sv/getNhacNhoChuaHoanThanh/', config, function (result) {

                    $scope.nhacNho = result.data.Items;
                    $scope.page = result.data.Page;
                    $scope.pagesCount = result.data.TotalPages;
                    $scope.TotalCount = result.data.TotalCount;

                }, function () {
                    console.log('Load thông tin failed.');
                });

            };


            function getThongBao(page) {
                page = page || 0;
                var config = {
                    params: {
                        page: page,
                        mssv: $scope.mssv,
                        pageSize: 5,
                    }
                }
                apiService.get('/api/ThongBao/getTB/', config, function (result) {
                    $scope.thongBao = result.data.Items;
                    $scope.page = result.data.Page;
                    $scope.pagesCount = result.data.TotalPages;
                    $scope.TotalCount = result.data.TotalCount;


                }, function () {
                    console.log('Load thông tin failed.');
                });

            }


            function getTinTuc(page) {
                page = page || 0;
                var config = {
                    params: {
                        page: page,
                        mssv: $scope.mssv,
                        pageSize: 5,
                    }
                }
                apiService.get('/api/TinTuc/getTT/', config, function (result) {
                    $scope.tintuc = result.data.Items;
                    $scope.page1 = result.data.Page;
                    $scope.pagesCount = result.data.TotalPages;
                    $scope.TotalCount = result.data.TotalCount;



                }, function () {
                    console.log('Load thông tin failed.');
                });

            }




            $scope.getTinTuc();
            $scope.getNhacNho();
            $scope.getThongBao();
            $scope.getViecPhaiLamSinhVien();
            $scope.getdondanop();
            //#endregion
        }
        else {
            loginService.logOut();
            var stateService = $injector.get('$state');
            stateService.go('dangnhap');
            notificationService.displayError("Bạn không có quyền truy cập trang này ");

        }
       
    }
})(angular.module('platformTH_GV'));
