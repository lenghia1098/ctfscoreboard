﻿(function (app) {
    app.controller('hocPhiController', hocPhiController);
    hocPhiController.$inject = ['$scope', 'apiService', '$stateParams', 'loginService', '$injector', 'notificationService'];
    function hocPhiController($scope, apiService, $stateParams, loginService, $injector, notificationService) {
        if ($scope.kiemtrarole('SinhVien') === 'SinhVien') {

            //#region
            $scope.thongTincaNhan = [];
            $scope.hocKyDaChon = [];
            $scope.hocPhi = [];


            $scope.mssv = $scope.authentication.userName;;
            $scope.hocKyDaChon = "1";

            // $scope.hocKyDaChon[0].HocKy = "1";


            $scope.getThongTinCaNhan = getThongTinCaNhan;
            $scope.getHocPhi = getHocPhi;


            function getThongTinCaNhan() {
                var config = {
                    params: {
                        Id: $scope.mssv,


                    }
                }
                apiService.get('/api/sinhvien/getTTCN/', config, function (result) {
                    $scope.thongTincaNhan = result.data;
                }, function () {
                    console.log('Load thông tin failed.');
                });

            }


            function getHocPhi() {

                var config = {
                    params: {
                        mssv: $scope.mssv,
                        hocKy: $scope.hocKyDaChon.HocKy,
                        namHoc: $scope.hocKyDaChon.NamHoc


                    }
                }
                apiService.get('/api/hocphi/getbyid/', config, function (result) {
                    $scope.hocPhi = result.data;
                }, function () {
                    console.log('Load thông tin failed.');
                });

            }





            ////in hoc phi

            $scope.print = function () {
                console.log('modal print');

                var table = document.querySelector('.CSSTableGenerator').innerHTML;
                var myWindow = window.open('', '', 'width=800, height=600');
                myWindow.document.write(table);
                myWindow.print();

            };

            ///get hoc ky


            $scope.getHocKy = getHocKy
            function getHocKy() {
                var config = {
                    params: {
                        mssv: $scope.mssv,


                    }
                }
                apiService.get('/api/hocphi/GetHocKy', config, function (result) {
                    $scope.hocKy = result.data;
                }, function () {
                    console.log('Load thông tin failed.');
                });

            }
            $scope.getHocKy();
            $scope.getThongTinCaNhan();

            //#endregion
        }
        else {
            loginService.logOut();
            var stateService = $injector.get('$state');
            stateService.go('dangnhap');
            notificationService.displayError("Bạn không có quyền truy cập trang này");

        }
        

    }
})(angular.module('platformTH_GV.hocphi'));