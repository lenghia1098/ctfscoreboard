﻿/// <reference path="../../../assets/sinhvien/libs/angular/angular.js" />


(function () {
    angular.module('platformTH_GV.bangdiem', ['platformTH_GV.common']).config(config);
    window.appVersion = angular.element(document.getElementsByTagName('html')).attr('data-ver');

    config.$inject = ['$stateProvider', '$urlRouterProvider'];

    function config($stateProvider, $urlRouterProvider) {
        $stateProvider.state('bangdiem', {
            url: "/bangdiem/:Id",
            parent: 'base',
            templateUrl: "app_giaovien/conponents_sv/bangdiem/bangDiemView.html?v=" + window.appVersion,
            controller: "bangDiemController"
        });
       
    }
})();