﻿/// <reference path="../../../assets/sinhvien/libs/angular/angular.js" />


(function () {
    angular.module('platformTH_GV.thongbao_sv', ['platformTH_GV.common']).config(config);
    window.appVersion = angular.element(document.getElementsByTagName('html')).attr('data-ver');

    config.$inject = ['$stateProvider', '$urlRouterProvider'];

    function config($stateProvider, $urlRouterProvider) {
        $stateProvider.state('thongbao_sv', {
            url: "/thongbao_sv/:MaSoTB",
            parent: 'base',
            templateUrl: "app_giaovien/conponents_sv/thongbao/thongBao_svView.html?v=" + window.appVersion,
            controller: "thongBao_svController"
        });
        $urlRouterProvider.otherwise('/trangchu');
    }
})();