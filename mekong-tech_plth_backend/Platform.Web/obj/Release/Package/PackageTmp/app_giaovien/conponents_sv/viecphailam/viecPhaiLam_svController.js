﻿(function (app) {
    app.controller('viecPhaiLam_svController', viecPhaiLam_svController);

    function viecPhaiLam_svController($scope, apiService, $stateParams, loginService, $injector, notificationService) {
        if ($scope.kiemtrarole('SinhVien') === 'SinhVien') {

            //#region
            $scope.viecPhaiLam = [];
            $scope.mssv = $scope.authentication.userName;
            $scope.getViecPhaiLam = getViecPhaiLam;


            function getViecPhaiLam(page) {
                page = page || 0;
                var config = {
                    params: {
                        page: page,
                        mssv: $scope.mssv,
                        pageSize: 5,
                    }
                }
                apiService.get('/api/ViecPhaiLamSinhVien/getviecphailam/', config, function (result) {

                    $scope.viecPhaiLam = result.data.Items;
                    $scope.page = result.data.Page;
                    $scope.pagesCount = result.data.TotalPages;
                    $scope.TotalCount = result.data.TotalCount;
                }, function () {
                    console.log('Load thông tin failed.');
                });

            }
            $scope.getViecPhaiLam();
            //#endregion
        }
        else {
            loginService.logOut();
            var stateService = $injector.get('$state');
            stateService.go('dangnhap');
            notificationService.displayError("Bạn không có quyền truy cập trang này");

        }
       

    }
})(angular.module('platformTH_GV.viecphailam_sv'));