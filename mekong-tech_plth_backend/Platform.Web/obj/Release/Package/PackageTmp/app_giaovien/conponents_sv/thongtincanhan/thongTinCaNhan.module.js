﻿/// <reference path="../../../assets/sinhvien/libs/angular/angular.js" />


(function () {
    angular.module('platformTH_GV.thongtincanhan_sv', ['platformTH_GV.common']).config(config);
    window.appVersion = angular.element(document.getElementsByTagName('html')).attr('data-ver');

    config.$inject = ['$stateProvider', '$urlRouterProvider'];

    function config($stateProvider, $urlRouterProvider) {
        $stateProvider.state('thongtincanhan_sv', {
            url: "/thongtincanhan_sv/:Id",
            parent: 'base',
            templateUrl: "app_giaovien/conponents_sv/thongtincanhan/thongTinCaNhan_svView.html?v=" + window.appVersion,
            controller: "thongTinCaNhan_svController"
        });
        $urlRouterProvider.otherwise('/trangchu');
   

    }
})();