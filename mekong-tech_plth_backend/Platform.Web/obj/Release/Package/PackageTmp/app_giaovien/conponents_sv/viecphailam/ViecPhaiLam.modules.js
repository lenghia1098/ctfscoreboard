﻿/// <reference path="../../../assets/sinhvien/libs/angular/angular.js" />


(function () {
    angular.module('platformTH_GV.viecphailam_sv', ['platformTH_GV.common']).config(config);
    window.appVersion = angular.element(document.getElementsByTagName('html')).attr('data-ver');

    config.$inject = ['$stateProvider', '$urlRouterProvider'];

    function config($stateProvider, $urlRouterProvider) {
        $stateProvider.state('viecphailam_sv', {
            url: "/viecphailam_sv",
            parent: 'base',
            templateUrl: "app_giaovien/conponents_sv/viecphailam/viecPhaiLam_svView.html?v=" + window.appVersion,
            controller: "viecPhaiLam_svController"
        });

    }
})();