﻿(function (app) {
    app.controller('chuongTrinhDaoTaoController', chuongTrinhDaoTaoController);
    chuongTrinhDaoTaoController.$inject = ['$scope', 'apiService', '$stateParams', 'loginService', '$injector', 'notificationService'];
    function chuongTrinhDaoTaoController($scope, apiService, $stateParams, loginService, $injector, notificationService) {
        if ($scope.kiemtrarole('SinhVien') === 'SinhVien') {

            //#region
            $scope.chuongTrinhDaoTao = [];
            $scope.monDaHoc = [];
            $scope.mssv = $scope.authentication.userName;;

            $scope.getChuongTrinhDaoTao = getChuongTrinhDaoTao;

            function getChuongTrinhDaoTao() {
                var config = {
                    params: {
                        mssv: $scope.mssv
                    }
                }
                apiService.get('/api/ctdt/GetChucnangchuongtrinhdaotaos/', config, function (result) {
                    $scope.chuongTrinhDaoTao = result.data;
                }, function () {
                    console.log('Load thông tin failed.');
                });

            }
            $scope.getChuongTrinhDaoTao();


            $scope.getMonDaHoc = getMonDaHoc;

            function getMonDaHoc() {
                var config = {
                    params: {
                        mssv: $scope.mssv
                    }
                }
                apiService.get('/api/ctdt/getMonDaHoc/', config, function (result) {
                    $scope.monDaHoc = result.data;
                }, function () {
                    console.log('Load thông tin failed.');
                });

            }
            $scope.getMonDaHoc();



            ////in CTDT


            $scope.print = function () {
                console.log('modal print');

                var table = document.querySelector('.CSSTableGenerator').innerHTML;
                var myWindow = window.open('', '', 'width=800, height=600');
                myWindow.document.write(table);
                myWindow.print();

            };


            ///checked
            $scope.getCheckMonDaHoc = function (id) {
                var result = [];
                $scope.monDaHoc.filter(function (item) {
                    if (item.MaMH === id) {
                        result.push(id);
                    }

                })
                return result;
            }
            $scope.getCheckDau = function (id) {
                var result = [];
                $scope.monDaHoc.filter(function (item) {
                    if (item.MaMH === id && item.Dau === true) {
                        result.push(id);
                    }

                })
                return result;
            }


            ////get TTCN
            $scope.thongTincaNhan = [];
            $scope.getThongTinCaNhan = getThongTinCaNhan;


            function getThongTinCaNhan() {
                var config = {
                    params: {
                        Id: $scope.mssv,

                    }
                }
                apiService.get('/api/sinhvien/getTTCN/', config, function (result) {
                    $scope.thongTincaNhan = result.data;
                }, function () {
                    console.log('Load thông tin failed.');
                });

            }

            $scope.getThongTinCaNhan();
            //#endregion
        }
        else {
            loginService.logOut();
            var stateService = $injector.get('$state');
            stateService.go('dangnhap');
            notificationService.displayError("Bạn không có quyền truy cập trang này");

        }
        




    }
})(angular.module('platformTH_GV.chuongtrinhdaotao'));