﻿/// <reference path="../../../assets/sinhvien/libs/angular/angular.js" />


(function () {
    angular.module('platformTH_GV.thoikhoabieu_sv', ['platformTH_GV.common']).config(config);

    config.$inject = ['$stateProvider', '$urlRouterProvider'];
    window.appVersion = angular.element(document.getElementsByTagName('html')).attr('data-ver');

    function config($stateProvider, $urlRouterProvider) {
        $stateProvider.state('thoikhoabieu_sv', {
            url: "/thoikhoabieu_sv",
            parent: 'base',
            templateUrl: "app_giaovien/conponents_sv/thoikhoabieu/thoiKhoaBieu_svView.html?v=" + window.appVersion,
            controller: "thoiKhoaBieu_svController"
        })

       

       
    }

})();