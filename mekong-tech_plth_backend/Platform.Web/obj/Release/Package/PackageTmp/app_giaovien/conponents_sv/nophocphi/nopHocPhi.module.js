﻿/// <reference path="../../../assets/sinhvien/libs/angular/angular.js" />


(function () {
    angular.module('platformTH.nophocphi', ['platformTH.common']).config(config);

    config.$inject = ['$stateProvider', '$urlRouterProvider'];
    window.appVersion = angular.element(document.getElementsByTagName('html')).attr('data-ver');

    function config($stateProvider, $urlRouterProvider) {
        $stateProvider.state('nophocphi', {
            url: "/nophocphi",
            parent: 'base',
            templateUrl: "app/conponents/nophocphi/nopHocPhiView.html?v=" + window.appVersion,
            controller: "nopHocPhiController"
        });
        $urlRouterProvider.otherwise('/trangchu');
    }
})();