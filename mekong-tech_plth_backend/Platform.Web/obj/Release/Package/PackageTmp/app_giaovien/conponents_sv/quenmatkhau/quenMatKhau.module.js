﻿/// <reference path="../../../assets/sinhvien/libs/angular/angular.js" />


(function () {
    angular.module('platformTH.quenmatkhau', ['platformTH.common']).config(config);

    config.$inject = ['$stateProvider', '$urlRouterProvider'];
    window.appVersion = angular.element(document.getElementsByTagName('html')).attr('data-ver');

    function config($stateProvider, $urlRouterProvider) {
        $stateProvider.state('quenmatkhau', {
            url: "/quenmatkhau",
            templateUrl: "app/conponents/quenmatkhau/quenMatKhauView.html?v=" + window.appVersion,
            controller: "quenMatKhauController"
        });
      

    }
})();