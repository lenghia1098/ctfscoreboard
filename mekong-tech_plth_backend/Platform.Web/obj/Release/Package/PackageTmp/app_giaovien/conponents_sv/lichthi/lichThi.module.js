﻿/// <reference path="../../../assets/sinhvien/libs/angular/angular.js" />


(function () {
    angular.module('platformTH_GV.lichthi_sv', ['platformTH_GV.common']).config(config);

    config.$inject = ['$stateProvider', '$urlRouterProvider'];
    window.appVersion = angular.element(document.getElementsByTagName('html')).attr('data-ver');

    function config($stateProvider, $urlRouterProvider) {
        $stateProvider.state('lichthi_sv', {
            url: "/lichthi_sv",
            parent: 'base',
            templateUrl: "app_giaovien/conponents_sv/lichthi/lichThi_svView.html?v=" + window.appVersion,
            controller: "lichThi_svController"
        });
    }
})();