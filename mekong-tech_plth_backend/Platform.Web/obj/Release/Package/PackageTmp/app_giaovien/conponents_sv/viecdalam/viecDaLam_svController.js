﻿(function (app) {
    app.controller('viecDaLam_svController', viecDaLam_svController);

    function viecDaLam_svController($scope, apiService, $stateParams, $filter, $ngBootbox, notificationService, loginService, $injector) {
        if ($scope.kiemtrarole('SinhVien') === 'SinhVien') {

            //#region
            $scope.viecDaLam = [];
            $scope.page = 0;
            $scope.pagesCount = 0;
            $scope.mssv = $scope.authentication.userName;
            $scope.getViecDaLam = getViecDaLam;
            $scope.selectAll = selectAll;
            $scope.selected = [];
            $scope.exist = function (item) {
                return $scope.selected.indexOf(item) > -1;

            }

            $scope.toge = function (item) {
                var idx = $scope.selected.indexOf(item);
                if (idx > -1) {
                    $scope.selected.splice(idx, 1);

                }
                else {
                    $scope.selected.push(item);
                    console.log($scope.selected);
                }
            }


            $scope.checkAll = function () {
                if ($scope.selectedAll) {
                    angular.forEach($scope.viecDaLam, function (item) {
                        idx = $scope.selected.indexOf(item);
                        if (idx >= 0) {
                            return true;
                        } else {
                            $scope.selected.push(item);
                        }
                    })
                }
                else {
                    $scope.selected = [];
                }
            }

            $scope.luu = {


            }





            function selectAll() {
                if ($scope.isAll === false) {
                    angular
                }
            }

            $scope.$watch("viecDaLam", function (n, o) {
                var checked = $filter("filter")(n, { checked: true });
                if (checked.length) {
                    $scope.selected = checked;
                    $('#btnDelete').removeAttr('disabled');

                } else {
                    $('#btnDelete').attr('disabled', 'disabled');
                }

            }, true);

            function getViecDaLam(page) {
                page = page || 0;
                var config = {
                    params: {
                        page: page,
                        mssv: $scope.mssv,
                        pageSize: 5,
                    }
                }
                apiService.get('/api/Sinhvien_ViecPhaiLam/viecdalam/', config, function (result) {
                    $scope.viecDaLam = result.data.Items;

                    $scope.page = result.data.Page;
                    $scope.pagesCount = result.data.TotalPages;
                    $scope.TotalCount = result.data.TotalCount;


                }, function () {
                    console.log('Load thông tin failed.');
                });

            }

            $scope.loadId = loadId;

            function loadId(id) {

                var config = {
                    params: {
                        mscv: id,

                    }
                }

                apiService.get('api/Sinhvien_ViecPhaiLam/getID/', config, function (result) {

                    $scope.luu = result.data;
                    console.log($scope.luu);
                    $scope.DeleteViecDaLam()

                }, function (error) {
                    console.log('Load product failed.');
                });
            }



            $scope.DeleteViecDaLam = DeleteViecDaLam;


            function DeleteViecDaLam() {



                $ngBootbox.confirm('Bạn chắc muốn xóa không').then(function () {

                    apiService.put('api/Sinhvien_ViecPhaiLam/update', $scope.luu, function (result) {

                        notificationService.displaySuccess("Xóa thành công");
                        $scope.getViecDaLam();


                    }, function () {
                        notificationService.displaySuccess('Xóa không thành công');
                    })
                });
            }


            $scope.DeleteAllViecDaLam = DeleteAllViecDaLam;


            function DeleteAllViecDaLam() {

                $ngBootbox.confirm('Bạn chắc muốn xóa không').then(function () {
                    angular.forEach($scope.selected, function (item) {
                        item.NguoiXoa = $scope.authentication.userName

                        apiService.put('api/Sinhvien_ViecPhaiLam/updateAll', item, function (result) {

                            //notificationService.displaySuccess("Xóa thành công");
                            $scope.getViecDaLam();


                        }, function () {
                            notificationService.displaySuccess('Xóa không thành công');
                        });
                    });
                });

            };



            $scope.allID = [];
            $scope.getAllId = getAllID;
            function getAllID() {
                var config = {
                    params: {
                        mssv: $scope.mssv,

                    }
                }

                apiService.get('api/Sinhvien_ViecPhaiLam/getIdAll/', config, function (result) {

                    $scope.allID = result.data;



                }, function (error) {
                    console.log('Load product failed.');
                });
            }

            $scope.getViecDaLam();
            $scope.getAllId();

            //#endregion
        }
        else {
            loginService.logOut();
            var stateService = $injector.get('$state');
            stateService.go('dangnhap');
            notificationService.displayError("Bạn không có quyền truy cập trang này");

        }
       
    }
})(angular.module('platformTH_GV.viecdalam_sv'));