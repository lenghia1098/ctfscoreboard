﻿/// <reference path="../../../assets/sinhvien/libs/angular/angular.js" />


(function () {
    angular.module('platformTH_GV.tintuc_sv', ['platformTH_GV.common']).config(config);

    config.$inject = ['$stateProvider', '$urlRouterProvider'];
    window.appVersion = angular.element(document.getElementsByTagName('html')).attr('data-ver');

    function config($stateProvider, $urlRouterProvider) {
        $stateProvider.state('tintuc_sv', {
            url: "/tintuc_sv/:MaTinTuc",
            parent: 'base',
            templateUrl: "app_giaovien/conponents_sv/tintuc/tinTuc_svView.html?v=" + window.appVersion,
            controller: "tinTuc_svController"
        });
        $urlRouterProvider.otherwise('/trangchu');
    }
})();