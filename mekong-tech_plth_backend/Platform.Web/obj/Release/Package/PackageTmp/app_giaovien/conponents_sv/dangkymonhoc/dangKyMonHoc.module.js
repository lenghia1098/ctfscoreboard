﻿/// <reference path="../../../assets/sinhvien/libs/angular/angular.js" />


(function () {
    angular.module('platformTH_GV.dangkymonhoc', ['platformTH_GV.common']).config(config);
    window.appVersion = angular.element(document.getElementsByTagName('html')).attr('data-ver');

    config.$inject = ['$stateProvider', '$urlRouterProvider'];

    function config($stateProvider, $urlRouterProvider) {
        $stateProvider.state('dangkymonhoc', {
            url: "/dangkymonhoc",
            parent: 'base',
            templateUrl: "app_giaovien/conponents_sv/dangkymonhoc/dangKyMonHocView.html?v=" + window.appVersion,
            controller: "dangKyMonHocController"
        });

    }
})();