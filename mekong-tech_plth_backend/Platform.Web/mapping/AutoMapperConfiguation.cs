﻿using AutoMapper;
using Platform.Model;
using Platform.Model.Models;
using Platform.Web.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Platform.Web.mapping
{
    public class AutoMapperConfiguation
    {
        public static void Configure()
        {
          
            Mapper.CreateMap<CauHoiThuongGap, CauHoiThuongGapViewModel>();
            Mapper.CreateMap<ChuongTrinhDaoTao, ChuongTrinhDaoTaoViewModel>();
            Mapper.CreateMap<ChuongTrinhDaoTao_MonHoc, ChuongTrinhDaoTao_MonHocViewModel>();
            Mapper.CreateMap<DangKy, DangKyViewModel>();
            Mapper.CreateMap<Don_SinhVien, Don_SinhVienViewModel>();
            Mapper.CreateMap<Don_VienChuc, Don_VienChucViewModel>();
            Mapper.CreateMap<DonSinhVien, DonSinhVienViewModel>();
            Mapper.CreateMap<DonVienChuc, DonVienChucViewModel>();
            Mapper.CreateMap<GioHoc, GioHocViewModel>();
            Mapper.CreateMap<KetQua, KetQuaViewModel>();
            Mapper.CreateMap<LichThi, LichThiViewModel>();
            Mapper.CreateMap<Loi, LoiViewModel>();
            Mapper.CreateMap<Lop, LopViewModel>();
            Mapper.CreateMap<Lop_ChuongTrinhDaoTao, Lop_ChuongTrinhDaoTaoViewModel>();
            Mapper.CreateMap<MonHoc, MonHocViewModel>();
            Mapper.CreateMap<Nganh, NganhViewModel>();
            Mapper.CreateMap<NhacNho, NhacNhoViewModel>();
            Mapper.CreateMap<NhacNho_SinhVien, NhacNho_SinhVienViewModel>();
            Mapper.CreateMap<Nhom, NhomViewModel>();
            Mapper.CreateMap<PhongHoc, PhongHocViewModel>();
            Mapper.CreateMap<PhongKhoa, PhongKhoaViewModel>();
            Mapper.CreateMap<PhongKhoa_VienChuc, PhongKhoa_VienChucViewModel>();
            Mapper.CreateMap<SinhVien, SinhVienViewModel>();
            Mapper.CreateMap<SinhVien_ViecPhaiLam, SinhVien_ViecPhaiLamViewModel>();
            Mapper.CreateMap<ThoiKhoaBieu, ThoiKhoaBieuViewModel>();
            Mapper.CreateMap<ThongBao, ThongBaoViewModel>();
            Mapper.CreateMap<Truong, TruongViewModel>();        
            Mapper.CreateMap<ViecPhaiLamSinhVien, ViecPhaiLamSinhVienViewModel>();
            Mapper.CreateMap<ViecPhaiLamVienChuc, ViecPhaiLamVienChucViewModel>();
            Mapper.CreateMap<VienChuc_ViecPhaiLam, VienChuc_ViecPhaiLamViewModel>();
            Mapper.CreateMap<VienChuc, VienChucViewModel>();
            Mapper.CreateMap<ChucNangTKB, ChucNangTKBViewModel>();
            Mapper.CreateMap<ChucNangBangDiem, ChucNangLichThiViewModel>();
            Mapper.CreateMap<HocPhi, HocPhiViewModel>();
            Mapper.CreateMap<DangKy_TamThoi, DangKy_TamThoiViewModel>();
            Mapper.CreateMap<TinTuc, TinTucViewModel>();
            Mapper.CreateMap<NhacNho_VienChuc, NhacNho_VienChucViewModel>();
            Mapper.CreateMap<viecdalam, viecdalamViewModel>();
			Mapper.CreateMap<ThongBao, ThongBaoViewModel>();
            Mapper.CreateMap<ThoiHanDangKyMonHoc, ThoiHanDangKyMonHocViewModel>();
            Mapper.CreateMap<HocKyHienTai, HocKyHienTaiViewModel>();
            Mapper.CreateMap<NgayLeThayDoi, NgayLeThayDoiViewModel>();
			Mapper.CreateMap<LichGacThi, LichGacThiViewModel>();
			Mapper.CreateMap<HocKyHienTai , HocKyHienTaiViewModel>();
            Mapper.CreateMap<SinhVienDauVao, SinhVienDauVaoViewModel>();
			Mapper.CreateMap<KhungGio , KhungGioViewModel>();
            Mapper.CreateMap<CongViecDeXuat, CongViecDeXuatViewModel>();
            Mapper.CreateMap<DonSinhVien_PhongKhoa, DonSinhVien_PhongKhoaViewModel>();
            Mapper.CreateMap<DonVienChuc_PhongKhoa, DonVienChuc_PhongKhoaViewModel>();
            Mapper.CreateMap<GiangDay, GiangDayViewModel>();
            Mapper.CreateMap<GiangDay_TamThoi, GiangDay_TamThoiViewModel>();
            Mapper.CreateMap<Nhom_TamThoi, Nhom_TamThoiViewModel>();
            Mapper.CreateMap<PhongHoc, PhongHocViewModel>();
            Mapper.CreateMap<NgayLe, NgayLeViewModel>();
            Mapper.CreateMap<QuaTrinhDaoTao, QuaTrinhDaoTaoViewModel>();
            Mapper.CreateMap<SinhVien_Lop, SinhVien_LopViewModel>();
            Mapper.CreateMap<Thi, ThiViewModel>();
            Mapper.CreateMap<ThoiGianCongTac, ThoiGianCongTacViewModel>();
            Mapper.CreateMap<ThoiKhoaBieu_TamThoi, ThoiKhoaBieu_TamThoiViewModel>();
            Mapper.CreateMap<TrinhDoNgoaiNgu, TrinhDoNgoaiNguViewModel>();
            Mapper.CreateMap<ApplicationGroup, ApplicationGroupViewModel>();
            Mapper.CreateMap<ApplicationRole, ApplicationRoleViewModel>();
            Mapper.CreateMap<ApplicationUser, ApplicationUserViewModel>();
            Mapper.CreateMap<SinhVienDauVao_MSSV, SinhVienDauVao_MSSVViewModel>();
            Mapper.CreateMap<Flats, FlatsViewModel>();
            Mapper.CreateMap<TeamInfo, TeamInfoViewModel>();
            Mapper.CreateMap<ServiceCTF, ServiceCTFViewModel>();

        }
    }
}